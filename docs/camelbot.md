# NAME

camelbot manipulates or gets information from wiki pages

# DESCRIPTION

this program is a CLI tool for automatic editing of MediaWiki pages. one of the
main tasks is the replacement of external links (urls). furthermore this tool can
be used to just retrieve information on a MediaWiki.

# SYNOPSIS

camelbot \[options\]

(there are no mandatory options, only mandatory sub-options)

general options:

    -t, --test                    don't change anything, just print possible changes

mediawiki/wikipedia login:

     --db-access=s             filename of file containing database access data,
                                i.e., host, port, user and password
     --host=s                  host name (default = de.wikipedia.org)
     --username=s              login as different user (default = shell login name)

mediawiki/wikipedia commands:

     passive commands in order to get information:

        --diff-from=s             revision id to get a diff from
        --diff-to=s               revision id (or 'prev' or 'next') to get a diff to
        --download-by-prefix=s    download pages with given prefix
        --download-with-images-by-prefix=s same as --download-by-prefix but download
                                   images (used on that pages) too
    -g, --get-page                get one or more wiki pages' content, print to stdout
          --search=s               use the mediawiki search to add matching pages
          --namespaces=s           when using --search, use only these list of
                                    namespaces, separated by commas, e.g. '0,1,9'
          --section=s              get this section in wiki page (e.g. 'some section')
          --wikipage=s             get this wiki page (e.g. 'some page')
          --wikipages=s            get these wiki pages by regexp (e.g. 'page[0-9]')
        --http-status=s           check http status of all urls that are found when
                                   doing a link search for s. omit the protocol in s,
                                   so not 'https://example.org' but just 'example.org'.
                                   the search will use http and https automatically.
        --list-pages              list one or wiki pages
          --search=s               use the mediawiki search to add matching pages
          --namespaces=s           when using --search, use only these list of
                                    namespaces, separated by commas, e.g. '0,1,9'
          --section=s              get this section in wiki page (e.g. 'some section')
          --wikipage=s             get this wiki page (e.g. 'some page')
          --wikipages=s            get these wiki pages by regexp (e.g. 'page[0-9]')
        --save-as-html=s          saves a given wiki page as local html-file

     other commands:

        --archive-ext-links=s     save external links at archive.org, using db
                                   param is of format
                                     'from_id=...;to_id=...;file=...;'
                                   (syntax may change in future)
        --cat-add                 add a given bunch of pages to a given category
    -c, --cat-change              replaces pre-defined categories
        --cat-dead                check recent changes (in dewiki) for additions of
                                   categories of (recently) died people
        --delete                  delete some pre-defined pages
        --irc                     short for --rc-monitoring=irc
        --link-replacement        replaces pre-defined links
        --clean-up                use clean-up functions on given pages
          --search=s               use the mediawiki search to add matching pages
          --namespaces=s           restrict search to these namespaces (separated by
                                    commas), default = 0
          --wikipage=s             use this wiki page (e.g. 'page')
          --wikipages=s            use these wiki pages by regexp (e.g. 'page[0-9]')
        --text-replacement        replaces text in given wiki pages by regexp or by
                                   given file
          --file=s                 use content of this file as replacement, i.e., as
                                    new text
          --search-pattern=s       use this perl-style regexp to search a pattern to be
                                    replaced
          --section=s              use this section in wiki page (e.g. 'own packages')
          --summary=s              a summary of the edit (mandatory)
          --use-eval               use eval on replacement text (default = no eval)
          --search=s               use the mediawiki search to add matching pages
          --namespaces=s           when using --search, use only these list of
                                    namespaces, separated by commas, e.g. '0,1,9'
          --wikipage=s             use this wiki page (e.g. 'page')
          --wikipages=s            use these wiki pages by regexp (e.g. 'page[0-9]')
    -m, --minor                   mark edit(s) as minor (default)
        --no-minor                don't mark edit(s) as minor, default = --minor
        --parse=s                 parses wikitext from file and saves result as new
                                   file
        --rc-monitoring=s         start monitoring of recent changes (in dewiki) via
                                   irc - an irc bot
                                   db  - an db connection
        --search-sbl-attempts=s   search the list of blocked edits (blocked by sbl)
                                   for a given url-regexp or for a complete given sbl.
                                   this may search in several wikis.
                                   param is of format
                                     'regexp=...;proj=...;sbl=...;'
                                   e.g. 'regexp=evil-?domain\.;proj=enwiki;' or
                                   e.g. 'proj=enwiki;sbl=meta;'
                                   (syntax may change in future)
        --update-editfilter-index update edit filter index (at dewiki)
        --upload                  upload a file
                                  iff this param is set, you should additionally set
                                  the following params, too.
          --source=s               source filename (e.g. '../somefile.txt')
          --summary=s              a summary/description of the file
                                    (e.g. "cat with hat\n\n[[Category:Nonsense]]")
          --dest=s                 destination filename (e.g. 'a_descriptive_name.txt')
        --usercontribs            fetch user contributions

meta options:

    -V, --version                 display version and exit.
    -h, --help                    display brief help
        --man                     display long help (man page)
    -q, --silent                  same as --verbose=0
    -v, --verbose                 same as --verbose=2
    -vv,--very-verbose            same as --verbose=3
    -v, --verbose=x               grade of verbosity
                                   x=0: no output
                                   x=1: default output
                                   x=2: much output

# EXAMPLES

camelbot -cl
  replaces links and cats.

# OPTIONS

## GENERAL

- **--archive-ext-links**=_string_

    save external links at archive.org, using db. param is of format
     'from\_id=...;to\_id=...;file=...;'

- **--cat-add**

    add a given bunch of pages to a given category.

- **--cat-change**, **-c**

    replaces categories

- **--cat-dead**

    check recent changes (in dewiki) for additions of categories of (recently) died
    people

- **--clean-up**

    use typical clean-up functions on given pages. pages can be given by
    **--search**, **--wikipage**, or **--wikipages**.

- **--db-access=_filename_**

    if you are lucky and have database access to the mediawiki db, then camelbot will
    try to make use of this database. just provide the _filename_ (default =
    'replica.my.cnf') of a file containing something like
     user='your name'
     password='your password'
     port='e.g. 3306'
     host='e.g. 127.0.0.1'

- **--delete**

    delete some pre-defined pages

- **--diff-from**=_string_

    returns diff of two revisions of a wiki page to stdout. If _string_ is a number,
    it's treated as a page revision. If it's not a number, it's treated as a page title
    and the newest revision is used.

    should be combined with **--diff-to**=_string_, where a second revision number or
    'prev' (default) or 'next' can be given to choose a revision to diff to.

- **--diff-to**=_string_

    see **--diff-from**

- **--download-by-prefix**=_string_

    download pages by given prefix and save the pages with the extension '.wikitext'.

- **--download-with-images-by-prefix**=_string_

    download pages by given prefix and save the pages wieth the extension '.wikitext'.
    all images used in those pages will be downloaded too.

- **--get-page**

    get content of one or more pages given by **--wikipage**, **--wikipages** or **--search**.
    use **--section** to get just a section of a page.

    - **--section**=_string_

        get this section in wiki page (e.g. 'some funny section').

    - **--search**=_string_

        use this string as search.

    - **--wikipage**=_string_

        use this wiki page (e.g. 'User:CamelBot/Something').

    - **--wikipages**=_string_

        use these wiki pages given by perl-style regexp (e.g. 'page\[0-9\]+').

- **--host=**_string_

    mediawiki host to connect with. default: _string_ = de.wikipedia.org

- **--http-status**=_string_

    does a call of \[\[special:linksearch/_string_\]\] and
    \[\[special:linksearch/https://_string_\]\], checks http response status code
    for each resulting URL.

- **--irc**

    same as **--rc-monitoring**=irc

- **--link-replacement**

    replace links

- **--list-pages**

    list titles of pages given by **--wikipage**, **--wikipages**, or **--ssearch**.

- **--minor**, **--no-minor**

    mark edit(s) as minor or not, default = **--minor**.

- **--parse**=_filename_

    parses wikitext from file and saves result to file with same name but with
    extension .html.

- **--rc-monitoring**=_type_

    start monitoring of recent changes (in de-wikipedia) via an irc bot (_type_==irc)
    or a db (_type_==db) connection to a "real time" db.

- **--save-as-html**=_pagename_

    parses wikitext of given page and saves result to file with same name but with
    extension .html.

- **--search-sbl-attempts**=_string_

    search the log of edits blocked by sbl for a given url-regexp _string_ or all
    entries of a given spam-blacklist (sbl). may search several wikis.

    _string_ contains at least one of the following parameters

    'regexp=...;proj=...;sbl=...;'

    e.g.

    'regexp=some-?evil-?domain;proj=de.wikipedia;'

    'sbl=meta;proj=dewiki;'

    'sbl=dewiki;proj=dewiki;'

    'sbl=meta;proj=all;'

    see &lt;https://noc.wikimedia.org/conf/all.dblist> for a complete list of projects

- **--test**, **-t**

    don't change anything, just print possible changes.

- **--text-replacement**

    replace content of **--section** of pages or whole pages. you may choose be a regular
    expression (via **--search-pattern**) which part to be replaced. the new text may
    be the content of a given **--file**.

    - **--file**=_filename_

        the content of this file will be used as replacement.

    - **--search-pattern**=_regexp_

        _regexp_ is a perl-style search pattern that will be raplaced.

        default = (?s:^.\*$)

        handle with care. it's always recommended to **--test** before using.

    - **--section**=_string_

        use this section in wiki page (e.g. 'some funny section').

    - **--summary**=_string_

        a one-line summary of the edit (e.g. "inserted ultimate theory")

    - **--use-eval**

        use eval() on replacement text. normally the text will be replaced literally.
        if **--use-eval** is set, the replacement will be treated like

            replace /pattern/ by eval(replacement)

        so if replacement contains something like $1, this will be treated as a
        back-reference, if **--use-eval** is set.

    - **--search**=_string_

        use the mediawiki search to add all pages that are matched by _string_.

    - **--wikipage**=_string_

        use this wiki page (e.g. 'User:CamelBot/Something').

    - **--wikipages**=_string_

        use these wiki pages given by perl-style regexp (e.g. 'page\[0-9\]+').

- **--update-editfilter-index**

    update overview of discussions concerning single rules of edit filter (in w:de)

- **--upload**

    upload a file iff this param is set, you should additionally set the following
    params, too.

    - **--source**=_string_

        source filename (e.g. '../somefile.txt')

    - **--summary**=_string_

        a summary/description of the file (e.g. "cat with hat\\\\n\\\\n\[\[Category:Nonsense\]\]")

    - **--dest**=_string_

        destination filename (e.g. 'a\_descriptive\_name.txt')

- **--username=**_string_

    login as different user. default: _string_ = shell login name

## META

- **--version**, **-V**

    prints version and exits.

- **--help**, **-h**, **-?**

    prints a brief help message and exits.

- **--man**

    prints the manual page and exits.

- **--verbose**=_number_, **-v** _number_

    set grade of verbosity to _number_. if _number_==0 then no output
    will be given, except hard errors. the higher _number_ is, the more
    output will be printed. default: _number_ = 1.

- **--silent, --quiet, -q**

    same as **--verbose=0**.

- **--very-verbose, -vv**

    same as **--verbose=3**. you may use **-vvv** for **--verbose=4** a.s.o.

- **--verbose, -v**

    same as **--verbose=2**.

# LICENCE

Copyright (c) 2016, seth
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

\* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

\* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

originally written by seth (see https://github.com/wp-seth/camelbot)
