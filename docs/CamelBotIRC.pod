=head1 NAME

CamelBotIRC - Perl module that uses a L<CamelBot> in order to monitor all recent 
changes on the wikipedia via IRC.

=head1 SYNOPSIS

  use CamelBotIRC;
  my $bot_irc = CamelBotIRC->new($camelbot,{
    server   => 'irc.wikimedia.org',
    channels => ['#de.wikipedia'],
    nick     => $username,
    username => $username,
    name     => $username,
    alias    => 'rc',
  });
  $bot_irc->run();

=head1 DESCRIPTION

TODO

=head2 Methods/Functions

=over 4

=item sub new
constructor

=item help

=item said

=back

=head2 EXPORT

None by default.

=head1 SEE ALSO

* L<MediaWiki::Bot>
* https://gitlab.com/wp-seth/camelbot

=head1 AUTHOR

seth, https://gitlab.com/wp-seth/

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2019 by seth

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself, either Perl version 5.28.1 or,
at your option, any later version of Perl 5 you may have available.

