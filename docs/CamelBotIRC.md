# NAME

CamelBotIRC - Perl module that uses a [CamelBot](https://metacpan.org/pod/CamelBot) in order to monitor all recent 
changes on the wikipedia via IRC.

# SYNOPSIS

    use CamelBotIRC;
    my $bot_irc = CamelBotIRC->new($camelbot,{
      server   => 'irc.wikimedia.org',
      channels => ['#de.wikipedia'],
      nick     => $username,
      username => $username,
      name     => $username,
      alias    => 'rc',
    });
    $bot_irc->run();

# DESCRIPTION

TODO

## Methods/Functions

- sub new
constructor
- help
- said

## EXPORT

None by default.

# SEE ALSO

\* [MediaWiki::Bot](https://metacpan.org/pod/MediaWiki%3A%3ABot)
\* https://gitlab.com/wp-seth/camelbot

# AUTHOR

seth, https://gitlab.com/wp-seth/

# COPYRIGHT AND LICENSE

Copyright (C) 2019 by seth

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself, either Perl version 5.28.1 or,
at your option, any later version of Perl 5 you may have available.
