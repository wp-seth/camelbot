=head1 NAME

camelbot manipulates or gets information from wiki pages

=head1 DESCRIPTION

this program is a CLI tool for automatic editing of MediaWiki pages. one of the
main tasks is the replacement of external links (urls). furthermore this tool can
be used to just retrieve information on a MediaWiki.

=head1 SYNOPSIS

camelbot [options]

(there are no mandatory options, only mandatory sub-options)

general options:

 -t, --test                    don't change anything, just print possible changes

mediawiki/wikipedia login:

     --db-access=s             filename of file containing database access data,
                                i.e., host, port, user and password
     --host=s                  host name (default = de.wikipedia.org)
     --username=s              login as different user (default = shell login name)

mediawiki/wikipedia commands:

  passive commands in order to get information:

     --diff-from=s             revision id to get a diff from
     --diff-to=s               revision id (or 'prev' or 'next') to get a diff to
     --download-by-prefix=s    download pages with given prefix
     --download-with-images-by-prefix=s same as --download-by-prefix but download
                                images (used on that pages) too
 -g, --get-page                get one or more wiki pages' content, print to stdout
       --search=s               use the mediawiki search to add matching pages
       --namespaces=s           when using --search, use only these list of
                                 namespaces, separated by commas, e.g. '0,1,9'
       --section=s              get this section in wiki page (e.g. 'some section')
       --wikipage=s             get this wiki page (e.g. 'some page')
       --wikipages=s            get these wiki pages by regexp (e.g. 'page[0-9]')
     --http-status=s           check http status of all urls that are found when
                                doing a link search for s. omit the protocol in s,
                                so not 'https://example.org' but just 'example.org'.
                                the search will use http and https automatically.
     --list-pages              list one or wiki pages
       --search=s               use the mediawiki search to add matching pages
       --namespaces=s           when using --search, use only these list of
                                 namespaces, separated by commas, e.g. '0,1,9'
       --section=s              get this section in wiki page (e.g. 'some section')
       --wikipage=s             get this wiki page (e.g. 'some page')
       --wikipages=s            get these wiki pages by regexp (e.g. 'page[0-9]')
     --save-as-html=s          saves a given wiki page as local html-file

  other commands:

     --archive-ext-links=s     save external links at archive.org, using db
                                param is of format
                                  'from_id=...;to_id=...;file=...;'
                                (syntax may change in future)
     --cat-add                 add a given bunch of pages to a given category
 -c, --cat-change              replaces pre-defined categories
     --cat-dead                check recent changes (in dewiki) for additions of
                                categories of (recently) died people
     --delete                  delete some pre-defined pages
     --irc                     short for --rc-monitoring=irc
     --link-replacement        replaces pre-defined links
     --clean-up                use clean-up functions on given pages
       --search=s               use the mediawiki search to add matching pages
       --namespaces=s           restrict search to these namespaces (separated by
                                 commas), default = 0
       --wikipage=s             use this wiki page (e.g. 'page')
       --wikipages=s            use these wiki pages by regexp (e.g. 'page[0-9]')
     --text-replacement        replaces text in given wiki pages by regexp or by
                                given file
       --file=s                 use content of this file as replacement, i.e., as
                                 new text
       --search-pattern=s       use this perl-style regexp to search a pattern to be
                                 replaced
       --section=s              use this section in wiki page (e.g. 'own packages')
       --summary=s              a summary of the edit (mandatory)
       --use-eval               use eval on replacement text (default = no eval)
       --search=s               use the mediawiki search to add matching pages
       --namespaces=s           when using --search, use only these list of
                                 namespaces, separated by commas, e.g. '0,1,9'
       --wikipage=s             use this wiki page (e.g. 'page')
       --wikipages=s            use these wiki pages by regexp (e.g. 'page[0-9]')
 -m, --minor                   mark edit(s) as minor (default)
     --no-minor                don't mark edit(s) as minor, default = --minor
     --parse=s                 parses wikitext from file and saves result as new
                                file
     --rc-monitoring=s         start monitoring of recent changes (in dewiki) via
                                irc - an irc bot
                                db  - an db connection
     --search-sbl-attempts=s   search the list of blocked edits (blocked by sbl)
                                for a given url-regexp or for a complete given sbl.
                                this may search in several wikis.
                                param is of format
                                  'regexp=...;proj=...;sbl=...;'
                                e.g. 'regexp=evil-?domain\.;proj=enwiki;' or
                                e.g. 'proj=enwiki;sbl=meta;'
                                (syntax may change in future)
     --update-editfilter-index update edit filter index (at dewiki)
     --upload                  upload a file
                               iff this param is set, you should additionally set
                               the following params, too.
       --source=s               source filename (e.g. '../somefile.txt')
       --summary=s              a summary/description of the file
                                 (e.g. "cat with hat\n\n[[Category:Nonsense]]")
       --dest=s                 destination filename (e.g. 'a_descriptive_name.txt')
     --usercontribs            fetch user contributions

meta options:

 -V, --version                 display version and exit.
 -h, --help                    display brief help
     --man                     display long help (man page)
 -q, --silent                  same as --verbose=0
 -v, --verbose                 same as --verbose=2
 -vv,--very-verbose            same as --verbose=3
 -v, --verbose=x               grade of verbosity
                                x=0: no output
                                x=1: default output
                                x=2: much output

=head1 EXAMPLES

camelbot -cl
  replaces links and cats.

=head1 OPTIONS

=head2 GENERAL

=over 8

=item B<--archive-ext-links>=I<string>

save external links at archive.org, using db. param is of format
 'from_id=...;to_id=...;file=...;'

=item B<--cat-add>

add a given bunch of pages to a given category.

=item B<--cat-change>, B<-c>

replaces categories

=item B<--cat-dead>

check recent changes (in dewiki) for additions of categories of (recently) died
people

=item B<--clean-up>

use typical clean-up functions on given pages. pages can be given by
B<--search>, B<--wikipage>, or B<--wikipages>.

=item B<--db-access=I<filename>>

if you are lucky and have database access to the mediawiki db, then camelbot will
try to make use of this database. just provide the I<filename> (default =
'replica.my.cnf') of a file containing something like
 user='your name'
 password='your password'
 port='e.g. 3306'
 host='e.g. 127.0.0.1'

=item B<--delete>

delete some pre-defined pages

=item B<--diff-from>=I<string>

returns diff of two revisions of a wiki page to stdout. If I<string> is a number,
it's treated as a page revision. If it's not a number, it's treated as a page title
and the newest revision is used.

should be combined with B<--diff-to>=I<string>, where a second revision number or
'prev' (default) or 'next' can be given to choose a revision to diff to.

=item B<--diff-to>=I<string>

see B<--diff-from>

=item B<--download-by-prefix>=I<string>

download pages by given prefix and save the pages with the extension '.wikitext'.

=item B<--download-with-images-by-prefix>=I<string>

download pages by given prefix and save the pages wieth the extension '.wikitext'.
all images used in those pages will be downloaded too.

=item B<--get-page>

get content of one or more pages given by B<--wikipage>, B<--wikipages> or B<--search>.
use B<--section> to get just a section of a page.

=over 8

=item B<--section>=I<string>

get this section in wiki page (e.g. 'some funny section').

=item B<--search>=I<string>

use this string as search.

=item B<--wikipage>=I<string>

use this wiki page (e.g. 'User:CamelBot/Something').

=item B<--wikipages>=I<string>

use these wiki pages given by perl-style regexp (e.g. 'page[0-9]+').

=back

=item B<--host=>I<string>

mediawiki host to connect with. default: I<string> = de.wikipedia.org

=item B<--http-status>=I<string>

does a call of [[special:linksearch/I<string>]] and
[[special:linksearch/https://I<string>]], checks http response status code
for each resulting URL.

=item B<--irc>

same as B<--rc-monitoring>=irc

=item B<--link-replacement>

replace links

=item B<--list-pages>

list titles of pages given by B<--wikipage>, B<--wikipages>, or B<--ssearch>.

=item B<--minor>, B<--no-minor>

mark edit(s) as minor or not, default = B<--minor>.

=item B<--parse>=I<filename>

parses wikitext from file and saves result to file with same name but with
extension .html.

=item B<--rc-monitoring>=I<type>

start monitoring of recent changes (in de-wikipedia) via an irc bot (I<type>==irc)
or a db (I<type>==db) connection to a "real time" db.

=item B<--save-as-html>=I<pagename>

parses wikitext of given page and saves result to file with same name but with
extension .html.

=item B<--search-sbl-attempts>=I<string>

search the log of edits blocked by sbl for a given url-regexp I<string> or all
entries of a given spam-blacklist (sbl). may search several wikis.

I<string> contains at least one of the following parameters

'regexp=...;proj=...;sbl=...;'

e.g.

'regexp=some-?evil-?domain;proj=de.wikipedia;'

'sbl=meta;proj=dewiki;'

'sbl=dewiki;proj=dewiki;'

'sbl=meta;proj=all;'

see <https://noc.wikimedia.org/conf/all.dblist> for a complete list of projects

=item B<--test>, B<-t>

don't change anything, just print possible changes.

=item B<--text-replacement>

replace content of B<--section> of pages or whole pages. you may choose be a regular
expression (via B<--search-pattern>) which part to be replaced. the new text may
be the content of a given B<--file>.

=over 8

=item B<--file>=I<filename>

the content of this file will be used as replacement.

=item B<--search-pattern>=I<regexp>

I<regexp> is a perl-style search pattern that will be raplaced.

default = (?s:^.*$)

handle with care. it's always recommended to B<--test> before using.

=item B<--section>=I<string>

use this section in wiki page (e.g. 'some funny section').

=item B<--summary>=I<string>

a one-line summary of the edit (e.g. "inserted ultimate theory")

=item B<--use-eval>

use eval() on replacement text. normally the text will be replaced literally.
if B<--use-eval> is set, the replacement will be treated like

  replace /pattern/ by eval(replacement)

so if replacement contains something like $1, this will be treated as a
back-reference, if B<--use-eval> is set.

=item B<--search>=I<string>

use the mediawiki search to add all pages that are matched by I<string>.

=item B<--wikipage>=I<string>

use this wiki page (e.g. 'User:CamelBot/Something').

=item B<--wikipages>=I<string>

use these wiki pages given by perl-style regexp (e.g. 'page[0-9]+').

=back

=item B<--update-editfilter-index>

update overview of discussions concerning single rules of edit filter (in w:de)

=item B<--upload>

upload a file iff this param is set, you should additionally set the following
params, too.

=over 8

=item B<--source>=I<string>

source filename (e.g. '../somefile.txt')

=item B<--summary>=I<string>

a summary/description of the file (e.g. "cat with hat\\n\\n[[Category:Nonsense]]")

=item B<--dest>=I<string>

destination filename (e.g. 'a_descriptive_name.txt')

=back

=item B<--username=>I<string>

login as different user. default: I<string> = shell login name

=back

=head2 META

=over 8

=item B<--version>, B<-V>

prints version and exits.

=item B<--help>, B<-h>, B<-?>

prints a brief help message and exits.

=item B<--man>

prints the manual page and exits.

=item B<--verbose>=I<number>, B<-v> I<number>

set grade of verbosity to I<number>. if I<number>==0 then no output
will be given, except hard errors. the higher I<number> is, the more
output will be printed. default: I<number> = 1.

=item B<--silent, --quiet, -q>

same as B<--verbose=0>.

=item B<--very-verbose, -vv>

same as B<--verbose=3>. you may use B<-vvv> for B<--verbose=4> a.s.o.

=item B<--verbose, -v>

same as B<--verbose=2>.

=back

=head1 LICENCE

Copyright (c) 2016, seth
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

originally written by seth (see https://github.com/wp-seth/camelbot)

