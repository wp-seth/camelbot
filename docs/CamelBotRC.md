# NAME

CamelBotRC - Perl module that uses a [CamelBot](https://metacpan.org/pod/CamelBot) in order to monitor all recent 
changes on the wikipedia.

# SYNOPSIS

    use CamelBotRC;
    my $camelbot_rc = CamelBotRC->new($camelbot);
    $camelbot_rc->db_rc_monitoring();

# DESCRIPTION

TODO

## Methods/Functions

- new
constructor
- db\_cat\_of\_dead\_monitoring
use db to monitor added categories of dead people in an endless loop
- db\_extlinks\_monitoring
use db to monitor external links in an endless loop
- db\_rc\_monitoring
use db to monitor recent changes in an endless loop
- delete\_double\_pages\_from\_array
given an array of hashes with page info (title and timestamp of change), delete all
old redundant entries, i.e., if there are two or more entries for the same page, 
then keep the newest only
- delete\_redundant\_pages\_from\_array
given two array refs A, B with meta info of pages, and a timestamp t, this function 
removes all entries from A, that B already contains and that are of the timestamp t.
furthermore the timestamp will be modified to the newest timestamp of A.
finally B is emptied and refilled with all pages from A that are of the newest 
timestamp.
- subtract\_days\_from\_now

    given a float number $x, this function returns a UTC timestamp in iso format of 
    now() - $x days.

    e.g. .5 -> iso timestamp of the moment 12 hours ago (in UTC).

- get\_extlinks\_via\_db
get ref to an array of hashes, where each hash represents an external link and 
one page where it's used
- get\_pages\_of\_cat\_of\_dead\_via\_db
get ref to an array of hashes, where each hash represents a category and 
one page where it's used
- get\_rc\_via\_db
get ref to an array of hashes, where each hash represents a recent change
- handle\_extlinks
given a bunch of external links, this function calls functions like 
archive\_ext\_links
- handle\_rc\_pages
major function for handling of recent changes of short-term and mid-term
- put\_new\_rc
put new rc pages to short-term and mid-term lists, call handle\_rc\_pages()
- wait\_till\_abs\_unixtime
given a unix timestamp, wait until this timestamp is reached (or in the past)

## EXPORT

None by default.

# SEE ALSO

\* [MediaWiki::Bot](https://metacpan.org/pod/MediaWiki%3A%3ABot)
\* https://gitlab.com/wp-seth/camelbot

# AUTHOR

seth, https://gitlab.com/wp-seth/

# COPYRIGHT AND LICENSE

Copyright (C) 2019 by seth

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself, either Perl version 5.28.1 or,
at your option, any later version of Perl 5 you may have available.
