#!/bin/bash
mkdir -p docs

# executables
for exe in camelbot
do
	podselect ${exe} > docs/${exe}.pod
done

# libraries
for lib in lib/*.pm
do
	podselect ${lib} > docs/$(basename $lib .pm).pod
done

# pod2md
for pod in docs/*.pod
do
	pod2markdown ${pod} > ${pod%%.pod}.md
done
