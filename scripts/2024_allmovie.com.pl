#!/usr/bin/perl
use strict;
use warnings;
use Data::Dumper;             # for debugging purposes
use File::Slurp qw(slurp write_file);  # read/write files
use FindBin;
use lib "$FindBin::RealBin/../lib";
use CamelBot;
use CamelBotRC;
use CamelBotIRC;

binmode STDOUT, ":encoding(UTF-8)";  # avoid 'wide character' warning

# init bot and global vars
my $_camelbot = CamelBot->new({
	'ask_user'      => 0,
	'db_access'     => 'replica.my.cnf',
	'host'          => 'de.wikipedia.org',
	'max_edits_per_min' => 5,
	'minor'         => 1,
	'mw_username'   => 'CamelBot',
	'namespaces_used' => undef,
	'simulation'    => 1,
	'showdiff'      => 0,
	'verbosity'     => 1,
});
$_camelbot->{'use_www_cache'} = 1;
$_camelbot->login() or die;
my $_debug_info = [];
my $_www_delay = 10;
my $_last_num_urls = 0;

sub debugging{
	my $level = shift;
	my $str = shift;
	my $type = shift;
	push @$_debug_info, $str;
	my $caller_inc = 1;
	$_camelbot->msg($level, $str, $type, $caller_inc);
	return 1;
}

sub decompose_allmovie_url{
	my $url = shift;
	my $url_struct = {};
	if($url !~ /^(?<website>https?:\/\/(?:www\.|)allmovie\.com)\b(?<path>.*)/){
		$_camelbot->msg(0, "no allmovie-url: '$url'", 'warning');
		return;
	}
	$url_struct->{'website'} = $+{'website'};
	$url_struct->{'path'} = $+{'path'};
	if($url_struct->{'path'} !~ /^\/(?<type>[a-z]+)\/
		(?<name>(?:(?:[áa-z-]+|\x25[0-9a-f]{2,4})+[áa-z]|)-|)
		(?<id>[a-z]{0,2}[0-9]+)
		\z/x
	){
		$_camelbot->msg(0, "unrecognized allmovie-path: '$url_struct->{'path'}'", 'warning');
		return;
	}
	$url_struct->{'type'} = $+{'type'};
	$url_struct->{'id'} = $+{'id'};
	$url_struct->{'name'} = $+{'name'};
	$url_struct->{'minus'} = substr($url_struct->{'name'}, -1) eq '-';
	if($url_struct->{'name'} =~ /^-?\z/){
		undef $url_struct->{'name'};
	}
	return $url_struct;
}

sub fill_allmovie_template{
	my $camelbot = shift;
	my $params = shift;
	my $allmovie_tpl = {};
	my $timestamp = get_isotimestamp_from_date($camelbot, $params->{'timestamp'});
	unless(defined $timestamp){
		debugging(0, 'timestamp not defined', 'warning');
		return;
	}
	$timestamp += 2; # next day (ok, even if it's Dec 32th)
	my $url_struct = decompose_allmovie_url($params->{'url'});
	return unless defined $url_struct;
	my $urls_to_try = possible_allmovie_urls($url_struct);
	# add redirect if existing
	unless(defined $url_struct->{'name'}){
		my $new_url = $camelbot->get_http_redir_location($params->{'url'});
		if(defined($new_url) && $new_url !~ /\/search\//){
			$urls_to_try->[3] = $new_url;
		}
	}
	# check timestamps
	my $wayback_opts = {
		'start_time' => undef,
		'end_time' => '20240531',
		'preferred_max_time' => $timestamp,
		'num_tries' => 10,
		'delay' => $_www_delay,
	};
	my $new_url;
	$urls_to_try = {map {$_ => undef} grep {defined} @$urls_to_try};
	for my $url(keys %$urls_to_try){
		if($_last_num_urls < @{$camelbot->{'www_history'}}){
			sleep $_www_delay;
			$_last_num_urls = scalar(@{$camelbot->{'www_history'}});
		}
		$urls_to_try->{$url} = $camelbot->get_archived_url_timestamp(
			$url, $wayback_opts);
		next if ($urls_to_try->{$url} // -1) < 0;
		if($urls_to_try->{$url} > $timestamp 
			&& defined($allmovie_tpl->{'timestamp'}) && $urls_to_try->{$url} > $allmovie_tpl->{'timestamp'}
		){
			next;
		}
		$new_url = $url;
		$allmovie_tpl->{'timestamp'} = $urls_to_try->{$url};
	}
	unless(defined $allmovie_tpl->{'timestamp'}){
		debugging(0, 'could not fetch timestamp', 'warning');
		return;
	}
	debugging(2, "got timestamp $allmovie_tpl->{'timestamp'}");
	$allmovie_tpl->{'text'} = $params->{'text'};
	if(defined $params->{'text'}){
		$allmovie_tpl->{'text'} =~ s/ \{\{!\}\}.*//;
	}
	$allmovie_tpl->{'author'} = $params->{'author'} if defined $params->{'author'};
	my $got_allmovie_data = fill_allmovie_template_from_url(
		$camelbot, $allmovie_tpl, $params->{'url'}, $new_url);
	if($_last_num_urls < @{$camelbot->{'www_history'}}){
		sleep($_www_delay);
		$_last_num_urls = scalar(@{$camelbot->{'www_history'}});
	}
	unless($got_allmovie_data){
		unless($allmovie_tpl->{'id'}){
			debugging(0, "url '$params->{'url'}' is not of needed format", 'warning');
		}
		return -1;
	}
	my $a_template = $camelbot->fill_template('AllMovie', $allmovie_tpl, 0);
	debugging(2, $a_template->{'str'});
	return $a_template->{'str'};
}

sub fill_allmovie_template_from_url{
	my $camelbot = shift;
	my $allmovie_tpl = shift;
	my $orig_url = shift;
	my $new_url = shift;  # might be undef
	my $url = $new_url // $orig_url;
	if($url =~ /https?:\/\/(?:www\.|)allmovie\.com
		\/(movie|artist)\/((?:[áa-z-]+|\x25[0-9a-f]{2,4})+[áa-z]|)(-?[a-z]{0,2}[0-9]+)
	/x){
		$allmovie_tpl->{'id'} = $2 . $3;
		$allmovie_tpl->{'bot'} = 'war ' . $2 . $3;
		if(defined $new_url){
			if($orig_url =~ /\/(?:movie|artist)\/(.*)/){
				$allmovie_tpl->{'bot'} = 'war ' . $1;
			}
		}
		my $wayback = 'https://web.archive.org/web/' . $allmovie_tpl->{'timestamp'}
			. '/' . $url;
		unless(defined $allmovie_tpl->{'author'}){
			my $author = get_author_from_allmovie($camelbot, $wayback);
			return unless defined $author;
			$allmovie_tpl->{'author'} = $author if $author ne '';
		}
		#unless(defined $allmovie_tpl->{'text'}){
		#	my $title = get_title_from_allmovie($camelbot, $wayback);
		#	return unless $title;
		#	$allmovie_tpl->{'text'} = $title;
		#}
		return 1;
	}
	return;
}

sub get_author_from_allmovie{
	my $camelbot = shift;
	my $content = shift;
	sub author_cosmetica{
		my $author = shift;
		if($author eq 'Hal Erickson'){
			$author = '[[' . $author . ']]';
		#}elsif($author =~ /^(?:All[mM]ovie\|Rovi)\z/){
		}elsif($author eq 'Rovi'){
			# set author to empty string to distinguish from undef
			$author = '';
		}
		return $author;
	};
	my $author;
	if($content !~ /\n/ && $content =~ /^https?:\/\//){
		my $wayback = $content;
		(my $http_status, $content) = $camelbot->get_http_content($wayback);
		debugging(1, "$http_status: $wayback");
		return if $http_status != 200;
	}
	if($content =~ /
		(?:itemprop="author"[^>]*>[^<]++<span\sitemprop="name">
		|class="review-author\sheadline">[^<]+\bby\b\s*<span>
		|<div\sclass="tab-content-t">\s*<div\sclass="tab-title">\s*<span>by\s+
		)
			(?<author>[^<]++)
		<\/span>
	/sx){
		$author = author_cosmetica($+{'author'});
	}elsif($content =~ /<td align="right" class="author">by ([^<]+)<\/td>/){
		$author = author_cosmetica($1);
	}else{
		debugging(0, 'could not fetch author', 'warning');
		# set author to empty string to distinguish from undef
		$author = '';
	}
	return $author;
}

sub get_title_from_allmovie{
	my $camelbot = shift;
	my $content = shift;
	my $title;
	if($content !~ /\n/ && $content =~ /^https?:\/\//){
		my $wayback = $content;
		(my $http_status, $content) = $camelbot->get_http_content($wayback);
		debugging(2, "$http_status: $wayback");
		return if $http_status != 200;
	}
	if($content =~ /<title>([^|<]+) (?:[|<]|&gt;)/){
		$title = $1;
	}else{
		debugging(0, 'could not fetch title', 'warning');
	}
	return $title;
}

sub get_isotimestamp_from_date{
	my $camelbot = shift;
	my $timestamp = shift;  # e.g. '3. Dezember 2001' or '2001-12-03'
	$timestamp =~ s/ $//g;
	$timestamp = $camelbot->convert_german_date2iso($timestamp);
	return unless defined $timestamp;
	$timestamp =~ s/-//g;
	return $timestamp;  # e.g. '20011203'
}

sub possible_allmovie_urls{
	my $url_struct = shift;
	my $path = 'https://www.allmovie.com/' . $url_struct->{'type'} . '/';
	my $urls = [$path . $url_struct->{'id'}];
	push @$urls, $path . '-' . $url_struct->{'id'};
	if($url_struct->{'name'}){
		push @$urls, $path . $url_struct->{'name'} . $url_struct->{'id'};
	}
	return $urls;
}

sub replace_plain_allmovie_link{
	# from:
	# Tsui Hark / Xu Ke ({{lang|zh-Hani|徐克}}) [https://www.allmovie.com/artist/tsui-hark-p93443 in Allmovie.] (englisch)
	# Buzz McClain: [https://www.allmovie.com/movie/shake-rattle-and-rock-vm1082788/review ''Shake, Rattle and Rock (1994)''] Rezension In: ''Allmovie''.com. (englisch). Abgerufen am 1. September 2023.
	# vgl. [http://www.allmovie.com/artist/nancy-coleman-14108 Biografie] bei allmovie.com (aufgerufen am 31. August 2010).
	# # to:
	# * {{AllMovie |tsui-hark-p93443 |20180424202436 |...
	my $str = shift;
	$_debug_info = [];
	debugging(2, "got $str");
	my $re_allmovie_url =
		qr/https?:\/\/(?:www\.|)allmovie\.com\/$_camelbot->{'re_url_rear'}/;
	my $allmovie_template;
	# blabla [link auf AllMovie] (englisch)
	if($str =~ /^(.*?\S|)\s*
		\[($re_allmovie_url)\s(?:in\s|auf\s|bei\s|\x{2013}\s|)All[mM]ovie[^\]]*+\]\s*
		(?:\(englisch\)|)\s*\z
	/x){
		my $description = $1;
		my $url = $2;
		$description =~ s/('{2,})(.*?)\1/$2/g;
		$description =~ s/ (?:bei|auf|in)\s*\z//g;
		$allmovie_template = fill_allmovie_template($_camelbot, {
				'timestamp' => '2022-01-01',
				'url' => $url,
				'text' => ($description eq '' ? undef : $description),
			}
		);
	# [link blabla] auf [[AllMovie]] (englisch)
	}elsif($str =~ /^\s*'*\[($re_allmovie_url)\s([^\]]+)\]'*\s+
		(?:auf|bei|at|in)\s(?:\[\[|)AllMovie(?:\]\]|)'*(?:\s+\(englisch\)|)\s*\z
	/x){
		$allmovie_template = fill_allmovie_template($_camelbot, {
				'timestamp' => '2022-01-01',
				'url' => $1,
				'text' => $2,
			}
		);
	# Lieschen Mueller: [link blabla] auf [[AllMovie]] (englisch)
	}elsif($str =~ /^(.*?):\s
		\[($re_allmovie_url)\s(?:''|)((?:[^'\]]|'(?!'))*)(?:''|)\]\s*
		Rezension\sIn:\s''Allmovie''\.com\s(?:\(englisch\)|)\.?\s*
		Abgerufen\sam\s([0-9]{1,2}\.\s[A-Z][a-z]+20[0-9]{2})\.?\z
	/x){
		$allmovie_template = fill_allmovie_template($_camelbot, {
				'timestamp' => $4 // '2022-01-01',
				'url' => $2,
				'author' => $1,
				'text' => $3,
			}
		);
	# vgl. [link blabla] ... (englisch)
	}elsif($str =~ /^(?:[vV]gl\.|)\s*
		\[($re_allmovie_url)\s*(?:''|)((?:[^'\]]*|'(?!'))*)(?:''|)\]\s*
		.*?(?:\(englisch\)|)\.?\s*
		\(?(?:auf|ab)gerufen\sam\s([0-9]{1,2}\.\s[A-Z][a-z]+\s20[0-9]{2})[.)]*\z
	/x){
		$allmovie_template = fill_allmovie_template($_camelbot, {
				'timestamp' => $3 // '2022-01-01',
				'url' => $1,
				'text' => $2,
			}
		);
	}elsif($str =~ /\/(?:essays|glossary|blog)\//){
		# skip
	}else{
		debugging(0, 'could not parse', 'warning');
		$_camelbot->msg(1, Dumper($_debug_info));
	}
	return $str unless defined $allmovie_template;
	return $allmovie_template if length($allmovie_template) > 2;
	$_camelbot->msg(1, Dumper($_debug_info));
	return $str;
}

sub replace_template_by_allmovie{
	# from:
	# {{Internetquelle |url=https://www.allmovie.com/artist/ronald-reagan-p59108 |titel=Ronald Reagan {{!}} Biography, Movie Highlights and Photos {{!}} AllMovie |abruf=2018-04-24}}
	# to:
	# {{AllMovie |ronald-reagan-p59108 |20180424202436 |Text=Ronald Reagan |Autor=Jason Ankeny |Bot=war ronald-reagan-p59108}}
	my $str = shift;
	$_debug_info = [];
	debugging(2, "got $str");
	my $old_template = $_camelbot->extract_templates($str);
	my $i_template = $old_template->[0]{'params'};
	#print Dumper $i_template;
	my $allmovie_template;
	if($old_template->[0]{'name'} eq 'Internetquelle'){
		$allmovie_template = fill_allmovie_template($_camelbot, {
				'timestamp' => $i_template->{'abruf'} // $i_template->{'zugriff'} // '2022-01-01',
				'url' => $i_template->{'url'},
				'text' => $i_template->{'titel'},
			}
		);
	}elsif($old_template->[0]{'name'} eq 'Lex All Movie Guide'){
		my $type = $i_template->{1} eq 'Person' ? 'artist' : 'movie';
		my $id = $i_template->{2};
		if($id =~ /^[0-9]+$/){
			$id = '-' . ($type eq 'artist' ? 'p' : 'v') . $id;
		}
		my $url = 'https://www.allmovie.com/' . $type . '/' . $id;
		$allmovie_template = fill_allmovie_template($_camelbot, {
				'timestamp' => $i_template->{4} // $i_template->{'abruf'} // '2022-01-01',
				'url' => $url,
				'text' => $i_template->{3},
			}
		);
	}else{
		debugging(0, 'could not parse', 'warning');
		$_camelbot->msg(1, Dumper($_debug_info));
	}
	return $str unless defined $allmovie_template;
	return $allmovie_template if length($allmovie_template) > 2;
	$_camelbot->msg(1, Dumper($_debug_info));
	return $str;
}

# main: get pages, then replace links
my $pages = $_camelbot->get_pages({
		'linksearch'    => {
			'namespaces' => 0,
			'simple_url' => '*.allmovie.com',
		},
	});
@$pages = grep {$_ !~ /^(?:AllMovie|John Cheever Cowdin)/} @$pages;

$_camelbot->msg(1, 'found ' . scalar(@$pages) . ' pages');

my $use_eval = 1;
my $pattern = [
	qr/(\{\{\s*[iI]nternetquelle\s*\|
		(?:[^}]+|\{\{!\}\})+
		\ballmovie\.
		(?:[^}]+|\{\{!\}\})+
		(?<!\{\{!)\}\})/sx,
	qr/\n\*\s\K(.*\bhttps?:\/\/www\.allmovie\.com.*)/,
	#qr/<ref[^>\/]*>\K
	#		(
	#			(?:[^<]+|<(?!\/ref>))*
	#			\bhttps?:\/\/www\.allmovie\.com
	#			(?:[^<]+|<(?!\/ref>))*
	#		)<\/ref>/x,
	qr/(\{\{\s*Lex\sAll\sMovie\sGuide\s*(?:\|[^}]+)+\}\})/x,
];
my $replacement = [
	'main::replace_template_by_allmovie($1)',
	'main::replace_plain_allmovie_link($1)',
	#'main::replace_plain_allmovie_link($1) . \'</ref>\'',
	'main::replace_template_by_allmovie($1)',
];
my $options = {
	'summary' => 'link fix, siehe [[WP:Bots/Anfragen#AllMovie-Archivierung]]',
	'cleanup' => 1,
	'skip_edits' => 0,
};

$_camelbot->text_replacement(
	$pages, undef, $pattern, $replacement, $options, $use_eval);

