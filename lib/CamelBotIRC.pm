package CamelBotIRC;
use 5.018002;
use strict;
use warnings;
use parent qw(Bot::BasicBot); # irc bot
use Data::Dumper;           # for debugging purposes
use Time::Local;            # timegm
$| = 1; # deactivate buffering, so flush all the time

use Exporter ();
our @ISA = qw(Exporter);
our %EXPORT_TAGS = ( ); # eg: TAG => [ qw!name1 name2! ],
# exported package globals, as well as any optionally exported functions
our @EXPORT_OK = qw();
our $VERSION = 1.61.0;

sub new{
	my $class    = shift;
	my $camelbot = shift;
	my $params   = shift;
	my $self = $class->SUPER::new(%$params);
	$self->{'rc'} = CamelBotRC->new($camelbot);
	$self->{'camelbot'} = $camelbot;
	return $self;
}

# bot help
sub help {
	my $self = shift;
	return 'i\'m just listening to recent changes in w:de and sometimes those ' 
		. 'changes trigger some of my functions. if you have any questions, ' 
		. 'please ask my master at ' 
		. 'https://de.wikipedia.org/wiki/user_talk:lustiger_seth.';
}

# bot handler: if somebody said something
sub said {
	my ($self, $message) = @_;
	my $camelbot = $self->{'camelbot'};
	# print everything what is said to you
	$camelbot->msg(4, "$message->{who}: $message->{body}");

	# general
	if(defined $message->{address} and $message->{address} eq $self->{nick}){
		$camelbot->msg(2, $message->{who}.': '.$message->{body});
		# help
		if($message->{body}=~/^(?:help|was machst du\?|was kannst du\?)/){
			$self->reply($message, $self->help());
		}
	}

	# rc-channel
	if($self->{'alias'} eq 'rc'){
		# reporting of specific edits
		if($message->{who} =~/^rc-\w+$/){
			# parse rc message
			my $rc_msg = $camelbot->parse_irc_rc($message->{body});
			$self->{'rc'}->put_new_rc([$rc_msg]);
			## for debugging only
			#if(# $rc_msg->{'user'} eq 'Lustiger seth' ||
			## don't track own edit
			#	$rc_msg->{'user'} ne $camelbot->{'mw_username'} &&
			## don't play edit war
			#	$rc_msg->{'summary'} !~ 
			#		/\bCamelBot\b.*\b(?:r\x{fc}ckg\x{e4}ngig\sgemacht|revertiert)\b/ &&
			#	$rc_msg->{'ns_id'} == 0
			#){
			#	$self->forkit(
			#		run => \&{$self->put_new_rc}, 
			#		channel => $message->{'channel'}, 
			#		#handler => \&_fork_said_handler,
			#		body => $self,
			#		arguments => [$rc_msg, time()],
			#	);
			#}
			##print Dumper $rc_msg;
			## update edit filter index
			#if($rc_msg->{'ns_id'} == 4 
			#	&& $rc_msg->{'page'} =~ /^$self->{'rc'}->{'re_edit_filter_name'}\//){
			#	$camelbot->update_edit_filter_index();
			#}
		}
	}
	return 1;
}

END { } # module clean-up code here (global destructor)
1;

=head1 NAME

CamelBotIRC - Perl module that uses a L<CamelBot> in order to monitor all recent 
changes on the wikipedia via IRC.

=head1 SYNOPSIS

  use CamelBotIRC;
  my $bot_irc = CamelBotIRC->new($camelbot,{
    server   => 'irc.wikimedia.org',
    channels => ['#de.wikipedia'],
    nick     => $username,
    username => $username,
    name     => $username,
    alias    => 'rc',
  });
  $bot_irc->run();

=head1 DESCRIPTION

TODO

=head2 Methods/Functions

=over 4

=item sub new
constructor

=item help

=item said

=back

=head2 EXPORT

None by default.

=head1 SEE ALSO

* L<MediaWiki::Bot>
* https://gitlab.com/wp-seth/camelbot

=head1 AUTHOR

seth, https://gitlab.com/wp-seth/

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2019 by seth

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself, either Perl version 5.28.1 or,
at your option, any later version of Perl 5 you may have available.

=cut
