package CamelBotRC;
use 5.018002;
use strict;
use warnings;
use POSIX qw/strftime/;     # format timestamp
use Data::Dumper;           # for debugging purposes
use File::Slurp qw(write_file); # read/write files
use Time::Local;            # timegm
$| = 1; # deactivate buffering, so flush all the time

use Exporter ();
our @ISA = qw(Exporter);
our %EXPORT_TAGS = ( ); # eg: TAG => [ qw!name1 name2! ],
# exported package globals, as well as any optionally exported functions
our @EXPORT_OK = qw();
our $VERSION = 2.1.0; # 2021-03-22

sub new{
	my $class    = shift;
	my $camelbot = shift;
	my $params = shift;
	my $self = bless {
		'camelbot'             => $camelbot,
		're_edit_filter_name'  => qr/(?:Bearbeitung|Missbrauch)sfilter/,
		're_sbl_pages'         => qr/Weblinks\/Block/,
		'rc_pages_shortterm'   => {},    # rc pages to check
		'rc_pages_midterm'     => {},    # rc pages to check
		# format: {$type1 => [$title1, ...], ...}
		'rc_pages_maintenance' => {},    # rc special pages to check
		'delay'                => 30*60, # seconds
		'delay_short'          => 10,    # seconds
		'short_max_age'        => 60*60*3, # seconds
		'start_ts'             => time,  # only for debugging purposes
		'last_ext_links'       => [],    # remember last ext links to avoid redundancy
		'max_ext_links'        => 1000,  # max length of list 'last_ext_links'
	}, $class;
	# initially update and fetch maintenance lists
	if(!defined($params->{'skip_maintenance'}) || $params->{'skip_maintenance'} == 0){
		$camelbot->update_maintenance_lists(
			{'list_name' => 'all_types', 'lists' => $self->{'rc_pages_maintenance'}});
	}
	return $self;
}

sub cat_of_dead_monitoring{
	my $self = shift;
	my $sleep_delay = shift // 115; # seconds
	my $start_before_days = shift // 7; # days
	my $sleep_delay_bak = $sleep_delay;
	my $last_check = 0;
	my $camelbot = $self->{'camelbot'};
	my $continue_timestamp = $self->subtract_days_from_now($start_before_days);
	my @edge_articles = (); # articles at a specific timestamp
	while(1){ # endless loop, break via ctrl-c
		# wait for $sleep_delay seconds before sending a query to db again
		$self->wait_till_abs_unixtime($last_check + $sleep_delay);
		$sleep_delay = $sleep_delay_bak;
		$last_check = time;
		$camelbot->msg(2, 'get new pages via ' . $camelbot->get_default_access_type());
		my $new_entries = $self->get_pages_of_cat_of_dead($continue_timestamp);
		if(@$new_entries > 0){
			# delete multiple entries of same page
			$camelbot->msg(2, 'delete multiple entries');
			$self->delete_double_pages_from_array($new_entries);
		}
		next if @$new_entries == 0; # needed for next call only
		$camelbot->msg(2, 'last timestamp: ' . $new_entries->[-1]{'timestamp'});
		$self->delete_redundant_pages_from_array(
			$new_entries, \@edge_articles, \$continue_timestamp);
		next if @$new_entries == 0; # needed for next call only
		$camelbot->msg(1, 'got ' . (0 + @$new_entries) . ' new entries');
		$camelbot->update_maintenance_lists_build_table(
			undef, 'cat dead', $new_entries, ['category']);
		$sleep_delay = 1;
	}
	return 1;
}

sub db_extlinks_monitoring{
	my $self    = shift;
	my $from_id = shift // 1;
	my $to_id   = shift;
	my $file_of_state = shift // 'temp_' . time . rand . '.tmp';
	my $last_check = 0;
	my $camelbot = $self->{'camelbot'};
	$camelbot->msg(2, 'to_id = ' . (defined $to_id ? $to_id: 'undef'));
	$camelbot->msg(2, "file_of_state = $file_of_state");
	my $sleep_delay = 30; # seconds
	while(not defined $to_id or $to_id >= $from_id){ # endless loop, break via ctrl-c
		# wait for $sleep_delay seconds before sending a query to db again
		$self->wait_till_abs_unixtime($last_check + $sleep_delay);
		$last_check = time;
		$camelbot->msg(2, 'get new external links from db');
		my $new_extlinks = $self->get_extlinks_via_db($from_id, $to_id);
		# delete multiple entries of same page
		$camelbot->msg(2, 'delete multiple entries');
		my %dbl_counter;
		$new_extlinks = [reverse
			grep {
				$dbl_counter{$_->{'el_to'}} = (exists $dbl_counter{$_->{'el_to'}}) ? 0 : 1;
				$dbl_counter{$_->{'el_to'}};
			} sort {$b->{'el_id'} <=> $a->{'el_id'}} @$new_extlinks
		];
		if(@$new_extlinks > 0){
			$from_id = $new_extlinks->[-1]->{'el_id'};
			$camelbot->msg(1, 'last from_id: ' . $from_id);
			++$from_id;
			$camelbot->msg(1, 'got ' . (0 + @$new_extlinks) . ' new external links');
			$self->handle_extlinks($new_extlinks);
			write_file($file_of_state, {binmode => ':utf8'},
				'continue_from=' . ($from_id + 1) . ';' .
				'continue_to=' . ($to_id // ''));
			$sleep_delay = 1;
		}else{
			$sleep_delay = 55;
		}
	}
	return 1;
}

sub rc_monitoring{
	my $self = shift;
	my $sleep_delay = shift // 30; # seconds
	my $start_before_days = shift // .07; # days
	my $last_check = 0;
	my $camelbot = $self->{'camelbot'};
	my $continue_timestamp = $self->subtract_days_from_now($start_before_days);
	my @edge_articles = (); # articles at a specific timestamp
	$camelbot->msg(1, 'starting loop for listening on RecentChanges');
	while(1){ # endless loop, break via ctrl-c
		# wait for $sleep_delay seconds before sending a query to db again
		$self->wait_till_abs_unixtime($last_check + $sleep_delay);
		$last_check = time;
		$camelbot->msg(2, 'get recently changed pages');
		my $new_entries = $self->get_rc($continue_timestamp);
		$camelbot->msg(3, 'got ' . (0 + @$new_entries) . ' new recently changed raw pages');
		if($self->{'camelbot'}{'verbosity'} >= 4){
			map {print $_->{'page_with_ns'} . "\n"
			} grep {$_->{'ns_id'} == 0} @$new_entries;
		}
		if(@$new_entries > 0){
			# delete multiple entries of same page
			$camelbot->msg(2, 'delete multiple entries');
			$self->delete_double_pages_from_array($new_entries);
		}
		next if @$new_entries == 0; # needed for next command only
		$camelbot->msg(2, 'last timestamp: ' . $new_entries->[-1]{'timestamp'});
		$self->delete_redundant_pages_from_array(
			$new_entries, \@edge_articles, \$continue_timestamp);
		next if @$new_entries == 0; # check again
		my $msg_num_pages = 'got ' . (0 + @$new_entries) . ' new recently changed pages';
		if(@$new_entries <= 2){
			$msg_num_pages .= ' (';
			for my $e(@$new_entries){
				$msg_num_pages .= "'" . $e->{'page_with_ns'} . "', ";
			}
			substr($msg_num_pages, -2) = ')';
		}
		$camelbot->msg(1, $msg_num_pages);
		if($self->{'camelbot'}{'verbosity'} >= 3){
			map {
				print $_->{'page_with_ns'} . "\n";
			} grep {$_->{'ns_id'} == 0} @$new_entries;
		}
		$camelbot->msg(2, "mem usage:\n" . $camelbot->_get_mem_usage(undef, 'before put_new_rc'));
		$self->put_new_rc($new_entries);
		$camelbot->msg(2, "mem usage:\n" . $camelbot->_get_mem_usage(undef, 'after put_new_rc'));
	}
	return 1;
}

sub review_request_monitoring{
	my $self = shift;
	my $sleep_delay = shift // 30; # seconds
	my $camelbot = $self->{'camelbot'};
	my $request_page = $camelbot->{'rev_req'}{'prefix'} . $camelbot->{'rev_req'}{'page_title'};
	my $last_check = 0;
	$camelbot->msg(1, 'starting loop for listening on review requests');
	while(1){ # endless loop, break via ctrl-c
		# wait for $sleep_delay seconds before sending a query to db again
		$self->wait_till_abs_unixtime($last_check + $sleep_delay);
		$last_check = time;

		# get states of requested pages
		$camelbot->msg(2, 'get states of review requested pages');
		my $review_states = $camelbot->get_default_access_type() eq 'db'
			? $camelbot->db_fetch_completed_review_requests()
			: $camelbot->fetch_completed_review_requests();
		next if keys %$review_states == 0;

		# modify review request page
		my $text_bak = $camelbot->get_text($request_page);
		next unless defined $text_bak;
		my $summary_struct = {
			'changed' => [],
			'hidden' => [],
			'invalid' => [],
			'never_reviewed' => [],
			'pending' => [],
			'reviewed' => [],
		};
		my $num_pending = 0;
		my @requests = grep {
			my $wikitextref = \$_;
			my $keep = 1;
			my ($page, $action) =
				$camelbot->check_review_request($wikitextref, $review_states);
			if(defined $action){
				push @{$summary_struct->{$action}}, $page;
				if($action eq 'invalid'){
					$keep = 0;
				}
			}
			my $review_state = (defined $page) ? $review_states->{$page} : undef;
			if(defined $review_state){
				$review_state = 'pending' if $review_state =~ /^[0-9]{4}/;
				push @{$summary_struct->{$review_state}}, $page;
				if($review_state eq 'reviewed'){
					$camelbot->msg(3, "removing '$page', because reviewed");
					$keep = 0;
				}
				if($review_state eq 'pending' || (defined($action) && $action eq 'changed')){
					$camelbot->msg(3, "pending '$page'");
					++$num_pending;
				}
				if($review_state eq 'never_reviewed'){
					$camelbot->msg(3, "removing '$page', because not reviewed at all");
					$keep = 0;
				}
			}
			$keep;
		} split /^(?=(?:\{\{.*\|)?=)/m, $text_bak;
		# line breaks between requests
		map {s/\s*$//s;} @requests;
		my $text = join("\n\n", @requests) . "\n";
		# generate summary after modifying page
		my $summary;
		my %msgs = (
			'reviewed' => 'removed reviewed pages',
			'never_reviewed' => 'removed never reviewed pages',
			'hidden' => 'hid too early requests',
			'changed' => 'changed redir to target',
			'invalid' => 'removed invalid requests',
		);
		for my $type(keys %msgs){
			if(@{$summary_struct->{$type}} > 0){
				$summary = $camelbot->i18n($msgs{$type}) . ': ';
				for my $page(@{$summary_struct->{$type}}){
					$summary .= "[[$page]], ";
				}
			}
		}
		if($num_pending == 0){
			$summary .= $camelbot->i18n('now empty');
		}else{
			$summary .= $num_pending . ' ' . $camelbot->i18n('pending');
		}
		$text =~ s/\n+\Z//;
		# save change review request page
		if($text_bak ne $text){
			$camelbot->msg(3, 'saving page');
			$camelbot->save_wiki_page($request_page, $summary, \$text, \$text_bak);
		}
	}
	return 1;
}

sub delete_double_pages_from_array{
	my $self = shift;
	my $entries = shift;
	my %dbl_counter;
	@$entries = (reverse
		grep {
			$dbl_counter{$_->{'page_with_ns'}} =
				(exists $dbl_counter{$_->{'page_with_ns'}}) ? 0 : 1;
			$dbl_counter{$_->{'page_with_ns'}};
		} sort {$b->{'timestamp'} cmp $a->{'timestamp'}} @$entries
	);
	return 1;
}

sub delete_redundant_pages_from_array{
	my $self = shift;
	my $entries = shift;
	my $edge_articles = shift;
	my $continue_timestamp_ref = shift;
	unless(defined $entries
		&& defined $edge_articles
		&& defined $continue_timestamp_ref
	){
		return;
	}
	$self->{'camelbot'}->msg(5, 'entries old: ' . Dumper($entries));
	$self->{'camelbot'}->msg(5, 'edge old: ' . Dumper($edge_articles));
	$self->{'camelbot'}->msg(3, 'ts old: ' . Dumper($continue_timestamp_ref));
	@$entries = grep {
		my $entry = $_;
		$entry->{'timestamp'} ne $$continue_timestamp_ref
		|| 0 == grep {
			$_->{'page_with_ns'} eq $entry->{'page_with_ns'}
		} @$edge_articles
	} @$entries;
	# remember all entries of newest timestamp, because they will be re-fetched
	# in next step along with others (that maybe of the same timestamp)
	if(@$entries > 0){
		$$continue_timestamp_ref = $entries->[-1]{'timestamp'};
	}
	@$edge_articles = grep {
		$_->{'timestamp'} eq $$continue_timestamp_ref
	} @$edge_articles;
	for(my $i = @$entries - 1;
		$i >= 0 && $entries->[$i]{'timestamp'} eq $$continue_timestamp_ref;
		--$i
	){
		push @$edge_articles, $entries->[$i];
	}
	$self->{'camelbot'}->msg(5, 'entries new: ' . Dumper($entries));
	$self->{'camelbot'}->msg(5, 'edge new: ' . Dumper($edge_articles));
	$self->{'camelbot'}->msg(3, 'ts new: ' . Dumper($continue_timestamp_ref));
	return 1;
}

sub get_extlinks_via_db{
	my $self    = shift;
	my $from_id = shift;
	my $to_id   = shift;
	my $extlinks = $self->{'camelbot'}->db_fetch_externallinks($from_id, $to_id);
	if(defined $extlinks){
		for my $dataset(@$extlinks){
			$dataset->{'page'} =~ s/_/ /g;
		}
		if($self->{'camelbot'}{'verbosity'} >= 2 or @$extlinks > 50){
			$self->{'camelbot'}->msg(1, 'got ' . (0 + @$extlinks) . ' entries.');
		}
	}
	return $extlinks;
}

sub get_pages_of_cat_of_dead{
	my $self  = shift;
	my $since = shift;
	my $camelbot = $self->{'camelbot'};
	my $pages = $camelbot->get_default_access_type() eq 'db'
		? $camelbot->db_fetch_pages_of_cats_of_the_dead($since)
		: $camelbot->fetch_pages_of_cats_of_the_dead($since);
	if(defined $pages){
		for my $dataset(@$pages){
			if($camelbot->get_default_access_type() eq 'db'){
				$dataset->{'page'} =~ s/_/ /g;
				$dataset->{'category'} =~ s/_/ /g;
			}
			$dataset->{'page_with_ns'} = $dataset->{'namespace'} == 0 ? '' :
				$camelbot->convert_ns($dataset->{'namespace'}, 'id2name') . ':';
			$dataset->{'page_with_ns'} .= $dataset->{'page'};
		}
		if($camelbot->{'verbosity'} >= 2 || @$pages > 50){
			$camelbot->msg(1, 'got ' . (0 + @$pages) . ' entries.');
		}
	}
	return $pages;
}

sub get_rc{
	my $self = shift;
	my $camelbot = $self->{'camelbot'};
	my $timestamp_begin = shift // $camelbot->get_time_iso(time() - 60 * 60 * 12);
	my $rc_pages = $camelbot->get_default_access_type() eq 'db'
		? $camelbot->db_fetch_recentchanges($timestamp_begin)
		: $camelbot->fetch_recentchanges($timestamp_begin);
	return unless defined $rc_pages;
	for my $dataset(@$rc_pages){
		my $timestamp_unix = $camelbot->convert_time_iso2unix($dataset->{'timestamp'});
		next unless defined $timestamp_unix;
		$dataset->{'timestamp_unix'} = $timestamp_unix;
		$dataset->{'page'} =~ s/_/ /g;
		$dataset->{'page_with_ns'} = $dataset->{'ns_id'} == 0 ? $dataset->{'page'} :
			$camelbot->convert_ns($dataset->{'ns_id'}, 'id2name')
			. ':' . $dataset->{'page'};
		$dataset->{'diffbytes'} =
			($dataset->{'rc_new_len'} // 0) - ($dataset->{'rc_old_len'} // 0);
	}
	# rc_msg format is now:
	#		diffbytes       int
	#		ns_id           int
	#		page            string (with spaces)
	#		page_with_ns    string (with spaces)
	#		rc_new_len      int or undef
	#		rc_old_len      int or undef
	#		rc_this_oldid   int (or undef?)
	#		rc_last_oldid   int (or undef?)
	#		summary         string (or undef?)
	#		timestamp       int/string (iso timestamp)
	#		timestamp_unix  int
	#		user            string (with spaces)
	#		rc_log_type     string ("move" or "delete") or undef
	#		move_target     string (with spaces) or undef
	# see https://www.mediawiki.org/wiki/Manual:Recentchanges_table for details
	if($camelbot->{'verbosity'} >= 2 || @$rc_pages > 50){
		$camelbot->msg(1, 'got ' . (0 + @$rc_pages) . ' entries.');
	}
	return $rc_pages;
}

sub handle_extlinks{
	my $self     = shift;
	my $extlinks = shift;
	for my $extlink(@$extlinks){
		$extlink->{'el_to'} =~ s/^\/\//https:\/\//;
		if($extlink->{'el_to'} !~ /
			https?:\/\/
			(?:[a-zA-Z0-9_-]+\.)?
			(?:wiki(?:books|data|[mp]edia)|wmflabs)
			\.org(?![^\/])/x
		){
			# check whether this link has been treated recently
			unless(grep {$_ eq $extlink->{'el_to'}} @{$self->{'last_ext_links'}}){
				# if not, then add link to list of recent ext. links
				push @{$self->{'last_ext_links'}}, $extlink->{'el_to'};
				if(@{$self->{'last_ext_links'}} > $self->{'max_ext_links'}){
					shift @{$self->{'last_ext_links'}};
				}
				$self->{'camelbot'}->archive_ext_links(
					[$extlink->{'el_to'}], $extlink->{'page'}
				);
			}
		}
	}
	return 1;
}

sub handle_rc_pages{
	my $self   = shift;
	# delayed clean up:
	my $max_pages_per_call = 200;
	my $min_info_number = 10; # for debugging/info only
	$self->handle_rc_pages_short_term($max_pages_per_call, $min_info_number);
	$self->handle_rc_pages_mid_term($max_pages_per_call, $min_info_number);
	return 1;
}

sub handle_rc_pages_mid_term{
	my $self   = shift;
	my $max_pages_per_call = shift;
	my $min_info_number = shift;
	my $now    = time;
	my $camelbot = $self->{'camelbot'};
	# cope with mid-term list
	#  after delay transfer pages from $rc_pages_midterm to @updList_midterm
	my @loop_array = sort {
		$self->{'rc_pages_midterm'}{$a}{'timestamp_unix'}
		<=> $self->{'rc_pages_midterm'}{$b}{'timestamp_unix'}
		} keys %{$self->{'rc_pages_midterm'}};
	if(@loop_array > 0){
		splice @loop_array, $max_pages_per_call if @loop_array > $max_pages_per_call;
		my @updList_midterm;
		my $oldest_date = $camelbot->get_time_iso_(
			$self->{'rc_pages_midterm'}{$loop_array[0]}{'timestamp_unix'});
		my $newest_date = $camelbot->get_time_iso_(
			$self->{'rc_pages_midterm'}{$loop_array[-1]}{'timestamp_unix'});
		for my $title(@loop_array){
			my $timestamp_unix = $self->{'rc_pages_midterm'}{$title}{'timestamp_unix'};
			if($timestamp_unix + $self->{'delay'} < $now){
				push @updList_midterm, $self->{'rc_pages_midterm'}{$title};
				delete $self->{'rc_pages_midterm'}{$title};
				$camelbot->msg(3, ' mid term update of page: '. $title);
			}
		}
		#  print some information
		my $numArticles = scalar(@updList_midterm);
		if($numArticles >= $min_info_number or $camelbot->{'verbosity'} >= 2){
			$camelbot->msg(1, "starting mid term checks of $numArticles (of " .
				($numArticles + keys(%{$self->{'rc_pages_midterm'}})) . ") articles, " .
				"dated from $oldest_date to $newest_date.");
		}
		#  all pages in @updList_midterm shall be treated now.
		my $cnt = 0;
		for my $change(@updList_midterm){
			$camelbot->msg(3,
				"fetch page '$change->{'page_with_ns'}' (" . (++$cnt) . ").");
			my $text = $camelbot->{'mw_bot'}->get_text($change->{'page_with_ns'});
			if(defined $text){
				my $text_bak = $text;
				# TODO: test this
				## find all external links and push to web-archive
				## find links
				#my @urls = ($$text =~ /https?:\/\/$self->{'re_url_rear'}/g);
				#$self->archive_ext_links(\@urls, $change->{'page_with_ns'});
				$camelbot->msg(3, 'clean-up page');
				my $changed;
				my ($dummy_changes, $summary) =
					$camelbot->cleanup_wiki_page(\$text, $change->{'page_with_ns'});
				if($text_bak ne $text){
					$camelbot->msg(3, 'saving page');
					$changed = $camelbot->save_wiki_page(
						$change->{'page_with_ns'}, $summary, \$text, \$text_bak) > 0;
				}
				$camelbot->update_maintenance_lists({
					'check_waiting' => 'mid-term',
					'list_name' => 'all_types',
					'lists' => $self->{'rc_pages_maintenance'},
					'rc_msg' => $change,
					'text' => ($changed ? \$text : \$text_bak),
				});
			}else{
				$camelbot->msg(2,
					"could not fetch page '$change->{'page_with_ns'}'. maybe deleted already",
					'notice');
			}
		}
		if($numArticles >= $min_info_number){
			$camelbot->msg(2, "end of mid term checks of $numArticles articles.");
		}
	}
	return 1;
}

sub handle_rc_pages_short_term{
	my $self   = shift;
	my $max_pages_per_call = shift;
	my $min_info_number = shift;
	my $now    = time;
	my $camelbot = $self->{'camelbot'};
	# cope with short-term list
	#  after delay transfer pages from $rc_pages_shortterm to @updList_shortterm.
	#  use oldest $max_pages_per_call articles only
	my @loop_array = sort { # sort by timestamp
		$self->{'rc_pages_shortterm'}{$a}{'timestamp_unix'}
		<=> $self->{'rc_pages_shortterm'}{$b}{'timestamp_unix'}
		} keys %{$self->{'rc_pages_shortterm'}};
	if(@loop_array > 0){
		# right trim array
		splice @loop_array, $max_pages_per_call if @loop_array > $max_pages_per_call;
		my @updList_shortterm;
		# just for debugging/info output
		my $oldest_date = $camelbot->get_time_iso_(
			$self->{'rc_pages_shortterm'}{$loop_array[0]}{'timestamp_unix'});
		my $newest_date = $camelbot->get_time_iso_(
			$self->{'rc_pages_shortterm'}{$loop_array[-1]}{'timestamp_unix'});
		for my $title(@loop_array){
			my $timestamp_unix = $self->{'rc_pages_shortterm'}{$title}{'timestamp_unix'};
			if($timestamp_unix + $self->{'delay_short'} < $now){
				push @updList_shortterm, $self->{'rc_pages_shortterm'}{$title};
				delete $self->{'rc_pages_shortterm'}{$title};
				$camelbot->msg(2, ' short term update of page: '. $title);
			}
		}
		# print some information
		my $numArticles = scalar(@updList_shortterm);
		if($numArticles >= $min_info_number or $camelbot->{'verbosity'} >= 2){
			$camelbot->msg(1, "starting notification check of $numArticles (of " .
				($numArticles + keys(%{$self->{'rc_pages_shortterm'}})) . ") articles, " .
				"dated from $oldest_date to $newest_date.");
		}
		# all pages in @updList_shortterm shall be treated now.
		for my $change(@updList_shortterm){
			# notifier on particular edits; there needs to be some delay, because
			# otherwise it may occur that the notification is faster than the saving
			# process itself.
			$camelbot->msg(2, "fetch page '$change->{'page_with_ns'}'.");
			my $text;
			$camelbot->notify_for_maintenance($change, \$text);
			# check if there is a call-back
			$camelbot->trigger_maintenance_callback($change, 'short-term');
			$camelbot->update_maintenance_lists({
				'check_waiting' => 'short-term',
				'list_name' => 'all_types',
				'lists' => $self->{'rc_pages_maintenance'},
				'rc_msg' => $change,
				'text' => \$text,
			});
		}
		if($numArticles >= $min_info_number){
			$camelbot->msg(2, "end of notification check of $numArticles articles.");
		}
	}
	return 1;
}

sub put_new_rc{
	my $self     = shift;
	my $rc_pages = shift;
	my $camelbot = $self->{'camelbot'};
	my %update = (
		'editfilter' => 0,
		'sbl' => 0,
	);
	for my $rc_msg(@$rc_pages){
		if(($rc_msg->{'user'} ne $camelbot->{'mw_username'} # don't track own edit
				|| $camelbot->{'mw_username'} eq $camelbot->{'operator'}) # only debugging
			&& (!(defined $rc_msg->{'summary'}) # don't play edit war
				|| $rc_msg->{'summary'} !~
					/\bCamelBot\b.*\b(?:r\x{fc}ckg\x{e4}ngig gemacht|revertiert)\b/)
		){
			# add page to mid term list
			$self->{'rc_pages_midterm'}{$rc_msg->{'page_with_ns'}} = {%$rc_msg};
			# add page to short term list
			if(time - $rc_msg->{'timestamp_unix'} < $self->{'short_max_age'}){
				$self->{'rc_pages_shortterm'}{$rc_msg->{'page_with_ns'}} = {%$rc_msg};
			}
			# print status
			$camelbot->msg(2, 'new rc: ' .
				($rc_msg->{'timestamp_unix'} - $self->{'start_ts'}) .
				' (#pages in stack: mt = ' .
				scalar(keys %{$self->{'rc_pages_midterm'}}) . ', st = ' .
				scalar(keys %{$self->{'rc_pages_shortterm'}}) . ') ' .
				$rc_msg->{'page_with_ns'} . ', editor = ' . $rc_msg->{'user'}
			);
			# check if page is on a maintenance list
			while(my ($type, $pages) = each %{$self->{'rc_pages_maintenance'}}){
				if(exists $pages->{$rc_msg->{'page_with_ns'}}){
					$camelbot->msg(2,
						"page '$rc_msg->{'page_with_ns'}' is on maintenance list '$type'.");
					$camelbot->update_maintenance_lists({
						'rc_msg' => $rc_msg,
						'list_name' => $type,
						'lists' => $self->{'rc_pages_maintenance'}
					});
				}else{
					$camelbot->msg(3,
						"page '$rc_msg->{'page_with_ns'}' is not on maintenance list '$type'.");
				}
			}
			# check if there is a call-back
			$camelbot->trigger_maintenance_callback($rc_msg, 'immediate');
		}
		# update edit filter index
		if($rc_msg->{'ns_id'} == 4 && $update{'editfilter'} == 0
				&& $rc_msg->{'page'} =~ /^$self->{'re_edit_filter_name'}\//
		){
			$self->{'camelbot'}->msg(2, 'detected change of edit filter page');
			$update{'editfilter'} = 1;
		}
		# update sbl index
		if($rc_msg->{'ns_id'} == 4 && $update{'sbl'} == 0
				&& $rc_msg->{'page'} =~ /^$self->{'re_sbl_pages'}\//
		){
			$self->{'camelbot'}->msg(2, 'detected change of sbl page');
			$update{'sbl'} = 1;
		}
	}
	if(keys(%{$self->{'rc_pages_shortterm'}}) > 0 ||
			keys(%{$self->{'rc_pages_midterm'}}) > 0){
		$self->handle_rc_pages();
	}
	if($update{'editfilter'}){
		$camelbot->update_edit_filter_index();
	}
	if($update{'sbl'}){
		$camelbot->update_sbl_index();
	}
	return 1;
}

sub subtract_days_from_now{
	my $self = shift;
	my $days = shift; # float
	return strftime("%Y-%m-%d %H:%M:%S", gmtime(time() - 60 * 60 * 24 * $days));
}

sub wait_till_abs_unixtime{
	my $self = shift;
	my $abs_time = shift;
	if($abs_time > time){
		my $sleep_time = $abs_time - time;
		$self->{'camelbot'}->msg(2, "sleep $sleep_time seconds");
		sleep($sleep_time);
	}
	return 1;
}

END { } # module clean-up code here (global destructor)
1;

=head1 NAME

CamelBotRC - Perl module that uses a L<CamelBot> in order to monitor all recent
changes on the wikipedia.

=head1 SYNOPSIS

  use CamelBotRC;
  my $camelbot_rc = CamelBotRC->new($camelbot);
  $camelbot_rc->rc_monitoring();

=head1 DESCRIPTION

TODO

=head2 Methods/Functions

=over 4

=item new
constructor

=item cat_of_dead_monitoring
use db to monitor added categories of dead people in an endless loop

=item db_extlinks_monitoring
use db to monitor external links in an endless loop

=item rc_monitoring
use db to monitor recent changes in an endless loop

=item delete_double_pages_from_array
given an array of hashes with page info (title and timestamp of change), delete all
old redundant entries, i.e., if there are two or more entries for the same page,
then keep the newest only

=item delete_redundant_pages_from_array
given two array refs A, B with meta info of pages, and a timestamp t, this function
removes all entries from A, that B already contains and that are of the timestamp t.
furthermore the timestamp will be modified to the newest timestamp of A.
finally B is emptied and refilled with all pages from A that are of the newest
timestamp.

=item subtract_days_from_now

given a float number $x, this function returns a UTC timestamp in iso format of
now() - $x days.

e.g. .5 -> iso timestamp of the moment 12 hours ago (in UTC).

=item get_extlinks_via_db
get ref to an array of hashes, where each hash represents an external link and
one page where it's used

=item get_pages_of_cat_of_dead
get ref to an array of hashes, where each hash represents a category and
one page where it's used

=item get_rc
get ref to an array of hashes, where each hash represents a recent change

=item handle_extlinks
given a bunch of external links, this function calls functions like
archive_ext_links

=item handle_rc_pages
major function for handling of recent changes of short-term and mid-term

=item put_new_rc
put new rc pages to short-term and mid-term lists, call handle_rc_pages()

=item wait_till_abs_unixtime
given a unix timestamp, wait until this timestamp is reached (or in the past)

=back

=head2 EXPORT

None by default.

=head1 SEE ALSO

* L<MediaWiki::Bot>
* https://gitlab.com/wp-seth/camelbot

=head1 AUTHOR

seth, https://gitlab.com/wp-seth/

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2019 by seth

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself, either Perl version 5.28.1 or,
at your option, any later version of Perl 5 you may have available.

=cut
