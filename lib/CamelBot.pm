package CamelBot;
use 5.018002;
use strict;
use warnings;
use Data::Dumper;          # for debugging purposes
use DateTime;              # e.g. current year
use DateTime::Format::Strptime; # parser for timestamps
use DBI;                   # db connection
use File::Slurp qw(write_file); # read/write files
use IO::Socket::SSL;       # part of LWP, needed for bugfix (see SSL_OCSP_NO_STAPLE)
use IPC::Run;              # used for tidy
use JSON;                  # json format is used for zeitde_converter for example
use LWP::UserAgent;        # fast, small web browser
use MediaWiki::Bot;        # bot for mediawiki, uses mediawiki-api-interface
use PHP::Serialization;    # decode db query
use POSIX qw/strftime/;    # format timestamp

use Exporter ();
our @ISA = qw(Exporter);
our %EXPORT_TAGS = ( ); # eg: TAG => [ qw!name1 name2! ],
# exported package globals, as well as any optionally exported functions
our @EXPORT_OK = qw();
our $VERSION = '2.8.0'; # 2024-12-24

# used to check whether this script is running on a labs server or somewhere else
# on labs e.g. Tk is disabled.
our $_running_on_labs = (-e '/data/project/camelbot/perl/lib');
#use if $_running_on_labs, lib => qw(/data/project/camelbot/perl/lib);

our %_loaded_mod;
$_loaded_mod{'Term::ReadKey'} = eval{
	require Term::ReadKey;    # used for user input
	Term::ReadKey->import();
	1;
};
$_loaded_mod{'Text::Diff'} = eval{
	require Text::Diff;       # diff
	Text::Diff->import();
	1;
};
$_loaded_mod{'Tk::DiffText'} = eval{
	require Tk::DiffText;     # graphical diff tool
	Tk::DiffText->import();
	1;
};
use Time::Local qw(timegm_modern);  # timegm_modern (array to unix timestamp)
$_loaded_mod{'URI::Escape'} = eval{
	require URI::Escape;      # uri_escape_utf8, uri_unescape
	URI::Escape->import();
	1;
};

sub new{
	my $class = shift;
	my $params = shift;
	my $self = bless {
		'access_types'   => [split(/;/, $params->{'access_types'} // 'db;api')],
		'assert_bot'     => $params->{'assert_bot'} // 1,
		# 0 = don't ask user; 1  = ask user for every action
		'ask_user'       => $params->{'ask_user'} // 0,
		'cleanup_replacements' => undef, # will be set below
		'db'             => {'file' => $params->{'db_access'}},
		'delete_cat'     => $params->{'delete_cat'} // "Category:L\x{f6}schen",
		'host'           => $params->{'host'} // 'de.wikipedia.org',
		'rel_url_path'   => $params->{'rel_url_path'} // 'w',
		'logged_in'      => 0,
		# maximum number of edits per minute (-1 = inf)
		'max_edits_per_min' => $params->{'max_edits_per_min'} // 5,
		'mem_usage'      => [{'mem' => [0, 0, 0, 0, 0], 'class_size' => 0}],
		'minor'          => $params->{'minor'} // 1, # 0 = non-minor; 1 = minor
		'mw_agent'       => $params->{'mw_agent'}, # agent param of MWBot
		'mw_api'         => undef, # instance of MediaWiki::API
		'mw_bot'         => undef, # instance of MediaWiki::Bot
		#'mw_password'    => $params->{'mw_password'}, # not needed permanently
		# user name maybe changed via login()!
		#
		'mw_lang'        => undef, # e.g. de, will be set in get_mw_language
		'mw_magicwords_aliases' => undef, # e.g. de, will be set in get_magicwords_alias
		'mw_username'    => $params->{'mw_username'},
		'mw_version'     => undef, # e.g. '1.27.4'
		'namespaces'     => undef, # all available namespaces
		'namespaces_used' => $params->{'namespaces_used'}, # namespaces to be used
		#'pw_file'        => undef, # will be set in function read_password_from_file
		'pw_ask'         => $params->{'pw_ask'} // 1, # ask user for password if not found
		'archive.today'  => {
			're_base_url'  => undef, # will be set later
			're_tld' => qr/(?:fo|is|li|md|ph|today|vn)/,
			're_short_url' => undef, # will be set later
			're_long_url'  => undef, # will be set later
		},
		're_editfilter'  => qr/^Wikipedia:Bearbeitungsfilter\/
				(?:(?:global-|)\d+|Antr\xe4ge$|Fehlerkennungen$|(?:Regelp|P)r\xfcfung$)/x,
		're_sbl_pages'   => qr/^Wikipedia:Weblinks\/Block\/(?:[0-9a-z].+)/x,
		're_url_class'        => qr/
			   [^\]\[<>"'\x00-\x20\x7F)]|'(?!')|\)(?!\s)/x,
		're_url_class_in_tpl' => qr/
			   [^\]\[<>"'\x00-\x20|\x7F)]|'(?!')|\)(?!\s)/x, # no pipes
		're_url_rear_explicit' => qr/
			(?:[^\]\[<>"'\x00-\x20\x2D\x7F)]|'(?!')|-(?!->)|\)(?!\s))*/x,
		're_url_rear'   => qr/ # no trailing points
			(?:[^\]\[<>"'\x00-\x20\x2D\x7F)]|'(?!')|-(?!->)|\)(?!\s))*
			(?:[^\]\[<>"'\x00-\x20\x2D\x7F)!,.?]|'(?!')|-(?!->)|\)(?!\s))/x,
		're_url_rear_maybetemplate'   => qr/  # TODO: this is just a work-around until #39 is fixed
			(?:[^\]\[<>"'\x00-\x20\x2D\x7F)|]|\|(?!(?:trans-|)title=|website=|titel=)|'(?!')|-(?!->)|\)(?!\s))*/x,
		're_url_rear_in_tpl' => qr/ # trailing points allowed; no pipes no trailing '}}'
			(?:[^\]\[<>"'\x00-\x20\x2D|}\x7F)]
				|'(?!')
				|}(?!})
				|-(?!->)
				|\)(?!\s)
				)*
			/x,
		're_url_tracking_filter' => undef,  # set via init_re_url_tracking_filter
		'rev_req'        => {
			'prefix' => 'Wikipedia:',
			'ns' => 4,
			'page_title' => 'Gesichtete_Versionen/Anfragen',
			'title' => 'WP:GV/A',
		},
		'maintenance'    => {}, # will be set in separate function
		'offline'        => 0,  # set to 1, camelbot tries to stay offline (TODO)
		'operator'       => $params->{'operator'} // 'lustiger_seth',
		# 0 = don't show diff; 1 = show a diff, 2 = gui
		'showdiff'       => $params->{'showdiff'} // 0,
		# 0 = do real edits; 1 = simulate only
		'simulation'     => $params->{'simulation'} // 0,
		'time_stack'     => [], # needed for edit/min constraint
		'use_www_cache'  => 0,  # see www_cache; 0 = no, 1 = yes
		# user answer initially is "no"
		'user_answer'    => $params->{'user_answer'} // '',
		'verbosity'      => $params->{'verbosity'} // 1,
		'wm_proj_type'   => undef, # e.g. wikipedia
		'wm_proj'        => undef, # e.g. dewiki
		'wm_lang'        => undef, # e.g. de
		'www_cache'      => undef, # reduce www requests; e.g. {url1 => content1, ...}
		'www_cache_file' => 'camelbot_www_cache.json',
		'www_history'    => [],  # last used urls
		'cliparams'      => $params->{'cliparams'},
	}, $class;
	$self->{'operator'} = $self->check_username($self->{'operator'});
	$self->{'pw_ask'} = 0 if $_running_on_labs;
	my $archivetoday =
		qr/https?:\/\/(?:www\.)?archive\.$self->{'archive.today'}{'re_tld'}/;
	$self->{'archive.today'}{'re_base_url'} = $archivetoday;
	$self->{'archive.today'}{'re_short_url'} =
		qr/$archivetoday\/(?!faq\b)(?!http\b)(?!post\/)(?:wip\/)?
			[0-9a-zA-Z_-]{0,5}[a-zA-Z][0-9a-zA-Z_-]{0,5}+(?:\/image|)
		(?:\x23selection[0-9.-]*+)?(?![\x23])/x;
	$self->{'archive.today'}{'re_long_url'} =
		qr/$archivetoday\/[0-9]{4}[0-9.-]*\//x;
	$self->set_host();
	$self->init_cleanup_replacements();
	$self->refresh_maintenance_params();
	return $self;
}

sub DESTROY{
	my $self = shift;
	$self->{'db'}{'handle'}->disconnect if defined $self->{'db'}{'handle'};
	return 1;
}

sub add_to_maintenance_table{
	my $self = shift;
	my $table = shift;
	my $new_entries = shift // [];
	my $additional_col = shift // [];
	unless(defined $table && defined $table->{'header'} && defined $table->{'body'}){
		$self->msg(0, 'table not (enough) defined', 'error');
		$self->msg(1, Dumper($table));
		return;
	}
	# number of additional (ignored/unchanged) columns
	my $num_add_cols = @{$table->{'header'}} - 2 - scalar(@$additional_col);
	if($num_add_cols < 0){
		$self->msg(0, 'header of table has not enough columns', 'warning');
		$num_add_cols = 0;
	}
	my @empty_cells = split //, (' ' x $num_add_cols);
	my $added_pages = [];
	for my $page(@$new_entries){
		$self->msg(1, "add new list entry '$page->{'page_with_ns'}'");
		# if table has more than 2 columns, then pad each row
		my @additional_cells = (); # content of ['column1', 'column2', ...]
		push @$added_pages, '';
		my $ns = $self->get_namespace_id($page->{'page_with_ns'});
		my $link_colon = ($ns == 6 || $ns == 14) ? ':' : '';
		$added_pages->[-1] .= "[[$link_colon$page->{'page_with_ns'}]]";
		map {
			my $add_cell = $page->{$_} // '';
			if($add_cell ne ''){
				$added_pages->[-1] .= ', ' . $_ . ': ' . $add_cell;
			}
			push @additional_cells, $add_cell;
		} @$additional_col;
		push @{$table->{'body'}}, [
			"[[$link_colon$page->{'page_with_ns'}]]",
			@additional_cells,
			$self->get_date_iso(),
			@empty_cells
		];
	}
	return $added_pages;
}

sub _api_cont{
	my $self         = shift;
	my $query        = shift;
	my $continue_params = shift // {}; # may be modified in this function
	my $mw_options   = shift;
	my $die_on_error = shift // 1;
	my $finished = 0;
	my $query_copy = {%$query, %$continue_params};
	$query_copy->{'continue'} = '' unless exists $query_copy->{'continue'};
	my $api_result = $self->api_simple($query_copy, $mw_options, $die_on_error);
	if(defined $api_result->{'continue'}){
		$self->msg(5, "got 'continue'");
		$self->msg(5, Dumper($api_result->{'continue'}));
		%$continue_params = %{$api_result->{'continue'}};
	}else{
		$finished = 1;
	}
	return ($api_result, $finished);
}

sub api_cont_complete{
	# experimental
	my $self         = shift;
	my $query        = shift; # may be modified in this function
	my $mw_options   = shift;
	my $die_on_error = shift // 1;
	my $results = {};
	my $finished = 0;
	my $continue = {};
	while(!$finished){
		(my $api_result, $finished) = $self->_api_cont($query, $continue);
		my $main_key = (keys %{$api_result->{'query'}})[0];
		my $main_result = $api_result->{'query'}{$main_key};
		$self->msg(3, 'got ' . scalar(%$main_result) . ' results');
		while(my ($page_id, $page_data) = each %$main_result){
			if($page_id !~ /^[0-9]+\z/){
				$self->msg(1, "page_id is '$page_id'; expected: /^[0-9]+\\z/", 'warning');
				return;
			}
			if(exists $results->{$page_id}){
				# merge
				for my $key(keys %$page_data){
					if(ref $page_data->{$key} eq 'ARRAY'){
						$results->{$page_id}{$key} = $page_data->{$key};
					}elsif(ref $page_data->{$key} eq 'HASH'){
						while(my ($k, $v) = each %{$page_data->{$key}}){
							$results->{$page_id}{$key}{$k} = $v;
						}
					}else{
						$results->{$page_id}{$key} = $page_data->{$key};
					}
				}
			}else{
				# add
				$results->{$page_id} = $page_data;
			}
		}
	}
	for my $p(values %$results){
		unless(defined($p->{'revisions'}) && @{$p->{'revisions'}} > 0){
			$self->msg(0, 'no revisions', 'warning');
			next;
		}
	}
	return $results;
}

sub api_simple{
	my $self         = shift;
	my $query        = shift;
	my $mw_options   = shift;
	my $die_on_error = shift // 1;
	$self->msg(4, Dumper($query));
	unless($self->{'mw_api'}){
		$self->_set_api();
	}
	my $api_result = $self->{'mw_api'}->api($query, $mw_options);
	unless(defined $api_result){
		my $caller_increment = 1;
		$self->_handle_api_error('api', $caller_increment);
		die if $die_on_error;
	}
	if(defined $api_result){
		$self->msg(4, Dumper($api_result));
		if(defined $api_result->{'warnings'}){
			$self->msg(1, Dumper($query));
			$self->msg(1, Dumper($api_result->{'warnings'}), 'warning');
		}
	}
	return $api_result;
}

sub api_reviewstate{
	my $self = shift;
	my $query = shift;
	my $finished = 0;
	my $result = {};
	my $continue = {};
	while(!$finished){
		(my $pages, $finished) = $self->_api_cont($query, $continue);
		map {
			# this is also fine for completely unreviewed pages, because 'flagged'
			# is not existent there.
			if(exists $_->{'flagged'}){
				if(exists $_->{'flagged'}{'pending_since'}){
					$result->{$_->{'title'}} =
						$self->convert_time_iso_strip_TZ($_->{'flagged'}{'pending_since'});
				}else{
					$result->{$_->{'title'}} = 'reviewed';
				}
			}else{
				$result->{$_->{'title'}} = 'never_reviewed';
			}
		} values %{$pages->{'query'}{'pages'}};
	}
	$self->msg(4, Dumper($result));
	return $result;
}

sub archive_ext_links{
	my $self = shift;
	my $urls = shift;
	my $wp_page = shift // '';
	my $wp_page_url = $self->title2url($wp_page);
	# TODO: 1. fill in params; 2. test
	my $email_address = '';
	map {
		my $encoded_url = $self->uri_escaper($_);
		my $url = 'https://web.archive.org/save/' . $_;
		#$self->msg(2, $url);
		$self->check_external_link(\'', \$url);
		#$url = "https://www.webcitation.org/archive?"
		#	. "email=$email_address&source=$wp_page_url&url=$encoded_url";
		#$self->msg(2, $url);
		#$self->check_external_link(\'', \$url);
	} grep {!/^https?:\/\/(?:[a-z]+\.)?(?:archive|webcitation)\.org/} @$urls;
	return 1;
}

sub array_contains_elem{
	# given an array and an elemnt, return 1 iff elem is contained in array
	my $self = shift;
	my $list = shift;
	my $elem = shift;
	$list = [$list] if ref $list eq '';
	return 0 < grep {$elem eq $_} @$list;
}

sub array2regexp{
	my $self = shift;
	my $arr = shift;
	my $re = join('|', @$arr);
	$re =~ s/\./\\./g;
	return qr/$re/;
}

sub cat_add{
	my $self  = shift;
	my $pages = shift;
	my $cat   = shift;
	my $summary = '+cat';
	my $cat_first = lc substr($cat, 0, 1);
	my $cat_tail = substr($cat, 1);
	$self->msg(1, "start cat adding...");
	for my $page(@$pages){
		$self->msg(1, "page = $page");
		my $text = $self->get_text($page);
		next unless defined $text;
		# page is already in cat
		next if $text =~ /\[\[[Cc]ategory:(?i:\Q$cat_first\E)\Q$cat_tail\E\]\]/;
		$self->msg(1, "add page to cat.");
		# add page to cat
		my $text_bak = $text;
		if($text =~ /\[\[[cC]ategory:/){
			$text =~ s/.*\K(?=\[\[[cC]ategory:)/[[category:$cat]]\n/g;
		}else{
			$text =~ s/\s*$/\n\n[[category:$cat]]/g;
		}
		$self->save_wiki_page($page, $summary, \$text, \$text_bak);
	}
	return 1;
}

sub cat_rename{
	my $self = shift;
	my $old_cat = shift;
	my $new_cat = shift;
	my $summary = 'cat change';
	my $old_cat_first = lc substr($old_cat, 0, 1);
	my $old_cat_tail = substr($old_cat, 1);
	$self->msg(1, "start cat renaming...");
	# get pages in (old) category
	my @pages = $self->{'mw_bot'}->get_pages_in_category("Category:$old_cat");
	$self->msg(1, 'found '.@pages." pages:");
	# for all pages in category change category
	for my $page(@pages){
		$self->msg(1, "page = $page");
		my $text = $self->get_text($page);
		next unless defined $text;
		my $text_bak = $text;
		$text =~ s/
			\[\[[cC]ategory:((?i:\Q$old_cat_first\E)\Q$old_cat_tail\E)\]\]
			/[[category:$new_cat]]/gx;
		if($text ne $text_bak){
			$self->msg(1, "changing cat: $1 -> $new_cat");
			$self->save_wiki_page($page, $summary, \$text, \$text_bak);
		}
	}
	return 1;
}

sub change_review_request{
	my $self = shift;
	my $req_textref = shift;
	my $old_req_page = shift;
	my $new_req_page = shift;
	unless(defined $req_textref){
		$self->msg(0, 'missing text ref', 'warning');
		return;
	}
	unless(defined $old_req_page){
		$self->msg(0, 'missing "from" page', 'warning');
		return;
	}
	unless(defined $new_req_page){
		$self->msg(0, 'missing "to" page', 'warning');
		return;
	}
	my $num = $$req_textref =~ s/[: |]\K$old_req_page(?=[\]}])/$new_req_page/g;
	# cope with underlines and encodings
	my $old_req_page_ue = $self->title2url_part($old_req_page);
	my $new_req_page_ue = $self->title2url_part($new_req_page);
	$num += $$req_textref =~ s/title=\K\s*$old_req_page_ue/$new_req_page_ue/g;
	if($num > 0){
		$$req_textref .= "\n:" . $self->i18n('redirect') . ' "' . $old_req_page . '" '
			. $self->i18n('replaced with') . ' "' . $new_req_page . "\". -- ~~~~\n";
	}
	return $num; # 0, 1, 2, ... number of replacements
}

sub check_abusefilter_match{
	my $self = shift;
	my $log_id = shift;
	my $condition = shift;
	my $query = {
		'action' => 'abusefiltercheckmatch',
		'filter' => $condition,
		'logid'  => $log_id,
	};
	my $api_result = $self->api_simple($query);
	return $api_result->{'abusefiltercheckmatch'}{'result'};
}

sub check_external_link{
	my $self      = shift;
	my $pretext   = shift;
	my $urlstring = shift;
	my $ok = 1;
	if($$urlstring =~ /^https?:\/\//){
		my $url = $$urlstring; #for vim: { {
		if($$pretext =~ /url\s*=\s*$/ && $$urlstring =~ /^([^|}]+)[|}]/){
			$url = $1;
		}
		# link recognition will fail on urls such as
		#   https://web.archive.org/web/19991231010101/http://example.org...
		# so check whether the last character before the url was a slash '/'
		#if($$pretext =~ /https?:\/\/web\.archive\.org\/web\/(?:[0-9]+|\*)\/\z/){
		if(length($$pretext) > 10 && substr($$pretext, -1) eq '/'){
			$ok = 0;
		}else{
			unless($self->{'offline'}){
				my ($response_code, $location) = $self->get_http_status($url);
				$self->msg(1, "  status of '$url': $response_code");
				$ok = 0 if $response_code != 200;
			}
		}
	}# else: no url, return true
	return $ok;
}

sub check_password{
	my $self = shift;
	my $wiki_password = shift;
	# if wiki user password is not defined, search typical password places or let
	# user type in wiki user password
	unless(defined $wiki_password){
		$wiki_password = $self->read_password_from_file();
		if($self->{'pw_ask'}
			&& !defined($wiki_password)
			&& !(# $self->{'mw_username'} =~/^(?:CamelBot)$/ &&
					-e $self->get_cookie_filename() &&
					!($self->{'cliparams'}{'delete'} || $self->{'cliparams'}{'upload'})
				)
		){
			$self->msg(0, "enter password (will not be echoed):");
			ReadMode('raw') if defined &ReadMode;
			if(defined &ReadLine){
				$wiki_password = ReadLine(0);
				if(defined $wiki_password){
					chomp($wiki_password);
					while($wiki_password =~ /\x{007f}/){
						$wiki_password =~ s/(?:^|[^\x{007f}])\x{007f}//g;
					}
				}
			}
			ReadMode('restore') if defined &ReadMode;
		}
	}
	return $wiki_password;
}

sub check_review_request{
	my $self = shift;
	my $request_textref = shift;
	my $review_states = shift; # {page_name1 => ('reviewed'|'never_reviewed'|timestamp), ...}
	my $request_page;
	my $action;
	# format used at $self->{'rev_req'}
	if($$request_textref =~ /^(?:\{\{\x23ifexpr:\s*\{\{.*?\}\}[^|]*\|\s*)?
			==\s*\[[^\s]*\s+(.*)\]\s*==\n/x
	){
		$request_page = $self->normalize_page_title($1);
		# see de.wikipedia.org/w/index.php?title=Wikipedia:Gesichtete_Versionen/Anfragen&diff=prev&oldid=236922971
		$request_page =~ s/\x{200e}//g;
		if(exists $review_states->{$request_page}){
			if($review_states->{$request_page} eq 'reviewed'){
				# cope with requests of redirects to pending pages
				# my $request_date = get date of review request using convert_time_signature2iso()
				# my $review_date = get date of reviewing of page
				# if($request_date gt $review_date){
				my $redirect = $self->is_page_redirect($request_page);
				if(defined($redirect) && $redirect =~ /^([^#]+)/){
					my $redirect_page = $1;
					my $state = $self->get_review_state($redirect_page);
					$state = $state->{$redirect_page};
					if($state eq 'pending'){
						$self->change_review_request(
							$request_textref, $request_page, $redirect_page);
						$action = 'changed';
					}
				}
				# }
			}elsif($review_states->{$request_page} eq 'never_reviewed'){
				my $users = $self->get_usernames_from_signatures($$request_textref);
				if(defined($users) && @$users > 0){
					my $heading = undef;
					$self->notify_user_predefined(
						$users->[0], $heading, 'unreviewed-pages', $request_page);
				}
			}elsif($review_states->{$request_page} =~ /^[0-9]{4}/){ # pending since
				my $yesterday_unixtime = time() - (60 * 60 * 24);
				my $pending_since_unixtime =
					$self->convert_time_iso2unix($review_states->{$request_page});
				my $request_date_unixtime = $self->convert_time_iso2unix(
					$self->convert_time_signature2iso($$request_textref));
				if($request_date_unixtime > $pending_since_unixtime
					&& $pending_since_unixtime >= $yesterday_unixtime
				){
					if($$request_textref !~ /^\{\{\x23ifexpr/){
						my $users = $self->get_usernames_from_signatures($$request_textref);
						$$request_textref = "{{#ifexpr: {{#time:YmdHis|-1 day}} > "
							. $self->convert_time_iso_sep($review_states->{$request_page})
							. "|" . $$request_textref . ' |('
							. $self->i18n('review request') . ' ' . $self->i18n('for')
							. " [[:$request_page]] " . $self->i18n('postponed') . ")}}\n\n";
						$action = 'hidden';
						if(defined($users) && @$users > 0){
							my $heading = undef;
							$self->notify_user_predefined(
								$users->[0], $heading, 'early-review-request', $request_page);
						}
					}
				}
			}else{
				$self->msg(0, "this should never happen: "
					. "'$request_page' has strange state '$review_states->{$request_page}'",
					'warning');
			}
		}else{
			$self->msg(1, "'$request_page' does not seem to exist.");
			# https://de.wikipedia.org/w/index.php?title=Wikipedia:Gesichtete_Versionen/Anfragen&diff=prev&oldid=239977426
			if($$request_textref =~ /
				title=Artikelname&diff=review\sArtikelname\].*
				\{\{Sichten\|Artikelname\}\}\s*--/sx
			){
				$action = 'invalid';
			}
		}
	}
	return ($request_page, $action);
}

sub _check_triggering_namespaces{
	my $self = shift;
	my $triggering_namespaces = shift;
	my $rc_msg = shift;
	my $ns_id_src = $rc_msg->{'ns_id'}
		// $self->get_namespace_id($rc_msg->{'page_with_ns'});
	my $ns_src_triggers = 0 < grep {$_ == $ns_id_src} @$triggering_namespaces;
	my $moved = ($rc_msg->{'rc_log_type'} // '') eq 'move';
	# if page is not moved, then just check namespace
	return 1 if(!$moved && $ns_src_triggers);
	# else: check source and destination namespaces.
	# if page is moved to namespace 0 and ns 0 is to be looked at
	my $ns_id_dest = $self->get_namespace_id($rc_msg->{'move_target'});
	my $ns_dest_triggers = $moved
		? 0 < grep {$_ == $ns_id_dest} @$triggering_namespaces : undef;
	if($moved && $ns_dest_triggers){  # page is moved and destination ns triggers
		if($ns_id_src != 0   # src is not in main namespace
			&& $ns_id_dest == 0  # moved to main ns
		){
			return 3;
		}else{
			return 2;
		}
	}
	return 0;
}

sub check_username{
	my $self = shift;
	my $username = shift;
	my $use_default_name = !(defined $username);
	if($use_default_name){
		$username = $self->{'mw_username'};
	}
	# if wiki user name is not defined, let user type in wiki user name
	unless(defined $username){
		$self->msg(1, "enter wiki username:");
		$username = <STDIN>;
		chomp($username);
	}
	# cope with auto-normalization (bug in MediaWiki::Bot)
	$username = $self->normalize_page_title($username);
	if($use_default_name){
		$self->{'mw_username'} = $username;
	}
	return $username;
}

sub check_mw_version{
	my $self = shift;
	my $to_cmp = shift;
	unless(defined $to_cmp){
		$self->msg(0, 'missing parameter. '
			. 'how should i compare versions if you do not give me one to compare?',
			'error');
		return;
	}
	unless($to_cmp =~ /^([<>=]=?)([0-9]+)\.([0-9]+)\.([0-9]+)\z/){
		$self->msg(0, 'wrong parameter. format: <op><version>, e.g. ">=1.30.0"',
			'error');
		return;
	}
	my $cmp_op = $1;
	$cmp_op = '==' if $1 eq '=';
	my $major = $2;
	my $minor = $3;
	my $patch = $4;
	my $version = $self->get_mw_version();
	unless(defined $version){
		$self->msg(0, 'could not fetch version of this mediawiki', 'error');
		return;
	}
	unless($version =~ /^([0-9]+)\.([0-9]+)(?:\.([0-9]+))?\z/){
		$self->msg(0, "cannot parse version '$version'."
			. ' this is an internal bug that has to be fixed.', 'error');
		return;
	}
	my $is_major = $1;
	my $is_minor = $2;
	my $is_patch = $3 // 0;
	my $cmp = $is_major > $major ? 1
		: $is_major < $major ? -1
		: $is_minor > $minor ? 1
		: $is_minor < $minor ? -1
		: $is_patch > $patch ? 1
		: $is_patch < $patch ? -1 : 0;
	if($cmp == 0){
		return length($cmp_op) == 2 ? 1 : 0;
	}elsif($cmp == 1){
		return substr($cmp_op, 0, 1) eq '>' ? 1 : 0;
	}elsif($cmp == -1){
		return substr($cmp_op, 0, 1) eq '<' ? 1 : 0;
	}
	$self->msg(0, 'this place should not be reachable', 'error');
	return;
}

sub cleanup_wiki_pages{
	my $self = shift;
	my $pages = shift; # array
	my $max_replacements = shift; # maximum number of replacements
	unless(defined $pages){
		$self->msg(0, '$pages is undefined', 'warning');
		return;
	}
	my $count_changed = 0;
	my $count_all = 0;
	for my $page(@$pages){
		$self->msg(1,
			"$count_all/" . scalar(@$pages) . " (changed: $count_changed): $page");
		my $text = $self->get_text($page);
		next unless defined $text;
		my $text_bak = $text;
		my ($changes, $summary) = $self->cleanup_wiki_page(\$text, $page);
		if($summary ne ''){
			if($self->save_wiki_page($page, $summary, \$text, \$text_bak) > 0){
				++$count_changed;
			}
		}
		++$count_all;
		last if(defined($max_replacements) && $count_changed >= $max_replacements);
	}
	return $count_changed;
}

sub cleanup_wiki_page{
	my $self     = shift;
	my $text_ref = shift; # ref to string
	my $page     = shift; # full article name
	my $params   = shift // {};
	# namespace of article (will be fetched if not set)
	my $ns = $params->{'ns'};
	# force to check for optional minor changes
	$params->{'force_minor'} = 0 unless defined $params->{'force_minor'};
	my $changes = {};
	my $allow_cleaning;
	if(defined $page){
		$ns = $self->get_namespace_id($page) unless defined $ns;
		if($self->namespace_allowed($ns)){
			unless(defined($text_ref)
				&& ref($text_ref) eq 'SCALAR'
				&& defined($$text_ref)
			){
				$self->msg(2, '$$text_ref is not defined; creating from page');
				$$text_ref = $self->get_text($page);
			}
			$allow_cleaning = 1 if defined $$text_ref;
		}else{
			$self->msg(3, "page '$page' is not in a namespace where i'm allowed.");
		}
	}else{
		$self->msg(0, '$page is not defined', 'warning');
	}
	unless($allow_cleaning){
		$self->msg(3, 'won\'t clean', 'warning');
		return ($changes, '');
	}
	my ($split_text, $successfully_split) = $self->split_wikitext($text_ref);
	unless($successfully_split){
		$self->msg(0, "could not parse wikitext of page '$page'", 'error');
		$self->msg(1, Dumper([$page, $text_ref, $split_text]));
		return ($changes, '');
	}
	$self->{'converted_urls'} = {};
	my $repl_sub = sub {
		my $idx  = shift;
		my $repl = shift;
		# given from outer scope: $page, $split_text, $changes
		if(defined $repl->{'cond'}){
			my $c = $repl->{'cond'};
			if(defined $c->{'page_exception_re'} && $page =~ $c->{'page_exception_re'}){
				$self->msg(3, 'page matches exception regexp');
				return;
			}
			if(defined $c->{'type'} && !$self->array_contains_elem($c->{'type'}, $split_text->[$idx]{'type'})){
				$self->msg(4, 'text is of wrong type');
				return;
			}
			if(defined $c->{'pre_type'}
				&& ($idx == 0 || !$self->array_contains_elem($c->{'pre_type'}, $split_text->[$idx - 1]{'type'}))
			){
				$self->msg(4, 'pre text is of wrong type');
				return;
			}
			if(defined $c->{'post_type'}
				&& ($idx + 1 == @$split_text
					|| !$self->array_contains_elem($c->{'post_type'}, $split_text->[$idx + 1]{'type'}))
			){
				$self->msg(4, 'post text is of wrong type');
				return;
			}
			if(defined $c->{'exclude_pre_type'} && $idx > 0
				&& $self->array_contains_elem($c->{'exclude_pre_type'}, $split_text->[$idx - 1]{'type'})
			){
				$self->msg(4, 'pre type is excluded');
				return;
			}
			if(defined $c->{'pre_text'}
				&& ($idx == 0 || $split_text->[$idx - 1]{'text'} !~ $c->{'pre_text'})
			){
				$self->msg(4, 'pre text is wrong');
				return;
			}
			if(defined $c->{'second'}
				&& ($split_text->[$idx]{'text'} !~ $c->{'second'})
			){
				$self->msg(4, 'second condition is not fulfilled');
				return;
			}
			if(defined $c->{'post_text'}
				&& ($idx + 1 == @$split_text
					|| $split_text->[$idx + 1]{'text'} !~ $c->{'post_text'})
			){
				$self->msg(4, 'pre text is wrong');
				return;
			}
			if(defined $c->{'exclude_pre_text'} && $idx > 0
				&& $split_text->[$idx - 1]{'text'} =~ $c->{'exclude_pre_text'}
			){
				return;
			}
			if(defined $c->{'exclude_post_text'} && $idx + 1 < @$split_text
				&& $split_text->[$idx + 1]{'text'} =~ $c->{'exclude_post_text'}
			){
				$self->msg(4, 'post text is excluded');
				return;
			}
		}
		if(defined $repl->{'group_backwards'}){
			for(my $i = $idx - 1; $i >= 0; --$i){
				last if $split_text->[$i]{'text'} !~ $repl->{'group_backwards'};
				$split_text->[$idx]{'text'} = $split_text->[$i]{'text'}
					. $split_text->[$idx]{'text'};
				$split_text->[$i]{'text'} = '';
				$split_text->[$i]{'type'} = 'text';
			}
		}
		my $text = \$split_text->[$idx]{'text'};
		my $chgs = $self->test_and_replace($text,
			$repl->{'search'}, $repl->{'repl'}, ($repl->{'eval'} // 0),
			($repl->{'print_non_changes'} // 1));
		push @{$changes->{$repl->{'key'}}}, @$chgs;
		return 1;
	};
	# standard fixes
	for(my $i = 0; $i < @$split_text; ++$i){
		map {$repl_sub->($i, $_)} @{$self->{'cleanup_replacements'}{'normal'}};
	}
	# optional fixes
	if($params->{'force_minor'} || 0 < grep {0 < @$_} values %$changes){
		for(my $i = 0; $i < @$split_text; ++$i){
			map {
				$repl_sub->($i, $_)
			} @{$self->{'cleanup_replacements'}{'optional'}};
		}
	}
	if($changes->{'url-converter'}){
		# modify summary in some cases, language-dependent
		$changes->{'linkfix: '
			. join(', ', map {$self->i18n($_)} keys %{$self->{'converted_urls'}})
		} = $changes->{'url-converter'};
		delete $changes->{'url-converter'};
	}
	$self->{'converted_urls'} = {};
	my $summary = '';
	my @changes_names = grep {0 < @{$changes->{$_}}} keys %$changes;
	if(@changes_names > 0){
		# modify summary in some cases, language-dependent
		my @i18_changes_names = $self->i18n(@changes_names);
		$summary = '' # $self->i18n('minor improvement') . ': '
			. (join ', ', sort @i18_changes_names) . '; '
			. $self->i18n('see') . ' [[user:CamelBot]].';
	}
	$$text_ref = '';
	map {$$text_ref .= $_->{'text'}} @$split_text;
	return ($changes, $summary);
}

sub compose_url{
	my $self = shift;
	my $url_struct = shift; # see decompose_url
	return unless(defined($url_struct) && defined($url_struct->{'host'}));
	my $url = ($url_struct->{'protocol'} // '') . '//'
		. ($url_struct->{'credentials'} // '')
		. $url_struct->{'host'}
		. ($url_struct->{'port'} // '') . ($url_struct->{'path'} // '');
	if(exists $url_struct->{'query'} && @{$url_struct->{'query'}} > 0){
		$url .= '?' . join('&', @{$url_struct->{'query'}});
	}
	$url .= ($url_struct->{'fragment'} // '');
	return $url;
}

sub convert_german_date2iso{
	my $self = shift;
	my $timestamp = shift;  # e.g. '3. Januar 2001'
	return unless defined $timestamp;
	my %month = (
		qr/Jan(?:uar|\.)/    => '01',
		qr/Feb(?:ruar|\.)/   => '02',
		qr/M\x{e4}r[z.]/     => '03',
		qr/Apr(?:il|\.)/     => '04',
		qr/Mai\.?/           => '05',
		qr/Jun[i.]/          => '06',
		qr/Jul[i.]/          => '07',
		qr/Aug(?:ust|\.)/    => '08',
		qr/Sep(?:tember|\.)/ => '09',
		qr/Okt(?:ober|\.)/   => '10',
		qr/Nov(?:ember|\.)/  => '11',
		qr/Dez(?:ember|\.)/  => '12',
	);
	if($timestamp =~ /^([0-9]+)\.\s*([A-Z][a-z\x{e4}]+\.?)\s*([12][0-9]{3})\z/){
		$timestamp = "$3-$month{$2}-" . sprintf('%02d', $1);
	}
	if($timestamp !~ /^[0-9]{4}(?:-[0-9]{2}){2}\z/){
		$self->msg(0, "timestamp '$timestamp' is not iso", 'warning');
		return;
	}
	return $timestamp;
}

sub convert_time_iso_strip_TZ{
	my $self = shift;
	my $timestamp = shift;  # e.g. '2001-12-31T12:34:56Z'
	return unless defined $timestamp;
	if($timestamp =~ /^
		([0-9]{4}-[0-9]{2}-[0-9]{2})[\sT]?([0-9]{2}:[0-9]{2}:[0-9]{2})Z?\z/x
	){
		return "$1 $2";
	}
	return;
}

sub convert_time_iso_sep{
	my $self = shift;
	my $timestamp = shift;  # e.g. '2001-12-31T12:34:56Z' or '20011231123456'
	return unless defined $timestamp;
	if($timestamp =~ /^
		([0-9]{4})-([0-9]{2})-([0-9]{2})[\sT]?([0-9]{2}):([0-9]{2}):([0-9]{2})Z?\z/x
	){
		return "$1$2$3$4$5$6";
	}elsif($timestamp =~ /^
		([12][0-9]{3})([0-9]{2})([0-9]{2})([0-9]{2})([0-9]{2})([0-9]{2})\z/x
	){
		return "$1-$2-$3 $4:$5:$6";
	}
	return;
}

sub convert_time_iso2signature_string{
	my $self = shift;
	my $timestamp = shift;  # 2001-12-31T12:34:56Z
	return unless defined $timestamp;
	my $lang = shift // 'en';
	my $tz = shift // 'UTC';
	my $months = {
		'en' => {
			'01' => 'January',
			'02' => 'February',
			'03' => 'March',
			'04' => 'April',
			'05' => 'May',
			'06' => 'June',
			'07' => 'July',
			'08' => 'August',
			'09' => 'September',
			'10' => 'October',
			'11' => 'November',
			'12' => 'December',
		},
		'de' => {
			'01' => 'Jan.',
			'02' => 'Feb.',
			'03' => "M\xe4r.",
			'04' => 'Apr.',
			'05' => 'Mai',
			'06' => 'Jun.',
			'07' => 'Jul.',
			'08' => 'Aug.',
			'09' => 'Sep.',
			'10' => 'Okt.',
			'11' => 'Nov.',
			'12' => 'Dez.',
		},
	};
	unless(defined $months->{$lang}){
		$self->msg(0, "signatures for language '$lang' are not configured yet.",
			'warning');
		return;
	}
	$months = $months->{$lang};
	my $timestamp_unix = $self->convert_time_iso2unix($timestamp);
	return unless defined $timestamp_unix;
	my $allow_undef = 1;
	$timestamp_unix = $self->convert_time_unix2unix_tz(
		$timestamp_unix, 'to', $tz, $allow_undef);
	unless(defined $timestamp_unix){
		# if timezone is unrecognised, it's still better to use the UTC time
		$tz = 'UTC';
		$timestamp_unix = $self->convert_time_iso2unix($timestamp);
	}
	my $sig = strftime("%H:%M, %d %m %Y", gmtime($timestamp_unix));
	$sig =~ s/, .. \K(..)/$months->{$1}/e; # replace month name
	if($lang eq 'de'){
		$sig =~ s/, ..\K /. /; # add trailing dot behind month of day
	}
	$sig =~ s/, \K0//; # delete leading zero of day of month
	$sig .= " ($tz)";
	return $sig;
}

sub convert_time_iso2unix{
	my $self = shift;
	my $timestamp = shift;  # e.g. '2001-12-31T12:34:56Z'
	return unless defined $timestamp;
	if($timestamp =~/^
		(?<year>[0-9]{4})-?
		(?<month>[0-9]{2})-?
		(?<mday>[0-9]{2})[\sT]?
		(?<hour>[0-9]{2}):?
		(?<min>[0-9]{2}):?
		(?<sec>[0-9]{2})
		Z?
		$/x
	){
		my $timestamp_unix = timegm_modern(
			$+{'sec'}, $+{'min'}, $+{'hour'}, $+{'mday'}, $+{'month'} - 1, $+{'year'});
		return $timestamp_unix;
	}
	return;
}

sub convert_time_log_string2iso{
	my $self = shift;
	my $string = shift;  # e.g. Thu Dec 01 07:39:41 CET 2016
	my $date = $string;
	my %month = (
		'Jan' => '01',
		'Feb' => '02',
		'Mar' => '03',
		'Apr' => '04',
		'May' => '05',
		'Jun' => '06',
		'Jul' => '07',
		'Aug' => '08',
		'Sep' => '09',
		'Oct' => '10',
		'Nov' => '11',
		'Dec' => '12',
	);
	if(defined($string)){
		if($string =~ /[A-Z][a-z]{2}\s
			(?<month>[A-Z][a-z]{2})\s
			(?<day>[0-9]{2})\s
			(?<time>[0-9]{2}:[0-9]{2}:[0-9]{2})\s
			(?<tz>[A-Z]{3,4})\s
			(?<year>[0-9]{4})/x
		){
			$date = "$+{'year'}-" . $month{$+{'month'}} . "-$+{'day'}T$+{'time'}";
			# TODO: use tz to normalize time
		}else{
			$self->msg(1, "could not read date from string '$string'", 'warning');
		}
	}else{
		$self->msg(0, 'string to convert is undefined', 'warning');
	}
	return $date;
}

sub convert_time_signature2iso{
	my $self = shift;
	my $sig = shift;
	my $date = $sig;
	# e.g. 08:34, 23 March 2021 (CET)
	# e.g. 08:34, 23. Dez. 2021 (CET)
	my %months = (
		'January'  => 1,
		'February' => 2,
		'March'    => 3,
		'April'    => 4,
		'May'      => 5,
		'June'     => 6,
		'July'     => 7,
		'August'   => 8,
		'Sepember' => 9,
		'October'  => 10,
		'November' => 11,
		'December' => 12,
		'Jan' => 1,
		'Feb' => 2,
		"Mär" => 3,
		"M\xe4r" => 3,
		'Apr' => 4,
		'Mai' => 5,
		'Jun' => 6,
		'Jul' => 7,
		'Aug' => 8,
		'Sep' => 9,
		'Okt' => 10,
		'Nov' => 11,
		'Dez' => 12,
	);
	if(defined($sig)){
		if($sig =~ /(?<pre_sig>.*)(?<hour>[0-9]{2}):(?<min>[0-9]{2}),\x20
			(?<mday>[1-9]|[123][0-9])\.?\x20(?<mon>[a-zA-Zä\xe4]{3,})\.?\x20
			(?<year>20[0-9]{2})\x20\((?<tz>[A-Z]{3,4})\)/gx
		){
			my $month = $months{$+{'mon'}};
			unless(defined $month){
				$self->msg(1, 'could not read date from signature', 'warning');
				return $date;
			}
			my $unix_timestamp = $self->convert_time_unix2unix_tz(
				timegm_modern(0, $+{'min'}, $+{'hour'}, $+{'mday'}, $month - 1, $+{'year'}),
				'from', $+{'tz'}
			);
			$date = $self->get_time_iso($unix_timestamp);
		}else{
			$self->msg(1, 'could not read date from signature', 'warning')
		}
	}else{
		$self->msg(0, 'signature to convert is undefined', 'warning')
	}
	return $date;
}

# just an alias
sub convert_time_unix2iso{
	my $self = shift;
	return $self->get_time_iso(@_);
}

sub convert_time_unix2unix_tz{
	my $self = shift;
	my $timestamp_unix = shift;
	my $direction = shift // 'from';
	my $timezone = shift;
	my $undef_if_tz_unrecognized = shift;
	return unless defined $timestamp_unix;
	$direction = $direction eq 'from' ? -1 : 1;
	if(defined $timezone){
		if($timezone eq 'CEST'){
			$timestamp_unix += $direction * 2 * 60 * 60;
		}elsif($timezone eq 'CET'){
			$timestamp_unix += $direction * 1 * 60 * 60;
		}elsif($timezone eq 'UTC'){
			# ok
		}else{
			# TODO: use also other possible tz
			if($undef_if_tz_unrecognized){
				$self->msg(0, 'did not recognize timezone', 'warning');
				return;
			}else{
				$self->msg(0, 'did not recognize timezone, treating as UTC', 'warning');
			}
		}
	}
	return $timestamp_unix;
}

sub convert_ns{
	my $self = shift;
	my $ns = shift;
	my $direction = shift // 'auto';
	return unless defined $ns;
	my $converted;
	unless(defined $self->{'namespaces'}){
		# get all namespaces (ids and names)
		my $res = $self->api_simple({
			'action' => 'query',
			'meta'   => 'siteinfo',
			'siprop' => 'namespaces|namespacealiases',
		});
		$self->{'namespaces'} = {
			map {
				$_ => $res->{query}{namespaces}{$_}{'*'}
			} keys %{$res->{query}{namespaces}}
		};
		$self->{'namespaces_canonical'} = {
			map {
				$_ => $res->{query}{namespaces}{$_}{'canonical'}
			} keys %{$res->{query}{namespaces}}
		};
		unless(defined $self->{'namespaces_canonical'}{0}){
			delete $self->{'namespaces_canonical'}{0};
		}
		$self->{'namespacealiases'} = {
			map {
				($_->{'*'} => $_->{'id'});
			} @{$res->{'query'}{'namespacealiases'}}
		};
	}
	if($direction eq 'id2name' || $direction eq 'auto' && $ns =~ /^[0-9]+$/){
		$converted = $self->{'namespaces'}{$ns};
	}elsif($direction eq 'id2names'){
		$converted = [$self->{'namespaces'}{$ns}];
		while(my ($name, $id) = each(%{$self->{'namespacealiases'}})){
			push(@$converted, $name) if $id == $ns;
		}
	}elsif($direction eq 'id2canonical'){
		$converted = $self->{'namespaces_canonical'}{$ns};
	}elsif($direction eq 'name2id' || $direction eq 'auto'){
		my %rev_ns = reverse(%{$self->{'namespaces'}});
		my %rev_ns_can = reverse(%{$self->{'namespaces_canonical'}});
		$converted = $rev_ns{$ns} // $self->{'namespacealiases'}{$ns} // $rev_ns_can{$ns};
	}else{
		$self->msg(0, "param \$direction '$direction' is not recognized.", 'warning');
	}
	$converted = 0 unless defined $converted;
	return $converted;
}

sub convert_ns_title2fulltitle{
	my $self = shift;
	my $ns = shift;
	my $page_title = shift;
	my $ns_name = $self->convert_ns($ns, 'id2name');
	my $full_page_title = $page_title;
	if($ns != '0'){
		$full_page_title = $ns_name . ':' . $full_page_title;
	}
	return $full_page_title;
}

sub createMWBot{
	my $self = shift;
	$self->msg(2, "creating bot");
	my $options = {
		#do_sul      => ($self->{'host'} =~ /\.wikipedia\.org$/ ? 1 : 0),
		protocol    => 'https',
		# maxlag      => 5, # default value = 5 (seconds)
		host        => $self->{'host'},
		path        => $self->{'rel_url_path'},
		operator    => $self->{'operator'},
		debug       => 1,
	};
	if($self->{'assert_bot'}){
		$options->{'assert'} = 'bot';
	}
	if($self->{'mw_agent'}){
		$options->{'agent'} = $self->{'mw_agent'}; # user agent
	}
	$self->{'mw_bot'} = MediaWiki::Bot->new($options);
	unless(defined $self->{'mw_bot'}){
		$self->msg(0, 'could not create bot', 'error');
		exit 1;
	}{
		# 2021-04-11 seth: bugfix of https://rt.cpan.org/Public/Bug/Display.html?id=120643
		$self->{'mw_bot'}->{'api'}->{'ua'}->ssl_opts(
			'SSL_ocsp_mode' => SSL_OCSP_NO_STAPLE);
		$self->msg(2, 'created instance of MediaWiki::Bot.');
	}
	return defined($self->{'mw_bot'});
}

sub create_notification{
	my $self     = shift;
	my $ntf_type = shift;
	my $page     = shift;
	my $revid    = shift;
	my $matches  = shift;
	my $notice_page = 'user:CamelBot/notice-' . $ntf_type;
	my $notice = $self->get_text($notice_page);
	return unless defined $notice;
	my $title = $self->title2url_part($page);
	my $wp_path = '//' . $self->{'host'} . '/' . $self->{'rel_url_path'}
		. '/index.php';
	# replace variables in notice template
	if(defined $wp_path && defined $title && defined $revid){
		my $complete_url = $wp_path . '?title=' . $title . '&diff=prev&oldid=' . $revid;
		$notice =~ s/\$diff\b/$complete_url/g;
	}
	$notice =~ s/\$article\b/$page/g;
	if(defined $matches){
		my $uniq = sub{
			my %seen;
			grep !$seen{$_}++, @_;
		};
		my $matches_formatted = [$uniq->(@$matches)];
		map {$_ = "* <code><nowiki>$_</nowiki></code>"} @$matches_formatted;
		$matches_formatted = join(",\n", @$matches_formatted);
		$notice =~ s/\$matches\b/$matches_formatted/sg;
	}
	$notice =~ s/\$signature\b.*/-- ~~~~/sg;
	return $notice;
}

sub create_signature{
	my $self = shift;
	my $user = shift;
	my $iso_timestamp = shift;
	my $lang = shift // 'en';
	unless(defined $user){
		$self->msg(0, 'user undefined', 'warning');
		return;
	}
	unless(defined $iso_timestamp){
		$self->msg(0, 'iso_timestamp undefined', 'warning');
		return;
	}
	my $sig_timestamp = $self->convert_time_iso2signature_string($iso_timestamp, $lang);
	unless(defined $sig_timestamp){
		$self->msg(0, 'iso_timestamp not readable', 'warning');
		return;
	}
	return "-- [[user:$user|$user]] ([[user talk:$user|talk]]) $sig_timestamp";
}

sub _db_init{
	my $self    = shift;
	my $proj    = shift // $self->{'wm_proj'};
	if(defined $self->{'db'}{'handle'}
		&& (
			(!defined($self->{'db'}{'proj'}) && !defined($proj))
			|| (defined($self->{'db'}{'proj'}) && defined($proj)
				&& $self->{'db'}{'proj'} eq $proj)
		)
	){
		# don't re-read db_file or reconnect, if still connected
		return 2;
	}else{
		unless(defined $proj){
			$self->msg(2, '$proj is not defined');
		}
		# check db conn file
		unless(-e $self->{'db'}{'file'}){
			$self->msg(1,
				" db conn file: '$self->{'db'}{'file'}' does not seem to exist.",
				'warning');
			if(substr($self->{'db'}{'file'}, 0, 1) ne '/'){
				$self->{'db'}{'file'} = $ENV{'HOME'} . '/' . $self->{'db'}{'file'};
				if(-e $self->{'db'}{'file'}){
					$self->msg(1, " using db conn file: '$self->{'db'}{'file'}'.");
				}else{
					# warning will appear soon
					# $self->msg(0, " db conn file does not seem to exist.", 'error');
				}
			}
		}
		# db connection parameters
		if(defined $proj){
			$self->{'db'}{'host'} = $proj . '.analytics.db.svc.eqiad.wmflabs'; # .labsdb
			#$self->{'db'}{'host'} = 's1.labsdb';
			$self->{'db'}{'name'} = $proj . '_p';
		}
		$self->{'db'}{'port'} = '3306';
		$self->{'db'}{'user'} = '';
		$self->{'db'}{'pw'} = '';
		my $opened = open(my $INFILE, '<', $self->{'db'}{'file'});
		unless($opened){
			$self->msg(0, "could not read db conn file: $!", 'error');
			return 0;
		}
		$self->_db_parse_init($INFILE);
		close($INFILE);
		$self->msg(2, 'connect to db: ' . Dumper($self->{'db'}));
		$self->{'db'}{'handle'} = DBI->connect( # db connect
			'DBI:mysql:' .
				'database=' . $self->{'db'}{'name'} .
				';host=' . $self->{'db'}{'host'} .
				';port=' . $self->{'db'}{'port'} .
				';mysql_client_found_rows=0',
			$self->{'db'}{'user'},
			$self->{'db'}{'pw'},
			{	'RaiseError' => 1,
				'AutoCommit' => 1,
				'mysql_enable_utf8' => 1
			}
		) or $self->msg(0, 'cannot connect to db: '.$DBI::errstr);
		$self->{'db'}{'proj'} = $proj if defined $proj;
	}
	return 1;
}

sub _db_parse_init{
	my $self = shift;
	my $fh   = shift;
	while(my $line = <$fh>){
		$self->{'db'}{'user'} = $1 if $line =~ /^\s*user='(.*)'/;
		$self->{'db'}{'pw'}   = $1 if $line =~ /^\s*password='(.*)'/;
		$self->{'db'}{'port'} = $1 if $line =~ /^\s*port='?(.*?)'?\s*(?:#.*)?\n/;
		$self->{'db'}{'host'} = $1 if $line =~ /^\s*host='?(.*?)'?\s*(?:#.*)?\n/;
		$self->{'db'}{'name'} = $1 if $line =~ /^\s*name='?(.*?)'?\s*(?:#.*)?\n/;
	}
	return 1;
}

sub _db_query{
	my $self  = shift;
	my $query = shift;
	my $return_header = shift // 1;
	unless(defined $self->{'db'}{'handle'} || $self->{'offline'}){
		$self->msg(0, 'no database handle', 'error');
		return 0;
	}
	my $names;
	my $rows_ref;
	if($self->{'offline'}){
		$rows_ref = $self->{'offline_db_query_rows'};
		$names = $self->{'offline_db_query_names'};
	}else{
		my $sth = $self->{'db'}{'handle'}->prepare($query)
			or $self->msg(0, $self->{'db'}{'handle'}->errstr . " query = '$query'");
		return 0 unless $sth;
		$sth->execute;
		$names = $sth->{'NAME'}; # or NAME_lc if needed
		$rows_ref = $sth->fetchall_arrayref();
		#$sth->finish;
	}
	unshift @$rows_ref, $names if $return_header;
	return $rows_ref;
}

sub _db_simplequery{
	my $self          = shift;
	my $db_query      = shift;
	my $return_header = shift // 1;
	my $proj          = shift // $self->{'wm_proj'};
	$self->msg(3, 'proj = ' . (defined $proj ? $proj : 'undefined'));
	unless($self->{'offline'}){
		$self->_db_init($proj) or return;
	}
	$self->msg(4, Dumper($db_query));
	if(ref(\$db_query) ne 'SCALAR'){
		#$db_query->[0] =~ s/\s+/ /g;
		$db_query->[0] =~ s/^\s+|\s+\z//g;
		$db_query = sprintf $db_query->[0], map {
			(defined($_) && /^-?(?:\d*\.\d+|\d+\.?)\z/) ? $_ :
			# (not defined $_) ? 'NULL' is not needed, will be done by quote, so ensure,
			# using '%s' even on numbers)
			$self->{'db'}{'handle'}->quote($_);
		} @$db_query[1..@$db_query-1];
	}
	$self->msg(4, $db_query);
	my $db_table = $self->_db_query($db_query, $return_header);
	return $db_table;
}

sub db_fetch_completed_review_requests{
	my $self = shift;
	my $return_header = 1;
	my $query;
	if($self->check_mw_version('>=1.43.0')){
		$query = '
			SELECT
				`Flagged`.`page_title`,
				`Flagged`.`page_namespace`,
				`fp_reviewed`,
				`fp_pending_since`
			FROM
				`pagelinks`
				LEFT JOIN `linktarget` ON `lt_id` = `pl_target_id`,
				`page` AS `LinkedFrom`,
				`page` AS `Flagged`
				LEFT JOIN `flaggedpages` ON `fp_page_id` = `Flagged`.`page_id`
			WHERE `Flagged`.`page_namespace` = `lt_namespace`
				AND `LinkedFrom`.`page_title` = \'' . $self->{'rev_req'}{'page_title'} . '\'
				AND `LinkedFrom`.`page_namespace` = ' . $self->{'rev_req'}{'ns'} . '
				AND `pl_from` = `LinkedFrom`.`page_id`
				AND `lt_namespace` IN (0, 6, 10, 14, 828)
				AND `lt_title` = `Flagged`.`page_title`';
	}else{
		$query = '
			SELECT
				`Flagged`.`page_title`,
				`Flagged`.`page_namespace`,
				`fp_reviewed`,
				`fp_pending_since`
			FROM
				`pagelinks`,
				`page` AS `LinkedFrom`,
				`page` AS `Flagged`
				LEFT JOIN `flaggedpages` ON `fp_page_id` = `Flagged`.`page_id`
			WHERE `Flagged`.`page_namespace` = `pl_namespace`
				AND `LinkedFrom`.`page_title` = \'' . $self->{'rev_req'}{'page_title'} . '\'
				AND `LinkedFrom`.`page_namespace` = ' . $self->{'rev_req'}{'ns'} . '
				AND `pl_from` = `LinkedFrom`.`page_id`
				AND `pl_namespace` IN (0, 6, 10, 14, 828)
				AND `pl_title` = `Flagged`.`page_title`';
	}
	my $db_table = $self->_db_simplequery([$query], !$return_header);
	my $result = {};
	map {
		my $page_title = $_->[0];
		utf8::decode($page_title);
		$page_title = $self->normalize_page_title(
			$self->convert_ns_title2fulltitle($_->[1], $page_title));
		$result->{$page_title} =
			!(defined $_->[2]) ? 'never_reviewed' :
			$_->[2] == 1 ? 'reviewed' :
			$self->convert_time_iso_sep($_->[3]); # pending: use date of last review
	} @$db_table;
	$self->msg(3, Dumper($result));
	return $result;
}

#sub db_fetch_recentchanges_detailed{
#	my $self = shift;
#	my $last_seconds = shift // 60*60*12; # default = last 12 hours
#	my $timestamp = $self->get_time_iso($time()-$last_seconds);
#	# check whether token exists:
#	my $db_table = db_simplequery([
#			'SELECT `rc_timestamp`, `rc_user_text`, `rc_namespace`, `rc_title`,
#				`rc_comment`, `rc_minor`, `rc_bot`, `rc_new`, `rc_cur_id`,
#				`rc_this_oldid`, `rc_last_oldid`, `rc_type`, `rc_source`, `rc_patrolled`,
#				`rc_old_len`, `rc_new_len`, `rc_deleted`
#			FROM `recentchanges` WHERE `rc_timestamp`>%s', $timestamp],
#		!$_return_header);
#	if(defined $db_table->[0]){
#		my $email = lc $db_table->[0][0];
#		my $user_id = $db_table->[0][1];
#	}
#}

sub db_fetch_externallinks{
	my $self    = shift;
	my $from_id = shift // 1;
	my $to_id   = shift;
	if($from_id !~ /^[0-9]+\z/){
		$self->msg(0, 'param $from_id is not valid, see source code', 'warning');
		return [];
	}elsif(defined $to_id and $to_id !~ /^[0-9]+\z/){
		$self->msg(0, 'param $to_id is not valid, see source code', 'warning');
		return [];
	}
	my $return_header = 1;
	my $db_table;
	my $sql_max_id = defined $to_id ? 'AND `el_id`<=%s' : '';
	my $query;
	if($self->check_mw_version('>=1.40.0')){
		$query = ['
			SELECT
				`el_id`,
				`el_from`,
				`el_to_domain_index`,
				`el_to_path`,
				`p`.`page_title` AS `page`
			FROM `externallinks`
			LEFT JOIN `page` AS `p`
				ON (`p`.`page_id`=`externallinks`.`el_from`)
			WHERE `el_id`>=%s ' . $sql_max_id . '
			ORDER BY `el_id`
			LIMIT 1000
	', $from_id, $to_id]
	}else{
		$query = ['
			SELECT `el_id`, `el_from`, `el_to`, `p`.`page_title` AS `page`
			FROM `externallinks`
			LEFT JOIN `page` AS `p`
				ON (`p`.`page_id`=`externallinks`.`el_from`)
			WHERE `el_id`>=%s ' . $sql_max_id . '
			ORDER BY `el_id`
			LIMIT 1000
	', $from_id, $to_id]
	}
	$db_table = $self->_db_simplequery($query, $return_header);
	my $ext_links = $self->db_table2array_of_hash($db_table);
	if($self->check_mw_version('>=1.40.0')){
		for my $el(@$ext_links){
			$el->{'el_to_domain_index'} =~ /^(?<protocol>[a-z]+:(?:\/\/)?)(?<domain>.*?)\.(?<port>:[0-9]+|)/;
			$el->{'el_to'} = $+{'protocoll'} . $+{'port'};
			my $domain = $+{'domain'};
			$el->{'el_to'} .= join '.', reverse split /\./, $domain;
			$el->{'el_to'} .= $el->{'el_to_path'};
		}
	}
	return $ext_links;
}

sub db_fetch_pages_of_cats_of_the_dead{
	my $self    = shift;
	my $since   = shift;
	if(defined $since and
		$since !~ /^[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}\z/)
	{
		$self->msg(0, 'param $since is not valid, see source code', 'warning');
		return [];
	}
	my $return_header = 1;
	my $sql_limit = 10_000;
	my $sql_query = '
		SELECT
			`page`.`page_title` AS `page`,
			`page`.`page_namespace` AS `namespace`,
			`cl_to` AS `category`,
			`cl_timestamp` AS `timestamp`
		FROM `categorylinks`
		LEFT JOIN `page`
			ON `page`.`page_id`=`cl_from`
		WHERE `cl_to` LIKE \'Gestorben_2%%\'
			AND `page_namespace` = \'0\'
			AND `cl_timestamp` >= '
				. (defined $since ? '%s' : 'date_sub(now(), interval 2 day)') .
		' LIMIT ' . $sql_limit;
	my $db_table = defined $since ?
	  $self->_db_simplequery([$sql_query, $since], $return_header)
	: $self->_db_simplequery([$sql_query], $return_header);
	if(@$db_table > $sql_limit * .75){
		$self->msg(1, 'found ' . scalar(@$db_table) . " entries (limit = $sql_limit)");
	}
	for my $row(@$db_table[1..(@$db_table-1)]){
		for my $i((0, 1, 2)){ # page, namespace, category
			utf8::decode($row->[$i]) if defined $row->[$i];
		}
	}
	return $self->db_table2array_of_hash($db_table);
}

sub db_fetch_recentchanges{
	my $self = shift;
	# time format 'yyyy-mm-dd hh:mm:ss' will be converted to yyyymmddhhmmss
	my $timestamp_begin = shift;
	my $timestamp_end   = shift;
	unless(defined $timestamp_begin){
		# default = last 12 hours
		$timestamp_begin = $self->get_time_iso(time() - 60 * 60 * 12);
	}
	$timestamp_begin = $self->convert_time_iso_sep($timestamp_begin);
	$timestamp_end = $self->convert_time_iso_sep($timestamp_end);
	my $sql_max_ts = defined $timestamp_end ? 'AND `rc_timestamp`<=%s' : '';
	my $return_header = 1;
	my $query;
	if($self->check_mw_version('>=1.31.0')){
		$query = ['
				SELECT
					`rc_timestamp` AS `timestamp`,
					`actor_name` AS `user`,
					`rc_namespace` AS `ns_id`,
					`rc_title` AS `page`,
					`comment_text` AS `summary`,
					`rc_this_oldid`,
					`rc_last_oldid`,
					`rc_old_len`,
					`rc_new_len`,
					`rc_log_type`,
					`log_params` AS `move_target`
				FROM `recentchanges`
				LEFT JOIN actor
				ON recentchanges.rc_actor = actor.actor_id
				LEFT JOIN comment
				ON recentchanges.rc_comment_id = comment.comment_id
				LEFT JOIN logging
				ON recentchanges.rc_logid = logging.log_id
				WHERE `rc_timestamp`>=%s
					' . $sql_max_ts . '
					AND (`rc_log_type` IS NULL OR `rc_log_type` = "delete" OR `rc_log_type` = "move")
				ORDER BY `rc_timestamp`
		', $timestamp_begin];
	}elsif($self->check_mw_version('<1.30.0')){
		$query = ['
				SELECT
					`rc_timestamp` AS `timestamp`,
					`rc_user_text` AS `user`,
					`rc_namespace` AS `ns_id`,
					`rc_title` AS `page`,
					`rc_comment` AS `summary`,
					`rc_this_oldid`,
					`rc_last_oldid`,
					`rc_old_len`,
					`rc_new_len`,
					`rc_log_type`,
					`log_params` AS `move_target`
				FROM `recentchanges`
				LEFT JOIN logging
				ON recentchanges.rc_logid = logging.log_id
				WHERE `rc_timestamp`>=%s
					' . $sql_max_ts . '
					AND (`rc_log_type` IS NULL OR `rc_log_type` = "delete" OR `rc_log_type` = "move")
				ORDER BY `rc_timestamp`
		', $timestamp_begin];
	}else{
		$self->msg(0, 'sorry, this mediawiki-version is not supported yet.', 'error');
		return;
	}
	push @$query, $timestamp_end if defined $timestamp_end;
	my $db_table = $self->_db_simplequery($query, $return_header);
	for my $row(@$db_table[1..(@$db_table-1)]){
		# time format yyyymmddhhmmss will be converted to 'yyyy-mm-dd hh:mm:ss'
		$row->[0] = $self->convert_time_iso_sep($row->[0]);
		for my $i((1, 3, 4)){ # use, page, summary
			utf8::decode($row->[$i]) if defined $row->[$i];
		}
		if(defined $row->[10]){
			$row->[10] = PHP::Serialization::unserialize($row->[10]);
			if(defined $row->[9] && $row->[9] eq 'move'){ # move_target
				$row->[10] = $row->[10]{'4::target'}; # e.g. "Benutzer:Lustiger seth/Whatever"
				utf8::decode($row->[10]);
			}else{
				$row->[10] = undef;
			}
		}
	}
	return $self->db_table2array_of_hash($db_table);
}

sub db_fetch_sbl_log{
	my $self   = shift;
	my $proj   = shift // $self->{'wm_proj'};
	my $url_re = shift // '.';
	# mariadb does not know perl's /(?^)/ pattern
	$url_re =~ s/\(\?\K\^//g;
	my $return_header = 1;
	my $db_table;
	my $rows = [
		'log_id',
		'log_timestamp',
		'log_namespace',
		'log_title',
		'comment_text',
		'log_params',
		'actor_name',
	];
	$rows = join ', ', map {$_ = '`'.$_.'`'} @$rows; ## no critic (MutatingListFunctions)
	$db_table = $self->_db_simplequery(['
			SELECT '.$rows.'
			FROM `logging`
			LEFT JOIN `actor`
				ON `logging`.`log_actor` = `actor`.`actor_id`
			LEFT JOIN `comment` AS `c`
				ON (`c`.`comment_id`=`logging`.`log_comment_id`)
			WHERE `log_type` = \'spamblacklist\'
				AND log_params regexp %s
			ORDER BY `log_timestamp`
	', $url_re], !$return_header, $proj);
	for my $row(@$db_table){
		for my $i((2, 3, 4, 6)){ # namespace, title, comment_text, actor_name
			utf8::decode($row->[$i]) if defined $row->[$i];
		}
	}
	return $db_table;
}

sub db_table2array_of_hash{
	my $self      = shift;
	my $db_table  = shift;
	my $db_header = shift @$db_table;
	my $db_hashtable = [];
	for my $r(@$db_table){
		push @$db_hashtable, {
			map {
				($db_header->[$_] => $r->[$_])
			} 0..@$db_header-1
		};
	}
	return $db_hashtable;
}

sub decode_html_entities{
	my $self = shift;
	my $text_ref = shift;
	$$text_ref =~ s/&gt;/>/g;
	$$text_ref =~ s/&lt;/</g;
	$$text_ref =~ s/&amp;/&/g;
	return 1;
}

sub decompose_url{
	my $self = shift;
	my $url = shift;
	return unless defined $url;
	$url =~ /^
		(?<protocol>[a-z]+:|)\/\/
		(?<credentials>[a-zA-Z0-9_-]+(?::.*|)\@|)
		(?<host>[^\@\/:\?\x23]+)
		(?<port>:[0-9]+|)
		# cope with ugly html encoding of '#'
		(?<path>\/ (?:&\x23(?:x?(?!23;)[0-9a-fA-F]+|&(?!35)[0-9]+);|[^?\x23&]|&(?!\x23))*|)
		(?<query>\?(?:&\x23(?:x?(?!23;)[0-9a-fA-F]+|&(?!35)[0-9]+);|[^\x23&]|&(?!\x23))*|)
		(?<fragment>(?:\x23|&\x{23}35;|&\x{23}x23;).*|)\z/x;
	my $url_struct = {%+};
	unless(exists $url_struct->{'query'}){
		$self->msg(0, "could not decompose url '$url'", 'warning');
		return;
	}
	$url_struct->{'query'} = [split /&/, $url_struct->{'query'}];
	if(@{$url_struct->{'query'}} > 0){
		$url_struct->{'query'}[0] =~ s/^\?//;
	}
	return $url_struct;
}

sub delete_empty_ref{
	my $self = shift;
	my $text_ref = shift;
	unless(defined $text_ref && ref $text_ref eq 'SCALAR'){
		$self->msg(0, 'first param must be a ref to string', 'error');
		return;
	}
	if($$text_ref =~ /
			<(?i:ref)(?:>|\sname)
			|EinwohnerOrtQuelle
			|EinwohnerRef
	/x){
		return 0;
	}
	my $re_del = qr/^=(=+)\s*(?:Belege|Einzelnachweise|Quellen)\s*\1=[\s\n]*\n
		(?:<references(?:\x20?)>[\s\n\r]*<\/references(?:\x20?)>
			|<references(?:\x20?\/)?>
		)\s*/mx;
	my $use_eval_in_repl = 1;
	my $print_non_changes = 1;
	my $text_tmp = $$text_ref;
	$self->test_and_replace(
		\$text_tmp, $re_del, '', !$use_eval_in_repl, $print_non_changes);
	my $parsed = $self->{'offline'} ? '' : $self->parse_wikitext($text_tmp);
	if($parsed !~ /class="error mw-ext-cite-error"/){
		my $repl = $self->test_and_replace(
			$text_ref, $re_del, '', !$use_eval_in_repl, $print_non_changes);
		return scalar(@$repl);
	}
	$self->msg(1, '<references /> not deleted, because it would lead ' .
		'to an error.', 'warning');
	return;
}

sub delete_from_maintenance_table{
	my $self = shift;
	my $mt_type = shift;
	my $table = shift;
	my $num = 0;
	if(exists $self->{'maintenance'}{'delete_after_days'}{$mt_type}){
		$num += $self->delete_old_from_maintenance_table($table->{'body'},
			$self->{'maintenance'}{'delete_after_days'}{$mt_type});
	}
	if($self->{'maintenance'}{'delete_if_deleted'}{$mt_type}){
		$num += $self->delete_deleted_from_maintenance_table($table->{'body'});
	}
	if($self->{'maintenance'}{'check_entries'}{$mt_type}){
		$num += $self->delete_resolved_from_maintenance_table(
			$table->{'body'}, $mt_type);
	}
	return $num;
}

sub delete_marked_pages{
	my $self = shift;
	my $summary = 'deleting marked page';
	utf8::decode($self->{'delete_cat'});
	my @pages = grep {!/Template:L.schen/}
		$self->{'mw_bot'}->get_pages_in_category($self->{'delete_cat'});
	$self->msg(1, 'found '.(@pages+0).' pages.');
	$self->msg(2, 'adding manually set pages');
	push @pages, ();
	for my $page(@pages){
		if($self->{'verbosity'} >= 3 || $self->{'simulation'}){
			$self->msg(1, " $page");
		}
		$self->delete_wiki_page($page, $summary);
	}
	return 1;
}

sub delete_old_from_maintenance_table{
	my $self = shift;
	my $table_body = shift;
	my $days_to_keep = shift;
	my $now = shift // time; # seconds
	unless(defined $table_body){
		$self->msg(0, '$table_body undefined', 'error');
		return;
	}
	unless(defined $days_to_keep){
		$self->msg(0, '$days_to_keep undefined', 'warning');
		return 0;
	}
	my $num_deleted = 0;
	my $del_before_date = $self->get_date_iso($now - (60 * 60 * 24) * $days_to_keep);
	@$table_body = (
		grep {
			my $keep_in_list = $_->[-1] ge $del_before_date;
			if(!$keep_in_list){
				++$num_deleted;
				$self->msg(4, "deleted $_->[0] because of old date $_->[-1].");
			}
			$keep_in_list; # return value for grep
		} @$table_body
	);
	return $num_deleted;
}

sub delete_deleted_from_maintenance_table{
	my $self = shift;
	my $table_body = shift;
	unless(defined $table_body){
		$self->msg(0, '$table_body undefined', 'error');
		return;
	}
	if(ref $table_body ne 'ARRAY'){
		$self->msg(0, '$table_body must be an array ref', 'error');
		return;
	}
	my $num_deleted = 0;
	@$table_body = (
		grep {
			my $keep_in_list = 0;
			if($_->[0] =~ /^\[\[:?+(.*)\]\]/){ # (?:\s\[[^\s]+\sedit\])?$
				my $page_from_list = $1;
				$self->msg(4, "check '$page_from_list'");
				my @hist = $self->{'offline'} ? @{$self->{'offline_mw_bot_get_history'}}
					: $self->get_history($page_from_list, {'rvlimit' => 1});
				$keep_in_list = @hist > 0;
				unless($keep_in_list){
					++$num_deleted;
					$self->msg(4, "page is deleted");
				}
			}
			$keep_in_list; # return value for grep
		} @$table_body
	);
	return $num_deleted;
}

sub delete_resolved_from_maintenance_table{
	my $self = shift;
	my $table_body = shift;
	my $mt_type = shift;
	unless(defined $table_body){
		$self->msg(0, '$table_body undefined', 'error');
		return;
	}
	if(ref $table_body ne 'ARRAY'){
		$self->msg(0, '$table_body must be an array ref', 'error');
		return;
	}
	unless(defined $mt_type){
		$self->msg(0, '$mt_type undefined', 'error');
		return;
	}
	unless(defined $self->{'maintenance'}{'text_re'}{$mt_type}){
		$self->msg(0, "maintenance regexp for type '$mt_type' is undefined", 'error');
		return;
	}
	my $num_deleted = 0;
	@$table_body = (
		grep {
			my $keep_in_list = 0;
			if($_->[0] =~ /^\[\[:?+(.*)\]\]/){ # (?:\s\[[^\s]+\sedit\])?$
				my $page_from_list = $1;
				$self->msg(4, "check '$page_from_list'");
				# may be deleted meanwhile
				my $page_content = $self->{'offline'} ? $self->{'offline_mw_bot_get_text'}
					: $self->get_text($page_from_list) // '';
				$keep_in_list =
					$page_content =~ /$self->{'maintenance'}{'text_re'}{$mt_type}/p;
				if($keep_in_list){
					$_->[1] = '<pre>' . ${^MATCH} . '</pre>';
				}else{
					++$num_deleted;
					$self->msg(4, "regexp did not match");
				}
			}
			$keep_in_list; # return value for grep
		} @$table_body
	);
	return $num_deleted;
}

sub delete_tracking_params_form_url{
	my $self = shift;
	my $url  = shift;
	unless(defined $url){
		$self->msg(0, 'url undefined', 'error');
		return;
	}
	if(length($url) <= 1){
		return $url;
	}
	my $url_struct = $self->decompose_url($url);
	unless(defined $url_struct){
		$self->msg(0, "failed: decompose_url($url)", 'warning');
		return $url;
	}
	if($url_struct->{'host'} =~ m~^(?:web\.|)archive\.[a-z]+\z~){
		$self->msg(3, 'skip archive url');
		return $url;
	}
	if($url_struct->{'host'} =~ m~^(?:beispiel\.com|example\.(?:com|org))\z~){
		$self->msg(2, 'skip example url');
		return $url;
	}
	# see discussion at
	# https://de.wikipedia.org/w/index.php?title=Wikipedia:Technik/Werkstatt&oldid=235007467#URL-Zusatz_zur_farblichen_Hervorhebung
	# #:~:text= (text fragment links)
	$url_struct->{'fragment'} =~ s/^(?:#.+?\K|#):(?:~|%7[eE]):text=.+\z//;
	if(defined($url_struct->{'query'}) && @{$url_struct->{'query'}} > 0){
		unless(defined $self->{'re_url_tracking_filter'}){
			$self->init_re_url_tracking_filter();
		}
		my $filt = $self->{'re_url_tracking_filter'};
		my $wl = $filt->{'whitelist'};
		$url_struct->{'query'} = [grep {
			my $param = $_; # e.g. foo=bar
			$param =~ /^([^=]*)/;
			my $param_name = $1; # foo
			my $keep = 0;
			# check whitelist
			while(my ($domain_part, $patterns) = each %{$wl->{'domain-specific'}}){
				if(!$keep && $url_struct->{'host'} =~ /(?:^|\.)$domain_part\z/){
					if(exists($patterns->{'key'}) && exists($patterns->{'key'}{$param_name})
						|| $self->match_any($param, $patterns->{'keyval'})
					){
						$keep = 1;
					}
				}
			}
			# check blacklist
			if(!$keep){
				$keep = !exists($filt->{'key'}{$param_name});
				$keep &&= 0 == grep {$param =~ /$_/} @{$filt->{'keyval'}};
				while(my ($domain_part, $patterns) = each %{$filt->{'domain-specific'}}){
					if($keep && $url_struct->{'host'} =~ /(?:^|\.)$domain_part\z/){
						if(exists($patterns->{'key'}) && exists($patterns->{'key'}{$param_name})
							|| $self->match_any($param, $patterns->{'keyval'})
						){
							$keep = 0;
						}
					}
				}
			}
			$keep;
		} @{$url_struct->{'query'}}];
	}
	$url_struct->{'fragment'} =~ s/^#\.X.*//;
	$url = $self->compose_url($url_struct);
	return $url;
}

sub delete_wiki_page{
	my $self        = shift;
	my $page        = shift;
	my $summary     = shift;
	my $deleted;
	if(!(defined $self->{'simulation'}) || !$self->{'simulation'}){
		$self->time_management();
		my $user_input;
		if(defined($self->{'ask_user'}) && $self->{'ask_user'}){
			print "execute? ('y' = yes, else = no) ";
			chomp($user_input = <STDIN>);
		}
		if(!(defined $self->{'ask_user'}) || !$self->{'ask_user'}
				|| $user_input =~ /y(?:es)?/
				|| ($user_input eq '' && $self->{'user_answer'} eq 'y')){
			$self->{'user_answer'} = 'y';
			$self->msg(1, "  deleting page [[$page]] ...");
			push @{$self->{'time_stack'}}, time;
			$self->{'mw_api'}->edit({
				'action' => 'delete',
				'minor'  => $self->{'minor'},
				'title'  => $page,
				'reason' => "Bot: $summary"
			}) or ($self->_handle_api_error() and die);
			$deleted = 1;
		}else{
			$self->{'user_answer'} = '';
		}
	}
	return $deleted;
}

sub download_css{
	my $self       = shift;
	my $cssurl     = shift;
	$cssurl =~ s/&amp;/&/g;
	my $css_dir    = shift;
	my $images_dir = shift; # todo: save url{...}
	my $filename = $cssurl;
	$filename =~s/[^a-zA-Z0-9~_-]/_/g;
	$filename = "$css_dir/$filename.css";
	my ($http_status, $css_content) = get_http_content($cssurl);
	if($http_status != 200){
		$self->msg(0, 'download of css failed.', 'error');
	}else{
		write_file($filename, $css_content);
	}
	return $filename;
}

sub download_files{
	my $self          = shift;
	my $files         = shift;
	my $no_warn_files = shift;
	my $files_prefix  = shift // '';
	for my $file(@$files){
		$self->msg(2, "getting '$file' from wiki");
		my $img_data = $self->{'mw_bot'}->get_image("File:$file");
		unless(defined $img_data){
			$self->msg(0, "could not read file '$file' from wiki", 'error');
			next;
		}
		$file = $self->title2filename($file);
		if(-e $file){
			unless(grep {$_ eq $file} @$no_warn_files){
				$self->msg(0, "file '$file' exists already", 'warning');
				push @$no_warn_files, $file;
			}
			next;
		};
		push @$no_warn_files, $file;
		$self->msg(2, "writing file '$file'");
		write_file($files_prefix . $file, {binmode => ':raw'}, \$img_data);
	}
	return 1;
}

sub download_pages_by_prefix{
	my $self      = shift;
	my $prefix    = shift;
	my $dl_images = shift // 0;
	my @prefix_pages = grep {!$_->{'redirect'} and $_ = $_->{'title'}} ## no critic (MutatingListFunctions, ProhibitMixedBooleanOperators)
		$self->{'mw_bot'}->prefixindex($prefix);
	my $success = 1;
	if(@prefix_pages == 0){
		$self->msg(0, "no pages found with prefix '$prefix'", 'warning');
		$success = 0;
	}else{
		my $page_texts = $self->get_pages_content(\@prefix_pages);
		my $no_warn_files = [];
		my $files_prefix = '';
		for my $page(@$page_texts){
			my $title = $page->{'page'};
			my $text = $page->{'content'};
			$title = $self->title2filename($title).'.wikitext';
			$self->msg(1, "saving '$title'");
			if(-e $title){
				$self->msg(0, "file '$title' exists already", 'error');
				$success = 0;
				last;
			}
			write_file($title, {binmode => ':utf8'}, $text);
			if($dl_images){
				my @images = $text =~ /
					\[\[
						(?:[fF]ile|[iI]mage|[mM]edia):
						(
							(?:
								(?!\]\])
								[^|]
							)*
						)/xg;
				$self->download_files(\@images, $no_warn_files, $files_prefix);
			}
		}
	}
	return $success;
}

sub extract_maintenance_list_parts{
	my $self = shift;
	my $text_ref = shift;
	my $parts = shift;
	my $additional_col = shift // []; # ['column1', 'column2', ...]
	my %pos;
	# this seems to be more complicated than
	#   $$text_ref =~ /^(.*\n== [lL]iste? ==\n+.*?)(\{\|.*?\|\})\n(.*)$/s
	# but iirc .* has limitations
	# so we tokenize safer:
	if($$text_ref =~ /\n== [lL]iste? ==\n+/g && $$text_ref =~ /\{\|/g){
		$pos{'intro_table'} = pos($$text_ref) - 2;
		if($$text_ref =~ /\|\}\n/g){
			$pos{'table_outro'} = pos($$text_ref);
		}
	}
	if(defined $pos{'table_outro'}){
		%$parts = (
			'intro'     => substr($$text_ref, 0, $pos{'intro_table'}),
			'wikitable' => substr($$text_ref, $pos{'intro_table'},
				$pos{'table_outro'} - $pos{'intro_table'}),
			'outro'     => substr($$text_ref, $pos{'table_outro'}),
		);
		return 'update';
	}else{ # or create a new one
		$self->msg(1, 'could not fetch table. resetting page.', 'notice');
		$parts->{'wikitable'} = '{| class="wikitable sortable"' . "\n" .
			"! page !! " .
			join(' !! ', (@$additional_col, "date of detection by CamelBot")) .
			"\n" . '|}';
		return 'create';
	}
}

sub extract_templates{
	my $self = shift;
	my $text_ref = shift;
	my $re_filter = shift // qr/[^{|]*[^{|\s]/;
	my $re_template = qr/\{\{
		(?<name>$re_filter)\s* # template name
		(?<content>\|          # pipe and template content
			(?:[^{}]++           # a non-brace char
				|\{(?!\{)          # a single opening brace
				|\}(?!\})          # a single closing brace
				|(?R)              # a nested template
			)*
		|)\}\}/xs;
	unless(defined $text_ref){
		$self->msg(0, 'cannot extract templates from undef reference', 'warning');
		return;
	}
	if(ref $text_ref eq ''){
		my $tmp = $text_ref;
		$text_ref = \$tmp;
	}
	my @templates;
	while($$text_ref =~ /$re_template/g){
		my $name = $+{'name'};
		my $content = $+{'content'};
		my %template_params = ();
		my $i = 0;
		while($content =~ /\G\s*\|(
			(?:[^|{}]++
				|\{(?!\{)
				|\}(?!\})
				|$re_template
			)*
		)/gsx){
			my $param = $1;
			if($param =~ /\s*+(\S.*?)\b\s*=\s*+(.*?)\s*+\z/){
				$template_params{$1} = $2;
			}elsif($param =~ /\s*+(.*?)\z/){
				$template_params{++$i} = $1;
			}elsif($param =~ /^\s*\z/){
			}else{
				$self->msg(1, "could not parse line '$_'", 'warning');
			}
		}
		push @templates, {
			'name' => $name,
			'params' => \%template_params,
		};
		$self->msg(4, Dumper(\%template_params));
	}
	return \@templates;
}

sub fetch_completed_review_requests{
	my $self = shift;
	my $request_page = shift // $self->{'rev_req'}{'prefix'} . $self->{'rev_req'}{'page_title'};
	# api action=query&generator=links&titles=Wikipedia:Gesichtete_Versionen/Anfragen&gplnamespace=0|6|10|14|828&prop=flagged
	my $query = {
		'action'       => 'query',
		'generator'    => 'links',
		'titles'       => $request_page,
		'gpllimit'     => '500',
		'gplnamespace' => '0|6|10|14|828',
		'prop'         => 'flagged',
	};
	return $self->api_reviewstate($query);
}

sub fetch_pages_of_cats_of_the_dead{
	my $self    = shift;
	my $since   = shift;
	if(defined $since and
		$since !~ /^[0-9]{4}-[0-9]{2}-[0-9]{2}[T ][0-9]{2}:[0-9]{2}:[0-9]{2}Z?\z/)
	{
		$self->msg(0, 'param $since is not valid, see source code', 'warning');
		return [];
	}
	my $current_year = DateTime->now->year - 1;
	my $start_year = 2000; # $current_year - 18;
	my $query = {
		'action'     => 'query',
		'list'       => 'categorymembers',
		'cmprop'     => 'title|timestamp',
		'cmnamespace'=> 0,
		'cmlimit'    => 'max',
		'cmsort'     => 'timestamp',
		'cmstart'    =>
			(defined $since ? $since : DateTime->now->subtract(days => 2)->iso8601() . 'Z'),
	};
	my @pages;
	for my $year($start_year..$current_year){
		$query->{'cmtitle'} = 'Category:Gestorben_' . $year;
		$self->msg(5, Dumper($query));
		my $api_result = $self->{'mw_api'}->list($query);
		$self->msg(5, Dumper($api_result));
		push @pages, grep {
			$_->{'category'} = $query->{'cmtitle'};
			$_->{'category'} =~ s/^Category://;
			$_->{'category'} =~ s/_/ /g;
			$_->{'namespace'} = $_->{'ns'};
			undef $_->{'ns'};
			$_->{'page'} = $_->{'title'};
			undef $_->{'title'};
			1
		} @$api_result;
	}
	if(@pages > 7500){ # just don\t print this message so often
		$self->msg(1, 'found ' . scalar(@pages) . " entries");
	}
	return \@pages;
}

sub fetch_recentchanges{
	my $self = shift;
	# time format 'yyyy-mm-dd hh:mm:ss' will be converted to yyyymmddhhmmss
	my $timestamp_begin = shift;
	my $timestamp_end   = shift;
	unless(defined $timestamp_begin){
		# default = last 12 hours
		$timestamp_begin = $self->get_time_iso(time() - 60 * 60 * 12);
	}
	$timestamp_begin = $self->convert_time_iso_sep($timestamp_begin);
	$timestamp_end = $self->convert_time_iso_sep($timestamp_end);
	my $api_query = {
		'action'      => 'query',
		'list'        => 'recentchanges',
		'rcend'       => $timestamp_begin,
		'rcnamespace' => '*',
		'rclimit'     => 500,
		'rcprop'      => 'comment|ids|sizes|loginfo|timestamp|title|user',
	};
	$api_query->{'rcstart'} = $timestamp_end if defined $timestamp_end;
	my $rc = [];
	my $finished = 0;
	my $continue = {};
	while(!$finished){
		(my $res, $finished) = $self->_api_cont($api_query, $continue);
		map{
			my $page = $self->strip_namespace_from_title($_->{'title'}, $_->{'ns'});
			push @$rc, {
				'ns_id'         => $_->{'ns'},
				'page'          => $page,
				'rc_new_len'    => $_->{'newlen'},
				'rc_old_len'    => $_->{'oldlen'},
				'rc_this_oldid' => $_->{'revid'},
				'rc_last_oldid' => $_->{'old_revid'},
				'rc_log_type'   => $_->{'logtype'},
				'summary'       => $_->{'comment'},
				'timestamp'     => $self->convert_time_iso_strip_TZ($_->{'timestamp'}),
				'user'          => $_->{'user'},
			};
			if(defined $_->{'logtype'} && $_->{'logtype'} eq 'move'
				&& defined $_->{'logparams'}
			){
				$rc->[-1]{'move_target'} =
					$self->convert_ns($_->{'logparams'}{'target_ns'}, 'id2name') . ':'
					. $_->{'logparams'}{'target_title'};
			}
		} @{$res->{'query'}{'recentchanges'}};
	}
	return $rc;
}

sub fetch_www_meta_info{
	my $self = shift;
	my $meta_data = shift;
	my $url = shift // $meta_data->{'url'};
	my $domain = $self->get_domain_from_url($url);
	$meta_data->{'url'} = $self->delete_tracking_params_form_url($url);
	$self->msg(2, "using url '$meta_data->{'url'}'");
	# set cookies and alike, needed for some websites, then get article
	my $options;
	if($domain eq 'zeit.de'){
		# normally a cookie is generated by some javascript, but as we try to keep this
		# simple, we use a simplified fix cookie and hope that it won't expire.
		$options = {'headers' => {'Cookie' =>
				"creid=1681106585781074313; "
				. "zonconsent=" . $self->get_date_iso() . "T00:00:00.000Z"
			}
		};
	}elsif($domain eq 'washingtonpost.com'){
		$options = {'headers' => {'Cookie' => "wp_gdpr=1|1"}};
	}
	my $response = $self->get_http_response($meta_data->{'url'}, $options);
	unless($response->is_success){
		$self->msg(0, "$meta_data->{'url'} returned " . $response->code, 'warning');
		return;
	}
	my $content = $response->decoded_content;
	# get meta data
	if($content =~ /<title>(.*?)<\/title>/s){
		$meta_data->{'title'} = $1; # may be overwritten by json data
		$meta_data->{'title'} =~ s/\s+/ /g;
		$meta_data->{'title'} =~ s/\s+:/:/g;
	}
	if($domain eq 'faz.net'){
		$self->fetch_www_meta_info_from_ldjson($content, $meta_data);
		$meta_data->{'work'} = '[[Frankfurter Allgemeine Zeitung]]';
	}elsif($domain eq 'spiegel.de'){
		while($content =~ /<script\stype="application\/settings\+json">\s*
				(\{.*?)<\/script>/gxs){
			my $json = JSON->new->utf8(0)->decode($1);
			if(defined $json->{'editorial'}{'author'}{'names'}){
				my @names = @{$json->{'editorial'}{'author'}{'names'}};
				map {s/^[Vv]on //} @names;
				$meta_data->{'author'} = join ', ', @names;
			}
			if(defined $json->{'editorial'}{'info'}{'headline'}){
				$meta_data->{'title'} = $json->{'editorial'}{'info'}{'headline'};
			}elsif(defined $json->{'page'}{'info'}{'title'}){
				$meta_data->{'title'} = $json->{'page'}{'info'}{'title'};
			}
			if(defined $json->{'editorial'}{'info'}{'date_modified'}){
				$meta_data->{'date'} = $json->{'editorial'}{'info'}{'date_modified'};
			}elsif(defined $json->{'editorial'}{'info'}{'date_published'}){
				$meta_data->{'date'} = $json->{'editorial'}{'info'}{'date_published'};
			}
			if(defined $meta_data->{'date'}){
				$meta_data->{'date'} =~ s/^[0-9]{4}-[0-9]{2}-[0-9]{2}\K.*//g;
			}
		}
		$meta_data->{'work'} = '[[Der Spiegel]]';
	}elsif($domain eq 'washingtonpost.com'){
		$self->fetch_www_meta_info_from_ldjson($content, $meta_data);
		$meta_data->{'work'} = '[[The Washington Post]]';
	}elsif($domain eq 'zeit.de'){
		$self->fetch_www_meta_info_from_ldjson($content, $meta_data);
		$meta_data->{'work'} = '[[Die Zeit]]';
	}
	if(2 == grep {defined $meta_data->{$_}} ('url', 'title')){
		my $renewed_template = $self->fill_template('Internetquelle', $meta_data, 0);
		if(@{$renewed_template->{'set_params'}} >= 4){ # incl. access_date
			return $renewed_template->{'str'};
		}
	}
	return;
}

sub fetch_www_meta_info_from_ldjson{
	my $self    = shift;
	my $content = shift;
	my $meta_data = shift;
	while($content =~ /<script\stype="application\/ld\+json"(?:[^>]*)>\s*
				(\{.*?)<\/script>/gsx
				#	(\{\s+(?:\s.*\n)+\})\s*<\/script>/gsx
	){
		#my $json_bytes = $1;
		#utf8::encode($json_bytes)
		my $json = JSON->new->utf8(0)->decode($1); # json_bytes
		if(defined $json->{'@type'}){
			if($meta_data->{'url'} =~ m~^https://www\.zeit\.de/autoren\/~ && $json->{'@type'} eq 'Person'){
				# example:
				#{
				#    "@context": "http://schema.org",
				#    "@type": "Person",
				#    "@id": "#author",
				#    "mainEntityOfPage": {
				#        "@type": "WebPage",
				#        "@id": "https://www.zeit.de/autoren/W/Marc_Widmann/index.xml"
				#    },
				#    "name": "Marc Widmann",
				#    "url": "https://www.zeit.de/autoren/W/Marc_Widmann/index.xml",
				#    [optional data:]
				#    "jobTitle": "Redakteur im Ressort Wirtschaft, DIE ZEIT",
				#    "description": "[...] in M\u00fcnchen [...]",
				#    "image": {
				#        "@type": "ImageObject",
				#        [...]
				#    }
				#}
				if(defined $json->{'name'}){
					$meta_data->{'title'} = 'Redaktionsprofil von ' . $json->{'name'};
					if(defined($json->{'jobTitle'})
						&& $json->{'jobTitle'} =~ /^(.*), DIE ZEIT\s*$/i
					){
						$meta_data->{'title'} .= ', ' . $1;
					}
					last;
				}
				# intended: don't use meta dates, because the article collection of the
				# author is dynamic, i.e. the link collection might change even if the
				# last modification is long ago.
				# <meta name="last-modified" content="2015-04-17T10:31:42+02:00">
				# <meta name="date" content="2015-04-17T10:31:42+02:00">
			}
			if($json->{'@type'} eq 'Article' || $json->{'@type'} eq 'NewsArticle'){
				# example:
				#{
				#    "@context": "http://schema.org",
				#    "@type": "Article",
				#    "@id": "#article",
				#    "author": {
				#        "@type": "Person",
				#        "url": "https://www.zeit.de/autoren/N/Some_Name/index.xml",
				#        "name": "some name"
				#    },
				#
				#    or (unfortunately):
				#
				#    "author": {
				#        "@type": "Person",
				#        "name": "DIE ZEIT (Archiv)"
				#    },
				#
				#    or
				#
				#    "author": {
				#        "@type": "Person",
				#        "url": "https://www.zeit.de/autoren/dpa",
				#        "name": "dpa"
				#    },
				#
				#    or:
				#    "author": [{
				#        "@type": "Person",
				#        "url": "https://www.zeit.de/autoren/M/Some_Mname/index.xml",
				#        "name": "some mname"
				#     },{
				#        "@type": "Person",
				#        "url": "https://www.zeit.de/autoren/R/Some_Rname/index.xml",
				#        "name": "some rname"
				#     },{
				#        "@type": "Person",
				#        "url": "https://www.zeit.de/autoren/S/Some_Sname/index.xml",
				#        "name": "some sname"
				#     }],
				#
				#    "datePublished": "2010-01-01T00:00:00+02:00",
				#    "dateModified": "2010-01-01T00:00:00+02:00",
				#    "headline": "some headline",
				#    "description": "some description",
				#    "isAccessibleForFree": "False",
				#}
				if(defined $json->{'author'}){
					if(ref $json->{'author'} eq 'ARRAY'){
						$meta_data->{'author'} = join ', ', map {
							$_->{'name'} =~ s/^\s+|\s+$//g;
							$_->{'name'}
						} @{$json->{'author'}};
					}else{
						$meta_data->{'author'} = $json->{'author'}{'name'};
					}
					if($meta_data->{'author'} =~ /\b(?:AF?P|Archiv|dpa|KNA|ZEIT ONLINE)\b/){
						undef $meta_data->{'author'};
					}
				}
				$meta_data->{'title'} = $json->{'headline'};
				$meta_data->{'date'} = $self->convert_time_log_string2iso(
					$json->{'dateModified'} // $json->{'datePublished'});
				$meta_data->{'date'} =~ s/^[0-9]{4}-[0-9]{2}-[0-9]{2}\K.*//g;
				$meta_data->{'work'} = $json->{'publisher'}{'name'};
				last;
			}
		}
	}
	return 1;
}

sub fill_template{
	my $self   = shift;
	my $template_name = shift; # e.g. 'Internetquelle'
	my $src_data = shift; # hash ref with meta data {paramname1 => value1, ...}
	# 0: use mapping defined by $maps, 1: use mapping as given (order might be random)
	my $use_given_mapping = shift // 0;
	my $result = {
		'set_params' => [],
		'str' => undef,
	};
	unless(defined $template_name){
		$self->msg(0,
			'first param is undefined, but needs to be a name of a template (string)',
			'error');
		return $result;
	}
	unless(defined $src_data){
		$self->msg(0, 'second param is undefined, but needs to be a hash ref', 'error');
		return $result;
	}
	if(ref $src_data ne 'HASH'){
		$self->msg(0, 'param needs to be a hash ref', 'error');
		return $result;
	}
	my $maps = {
		'Internetquelle' => [ # keep order
			['author', 'autor'],
			['url', 'url'],
			['title', 'titel'],
			['work', 'werk'],
			['date', 'datum'],
			['access_date', 'abruf'],
		],
		'AllMovie' => [
			['id', undef],
			['timestamp', undef],
			['text', 'Text'],
			['author', 'Autor'],
			['bot', 'Bot'],
		],
	};
	my $map = [];
	if(!$use_given_mapping && exists($maps->{$template_name})){
		$self->msg(2, "internal mapping for template '$template_name' used");
		$map = $maps->{$template_name};
	}elsif($use_given_mapping){
		$self->msg(2, 'using given param names and not internal mapping');
		for my $param(keys(%$src_data)){
			push @$map, [$param, $param];
		}
	}else{
		$self->msg(0, "no internal mapping defined for template '$template_name'. "
			. 'using given param names', 'warning');
		$map = $maps->{$template_name};
	}
	$result->{'str'} = '{{' . $template_name . ' ';
	for my $m(@$map){
		my $can_param_name = $m->[0];
		my $param_name = $m->[1];
		if(defined $src_data->{$can_param_name}){
			my $value_with_encoded_pipe = $src_data->{$can_param_name};
			$value_with_encoded_pipe =~ s/\{\{.*?}}(*SKIP)(*FAIL)|\|/&#x7c;/g;
			if(defined $param_name){
				$result->{'str'} .= "|$param_name=$value_with_encoded_pipe ";
			}else{
				$result->{'str'} .= "|$value_with_encoded_pipe ";
			}
			push @{$result->{'set_params'}}, $can_param_name;
		}
	}
	$result->{'str'} =~ s/ $/}}/;
	return $result;
}

sub get_abusefilter_info{
	my $self        = shift;
	my $search_meta = shift // 1;
	# api.php?action=query&list=abusefilters&abfprop=id|description
	# &abfstartid=1&abflimit=500
	# could be extended to
	# api.php?action=query&list=abusefilters&abfprop=
	# id|description|pattern|actions|hits|comments|lasteditor|lastedittime|status|
	# private&abflimit=500
	my $query = {
		'action'         => 'query',
		'list'           => 'abusefilters',
		'abflimit'       => '500',
		'abfprop'        => 'id|description',
		'abfstartid'     => '1',
	};
	my $mw_options = {'max' => 1};
	my $finished = 0;
	my %results;
	my $continue = {};
	while(!$finished){
		(my $pages, $finished) = $self->_api_cont($query, $continue, $mw_options);
		map {
			$results{$_->{'id'}} = $_->{'description'}
		} @{$pages->{'query'}{'abusefilters'}};
	}
	if($search_meta){
		my $global_results = $self->get_abusefilter_info_global();
		while(my ($id, $descr) = each %$global_results){
			$results{'global-' . $id} = $descr;
		}
	}
	return \%results;
}

sub get_abusefilter_info_global{
	my $self   = shift;
	my $host = $self->get_host();
	$self->set_host('meta.wikimedia.org');
	# https://meta.wikimedia.org/w/
	# api.php?action=query&list=abusefilters&abfprop=id|description
	# &abfstartid=1&abflimit=500
	my $search_meta = 0;
	my $results = $self->get_abusefilter_info($search_meta);
	$self->set_host($host);
	return $results;
}

sub get_abusefilter_log_ids{
	my $self = shift;
	my $filter_id = shift;
	my $query = {
		'action'         => 'query',
		'list'           => 'abuselog',
		'afllimit'       => '5000',
		'aflprop'        => 'ids', # ids|user|title|action|result|timestamp|hidden|revid
		'aflfilter'      => $filter_id,
	};
	my $mw_options = {'max' => 1};
	my $finished = 0;
	my @results;
	my $continue = {};
	while(!$finished){
		(my $pages, $finished) = $self->_api_cont($query, $continue, $mw_options);
		map {
			push @results, $_->{'id'}
		} @{$pages->{'query'}{'abuselog'}};
	}
	return \@results;
}

sub get_all_maintenance_triggers_of_page{
	my $self = shift;
	my $rc_msg = shift;
	my $page_text_ref = shift;
	my $check_triggering = shift // 1;  # checks for notification only
	if($check_triggering || !(defined $page_text_ref)){
		unless(defined $rc_msg && ref $rc_msg eq 'HASH'
			&& defined $rc_msg->{'page_with_ns'}
		){
			$self->msg(0, 'rc_msg should be a hash ref', 'warning');
			return;
		}
	}
	unless(defined $page_text_ref){
		my $page_text = $self->get_text(
			$rc_msg->{'move_target'} // $rc_msg->{'page_with_ns'});
		$page_text_ref = \$page_text;
	}
	return unless defined $$page_text_ref;
	my $matches = {};
	# get number of occurrences in present page
	for my $ntf_type(keys %{$self->{'maintenance'}{'text_re'}}){
		my $triggering = !$check_triggering;
		if($check_triggering){
			($triggering, my $trigger_text) = $self->triggers_maintenance_lists(
				$ntf_type, $rc_msg, $page_text_ref);
			if($triggering){
				$self->msg(4, 'trigger check: ' . $ntf_type . ' ' . $triggering);
			}
			$triggering = ($triggering == 1 || $triggering == 2 || $triggering == 4);
		}
		if($triggering){
			$matches->{$ntf_type} = [];
			while($$page_text_ref =~ /$self->{'maintenance'}{'text_re'}{$ntf_type}/gp){
				push @{$matches->{$ntf_type}}, ${^MATCH};
			}
		}
	}
	return $matches;
}

sub get_all_pages{
	my $self    = shift;
	my $pattern = shift;
	my $query = {
		'action'    => 'query',
		'list'      => 'allpages',
		'aplimit'   => '500',
	};
	my $mw_options = {'max' => 1};
	my %list_of_articles;
	my $pages = [];
	my $die_on_error = 1; # 0 = dont die; 1 = die
	my $finished = 0;
	my $continue = {};
	while(!$finished){
		(my $tmp, $finished) = $self->_api_cont(
			$query, $continue, $mw_options, !$die_on_error);
		my $allpages = $tmp->{'query'}{'allpages'};
		if(defined $allpages){
			for my $ap(@$allpages){
				if(exists($list_of_articles{$ap->{'title'}})
						|| $ap->{'title'} !~ /$pattern/){
					next;
				}
				$list_of_articles{$ap->{'title'}} = 1;
				push @$pages, $ap->{'title'};
			}
		}
	}
	return $pages;
}

sub _get_api_url{
	my $self = shift;
	my $wiki_base_url = 'https://' . $self->{'host'};
	return $wiki_base_url . '/' . $self->{'rel_url_path'} . '/api.php';
}

sub get_archived_url_timestamp{
	my $self = shift;
	my $url = shift;
	my $params = shift;
	my $start_time = $params->{'start_time'} // '19900000';  # 'yyyymmdd'
	my $end_time = $params->{'end_time'} // DateTime->now->ymd('') ;  # 'yyyymmdd'
	my $preferred_max_time = $params->{'preferred_max_time'};  # e.g. '20201231'
	my $num_tries = $params->{'num_tries'} // 1;  # max number of tries
	my $www_retry_delay = $params->{'delay'} // 20;  # seconds to wait before retry
	unless(defined $url){
		$self->msg(0, 'url is undefined', 'error');
		return;
	}
	my $wayback_url = 'https://web.archive.org/cdx/search/cdx'
		. "?url=$url&from=$start_time&to=$end_time&output=json&filter=statuscode:200";
	my $http_status = 0;
	my $content;
	while(--$num_tries >= 0 && $http_status != 200){
		($http_status, $content) = $self->get_http_content($wayback_url);
		sleep $www_retry_delay if($num_tries > 0 && $http_status != 200);
	}
	my $timestamp;
	unless($http_status == 200){
		$self->msg(0, "got $http_status for $wayback_url", 'warning');
		return -1;
	}
	unless(defined $content && $content ne 'eq'){
		$self->msg(0, "no content for $wayback_url", 'error');
		return -2;
	}
	my $json = decode_json($content);
	if(@$json == 0){
		$self->msg(0, "no saved versions found for $wayback_url", 'warning');
		return -3;
	}
	# maybe better us the following code instead?
	#my $json = JSON->new->utf8(0)->decode($content);
	shift @$json; # first is just the head
	for my $entry(sort {$b->[1] cmp $a->[1]} @$json){
		unless(defined $timestamp){
			$timestamp = $entry->[1];  # first (newest) found version
			last unless defined $preferred_max_time;
			next;
		}
		last if $timestamp le $preferred_max_time;
		$timestamp = $entry->[1];
	}
	unless(defined $timestamp){
		$self->msg(0, Dumper($content), 'warning');
		return -4;
	}
	return $timestamp;
}

sub get_categories_of_page{
	my $self = shift;
	my $page = shift;
	my $query = {
		'action' => 'query',
		'titles' => $page,
		'prop'   => 'categories',
	};
	my $api_result = $self->api_simple($query);
	return $self->_handle_api_error() unless $api_result;
	my ($pageref) = values %{ $api_result->{'query'}->{'pages'} };
	return $pageref->{'categories'};
}

sub get_cookie_filename{
	my $self = shift;
	if(defined $self->{'mw_username'}){
		return '.mediawiki-bot-' . $self->{'mw_username'} . '-cookies';
	}else{
		$self->msg(0, 'user name not set.', 'warning');
		return;
	}
}

sub get_default_access_type{
	my $self = shift;
	return $self->{'access_types'}[0];
}

sub get_diff{
	my $self   = shift;
	my $diff_from_or_page = shift; # {title=>...} or {revid=>...}
	my $diff_to = shift // 'prev';
	if(ref $diff_from_or_page ne 'HASH'){
		$self->msg(0, 'first param has to be a hash ref', 'warning');
		return;
	}
	# TODO: should be replaced by
	#my $diff = $self->{'mw_bot'}->diff({
	#		'revid' => $page->{'revid'},
	#		'title' => $page->{'title'},
	#		'oldid' => $diffto,
	#	}) or return;

	my $diff_html;
	my $result;
	if($self->check_mw_version('>=1.20.0')){
		# get diff via newer api (compare)
		my $query = {
			'action'     => 'compare',
			'prop'       => 'diff|ids|title|user|comment|timestamp',
		};
		if(defined $diff_from_or_page->{'title'}){
			$query->{'fromtitle'}  = $diff_from_or_page->{'title'};
		}elsif(defined $diff_from_or_page->{'revid'}){
			$query->{'fromrev'} = $diff_from_or_page->{'revid'};
		}else{
			$self->msg(0, 'first param has to contain a key "title" or "rvlimit"', 'error');
			return;
		}
		if($diff_to =~ /^[0-9]+\z/){
			$query->{'torev'}  = $diff_to;
		}else{
			$query->{'torelative'} = $diff_to;
		}
		my $diff = $self->api_simple($query) or return;
		$diff = $diff->{'compare'};
		$result = {
			'fromrevid'     => $diff->{'fromrevid'},
			#'fromuser'      => $diff->{'fromuser'},
			#'fromtimestamp' => $diff->{'fromtimestamp'},
			#'fromcomment'   => $diff->{'fromcomment'},
			#'fromtitle'     => $diff->{'fromtitle'},
			'tocomment'     => $diff->{'tocomment'},
			'torevid'       => $diff->{'torevid'},
			'totimestamp'   => $diff->{'totimestamp'},
			'totitle'       => $diff->{'totitle'},
			'touser'        => $diff->{'touser'},
		};
		$diff_html = $diff->{'*'};
	}else{
		# get diff via old api (query revisions)
		my $query = {
			'action'   => 'query',
			'prop'     => 'revisions',
			'rvdiffto' => $diff_to,
		};
		if(defined $diff_from_or_page->{'title'}){
			$query->{titles}  = $diff_from_or_page->{'title'};
			$query->{rvlimit} = 1;
		}elsif(defined $diff_from_or_page->{'revid'}){
			$query->{'revids'} = $diff_from_or_page->{'revid'};
		}else{
			$self->msg(0, 'first param has to contain a key "title" or "rvlimit"', 'error');
			return;
		}
		my $api_result = $self->api_simple($query) or return;
		# extract diff info
		my @revids = keys %{$api_result->{query}{pages}};
		my $diff = $api_result->{'query'}{'pages'}{$revids[0]}{'revisions'}->[0];
		$result = {
			#'revid'     => $diff->{'revid'},
			#'parentid'  => $diff->{'parentid'},
			#'minor'     => $diff->{'minor'},
			'fromrevid'   => $diff->{'diff'}{'from'},
			'tocomment'   => $diff->{'comment'},
			'torevid'     => $diff->{'diff'}{'to'},
			'totimestamp' => $diff->{'timestamp'},
			'totitle'     => $api_result->{'query'}{'pages'}{$revids[0]}{'title'},
			'touser'      => $diff->{'user'},
		};
		$diff_html = $diff->{'diff'}{'*'};
	}
	$result->{'diff'} = $self->parse_diff_html($diff_html);
	return $result;
}

sub get_domain_from_url{
	my $self = shift;
	my $url  = shift;
	$url =~ m~^(?:https?|ftp)://([^/]+)~;
	return unless defined $1;
	my $domain = $1;
	if(!$self->is_ip($domain)){
		if($domain =~ /([^.]+\.[^.]+)\z/){
			$domain = $1;
		}
	}
	return $domain;
}

sub get_usernames_from_signatures{
	my $self = shift;
	my $thread = shift;
	unless(defined $thread){
		$self->msg(0, 'no thread given', 'warning');
		return;
	}
	my $user_re = qr/\[\[
		(?:
			# 'user:...' or 'user talk:...'
			(?:[Bb]enutzer(?:in)?|[uU]ser)(?:\x20talk|Diskussion)?:|
			# or 'special:contributions...'
			[Ss]pe[zc]ial:(?:Beitr..?ge|[Cc]ontributions)\/
		)
		(?<username>[^|\/\]]+)|\{\{unsigned\|([^|]+)\|/x;
	my $users = [grep {defined} $thread =~ /$user_re/g];
	return $users;
}

sub get_history{
	my $self     = shift;
	my $pagename = shift;
	my $additional_params = shift;
	if($MediaWiki::Bot::VERSION ge '5.007'){
		if(defined $additional_params->{'rvlimit'}
			&& $additional_params->{'rvlimit'} ne 'max'
		){
			return $self->{'mw_bot'}->get_history_step_by_step(
				$pagename, $additional_params);
		}else{
			return $self->{'mw_bot'}->get_history($pagename, $additional_params);
		}
	}else{
		return defined $additional_params
			? $self->{'mw_bot'}->get_history($pagename, $additional_params->{'rvlimit'})
			: $self->{'mw_bot'}->get_history($pagename);
	}
}

# 2019-03-10 MediaWiki::Bot 5.006003 is buggy and can't cope with rvcontinue,
# so using an own function.
sub get_history_step_by_step{
	my $self     = shift;
	my $pagename = shift;
	my $additional_params = shift;
	if($MediaWiki::Bot::VERSION ge '5.007'){
		return $self->{'mw_bot'}->get_history_step_by_step($pagename, $additional_params)
	}else{
		$additional_params = {} unless defined $additional_params;
		my $query = {
			action  => 'query',
			prop    => 'revisions',
			titles  => $pagename,
			rvprop  => 'ids|timestamp|user|comment|flags',
		};
		while(my ($key, $value) = each %$additional_params){
			$query->{$key} = $value;
		}
		$query->{'rvlimit'} = $additional_params->{'rvlimit'} // 'max';
		my $res = $self->api_simple($query);
		my ($id) = keys %{ $res->{query}{pages} };
		my $array = $res->{query}{pages}{$id}{revisions};
		my @return;
		for my $hash(@{$array}){
			my $revid = $hash->{revid};
			my $user  = $hash->{user};
			my ($timestamp_date, $timestamp_time) = split(/T/, $hash->{timestamp});
			$timestamp_time =~ s/Z$//;
			my $comment = $hash->{comment};
			push(
				@return, {
					revid          => $revid,
					user           => $user,
					timestamp_date => $timestamp_date,
					timestamp_time => $timestamp_time,
					comment        => $comment,
					minor          => exists $hash->{minor},
				});
		}
		$self->msg(4, Dumper($res));
		$additional_params->{'continue'} = $res->{'continue'}{'continue'};
		$additional_params->{'rvcontinue'} = $res->{'continue'}{'rvcontinue'};
		return \@return;
	}
}

sub get_host{
	my $self = shift;
	return $self->{'host'};
}

sub get_http_content{
	my $self = shift;
	my $url  = shift;
	my $content;
	my $warnings = 0;
	if($self->{'use_www_cache'}){
		$self->read_www_cache;
		if(defined $self->{'www_cache'}{$url}){
			return (
				$self->{'www_cache'}{$url}{'code'},
				$self->{'www_cache'}{$url}{'body'},
			);
		}
	}
	if($self->{'offline'}){
		$self->msg(0, 'fake result (should only be used for testing and debugging)',
			'warning');
		return (@{$self->{'offline_get_http_content'}});
	}
	my $response = $self->get_http_response($url, {'warn' => $warnings});
	if($response->is_success){
		$content = $response->decoded_content;
		if($self->{'use_www_cache'}){
			$self->{'www_cache'}{$url} = {
				'code' => $response->code,
				'body' => $content,
			};
		}
	}else{
		$self->msg(0, $response->status_line, 'warning');
		if($self->{'use_www_cache'} && $response->code != 500){
			$self->{'www_cache'}{$url} = {'code' => $response->code};
		}
	}
	$self->write_www_cache;
	return ($response->code, $content);
}

sub get_http_redir_location{
	my $self = shift;
	my $url = shift;
	my $is_redir;
	if($self->{'use_www_cache'}){
		$self->read_www_cache;
		if(defined $self->{'www_cache'}{$url}){
			return (
				$self->{'www_cache'}{$url}{'code'} =~ /^30[12]\z/,
				$self->{'www_cache'}{$url}{'location'},
			);
		}
	}
	my $response = $self->get_http_response($url, {'max_redirect' => 0});
	if($response->code =~ /^30[12]\z/){
		$is_redir = 1;
		my $target_url = $response->header('location');
		if($self->{'use_www_cache'}){
			$self->{'www_cache'}{$url} = {
				'code' => $response->code,
				'location' => $target_url,
			};
		}
		$self->write_www_cache;
		return ($is_redir, $target_url);
	}
	return ($is_redir, undef);
}

sub get_http_response{
	my $self     = shift;
	my $url      = shift;
	my $options  = shift // {};
	$options->{'warn'} = 1 unless defined $options->{'warn'};
	#$options->{'max_redirect'}
	#$options->{'headers'}
	if($self->{'offline'}){
		$self->msg(0, 'fake result (should only be used for testing and debugging)',
			'warning');
		my $response = HTTP::Response->new(
			$self->{'offline_get_http_response'}{'code'} // 200);
		if(defined $self->{'offline_get_http_response'}{'header'}){
			$response->header(%{$self->{'offline_get_http_response'}{'header'}});
		}
		$response->content($self->{'offline_get_http_response'}{'content'} // '');
		return $response;
	}
	my $lwp = LWP::UserAgent->new(
		'keep_alive' => 1,
		'timeout' => 10,
		'agent' => 'Mozilla/5.0'
	);
	# 2021-04-11 seth: bugfix of https://rt.cpan.org/Public/Bug/Display.html?id=120643
	$lwp->ssl_opts('SSL_ocsp_mode' => SSL_OCSP_NO_STAPLE);
	my $content;
	$self->msg(3, "url = '$url'");
	my $max = $lwp->max_redirect;
	if(defined $options->{'max_redirect'}){
		$lwp->max_redirect($options->{'max_redirect'});
	}
	if(defined $options->{'headers'}){
		$lwp->default_header(%{$options->{'headers'}});
	}
	my $response = $lwp->get($url);
	push @{$self->{'www_history'}}, $url;
	$lwp->max_redirect($max) if defined $options->{'max_redirect'};
	if($options->{'warn'} && !$response->is_success){
		$self->msg(0, $response->status_line . " (result of getting $url)", 'notice');
	}
	return $response;
}

sub get_http_status{
	my $self = shift;
	my $url = shift;
	unless(defined $url){
		$self->msg(0, 'url param is not defined', 'warning');
		return;
	}
	if($self->{'offline'}){
		$self->msg(0, 'fake result (should only be used for testing and debugging)',
			'warning');
		return (@{$self->{'offline_get_http_status'}});
	}
	my $max_redirect = shift // 7;
	my $lwp = LWP::UserAgent->new(
		'keep_alive' => 1,
		'timeout' => 10,
		'agent' => 'Mozilla/5.0 (X11; Linux;) well, not true, actually just a wikipedia bot'
	);
	$lwp->max_redirect($max_redirect);
	if($url =~ m~^//~){
		$url = 'https:' . $url;
	}
	my $response = $lwp->head($url);
	# some stupid webservers (destatis.de) return 400 on 'head' requests, but 200 on
	# 'get' requests on the same urls.
	if($response->code == 400){
		$response = $lwp->get($url);
	}
	return ($response->code, ($response->header('location') // ${$response->base}));
}

sub get_image_info{
	my $self = shift;
	my $page = shift;
	my $query = {
		'action' => 'query',
		'titles' => $page,
		'prop'   => 'imageinfo',
		'iiprop' => 'url|size',
	};
	my $api_result = $self->api_simple($query);
	return $self->_handle_api_error() unless $api_result;
	my ($pageref) = values %{ $api_result->{'query'}->{'pages'} };
	return defined($pageref->{'imageinfo'}) ? $pageref->{'imageinfo'}->[0] : undef;
}

sub _get_mem_usage{
	my $self = shift;
	my $pid = shift // $$;
	my $msg = shift;
	my $page_size_in_kb = 4;
	# adapted from Memory::Usage
	# record mem usage
	sysopen(my $fh, "/proc/$pid/statm", 0) or die $!;
	sysread($fh, my $line, 255) or die $!;
	close($fh);
	my ($vsz, $rss, $share, $text, $dummy, $data, $dummy2) = split(/\s+/, $line, 7);
	shift @{$self->{'mem_usage'}} if @{$self->{'mem_usage'}} > 1; # keep 2
	push @{$self->{'mem_usage'}}, {
		'unix_timestamp' => time(),
		'msg' => $msg,
		'mem' => [map {$_ * $page_size_in_kb * 1.024 * 1e-3} ($vsz, $rss, $share, $data)],
		'class_size' => length(Dumper($self)) * 1e-6,
	};
	# report mem usage
	my $report = sprintf "%19s %6s (%6s) %6s (%6s) %6s (%6s) %6s (%6s) %6s (%6s) -- all data in MB\n",
	'timestamp',
	'vsz', 'diff',
	'rss', 'diff',
	'shared', 'diff',
	'data', 'diff',
	'class', 'diff';
	my $prev = $self->{'mem_usage'}[0];
	my $usg = $self->{'mem_usage'}[1];
	$report .= sprintf "% 19s % 6.0f (% 6.1f) % 6.0f (% 6.1f) % 6.0f (% 6.1f) % 6.0f (% 6.1f) % 6.0f (% 6.1f) %s",
	$self->get_time_iso_($usg->{'unix_timestamp'}),
	$usg->{'mem'}[0], ($usg->{'mem'}[0] - $prev->{'mem'}[0]),
	$usg->{'mem'}[1], ($usg->{'mem'}[1] - $prev->{'mem'}[1]),
	$usg->{'mem'}[2], ($usg->{'mem'}[2] - $prev->{'mem'}[2]),
	$usg->{'mem'}[3], ($usg->{'mem'}[3] - $prev->{'mem'}[3]),
	$usg->{'class_size'}, ($usg->{'class_size'} - $prev->{'class_size'}),
	$usg->{'msg'};
	return $report;
}

sub get_magicwords_alias{
	my $self = shift;
	my $magicword = shift;
	unless(defined $magicword){
		$self->msg(0, 'missing param "magicword"', 'warning');
		return;
	}
	unless(defined $self->{'mw_magicwords_aliases'}){
		my $query = {
			'action' => 'query',
			'meta'   => 'siteinfo',
			'siprop' => 'magicwords',
		};
		my $res = $self->api_simple($query);
		my $magicwords = $res->{'query'}{'magicwords'};
		for my $item(@$magicwords){
			$self->{'mw_magicwords_aliases'}{$item->{'name'}} = $item->{'aliases'};
		}
	}
	return $self->{'mw_magicwords_aliases'}{$magicword};
}


sub get_mw_language{
	my $self = shift;
	unless(defined $self->{'mw_lang'}){
		my $query = {
			'action' => 'query',
			'meta'   => 'siteinfo',
			'siprop' => 'general',
		};
		my $res = $self->api_simple($query);
		my $lang = $res->{'query'}{'general'}{'lang'};
		if(defined($lang) && $lang ne ''){
			$self->{'mw_lang'} = $lang;
		}else{
			$self->{'mw_lang'} = 'en';
		}
	}
	return $self->{'mw_lang'};
}

sub get_mw_version{
	my $self = shift;
	unless(defined $self->{'mw_version'}){
		my $query = {
			'action' => 'query',
			'meta'   => 'siteinfo',
			'siprop' => 'general',
		};
		my $res = $self->api_simple($query);
		my $version = $res->{'query'}{'general'}{'generator'};
		if(defined $version && $version =~ /^MediaWiki ([0-9]+\.[0-9]+(?:\.[0-9]+)?+)/){
			$self->{'mw_version'} = $1;
		}
	}
	return $self->{'mw_version'};
}

sub get_namespace_id{
	my $self = shift;
	my $page = shift;
	return unless defined $page;
	my $ns_id = 0;
	if($page =~ /^([^:]+):/){
		$ns_id = $self->convert_ns($1, 'name2id');
	}
	return $ns_id;
}

sub get_page_info{
	my $self   = shift;
	my $titles = shift;
	my $full_result = shift;
	$titles = join '|', @$titles if ref $titles eq 'ARRAY';
	# api.php?action=query&prop=info&titles=Wikipedia:Bearbeitungsfilter/latest%20topics&continue=
	my $query = {
		'action'         => 'query',
		'continue'       => '',
		'prop'           => 'info',
		'titles'         => $titles,
	};
	my $mw_options = {'max' => 1};
	my $pages = $self->api_simple($query, $mw_options);
	if($full_result){
		return $pages->{'query'};
	}else{
		my $page_infos;
		while(my ($page_id, $page_info) = each %{$pages->{'query'}{'pages'}}){
			utf8::decode($page_info->{'title'});
			$page_infos->{$page_info->{'title'}} = $page_info;
		}
		return $page_infos;
	}
}

sub get_pages{
	my $self = shift;
	my $collect_types = shift;
	my $pages = [];
	$self->msg(2, 'getting pages');
	if(defined $collect_types->{'from all_pages'}){
		$pages = $self->get_all_pages(qr/$collect_types->{'from all_pages'}/);
	}
	if(defined $collect_types->{'single page'}){
		utf8::decode($collect_types->{'single page'});
		push @$pages, $collect_types->{'single page'};
	}
	if(defined $collect_types->{'search'}){
		push @$pages, @{$self->search_pages($collect_types->{'search'})};
	}
	if(defined $collect_types->{'linksearch'}){
		my $links = $self->link_search_pages(
			'linksearch', # search_type
			$collect_types->{'linksearch'}{'simple_url'},
			$collect_types->{'linksearch'}{'namespaces'} // [0],
			undef,
		);
		push @$pages, map {$_->{'title'}} @$links;
	}
	if(defined $collect_types->{'whatlinkshere'}){
		push @$pages, map {$_->{'title'}}
			$self->{'mw_bot'}->what_links_here($collect_types->{'whatlinkshere'});
	}
	$self->msg(3, Dumper($pages));
	$self->msg(2, 'got ' . scalar(@$pages) . ' pages');
	return $pages;
}

sub get_pages_content{
	# similar to sub text_replacement
	my $self    = shift;
	my $pages   = shift; # wiki pages to get
	my $section = shift; # section to use
	my $contents = [];
	for my $page(@$pages){
		$self->msg(2, "page = '$page'");
		my $text = $self->get_text($page);
		next unless defined $text;
		my @split_text;
		if(defined $section){
			my $i = 0;
			@split_text = grep {++$i % 3}
				split /^((=+)(?!=).+(?<!=)\2(?:\n|\z))/m, $text;
			# 0, 2, 4, ... = text
			# 1, 3, 5, ... = headings
			my $section_num = -1;
			for($i = 1; $i < @split_text; $i+=2){
				if($split_text[$i] =~ /^(=+)\s*$section\s*\1$/){
					$section_num = $i;
					last;
				}
			}
			if($section_num == -1){
				$self->msg(1, 'could not find section in page', 'warning');
			}else{
				$self->msg(2, "found find section '$section' in page '$page'");
			}
			$text = $split_text[$section_num + 1];
		}
		push @$contents, {
			'page'    => $page,
			'section' => $section,
			'content' => $text,
		};
	}
	return $contents;
}

sub get_pages_by_prefix{
	my $self   = shift;
	my $params = shift;
	my $query = {
		'action'         => 'query',
		'generator'      => 'allpages',
		'gapnamespace'   => $params->{'namespace_id'},
		'gapprefix'      => $params->{'prefix'},
		'gapfilterredir' => 'nonredirects',
		'gaplimit'       => '500',
		'prop'           => 'info|revisions',
		'rvprop'         => 'content|timestamp',
		'rvslots'        => 'main',
	};
	my $mw_options = {'max' => 1};
	my $results = $self->api_cont_complete($query, $mw_options);
	$self->msg(4, 'number of results: ' . scalar(keys %$results));
	$self->msg(4, Dumper($params));
	$self->msg(3, Dumper($results));
	if($params->{'title_re'}){
		my @keys_to_delete = grep {
			$results->{$_}{'title'} !~ /$params->{'title_re'}/
		} keys %$results;
		$self->msg(3, 'number of keys to delete: ' . scalar(@keys_to_delete));
		delete @$results{@keys_to_delete};
	}
	$self->msg(3, 'number of results: ' . scalar(keys %$results));
	return [values %$results];
}

sub get_pages_by_prefix_sorted{
	# api.php?action=query&generator=allpages&gapnamespace=4&gapprefix=Bearbeitungsfilter/&gapfilterredir=nonredirects&gaplimit=500&prop=info|revisions&rvprop=content|timestamp
	my $self = shift;
	my $namespace = shift;
	my $prefix = shift;
	my $title_re = shift;
	my $pages_by_prefixes = $self->get_pages_by_prefix({
		'namespace_id' => $namespace,
		'prefix'       => $prefix,
		'title_re'     => $title_re,
	});
	for my $p(@$pages_by_prefixes){
		unless(defined($p->{'revisions'}) && @{$p->{'revisions'}} > 0){
			$self->msg(0, 'no revisions', 'warning');
			next;
		}
		unless(defined $p->{'revisions'}[0]{'timestamp'}){
			$self->msg(3, Dumper($p));
			$self->msg(0, 'did not get all needed data (timestamps)', 'warning');
		}
	}
	my $pages = [
		sort {$b->{'revisions'}[0]{'timestamp'}
			cmp $a->{'revisions'}[0]{'timestamp'}}
		@$pages_by_prefixes
	];
	return $pages;
}

sub get_random_pages{
	my $self = shift;
	my $limit = shift // 1;
	my $query = {
		'action' => 'query',
		'list'   => 'random',
		'rnnamespace' => 0,
		'rnlimit' => $limit,
	};
	my $res = $self->api_simple($query);
	my $pages = $res->{'query'}{'random'};
	my $page_titles;
	map {push @$page_titles, $_->{'title'};} @$pages;
	return $page_titles;
}

sub get_review_state{
	my $self = shift;
	my $page_title =shift;
	# api action=query&titles=$pagename&prop=flagged
	my $query = {
		'action'       => 'query',
		'titles'       => $page_title,
		'prop'         => 'flagged',
	};
	return $self->api_reviewstate($query);
}

sub get_sbl_entries{
	my $self = shift;
	my $wiki = shift;
	my $sbl_entries;
	my $sbl_text = '';
	if(defined $wiki){
		if($wiki eq 'meta'){
			(my $http_status, $sbl_text) = $self->get_http_content(
				'https://meta.wikimedia.org/w/index.php?' .
				'title=Spam_blacklist&action=raw&sb_ver=1'
			);
		}else{
			$self->msg(0, "unsupported param '$wiki'.", 'error');
		}
	}else{
		$sbl_text = $self->get_text('MediaWiki:Spam-blacklist');
	}
	# remove non-pre items
	if(defined($sbl_text)
		&& $sbl_text =~ s/.*?<pre\b[^>]*>(.*?)<\/pre>(?:.(?!<pre\b))*/$1/gs
	){
		# remove comments
		$sbl_text =~ s/\s*#.*//g;
		# remove empty lines
		$sbl_text =~ s/\n\K\n+//g;
		# remove preceding and trailing space
		$sbl_text =~ s/^\s+//;
		$sbl_text =~ s/\s+$//gm;
		$sbl_entries = [split /\n/, $sbl_text];
	}else{
		$self->msg(0, 'could not read sbl', 'error');
	}
	return $sbl_entries;
}

sub get_signature_from_last_edit{
	my $self = shift;
	my $page = shift;
	my $lang = shift // 'en';
	my @hist = $self->get_history($page, {'rvlimit' => 1});
	if(@hist == 0){
		$self->msg(0, "page '$page' does not seem to exist.", 'warning');
		return;
	}
	my $sig = $self->create_signature(
		$hist[0]->{'user'},
		$hist[0]->{'timestamp_date'} . 'T' . $hist[0]->{'timestamp_time'},
		$lang);
	return $sig;
}

sub get_table_sum{
	my $self = shift;
	my $table = shift;
	my $options = shift;
	unless(defined $options->{'rows'}){
		$options->{'rows'} = undef;
	}
	unless(defined $options->{'num_re'}){
		$options->{'num_re'} = qr/^\s*([0-9]+(?:[.,][0-9]+|))\s*\z/;
	}
	unless(defined $options->{'count_re'}){
		$options->{'count_re'} = undef;
	}
	unless(defined $table){
		$self->msg(0, 'table undefined', 'warning');
		return;
	}
	unless(ref $table eq 'HASH'){
		$self->msg(0, 'table must be a hash', 'warning');
		return;
	}
	unless(defined $table->{'header'}){
		$self->msg(0, 'table header undefined', 'warning');
		return;
	}
	unless(defined $table->{'body'}){
		$self->msg(0, 'table body undefined', 'warning');
		return ({}, {});
	}
	my $sum = {};
	my $num = {};
	for my $row(@{$table->{'body'}}){
		for(my $i = 0; $i < @$row; ++$i){
			if(defined $options->{'rows'}
				&& 0 == grep {$_ eq $table->{'header'}[$i]} @{$options->{'rows'}}
			){
				next;
			}
			if($row->[$i] =~ /$options->{'num_re'}/){
				my $value = $1;
				$value =~ y/,/./;
				$sum->{$table->{'header'}[$i]} += $value;
				++$num->{$table->{'header'}[$i]};
			}elsif(defined $options->{'count_re'} && $row->[$i] =~ /$options->{'count_re'}/){
				$sum->{$table->{'header'}[$i]} += length($1);
				++$num->{$table->{'header'}[$i]};
			}
		}
	}
	return ($sum, $num);
}

sub get_tables_from_wikitext{
	my $self = shift;
	my $wikitext = shift;
	return [grep {/^\{\|/} split /^(?=\{\|)|^\|\}\K$/m, $$wikitext];
}

sub get_text{
	my $self = shift;
	my $pagename = shift;
	my $additional_params = shift;
	my $text;
	if($MediaWiki::Bot::VERSION ge '5.007'){
		$text = $self->{'mw_bot'}->get_text($pagename, $additional_params);
	}else{
		$text = defined $additional_params
			? $self->{'mw_bot'}->get_text($pagename, $additional_params->{'rvstartid'})
			: $self->{'mw_bot'}->get_text($pagename);
	}
	unless(defined $text){
		$self->msg(0, "could not load page '$pagename'", 'warning');
	}
	return $text;
}

sub get_date_iso{
	my $self = shift;
	my $unixtimestamp = shift // time();
	return strftime("%Y-%m-%d", gmtime($unixtimestamp));
}

sub get_time_iso{
	my $self = shift;
	my $unixtimestamp = shift // time();
	return strftime("%Y-%m-%dT%H:%M:%S", gmtime($unixtimestamp));
}

sub get_time_iso_{
	my $self = shift;
	my $unixtimestamp = shift // time();
	return strftime("%Y-%m-%d %H:%M:%S", gmtime($unixtimestamp));
}

sub get_user_contribs{
	my $self    = shift;
	my $options = shift;
	my $ucstart = $options->{'ucstart'};
	my $ucend   = $options->{'ucend'};
	my $limit   = $options->{'limit'}; # this is not uclimit, but the max of total results
	my $user    = $options->{'username'} // $options->{'usernameprefix'};
	my $prop    = $options->{'ucprop'} // ['id', 'title'];
	my $result_style = $options->{'result_style'} // 'array'; # or 'per_page'
	$self->msg(1, "searching for contributions of user: $user");
	#my $ref = $self->{'mw_bot'}->contributions($options->{'username'}, 0, undef);
	my $query = {
		'action'    => 'query',
		'list'      => 'usercontribs',
		'ucprop'    => join('|', @$prop),
		'ucstart'   => $ucstart // 'now',
		'uclimit'   => defined $limit && $limit < 500 ? $limit : 500,
	};
	if(exists $options->{'username'}){
		$query->{'ucuser'}  = $user;
	}else{
		$query->{'ucuserprefix'}  = $user;
	}
	if(exists $options->{'ucshow'}){
		$query->{'ucshow'} = $options->{'ucshow'};
	}
	if(defined $options->{'namespace'}){
		$query->{'ucnamespace'}  = $options->{'namespace'};
	}
	$query->{'ucend'}   = $ucend if defined $ucend;
	my $mw_options = {};
	my $list_of_articles;
	my $die_on_error = 1; # 0 = dont die; 1 = die
	my $finished = 0;
	my $count = 0;
	my $continue = {};
	while(!$finished){
		(my $tmp, $finished) = $self->_api_cont(
			$query, $continue, $mw_options, !$die_on_error);
		my $usercontribs = $tmp->{'query'}{'usercontribs'};
		if(defined $usercontribs){
			# delete mostly redundant information
			for my $uc(@$usercontribs){
				delete $uc->{'user'};
				delete $uc->{'userid'};
			}
			if($result_style eq 'array'){
				push @$list_of_articles, @$usercontribs;
			}elsif($result_style eq 'per_page'){
				for my $uc(@$usercontribs){
					push @{$list_of_articles->{$uc->{'title'}}}, $uc;
				}
			}
			$count += @$usercontribs;
		}
		last if(defined $limit && $limit =~ /^[0-9]+\z/ && $count >= $limit);
	}
	return $list_of_articles;
}

sub get_wikitable_ascii_col_widths{
	my $self = shift;
	my $row_array = shift;
	my @counts;
	# save length of cells with their frequency
	for my $row(@$row_array){
		my @row_cell_lengths = map {$_ = length($_)} split /(?:\|\||!!)/, $row; ## no critic (MutatingListFunctions)
		for(my $i = 0; $i < @row_cell_lengths ; ++$i){
			++$counts[$i]->{$row_cell_lengths[$i]};
		}
	}
	#print Dumper \@counts;
	# get size with max frequency
	for my $count(@counts){
		my $length = 0;
		my $bak_count = 0;
		while(my ($size, $num_occ) = each %$count){
			if($num_occ > $bak_count){
				$bak_count = $num_occ;
				$length = $size + 0; # int
			}
		}
		$count = $length;
	}
	--$counts[0] if defined $counts[0]; # decrease, because of leading '|'
	#print Dumper \@counts;
	return \@counts;
}

sub _handle_api_error{
	my $self = shift;
	my $type = shift // 'api'; # or bot
	my $caller_inc = shift // 0;
	my $api = $type eq 'api' ? $self->{'mw_api'} : $self->{'mw_bot'}->{'api'};
	$self->msg(0, $api->{'error'}{'code'} . ': ' .
		$api->{'error'}{'details'}, 'error', 1 + $caller_inc);
	return 1;
}

sub has_access_type{
	my $self = shift;
	my $type = shift;
	my $found = grep {$_ eq $type} @{$self->{'access_types'}};
	return $found;
}

sub http2https{
	my $self = shift;
	my $url = shift;
	if($url =~ /^http:\/\//){
		my ($is_redir, $target_url) = $self->get_http_redir_location($url);
		if($is_redir){
			my $https_url = $url;
			$https_url =~ s/^http:/https:/;
			$https_url =~ s/\/\z//;
			if(defined($target_url) && $target_url =~ /^\Q$https_url\E\/?\z/){
				$url = $target_url;
			}elsif(!defined($target_url)){
				$self->msg(1, 'target_url could not be fetched from ' . $url , 'warning');
			}
		}
	}
	return $url;
}

sub i18n{
	my $self = shift;
	my @texts = @_;
	my %repl_de = (
		'abbr.. in links' => 'zweiter punkt hinter abkuerzung in querverweis',
		'abbr..' => 'zweiter punkt hinter abkuerzung',
		'automated edit' => 'automatisierte bearbeitung',
		'changed redir to target' => 'redirect durch ziel ersetzt',
		'comma before "sondern"' => 'komma vor "sondern"',
		'double full stop' => 'doppelter satz-ende-punkt',
		'for' => 'fuer',
		'hid too early requests'  => 'verfruehte requests versteckt',
		'minor improvement' => 'kleinere korrekturen',
		'now empty' => 'jetzt leer',
		'pending' => "ausstehend",
		'redundant link description' => 'redundante linkbeschreibung',
		'removal of tracking param' => 'entfernung von tracking-parameter',
		'removed invalid requests' => 'ungueltige requests entfernt',
		'removed reviewed pages' => 'gesichtete seiten entfernt',
		'removed never reviewed pages' => 'noch nie gesichtete seiten entfernt',
		'replaced with' => 'ersetzt durch',
		'review request' => 'sichtungsanfrage',
		'postponed' => 'zurueckgestellt',
		'see' => 'siehe',
		'superfluous br-tag in lists' => 'ueberfluessiges br-tag in liste"',
		'superfluous space in cats' => 'ueberfluessiges leerzeichen in kategorien"',
		'superfluous explicit "template:"' => 'ueberfluessige angabe von "vorlage:"',
		'u200e' => "[[H:SPUK]] \x{2192} [[U+200E]]",
		'whitespace before comma' => 'leerzeichen vor komma',
		'whitespace before ref end tag' => '[[whitespace]] vor ref-end-tag',
		'whitespace before ref-tag' => '[[whitespace]] vor <ref>',
		'whitespace in link right before comma' => 'leerzeichen in link vor komma',
		'wrong br-tag' => 'br-tag',
		'wrong left bracket' => 'einsame linke eckige klammer',
		'wrong right bracket' => 'einsame rechte eckige klammer',
		'wrong space before %; see [[WP:SVZ]]' => 'leerzeichen vor prozentzeichen, siehe [[WP:SVZ]]',
		'wrong tel: link' => 'ueberfluessiger tel: link',
		'zeros' => 'nullen',
	);
	my $num = 0;
	map {
		if(defined $self->{'wm_proj'} && $self->{'wm_proj'} eq 'dewiki'){
			if(exists $repl_de{$_}){
				$_ = $repl_de{$_};
				++$num;
			}
		}
	} @texts;
	return $texts[0] if @texts == 1;
	return @texts;
}

sub init_cleanup_replacements{
	my $self = shift;
	my $re_archive_pre_text = qr/<ref[^>]*>[^<]*|==\s*Weblinks?\s*==.*/s;
	my $re_archive_org = qr/https?:\/\/(?:wayback\.|web\.)?archive\.org\/web\//;
	my $re_archive_org_wo_title = qr/$re_archive_org
		(?<date>[0-9]{14})\/(?<url>[^\x20\]]+)/x;
	my $re_archive_org_w_title = qr/$re_archive_org_wo_title\x20(?<title>[^\]]*)/;
	my $re_archive_today = qr/$self->{'archive.today'}{'re_base_url'}\//;
	my $re_archive_today_wo_title = qr/$re_archive_today
		(?<date>[0-9]{4,14})\/(?<url>[^\x20\]]+)/x;
	my $re_archive_today_w_title = qr/$re_archive_today_wo_title\x20(?<title>[^\]]*)/;
	my $re_webcite = qr/https?:\/\/(?:www\.)?webcitation\.org\//;
	my $re_webcite_wo_title = qr/$re_webcite(?!query)(?<webciteID>[a-zA-Z0-9_]+)\?/;
	my $re_webcite_w_title = qr/$re_webcite_wo_title
		url=(?<url>http[^\x20\]]+)\x20(?<title>[^\]]*)/x;
	my $archive_repl_no_text = '"{{Webarchiv |url=" . $+{url} . "'
		. ' |wayback=" . $+{date} . "}}"';
	my $archive_repl = substr($archive_repl_no_text, 0, -4)
		. '" |text=" . $+{title}' . substr($archive_repl_no_text, -7);
	my $archivetoday_repl_no_text = $archive_repl_no_text;
	my $archivetoday_repl = $archive_repl;
	map {
		s/\|wayback/|archive-today/
	} ($archivetoday_repl_no_text, $archivetoday_repl);
	my $webcite_repl_no_text = '"{{Webarchiv |url=" . $+{url} . "'
		. ' |webciteID=" . $+{webciteID} . "}}"';
	my $webcite_repl = substr($webcite_repl_no_text, 0, -4)
		. '" |text=" . $+{title}' . substr($webcite_repl_no_text, -7);
	$self->{'cleanup_replacements'} = {
		'normal' => [{
				# == link fixes ==
				# === double protocol ===
				'key'    => 'double protocol',
				# examples => ['http:https://', '[//https://'],
				'search' => qr/(?:(?:http:|(?<=\[))\/\/)+(https?):?\/\//mx,
				'repl'   => '"$1:\/\/"',
				'eval'   => 1,
			},{
				# === span in doi links ===
				'key'    => 'span in doi link',
				# examples => ['[[doi:10.1038/nature05923|doi:10.1038/nature05923 <span></span>]]'],
				'search' => qr/(\[\[doi:)\s*([^| \[\]]+)\|doi:\s*[^| \[\]]+(?: <span><\/span>)+\]\]/i,
				'repl'   => '"$1$2]]"',
				'eval'   => 1,
			},{
				# === font tag ===
				'key'    => 'font tag',
				# examples => ['<font _mstmutation="1" _msthash="752895" _msttexthash="164788" _msthidden="1">Nancy Plain: </font>'],
				'search' => qr/<font _mstmutation=[^>]+>([^>]+)<\/font>/,
				'repl'   => '"$1"',
				'eval'   => 1,
			},{
				# === tel: protocol ===
				'key'    => 'wrong tel: link',
				# examples => ['[tel:276–2778 276–2778]'],
				'search' => qr/\[tel:[^ [\]]+ ([^\]]+)\]/,
				#'search' => qr/\[tel:(?:[-+.()\/\x{2012}\x{2013}]|&#x20;|[0-9]++)+ ([^\]]+)\]/,
				'repl'   => '"$1"',
				'eval'   => 1,
			},{
				# === missing/too much brackets ===
				#  was '"$1\x5b$2]$3"' until 2015-07-18, but deleting of such brackets seems
				#  more convenient
				'key'    => 'wrong left bracket',
				# examples => ['<ref>[https://www.example.org</ref>'],
				'search' => qr/(<ref>)\s*\[\s*(https?:\/\/$self->{'re_url_rear'})
					[\s!,.?]*+(<\/ref>)/mx,
				'repl'   => '"$1$2$3"',
				'eval'   => 1,
			},{
				'key'    => 'wrong right bracket',
				# examples => ['<ref>https://www.example.org]</ref>']
				'search' => qr/(<ref>)\s*(https?:\/\/$self->{'re_url_rear'})
					[\s!,.?]*+\]\s*(<\/ref>)/mx,
				'repl'   => '"$1$2$3"',
				'eval'   => 1,
			},{
				# == checking of spelling/typography ==
				'key'    => 'abbr..',
				# examples => [' Chr..', ' e.V..', 'usw..']
				'search' => qr/ (?:Chr|e\.(?: |&nbsp;)?V|ff|Jh|usw)\.\K\. /m,
				'repl'   => ' ',
			},{
				'key'    => 'abbr.. in links',
				# examples => ['.]].']
				'search' => qr/\.\]\]\K\./m,
				'repl'   => '',
			},{
				'key'    => 'double full stop',
				# examples => ['{{Literatur}}.']
				'cond'   => {
					'pre_text' => qr/
						\{\{\s*[Ll]iteratur\b
							[^{}]*?(?:Zitat\s*=(*SKIP)(*FAIL))?
						\}\}\s*\z/sx,
					'pre_type' => 'template',
				},
				'search' => qr/\A\s*\.(?!\.)/sm,
				'repl'   => '',
			},{
				'key'    => '%ig',
				# examples => ['5 %-ig']
				'search' => qr/[0-9]\K(?: |&nbsp;)?%-ig/m,
				'repl'   => '%ig',
			},{
				'key'    => 'comma before "sondern"',
				# examples => ['mehr sondern']
				#TODO: not in citations
				'search' => qr/(?:mehr|ist|wird|sind|werden|haben|hat)\K sondern/m,
				'repl'   => ', sondern',
			},{
				'key'    => 'comma before "indem"',
				# examples => ['wird indem']
				#TODO: not in citations
				'search' => qr/(?:ist|wird|sind|werden|haben|hat)\K indem/m,
				'repl'   => ', indem',
			},{
				'key'    => 'comma before "wobei"',
				# examples => ['sind wobei']
				#TODO: not in citations
				'search' => qr/(?:ist|wird|sind|werden|haben|hat)\K wobei/m,
				'repl'   => ', wobei',
			},{
				'key'    => 'whitespace before comma',
				# examples => [' , abgerufen']
				'search' => qr/\s+,\s+abgerufen/m,
				'repl'   => ', abgerufen',
				#},{
				#	'key'    => 'whitespace nach gestorben-symbol',
				## examples => ["\x{2020} "]
				#	'search' => qr/\x{2020}\K(?! |&nbsp;)/m,
				#	'repl'   => ' ',
			},{
				# == links: specific domains ==
				# fix spiegel.de (spon) links with wrong description
				'key'    => 'spon redirects',
				'cond'   => {
					'type' => 'template',
					'pre_text' => qr/<ref[^>]*>\s*$/s,
					'post_text' => qr/^\s*<\/ref>/s,
				},
				'search' => qr/(\{\{Internetquelle\s*\|\s*
					(?i:autor=spiegel[^|\}]+\s*\|)?
					url=https?:\/\/www\.spiegel\.de\/[^\x{20}|]*\s\|
					titel=DER\sSPIEGEL\s*\{\{!\}\}\s*Online-Nachrichten\s*\|
					(?:[a-z]+=\s*\|)*
					abruf=[0-9-]+
					(?:\s*\|[a-z]+=\s*)*
					\}\})/x,
				'repl'   => '$self->template_spon_converter($1) // $1',
				'eval'   => 1,
			},{
				# fix zeit.de links with wrong description
				'key'    => 'zeit.de redirects',
				'cond'   => {
					'type' => 'template',
					'pre_text' => qr/<ref[^>]*>\s*$/s,
					'post_text' => qr/^\s*<\/ref>/s,
				},
				'search' => qr/(\{\{Internetquelle\s*\|\s*
					(?:autor=Zeit[^|\}]+\s*\|)?
					url=https?:\/\/www\.zeit\.de\/[^\x{20}|]*\s\|
					titel=ZEIT\sONLINE\s[^|]{40,50}PUR-Abo\.[^|]{15,25}\|
					(?:[a-z]+=\s*\|)*
					abruf=[0-9-]+
					(?:\s*\|[a-z]+=\s*)*
					\}\})/x,
				'repl'   => '$self->template_zeitde_converter($1) // $1',
				'eval'   => 1,
			},{
			# fix short urls (such as archive.today), redirects, remove tracking params and alike,
			# see also maintenance list
				'key'    => 'url-converter',
				'cond' => {
					'type' => ['text', 'table'],
				},
				'search' => qr/(
						(?<=\[)https?:\/\/$self->{'re_url_rear_explicit'}
						|(?<=url\s{0,5}=\s{0,5})https?:\/\/$self->{'re_url_rear_maybetemplate'}
						|(?<!url\s{0,5}=\s{0,5})https?:\/\/$self->{'re_url_rear'}
					)/x,
				'repl'   => '$self->url_converter($1)',
				'eval'   => 1,
				'print_non_changes' => 0,
			},{
				'key'    => 'url-converter',
				'cond' => {
					'type' => 'template',
				},
				'search' => qr/\{\{\s*[Ww]ebarchive?\b\s*\|[^{}]*url\s*=\s*http(*SKIP)(*FAIL)
					|(https?:\/\/$self->{'re_url_rear_in_tpl'})/x,
				'repl'   => '$self->url_converter($1)',
				'eval'   => 1,
				'print_non_changes' => 0,
			},{
			# text fragment links
				'key' => 'rel. text fragment links',
				'search' => qr/
					# e.g. [[something#:~:text=anything|some text]]
					(\[\[[^|\]]+)(?:\x23.+?\K|\x23):(?:~|%7[eE]):text=[^|]++(\|.*?\]\])/mx,
				'repl'   => '"$1$2"',
				'eval'   => 1,
			},{
			# === webcite: convert normal archive-links to templates ===
				'key'    => '[[WebCite]]->templ. with title',
				'cond'   => {
					'second' => qr/$re_archive_pre_text\[$re_webcite_w_title\]/,
				},
				'search' => qr/\[$re_webcite_w_title\]/x,
				'repl'   => $webcite_repl,
				'eval'   => 1,
				'print_non_changes' => 0,
			},{
				'key'    => '[[archive.today]]->templ.',
				'cond'   => {
					'second' => qr/$re_archive_pre_text\[$re_webcite_wo_title\]/,
				},
				'search' => qr/\[$re_webcite_wo_title\]/x,
				'repl'   => $webcite_repl_no_text,
				'eval'   => 1,
				'print_non_changes' => 0,
			},{
			# test
				'key' => 'test',
				'search' => qr/seth sagt: hallo camelbot!/m,
				'repl' => 'camelbot sagt: hallo seth!',
			},
		],
		'optional' => [{
			# === unicode character 0x200e before categories ===
				'key'    => 'u200e',
				'search' => qr/\x{200e}(?=\]\]|\}\}|\|)/m,
				'repl'   => '',
			},{
			# === http2https ===
				'key'    => 'http2https',
				'cond' => {
					'type' => ['text', 'table'],
				},
				'search' => qr/(
						(?<=\[)http:\/\/$self->{'re_url_rear_explicit'}
						|(?<=url\s{0,5}=\s{0,5})http:\/\/$self->{'re_url_rear_maybetemplate'}
						|(?<!url\s{0,5}=\s{0,5})http:\/\/$self->{'re_url_rear'}
					)/x,
				'repl'   => '$self->http2https($1)',
				'eval'   => 1,
				'print_non_changes' => 0,
			},{
			# === superfluous space in cats ===
				'key'    => 'superfluous space in cats',
				'search' => qr/\[\[Kategorie:\K +/m,
				'repl'   => '',
			},{
			# === superfluous declaration as template via keyword ===
				'key'    => 'superfluous explicit "template:"',
				'search' => qr/\{\{\K[Vv]orlage:\s*/m,
				'repl'   => '',
			},{
			# === wrong html br-tag ===
				'key'    => 'wrong br-tag',
				# examples => ['</br>', '<\\br>', '<br.>', '<br\\>'],
				'search' => qr/<(?:[\/\\]br\s*|br[.\\])>/m,
				'repl'   => '<br />',
			},{
			# === superfluous br-tag ===
				'key'    => 'superfluous br-tag in lists',
				# examples => ['*moep<br />'],
				'search' => qr/^\*.*\K<(?:[\/\\]br\s*|br ?[.\\\/])> *$/m,
				'repl'   => '',
			},{
			# === wrong white space before '%' ===
				'key'    => 'wrong space before %; see [[WP:SVZ]]',
				# examples => ['&nbsp;%'],
				'search' => qr/&(?:nbsp|thinsp|#x202f|#8239);%/m,
				'repl'   => ' %',
				,
			},{
			# === wrong white space before ref-tag ===
				'key'    => 'whitespace before ref-tag',
				# examples => [' <ref'],
				'search' => qr/.(?<![|=\x{B0}\x{B2}\x{B3}]|^[*!;:])(?<!<\/sup>)
					(?<!\}\}|!!)\K(?:\x20|&(?:nbsp|thinsp);)+(?=<ref\b)/mx,
				'repl'   => '',
			},{
				'key'    => 'whitespace before ref-tag',
				# examples => [' <ref'],
				'cond'   => {
					'exclude_pre_type' => 'template',
				},
				'search' => qr/^(?:\x20|&(?:nbsp|thinsp);)+(?=<ref\b)/mx,
				'repl'   => '',
			},{
			# === too much white space before ref end tag ===
				'key'    => 'whitespace before ref end tag',
				# examples => ['&nbsp;</ref>'],
				'search' => qr/(?:\x20|&nbsp;)+(?=<\/ref>)/x,
				'repl'   => '',
			},{
			# === wrong white space inside of links before comma ===
			# optional only, because
			# https://de.wikipedia.org/w/index.php?title=Lithiumcyanid&diff=prev&oldid=193791759
			# https://de.wikipedia.org/w/index.php?title=Landkreis_Freudenstadt&diff=prev&oldid=193792887
				'key'    => 'whitespace in link right before comma',
				# examples => [' ], abgerufen', ' , abgerufen'],
				'search' => qr/\s+(\]?),\s+abgerufen/m,
				'repl'   => '"$1, abgerufen"',
				'eval'   => 1,
			},{
			# === wrong white space before celsius character ===
				'key'    => "\x{B0}C ([[WP:SVZ]])",
				# examples => [" \x{B0} C"],
				'search' => qr/(?: |&nbsp;|&thinsp;|)\x{B0}(?: |&nbsp;|&thinsp;)C\b/m,
				'repl'   => "&nbsp;\x{B0}C",
			},{
			# === redundant internal link description ===
				'key'    => 'redundant link description',
				# examples => ['[[moep|moep]]'],
				'search' => qr/\[\[\s*+([^|\]\[]+\S)\s*\|\s*+\1\s*+\]\]/m,
				'repl'   => '"[[$1]]"',
				'eval'   => 1,
			},{
			# === zeros ===
				'key'    => 'zeros',
				# examples => ['{{0}}{{0}}{{0}}'],
				'cond' => {
					'pre_type' => 'template',
					'pre_text' => qr/^\{\{\s*0\s*\}\}\z/,
					'exclude_post_text' => qr/^\{\{\s*0\s*\}\}\z/,
				},
				'group_backwards' => qr/^\{\{\s*0\s*\}\}\z/,
				'search' => qr/((?:\{\{0\}\}){2,})/m,
				'repl'   => '"{{0|" . ("0"x(length($1)/5)) . "}}"',
				'eval'   => 1,
			},{
			# see also https://gitlab.com/wp-seth/camelbot/-/issues/46
			# === archive.today and archive.org: convert double archive-links to templates ===
				'key'    => '[[archive.today]]->templ. with concatenated archives with title',
				'cond'   => {
					'second' => qr/$re_archive_pre_text\[
						$re_archive_today[0-9]{4,14}\/$re_archive_org_w_title]/x,
				},
				'search' => qr/\[$re_archive_today[0-9]{4,14}\/$re_archive_org_w_title]/x,
				'repl'   => $archive_repl,
				'eval'   => 1,
				'print_non_changes' => 0,
			},{
				'key'    => '[[archive.today]]->templ. with concatenated archives',
				'cond'   => {
					'second' => qr/$re_archive_pre_text\[
						$re_archive_today[0-9]{4,14}\/$re_archive_org_wo_title]/x,
				},
				'search' => qr/\[
					$re_archive_today[0-9]{4,14}\/$re_archive_org_wo_title]/x,
				'repl'   => $archive_repl_no_text,
				'eval'   => 1,
				'print_non_changes' => 0,
			},{
			# === archive.org: convert normal archive-links to templates ===
				'key'    => '[[archive.org]]->templ. with title',
				'cond'   => {
					'second' => qr/$re_archive_pre_text\[$re_archive_org_w_title\]/,
				},
				'search' => qr/\[$re_archive_org_w_title\]/x,
				'repl'   => $archive_repl,
				'eval'   => 1,
				'print_non_changes' => 0,
			},{
				'key'    => '[[archive.org]]->templ.',
				'cond'   => {
					'second' => qr/$re_archive_pre_text\[$re_archive_org_wo_title\]/,
				},
				'search' => qr/\[$re_archive_org_wo_title\]/,
				'repl'   => $archive_repl_no_text,
				'eval'   => 1,
				'print_non_changes' => 0,
			},{
			# === archive.today: convert normal archive-links to templates ===
				'key'    => '[[archive.today]]->templ. with title',
				'cond'   => {
					'second' => qr/$re_archive_pre_text\[$re_archive_today_w_title\]/,
				},
				'search' => qr/\[$re_archive_today_w_title\]/,
				'repl'   => $archivetoday_repl,
				'eval'   => 1,
				'print_non_changes' => 0,
			},{
				'key'    => '[[archive.today]]->templ.',
				'cond'   => {
					'second' => qr/$re_archive_pre_text\[$re_archive_today_wo_title\]/,
				},
				'search' => qr/\[$re_archive_today_wo_title\]/,
				'repl'   => $archivetoday_repl_no_text,
				'eval'   => 1,
				'print_non_changes' => 0,
			},
		],
	};
	if(defined $self->{'wm_lang'} && defined $self->{'wm_proj_type'}){
		push @{$self->{'cleanup_replacements'}{'optional'}}, {
			# === technically superfluous external links ===
			'key'    => 'linkfix pseudo-external links',
			'search' => qr/(?<!<ref>)\[{1,2}
			(https?:\/\/$self->{'wm_lang'}\.$self->{'wm_proj_type'}\.org\/wiki\/
			[^\]|\x20\x23?]++
			(?:\x23(?!mediaviewer)[^\]|\x20?]+)?)\s
			([^\]]+)
			\]{1,2}/mx,
			'repl'   => '"[[".$self->url2title($1).($self->url2title($1) eq $2 ? '
			. '"" : "|$2")."]]"',
			'eval'   => 1,
			'cond' => {
				'page_exception_re' => qr/Hypertext[ _]Transfer[ _]Protocol[ _]Secure/,
			},
		};
	}
	return 1;
}

sub init_re_url_tracking_filter{
	my $self = shift;
	$self->{'re_url_tracking_filter'} = {
		'key' => {
			'adword' => 1,
			'amp_gsa' => 1,  # e.g. www.sueddeutsche.de/...?amp_js_v=a3&amp_gsa=1&usqp=mq33...
			'amp_js_v' => 1,  # e.g. www.sueddeutsche.de/...?amp_js_v=a3&amp_gsa=1&usqp=mq33...
			'_branch_referrer' => 1,  # e.g. podcasters.spotify.com/...?%24web_only=true&_branch_match_id=...&_branch_referrer=...
			'dmcid' => 1,
			'fbc' => 1,  # facebook
			'fbp' => 1,  # facebook
			'igshid' => 1,  # instagram
			'intpro' => 1,
			'jsessionid' => 1,  # see also url_converter
			'mbid' => 1,  # https://pitchfork.com/.../?utm_social-type=owned&mbid=social_twitter
			'mc_cid' => 1,  # facebook
			'mktcid' => 1,
			'mktcval' => 1,
			'ncid' => 1,
			'pk_kwd' => 1, # www.dresden.de/....php?pk_campaign=Shortcut&pk_kwd=gps-referenzpunkt
			'returnto' => 1,  # implicit tracking
			'__s' => 1,  # drip
			'__twitter_impression' => 1,
			'wtrid' => 1,
			'zanpid' => 1,
		},
		'keyval' => [
			qr/^et_[clr]id=/i,
			qr/^ga_(?:campaign|content|medium|place|source|term)=/i,
			qr/^mktfb(?:adn|adsetn|campn|p|plat)=/i,
			qr/^sc_(?:src|[lu]+id)=/i,
			qr/^utm_[a-zA-Z\[\]%0-9]+=/i,  # urchin, google
			qr/^amp(?:=.*)?\z/i,
			qr/^goal=[0-9a-f_-]+\z/i,
			qr/^outputType=amp\z/i,
			qr/^ref=rss\z/i,
			qr/^wtmc=socialmedia[a-z.]+/i,
		],
		'domain-specific' => {
			qr/ebay\.de\z/ => {
				'key' => {
					'abcId' => 1,
					'chn' => 1,
					'device' => 1,
					'merchantid' => 1,
					'mkcid' => 1,
					'mkevt' => 1,
					'mkgroupid' => 1,
					'mkrid' => 1,
					'mktype' => 1,
					'norover' => 1,
					'poi' => 1,
				},
			},
			qr/facebook\.com\z/ => {
				'key' => {'eid' => 1, '__tn__' => 1},
			},
			qr/focus\.de\z/ => {
				'key' => {'ts' => 1, 'cid' => 1},
			},
			qr/handelsblatt\.com\z/ => {
				'key' => {'ticket' => 1},
			},
			qr/heise\.de\z/ => {
				'key' => {'agn' => 1, 'userid' => 1},
			},
			qr/lequipe\.fr\z/ => {
				'key' => {'prov' => 1, 'm_i' => 1, 'M_BT' => 1},
			},
			qr/nytimes\.com\z/ => {
				'key' => {
					'action' => 1,
					'contentCollection' => 1,
					'emc' => 1,
					'instance_id' => 1,
					'module' => 1,
					'nl' => 1,
					'pgtype' => 1,
					'regi_id' => 1,
					'region' => 1,
					'segment_id' => 1,
					'te' => 1,
					'user_id' => 1,
					'version' => 1,
				},
			},
			qr/twitter\.com\z/ => {
				'key' => {'tfw_site' => 1},
			},
			qr/youtube\.com\z/ => {
				'key' => {'si' => 1},
			},
		},
		'whitelist' => {},
	},
	$self->load_external_url_param_tracking_list();
	# see https://de.wikipedia.org/w/index.php?title=Bauwerke_in_Bockenheim&diff=prev&oldid=239478396
	# example: http://www.historische-wertpapiere.de/de/HSK-Auktion-XXXI/?AID=86087&AKTIE=Deutsch-Schweizerische+Verwaltungsbank+AG
	delete $self->{'re_url_tracking_filter'}{'key'}{'AID'};
	# https://de.wikipedia.org/w/index.php?title=Rolex_Sports_Car_Series_2012&diff=prev&oldid=239484339
	# example: http://www.grand-am.com/scheduleResults/results.aspx?eid=3428&sid=1
	delete $self->{'re_url_tracking_filter'}{'key'}{'eid'};
	# https://de.wikipedia.org/w/index.php?title=National_Public_Radio&diff=prev&oldid=240084758&diffmode=source
	# example: http://www.publicradiofan.com/cgibin/source.pl?cmd=ps&sourceid=51
	delete $self->{'re_url_tracking_filter'}{'key'}{'sourceid'};
	# https://de.wikipedia.org/w/index.php?title=Santa_Maria_Assunta_%28Muggia%29&diff=244700483&oldid=244699722
	# https://patrimonioculturale.regione.fvg.it/architettura/?s_id=445490
	delete $self->{'re_url_tracking_filter'}{'key'}{'s_id'};
	# https://de.wikipedia.org/w/index.php?title=Liste_der_Fu%C3%9Fballspielerinnen_mit_mindestens_100_L%C3%A4nderspielen&diff=prev&oldid=253019495
	# https://www.thecfa.cn/src/PlayerBio/WomanPlayerBio.html?data1=420154
	delete $self->{'re_url_tracking_filter'}{'key'}{'data1'};
	# https://de.wikipedia.org/w/index.php?title=Kino_in_Hamburg&diff=241939501&oldid=241938566
	# example: https://www.filmmuseum-hamburg.de/kinos/kino-datenbank/kinos-von-a-z.html?ds_id=511
	$self->{'re_url_tracking_filter'}{'keyval'} = [
		grep {!/\^ds_/} @{$self->{'re_url_tracking_filter'}{'keyval'}}];
	return 1;
}

sub is_allowed{
	my $self    = shift;
	my $textref = shift;
	my $page    = shift // 'unknown';
	my $allowed = 1;
	# adapted code from https://en.wikipedia.org/wiki/Template:Bots
	if(defined $$textref){
		# nobots/bots/inuse template
		if($$textref =~ /\{\{\s*(?: # for vim: { { { { { {
			[nN]obots
			|[iI]n(?:use|\sBearbeitung)(?:\s*\|.*?|)
		)\s*}}/x){
			$allowed = 0;
		}elsif($$textref =~ /\{\{\s*[bB]ots\s*}}/){
			#$allowed = 1;
		}elsif($$textref =~ /\{\{\s*
			(?<template_name>
				[bB]ots\s*\|\s*(?<allow_or_deny>allow|deny)\s*=
				|[nN]obots\s*\|
			)
			\s*(?<names>.*?)\s*}}/sx
		){
			my $invert = lc(substr($+{'template_name'}, 0, 6)) eq 'nobots'
				|| $+{'allow_or_deny'} eq 'deny';
			if($+{'names'} eq 'all'){
				#$allowed = 1;
			}elsif($+{'names'} eq 'none'){
				$allowed = 0;
			}else{
				my @bots = split(/\s*,\s*/, $+{'names'});
				$allowed = (grep $_ eq $self->{'mw_username'}, @bots) ? 1 : 0;
			}
			if($invert){
				$allowed = int(!$allowed);
			}
		}
		# for vim: { {
		#if(defined($opt) && $$textref =~ /\{\{[bB]ots\s*\|\s*optout\s*=\s*(.*?)\s*}}/s){
		#	return 0 if $1 eq 'all';
		#	my @opt = split(/\s*,\s*/, $1);
		#	return (grep $_ eq $opt, @opt) ? 0 : 1;
		#}
	}
	$self->msg(1, "bot is not allowed on page '$page'.") if $allowed == 0;
	return $allowed;
}

sub is_ip{
	my $self = shift;
	my $ip = shift;
	return $self->is_ipv4($ip) || $self->is_ipv6($ip);
}

sub is_ipv4{
	my $self = shift;
	my $ip = shift;
	return $ip =~ /^(?:
			25[0-5]
			|2[0-4][0-9]
			|[01]?[0-9][0-9]?
		)(?:\.(?:
			25[0-5]
			|2[0-4][0-9]
			|[01]?[0-9][0-9]?
		)){3}\z/x;
}

sub is_ipv6{
	my $self = shift;
	my $ip = shift;
	my $h = '[0-9a-fA-F]{1,4}';
	return $ip =~ /^(?:
		(?:$h:){7}$h
		|(?:$h:){1,7}:
		|(?:$h:){1,6}:$h
		|(?:$h:){1,5}(?::$h){1,2}
		|(?:$h:){1,4}(?::$h){1,3}
		|(?:$h:){1,3}(?::$h){1,4}
		|(?:$h:){1,2}(?::$h){1,5}
		|$h:(?:(?::$h){1,6})
		|:(?:(?::$h){1,7}|:)
		|fe80:(?::[0-9a-fA-F]{0,4}){0,4}%[0-9a-zA-Z]+
		|::(?:ffff(?::0{1,4})?:)?
			(?:(?:25[0-5]|(2[0-4]|1?[0-9])?[0-9])\.){3}
			(?:25[0-5]|(2[0-4]|1?[0-9])?[0-9])
		|(?:$h:){1,4}:
			(?:(?:25[0-5]|(2[0-4]|1?[0-9])?[0-9])\.){3}
			(?:25[0-5]|(2[0-4]|1?[0-9])?[0-9])
	)\z/x;
}

sub is_logged_in{
	my $self = shift;
	return $self->{'logged_in'};
}

sub is_page_in_maintenance_table{
	my $self  = shift;
	my $page  = shift; # hash ref {page_with_ns => '.', ...}
	my $table = shift; # hash ref {body => [...], ...}
	my $additional_col = shift // []; # array ref ['category' , ...]
	return unless defined $table->{'body'};
	my $is_page_in_maintenance_table = grep {
		my $row = $_; # table row
		my $is_in_list = # check first/main column
			(defined $page->{'page_with_ns'}
				&& $row->[0] =~ /^\[\[:?\Q$page->{'page_with_ns'}]]\E/) ? 1 : 0;
		if($is_in_list){ # check other columns, if page title is in list
			for(my $i = 0; $i < @$additional_col; ++$i){
				# if additional column is set as key in page hash, then compare values.
				# if at least one column differs from page attribute, then treat page as
				# not in list.
				unless(defined $page->{$additional_col->[$i]} &&
					defined $row->[$i + 1] &&
					$page->{$additional_col->[$i]} eq $row->[$i + 1]
				){
					$is_in_list = -1;
					last;
				}
			}
		}
		if($is_in_list > 0){
			$self->msg(1, "page '$page->{'page_with_ns'}' already in list");
		}
		$is_in_list > 0;
	} @{$table->{'body'}};
	return $is_page_in_maintenance_table > 0;
}

sub is_page_redirect{
	my $self     = shift;
	my $page     = shift;
	my $text_ref = shift;
	my $redirect;
	unless(defined $text_ref){
		unless(defined $page){
			$self->msg(0,
				'page and text not defined. what the hell do you expect?', 'warning');
			return;
		}
		$$text_ref = $self->get_text($page);
	}
	return unless defined $$text_ref;
	my $redirect_aliases = $self->get_magicwords_alias('redirect');
	for my $alias(@$redirect_aliases){
		if($$text_ref =~ /^\Q$alias\E\s+\[\[(.*?)\]\]/i){
			$redirect = $1;
			last;
		}
	}
	return $redirect;
}

sub link_delete_in_text{
	my $self         = shift;
	my $text         = shift; # ref to string
	my $re_prot_part = shift; # regexp
	my $re_url_part  = shift; # regexp
	my $namespace0   = shift; # bool
	my $del_options  = shift; # hash ref
	my $re_url = $re_prot_part . $re_url_part . $self->{'re_url_class'};
	my $re_descr_class = qr/[^\]\x00-\x08\x0a-\x1F]/;
	my $use_eval_in_repl = 1;
	my $print_non_changes = 1;
	my $success = 1;
	if($namespace0){
		if($del_options->{'refs'}){
			if($del_options->{'ref2deadlink'}){
				if($$text !~ /url\s*=\s*$re_url/){ # special templates
					$self->test_and_replace($text,
						qr/
							(
								<(?i:ref)\b[^>]*>\s*          # <ref>
								(?s:(?!<ref)(?!<\/ref>).)*?   # blabla
							)(?|
								\[($re_url*+)(\x20[^\]]+|)\]| # [url] or
								($re_url*+)()                # url (not preceeded by "url = ")
							)(
								.*?                           # blabla
								<\/ref>                       # <\/ref>
							)/x,
						'"${1}[{{dead link|inline=yes|bot=' . $self->{'mw_username'}
							. '|date=' . $self->get_date_iso() . '|url=$2}}$3]$4"',
						$use_eval_in_repl, $print_non_changes
					);
				}
			}else{
				my $re_ref_tag_name = qr/<ref [^>]*name\s*=\s*['"]?([^>]*?)['"]?>/;
				my $re_ref_inner_part =
					qr/(?s:(?!<ref)(?!<\/ref>).)+?$re_url_part.*?<\/ref>/;
				# backrefs
				if($$text =~ /$re_ref_tag_name$re_ref_inner_part/){
					my $ref_name = $1;
					$self->msg(1, $ref_name, 'debug');
					$self->test_and_replace(
						$text,
						qr/<(?i:ref) name\s*=\s*['"]?\s*$ref_name\s*['"]?\s*\/\s*>/,
						'', !$use_eval_in_repl, $print_non_changes
					);
				}
				# refs
				$self->test_and_replace(
					$text,
					qr/<(?i:ref)\b[^>]*>\s*$re_ref_inner_part/,
					'', !$use_eval_in_repl, $print_non_changes
				);
			}
		}
		# <references />
		$self->delete_empty_ref($text);
		if($del_options->{'nonrefs'}){
			# links like [link descr] in lists (as in "external links")
			$self->test_and_replace(
				$text,
				qr/^\*(?i:.(?!<ref))*\[$re_url*?\s*$re_descr_class*\].*\n?/m,
				'', !$use_eval_in_repl, $print_non_changes
			);
			# plain links in lists (as in "external links")
			$self->test_and_replace(
				$text,
				qr/^\*(?i:.(?!<ref))*$re_url*?.*\n?/m,
				'', !$use_eval_in_repl, $print_non_changes
			);
		}
		# heading "weblinks" without entries
		my $weblinks_heading_re = qr/^=(=+)\s*Weblinks?\s*=\1[\s\n]*\n
			(\[\[[Kk]atego|\{\{
				(?:Coordinate|DEFAULTSORT|Hinweis\x20|(?:Vorlage:)?Navigations
					|Normdaten|(?:Vorlage:)?Orden,|SORTIERUNG)
				|(?:<!--\x20?)?==)/mx;
		$self->test_and_replace($text, $weblinks_heading_re, '"$2"',
			$use_eval_in_repl, $print_non_changes);
		if($$text =~ /(.*)($re_url_part.*)/){
			my $pre = $1;
			my $url_and_post = $2;
			if(length($pre) > 10 && substr($pre, -1) eq '/'){
				$success = 0;
				if($pre =~ /https?:\/\/web\.archive\.org\/web\/(?:[0-9]+|\*)\/\z/){
					$self->msg(1, " not resolved, because archived: $pre$url_and_post");
				}else{
					$self->msg(1, " not resolved: $pre$url_and_post");
				}
			}elsif($pre !~ /\|\s*url\s*=\s*$re_prot_part\z/){
				$success = 0;
				$self->msg(1, " not resolved: $pre$url_and_post");
			}
		}
	}else{
		if($del_options->{'nonrefs'}){
			# [link]
			$self->test_and_replace($text,
				qr/\[$re_prot_part($re_url_part$self->{'re_url_class'}*?)\]/,
				'$1', $use_eval_in_repl, $print_non_changes);
			# [link descr]
			$self->test_and_replace($text,
				qr/(?<!<nowiki>)(\[$re_url*\s*$re_descr_class*\])/,
				'"<nowiki>".$1."<\/nowiki>"', $use_eval_in_repl, $print_non_changes);
			# plain_link or word:plainlink
			$self->test_and_replace($text,
				qr/(?<!$self->{'re_url_class'})(?<!<nowiki>\[)([a-zA-Z0-9]*:|)
					$re_prot_part($re_url_part$self->{'re_url_class'}*)/x,
				'$1.$2', $use_eval_in_repl, $print_non_changes);
			# ==plain_link==
			$self->test_and_replace($text,
				qr/^(=+) *$re_prot_part($re_url_part$self->{'re_url_class'}*) *\1 */m,
				'$1." ".$2." ".$1', $use_eval_in_repl, $print_non_changes);
			# :*plain_link
			$self->test_and_replace($text,
				qr/^([:\s*]*)$re_prot_part($re_url_part$self->{'re_url_class'}*)/m,
				'$1.$2', $use_eval_in_repl, $print_non_changes);
			# (plain_link)
			$self->test_and_replace($text,
				qr/([()]\s*)$re_prot_part($re_url_part$self->{'re_url_class'}*)(\s*\)|\s)/,
				'$1.$2.$3', $use_eval_in_repl, $print_non_changes);
			# {|plain_link|}
			$self->test_and_replace($text,
				qr/([|{]\s*)$re_prot_part($re_url_part$self->{'re_url_class'}*)(\s*[|}])/,
			'$1.$2.$3', $use_eval_in_repl, $print_non_changes);
		}
 		if($$text =~ /^(.*(?<!<nowiki>)(?<!<nowiki>\[)$re_prot_part$re_url_part.*)/m){
			$success = 0;
 			$self->msg(1, " not resolved: $1");
 		}
 		# get all unresolved links:
		#while($$text =~ /(^.*$re_prot_part$re_url_part.*)/gmo){
		#	my $temp = $1;
		#	if($temp =~ /(?<!<nowiki>)(?<!<nowiki>\[)$re_prot_part$re_url_part/){
		#		$self->msg(1, " not resolved: $temp");
		#	}
		#}
	}
	return $success;
}

sub link_modification{
	my $self         = shift;
	my $summary      = shift; # wiki summary of page edit
	my $re_prot_part = shift; # regexp pattern of protocol, e.g., /http:\/\//
	# regexp pattern of searched url (without protocol)
	#  e.g., /(?:www\.)?example\.com/
	my $re_url_part  = shift;
	my $params       = shift;
	my $del_params   = shift;
	# replacement function
	my $replacement              = $params->{'replacement'};
	my (%options, %del_options);
	# 1 = delete links
	$del_options{'delete_link'}  = $del_params->{'delete_link'};
	# 0 = don't touch non-refs; 1 = replace non-refs
	$del_options{'nonrefs'}      = $del_params->{'nonrefs'} //  1;
	# 0 = don't touch refs; 1 = replace refs
	$del_options{'refs'}         = $del_params->{'refs'}    //  1;
	# 1 = replace links in refs by 'dead link' template.
	$del_options{'ref2deadlink'} = $del_params->{'ref2deadlink'};
	# pages to be skipped;
	$options{'exception_pages'}  = $params->{'exception_pages'};
	# linksearch: link to use in api link search
	# if user contributions shall be searched...
	# params to use for getting the pages to be used for link modification
	my $search_type = $params->{'search'}{'get_pages_by'};
	unless(defined $search_type){
		$self->msg(0, 'get_pages_by (search type) undefined', 'error');
		return 0;
	}
	my $search_opts = $params->{'search'}{$search_type};
	unless(defined $search_opts){
		$self->msg(0, "search for '$search_opts' undefined", 'error');
		return 0;
	}
	# 0 = don't append on ns 0, 1 = append on ns 0
	$options{'articles'}         = $params->{'articles'}    //  0;
	# 0 = don't append on ns!=0, 1 = append on ns!=0
	$options{'nonarticles'}      = $params->{'nonarticles'} //  0;
	# maximum number of edits (-1 = inf)
	$options{'max_edits'}        = $params->{'max_edits'}   // -1;
	# skip first n pages.
	$options{'skip_edits'}       = $params->{'skip_edits'}  //  0;
	# print results (1) or don't (0)
	$options{'results'}          = $params->{'results'}     //  0;
	my $edit_counter = 0;          # edit counter
	my $old_page_0 = {};           # list of edited pages in ns==0
	my $old_page_1 = {};           # list of edited pages in ns!=0
	# some regexp vars should make source more readable
	my $re_url = $re_prot_part . $re_url_part . $self->{'re_url_class'};
	my %results = (
		'num_ns0_links'  => 0,
		'num_ns!0_links' => 0,
	);
	my $use_eval_in_repl = 1;
	if($options{'articles'}){
		my $namespaces = [0];
		my $namespace0 = 1;
		my $links = $self->link_search_pages(
			$search_type, $search_opts, $namespaces, $re_url
		);
		$results{'num_ns0_links'} = 0 + @$links;
		for my $hash(@$links){
			my $page = $hash->{'title'};
			next if exists $old_page_0->{$page};
			$old_page_0->{$page} = 1;
			if(0 < grep {$_ eq $page} @{$options{'exception_pages'}}){
				$self->msg(1, "skip page '$page' (defined exception)");
				next;
			}
			if(++$edit_counter < $options{'skip_edits'} || $options{'max_edits'} == 0){
				next;
			}elsif($edit_counter > $options{'max_edits'}
					and $options{'max_edits'} != -1){
				last;
			}
			#next unless $hash->{'url'} =~ /$re_prot_part$re_url_part/;
			$self->msg(1, "$edit_counter: page= $page, url= " . $hash->{'url'});
			$self->link_replace_in_page(
				$page, $summary, $replacement, \%del_options,
				$re_prot_part, $re_url_part, $namespace0
			);
		}
		$results{'num_ns0_unique_links'} = $edit_counter;
	}
	if($options{'nonarticles'}){
		# get all namespaces, except from 0
		my $namespaces = {$self->{'mw_bot'}->get_namespace_names()};
		$namespaces = [grep {$_ != 0} keys %$namespaces];
		my $namespace0 = 0;
		my $links = $self->link_search_pages(
			$search_type, $search_opts, $namespaces, $re_url
		);
		$results{'num_ns!0_links'} = 0 + @$links;
		for my $hash(@$links){
			my $page = $hash->{'title'};
			next if exists $old_page_1->{$page};
			$old_page_1->{$page} = 1;
			if(++$edit_counter < $options{'skip_edits'} or $options{'max_edits'} == 0){
				next;
			}elsif($edit_counter > $options{'max_edits'}
					and $options{'max_edits'} != -1){
				last;
			}
			$self->msg(1, "$edit_counter: page= $page, url= " . $hash->{'url'});
			$self->link_replace_in_page(
				$page, $summary, $replacement, \%del_options,
				$re_prot_part, $re_url_part, $namespace0
			);
		}
		$results{'num_ns!0_unique_links'} =
			$edit_counter - ($results{'num_ns0_unique_links'} // 0);
	}
	if($options{'results'}){
		print "\n";
		$self->msg(1, "results:");
		print " found " . ($results{'num_ns0_links'} + $results{'num_ns!0_links'}) .
			' links in ' . (0 + keys(%$old_page_0) + keys(%$old_page_1)) . " pages:\n";
		if($options{'articles'} && $options{'nonarticles'}){
			print "       " . $results{'num_ns0_links'} . " links in "
				. $results{'num_ns0_unique_links'} . " pages in ns==0\n";
			print "       " . $results{'num_ns!0_links'} . " links in "
				. $results{'num_ns!0_unique_links'} . " pages in ns!=0\n";
		}
		if($options{'articles'}){
			print "\n pages in ns==0:\n";
			print "  $_\n" for sort keys %$old_page_0;
		}
		if($options{'nonarticles'}){
			print "\n pages in ns!=0:\n";
			print "  $_\n" for sort keys %$old_page_1;
		}
	}
	return 1;
}

sub link_replace_in_page{
	my $self             = shift;
	my $page             = shift; # string
	my $summary          = shift; # string
	my $replacement      = shift; # string or sub or undef
	my $del_options      = shift; # hash ref
	my $re_prot_part     = shift; # regexp
	my $re_url_part      = shift; # regexp
	my $namespace0       = shift; # array
	my $use_eval_in_repl = 1;
	my $print_non_changes = 1;
	my $text = $self->get_text($page);
	return unless defined $text;
	my $success = 0;
	my $text_bak = $text;
	my ($clean_changes, $clean_summary) = $namespace0
		? $self->cleanup_wiki_page(\$text, $page) : ({}, '');
	my $changes;
	if(defined $replacement){
		$changes = $self->link_replace_in_text(\$text, $replacement,
			$re_prot_part . $re_url_part, $namespace0);
		$success = 1;
		my $alt_summary = @$changes == 0 ? $clean_summary : $summary . ' ' . $clean_summary;
		$self->save_wiki_page($page, $alt_summary, \$text, \$text_bak);
	}elsif($del_options->{'delete_link'}){
		$success = $self->link_delete_in_text(\$text,
			$re_prot_part, $re_url_part, $namespace0, $del_options);
		$self->save_wiki_page($page, $summary, \$text, \$text_bak);
	}
	if($namespace0){
		# clean-up bot messages on talk page
		if(defined $replacement && @$changes > 0 || $del_options->{'delete_link'}){
			my $page = 'talk:' . $page;
			$self->msg(1, "clean talk page '$page'");
			my $text = $self->get_text($page);
			if(defined $text){
				my $text_bak = $text;
				# delete link in bot message
				if($del_options->{'delete_link'}){  #{ vim
					$text =~ s/(\{\{Defekter\s+Weblink\|Bot=[^}]*\}\})/
						join "\n", grep {$_ !~ \/$re_prot_part$re_url_part\/} split \/\n\/, $1;
					/se;
				}else{  #{ vim
					$text =~ s/(\{\{Defekter\s+Weblink\|Bot=[^}]*\}\})/
						join "\n", grep {
							!($_ =~ \/$re_prot_part$re_url_part\/g && 0 < grep {$_->{'match'} eq ${^MATCH}} @$changes)
						} split \/\n\/, $1;
					/se;
				}
				# clean thread if empty now
				$self->test_and_replace(
					\$text,
					#  https?:\/\/  example\.org
					qr/==\s+\{\{Anker\|deadurl_[0-9-]+\}\}\s*Defekter?\s+Weblinks?\s+==\s
					\{\{nicht\s+archivieren\|Zeigen=nein\}\}
					\{\{Defekter\s+Weblink\|Bot=\S+\s*\}\}\s+
					\x{2013}\s*\[\[[^\]]+Bot]][^\r\n]+\s*
					(?===|$)
					/sx,
					'', !$use_eval_in_repl, $print_non_changes
				);
				$self->save_wiki_page($page, $summary, \$text, \$text_bak);
			}
		}
	}
	return $success;
}

sub link_replace_in_text{
	my $self             = shift;
	my $text             = shift; # ref to string
	my $replacement      = shift; # string or sub
	my $re_prot_url_part = shift; # regexp
	my $namespace0       = shift; # array
	my $use_eval_in_repl = 1;
	my $print_non_changes = 1;
	my $changes = [];
	if($namespace0){
		my $repl_mod;
		if(ref $replacement eq 'CODE'){
			$repl_mod = sub {
				my $camelbot = shift;
				my $url      = shift;
				my $title = eval('$+{text}');
				my $return = $replacement->($camelbot, $url);
				$return eq $url ? $url : '[' . $return . ' ' . $title . ']';
			};
		}else{
			$repl_mod = '"[" . ' . $replacement . ' . " $+{text}]"';
		}
		my $repl_changes = $self->test_and_replace(
			$text,
			# template webarchive
			qr/\{\{\s*
					[Ww]ebarchive?\s*\|\s*
					text\s*=\s*(?<text>[^|]+?)\s*\|\s*
					url\s*=\s*$re_prot_url_part
						(?<trail>$self->{'re_url_class_in_tpl'}*)\s*\|\s*
					wayback\s*=\s*[0-9]+\s*\|\s*
					archiv-bot\s*=\s*[^|\}]*\s*
				\}\}/x,
			$repl_mod, $use_eval_in_repl, $print_non_changes
		);
		push @$changes, @$repl_changes;
		# same with changed order of params url and text
		$repl_changes = $self->test_and_replace(
			$text,
			# template webarchive
			qr/\{\{\s*
					[Ww]ebarchive?\s*\|\s*
					url\s*=\s*$re_prot_url_part
						(?<trail>$self->{'re_url_class_in_tpl'}*)\s*\|\s*
					text\s*=\s*(?<text>[^|]+?)\s*\|\s*
					wayback\s*=\s*[0-9]+\s*\|\s*
					archiv-bot\s*=\s*[^|\}]*\s*
				\}\}/x,
			$repl_mod, $use_eval_in_repl, $print_non_changes
		);
		push @$changes, @$repl_changes;
	}
	# "normal" links
	my $re_deadlink = qr/ # vim {{
		\|offline\s*=\s*ja\s*\|\s*archiv-url\s*=\s*[^}]*?\}\}
		|\{\{\s*(?:[dD]ead\slink|Toter\sLink)\s*\|[^}]+\}\}/x;
	my $repl_changes = $self->test_and_replace(
		$text,
		#  https?:\/\/  example\.org
		qr/$re_prot_url_part
			(?<trail>$self->{'re_url_class'}*)(?:(?<posturl>.*?)$re_deadlink)?/mx,
		$replacement, $use_eval_in_repl, $print_non_changes
	);
	push @$changes, @$repl_changes;
	return $changes;
}

sub external_link_search{
	my $self          = shift;
	my $searched_link = shift; # e.g. '*.example.com' or undef
	my $namespaces    = shift // [0];
	my $links = [];
	my $options = {'max' => 100_000}; # 0 not working in MWBot 5.6.3
	if(defined $searched_link){
		@$links = $self->{'mw_bot'}->linksearch(
			$searched_link, $namespaces, undef, $options);
		my $bucket = {};
		map {++$bucket->{$_->{'url'} . ' ' . $_->{'title'}}} @$links;
		my @https_links = $self->{'mw_bot'}->linksearch(
			$searched_link, $namespaces, 'https', $options);
		map {
			if(++$bucket->{$_->{'url'} . ' ' . $_->{'title'}} == 1){
				push @$links, $_;
			}
		} @https_links;
	}
	return $links;
}

sub link_search_by_user{
	my $self          = shift;
	my $user          = shift; # e.g. 'CamelBot' or undef
	# e.g. qr/https?:\/\/(?:[a-zA-Z0-9-]+\.)?(?:\Qexample.com\E)/;
	my $namespaces    = shift // [0];
	my $re_url        = shift;
	$self->msg(1, "starting linksearch in ns " . join(', ', $namespaces));
	my $links = [];
	if(defined $user){
		my $pages = {};
		$self->msg(1, 'searching for contributions of user:' .  $user);
		my $uc_options = {
			'ucstart'   => 'now',
			'ucend'     => '2001-01-01T00:00:00Z',
			'username'  => $user,
			'namespace' => $namespaces,
		};
		$pages = $self->get_user_contribs($uc_options);
		$self->msg(1, 'found ' . keys(%$pages)
			. ' pages of user. start searching for links.');
		while(my ($page, $edits) = each %$pages){
			$self->msg(3, $page);
			for my $edit(@$edits){
				my $diff = $self->{'mw_bot'}->diff({
						'revid' => $edit->{'revid'},
						'oldid' => 'prev',
					});
				#my $found = 0;
				#print Dumper $diff;
				# collect added lines
				my @added_lines = $diff =~ /<td class=\"diff-addedline\">(.*?)<\/td>/g;
				# search added lines for pattern
				for my $added_line(@added_lines){
					$added_line =~ s/<ins class="[^"]+">|<\/ins>//g;
					$added_line =~ s/<del class="[^"]+">[^<]+<\/del>//g;
					if($added_line =~ /($re_url*)/){
						push @$links, {
							'title' => $page,
							'url' => $1,
						};
						#$found = 1;
						last;
					}
				}
			}
			#print Dumper($diff) if $found;
		}
	}
	$self->msg(1, 'results (found ' . @$links . ' links in '.
		$self->num_unique_elem(map {$_->{'title'}} @$links) . " pages)");
	return $links;
}

sub link_search_pages{
	my $self        = shift;
	my $search_type = shift;
	my $search_opt  = shift;
	my $namespaces  = shift;
	my $re_url      = shift;
	my $links;
	if($search_type eq 'user_contribs'){
		$links = $self->link_search_by_user($search_opt, $namespaces, $re_url);
	}elsif($search_type eq 'linksearch'){
		$links = $self->external_link_search($search_opt, $namespaces);
	}elsif($search_type eq 'wikisearch'){
		$links = $self->search_pages($search_opt, $namespaces);
		$links = [map {{'url' => 'unknown', 'title' => $_}} @$links];
	}elsif($search_type eq 'explicit'){
		$links = [map {{'url' => 'unknown', 'title' => $_}} grep {
				my $ns = $self->get_namespace_id($_);
				0 < grep {$ns == $_} @$namespaces;
			} @$search_opt
		];
	}else{
		$self->msg(0, "search_type '$search_type' is not implemented", 'error');
		return [];
	}
	return $links; # [{url => ..., title => ...}, ...]
}

sub load_external_url_param_tracking_list{
	my $self = shift;
	my $success = 1;
	my %external_blacklists = (
		'AdGuard' => {
			'filename' => 'tmp_adtidy_filters.txt',
			'url' => 'https://filters.adtidy.org/windows/filters/17.txt',
			# see also:
			# https://github.com/AdguardTeam/AdguardFilters/blob/master/TrackParamFilter/sections/general_url.txt
			# https://github.com/AdguardTeam/AdguardFilters/blob/master/TrackParamFilter/sections/specific.txt
		},
		'adfilt' => {
			'filename' => 'tmp_adfilt_lurls.txt',
			'url' => 'https://raw.githubusercontent.com/DandelionSprout/adfilt/master/LegitimateURLShortener.txt',
			# see: https://github.com/DandelionSprout/adfilt/blob/master/LegitimateURLShortener.txt
		},
	);
	my $caching_time = 60 * 60 * 24 * 3;  # 3 days
	while(my ($eb_name, $eb_data) = each %external_blacklists){
		my $filename = $eb_data->{'filename'};
		unless($self->{'offline'} || -e $filename && (time() - (stat $filename)[9]) < $caching_time){
			my $url = $eb_data->{'url'};
			$self->msg(1, "getting external tracking param url list '$eb_name'");
			my $response = $self->get_http_response($url);
			if($response->is_success){
				open(my $OUTFILE, '>:encoding(UTF-8)', $filename) or die "$!\n";
					print $OUTFILE $response->content;
				close($OUTFILE);
			}
		}
		if(-e $filename){
			my $opened = open(my $INFILE, '<:encoding(UTF-8)', $filename);
			unless($opened){
				$self->msg(0, "could not read tracking url param list: $!", 'error');
				return;
			}
			my @lines = grep {!/^!/} <$INFILE>;
			for my $line(@lines){
				$self->parse_ad_filter_line($line);
			}
			#print Dumper $self->{'re_url_tracking_filter'};
		}elsif($self->{'offline'}){
			$self->msg(0, "offline-mode! could not download external tracking url param list: $!", 'warning');
			$success = 0;
		}else{
			$self->msg(0, "could not download external tracking url param list: $!", 'error');
			$success = 0;
		}
	}
	return $success;
}

sub login{
	my $self = shift;
	my $wiki_password = shift;
	$self->createMWBot();
	$self->check_username();
	$self->msg(1, "login user name is '$self->{'mw_username'}'.");
	$wiki_password = $self->check_password($wiki_password);
	unless(defined $wiki_password){
		$self->msg(1, "could not get password, skipping login.");
		return;
	}
	$self->msg(2, "logging in at $self->{'host'}...");
	# check whether authentication is needed
	$self->_login_basic_auth($self->{'mw_bot'}->{'api'}->{'ua'}, $wiki_password);
	if($self->{'mw_bot'}->login({
		username => $self->{'mw_username'},
		password => $wiki_password,
		#lgdomain => ''
		#basic_auth => $basic_auth,
	})){
		$self->msg(3, 'testing whether i\'m blocked ...');
		my $mw_user = 'User:' . $self->{'mw_username'};
		if($self->{'mw_bot'}->is_blocked($mw_user)){
			$self->msg(0, $mw_user . ' is blocked', 'error');
			$self->{'logged_in'} = 0;
			return;
		}
		$self->msg(3, 'i\'m not blocked. :-)');
		$self->msg(2, 'logged in via MediaWiki::Bot.');
	}else{
		my $cookie = $self->get_cookie_filename();
		$self->msg(0, "could not login.", 'error');
		$self->msg(0, 'check upper/lower case. '
			. 'or maybe there\'s an expired cookie file. please delete \''
			. $cookie . '\' and try again.', 'error') if -e $cookie;
		$self->{'logged_in'} = 0;
		return;
	}
	$self->_set_api();
	$self->_login_basic_auth($self->{'mw_api'}->{'ua'}, $wiki_password);
	if(  $self->{'cliparams'}{'delete'}
		|| $self->{'cliparams'}{'upload'}
		|| $self->{'cliparams'}{'link-replacement'}
		|| $self->{'cliparams'}{'search-sbl-attempts'}
	){
		if($self->{'mw_api'}->login({
			'lgname' => $self->{'mw_username'},
			'lgpassword' => $wiki_password,
			#'lgdomain' => ''
		})){
			$self->msg(2, 'logged in via MediaWiki::API.');
		}else{
			$self->_handle_api_error;
			$self->{'logged_in'} = 0;
			return;
		}
	}
	$self->msg(1, 'logged in and ready for any iniquities.');
	$self->{'logged_in'} = 1;
	return $self->{'logged_in'};
}

sub _login_basic_auth{
	my $self = shift;
	my $ua   = shift;
	my $wiki_password = shift;
	my $api_url = $self->_get_api_url();
	my $response = $ua->head($api_url);
	$self->msg(3, $response->status_line);
	if($response->code() == 401){
		my $auth_options = [$response->header('WWW-Authenticate')];
		if(0 < grep {$_ =~ /^Basic\b/} @$auth_options){
			$self->msg(3, 'basic authentication seems to be an option');
		}else{
			$self->msg(0, 'basic authentication not supported?', 'warning');
		}
		$self->msg(2, 'trying basic authentication');
		my $basic_auth = {
			netloc => $self->{'host'} . ':443',
			#realm  => '',
			uname  => $self->{'mw_username'},
			pass   => $wiki_password,
		};
		# this may vary
		$basic_auth->{'uname'} = lcfirst($basic_auth->{'uname'});
		$basic_auth->{'uname'} =~ y/ /_/;
		# force basic authentication
		$ua->default_header(
			'Authorization' => 'Basic ' . MIME::Base64::encode(
				$basic_auth->{'uname'} . ':' . $basic_auth->{'pass'},	'')
		);
	}
	$response = $ua->head($api_url);
	$self->msg(2, $response->status_line);
	unless($response->is_success){
		$self->msg(0,
			'it seems like we cannot reach the server (code = '
			. $response->code() . '). anyway trying to login now ...',
			'warning');
		return 0;
	}
	return 1;
}

sub match_any{
	my $self = shift;
	my $str = shift;
	my $arr = shift;
	return unless defined $arr;
	my $found = 0;
	for my $pattern(@$arr){
		if($str =~ /$pattern/){
			$found = 1;
			last;
		}
	}
	return $found;
}

sub msg{
	my $self           = shift;
	my $verb_threshold = shift;
	my $msg            = shift;
	my $type           = shift;
	my $caller_inc     = shift // 0;
	return 0 if $self->{'verbosity'} < $verb_threshold;
	$type = (defined $type ? "$type in ": '');
	my $timestamp = $self->get_time_iso_;
	# my ($package, $filename, $line, $subr, $has_args, $wantarray, $evaltext,
	# $is_require, $hints, $bitmask, $hinthash) = caller(0);
	my @callers = caller(0 + $caller_inc);
	my $line = $callers[2];
	@callers = caller(1 + $caller_inc);
	my $subr = $callers[3] // '[no sub]';
	print "$timestamp $type$subr:$line: $msg\n";
	return 1;
}

sub namespace_allowed{
	my $self = shift;
	my $ns = shift;
	if(defined $self->{'namespaces_used'}){
		return 0 < grep {$ns == $_} @{$self->{'namespaces_used'}};
	}else{
		return $ns == 0;
	}
}

sub newest_post_info{
	my $self     = shift;
	my $wikitext = shift;
	unless(defined $wikitext){
		$self->msg(0, 'wikitext param is undef?', 'warning');
		return;
	}
	my $threads = [
		grep {$_ !~ /^=+$/} split /\n(?=(=++)[^=].*\1(?:\n|$))/, $wikitext];
	my $newest_thread = {
		'date'   => 0,
		'author' => undef,
		#'thread' => undef,
	};
	$self->msg(5, 'got threads: ' . Dumper($threads));
	for my $t(@$threads){
		next unless $t =~ /^(=++)\s*(.*?)\x20*\1/;
		my $thread_title = $2;
		unless(defined($newest_thread->{'thread'})){
			$newest_thread->{'thread'} = $thread_title;
		}
		my $unix_timestamp;
		while($t =~ /(?<pre_sig>.*)(?<hour>[0-9]{2}):(?<min>[0-9]{2}),\x20
			(?<mday>[1-9]|[123][0-9])\.\x20(?<mon>[a-zA-Zä\xe4]{3,4})\.?\x20
			(?<year>20[0-9]{2})\x20\((?<tz>[A-Z]{3,4})\)/gx){
			my $month =
				$+{'mon'} eq 'Jan' ? 0 :
				$+{'mon'} eq 'Feb' ? 1 :
				($+{'mon'} eq "Mär" or $+{'mon'} eq "M\xe4r") ? 2 :
				$+{'mon'} eq 'Apr' ? 3 :
				$+{'mon'} eq 'Mai' ? 4 :
				$+{'mon'} eq 'Jun' ? 5 :
				$+{'mon'} eq 'Jul' ? 6 :
				$+{'mon'} eq 'Aug' ? 7 :
				$+{'mon'} eq 'Sep' ? 8 :
				$+{'mon'} eq 'Okt' ? 9 :
				$+{'mon'} eq 'Nov' ? 10 :
				$+{'mon'} eq 'Dez' ? 11 : 12
			;
			next if $month == 12;
			$unix_timestamp = $self->convert_time_unix2unix_tz(
				timegm_modern(0, $+{'min'}, $+{'hour'}, $+{'mday'}, $month, $+{'year'}),
				'from', $+{'tz'}
			);
			if($unix_timestamp > $newest_thread->{'date'}){
				$newest_thread = {
					'thread' => $thread_title,
					'author' => $+{'pre_sig'},
					'date'   => $unix_timestamp,
				};
				$self->msg(4, 'got author pre sig: ' . Dumper($+{'pre_sig'}));
			}
		}
	}
	if(defined $newest_thread->{'author'}){
		my $usernames = $self->get_usernames_from_signatures($newest_thread->{'author'});
		if(defined $usernames && @$usernames > 0){
			$newest_thread->{'author'} = $usernames->[-1];
		}
	}
	return $newest_thread;
}

sub normalize_page_title{
	my $self = shift;
	my $name_param = shift;
	my $offline = shift;
	unless(defined $name_param){
		$self->msg(0, 'undef as first param makes no sense', 'warning');
		return;
	}
	my $name = (ref $name_param eq 'SCALAR') ? $$name_param : $name_param;
	my $normalized;
	my $full_result = 1;
	unless($offline){
		$normalized = $self->get_page_info($name, $full_result);
		if(defined $normalized){
			$normalized = $normalized->{'normalized'}[0]{'to'}
				// $normalized->{'interwiki'}[0]{'title'}
				// $normalized->{'pages'}{'-1'}{'title'};
		}
	}
	# TODO: should there be an option to re-add a leading ':',
	# e.g. ':en:moep_moep' -> ':en:moep moep' instead of 'en:moep moep'?
	if(defined $normalized){
		# for fragments such as [[Hudelsprudel#Moep_Moep]]
		if($name =~ /(#.*)/ && index($normalized, '#') == -1){
			$normalized .= $1;
		}
	}else{
		# offline normalization
		# order of replacements is important
		$normalized = $name;
		$normalized =~ s/^ +//;
		$normalized =~ s/ +\z//;
		$normalized = ucfirst($normalized); #  will not work for wiktionary
		# TODO: not possible offline:
		# https://de.wikipedia.org/w/api.php?action=query&meta=siteinfo&siprop=general&format=json
		# ->{'query'}{'general'}{'case'} eq 'first-letter'|'case-sensitive'
	}
	$normalized =~ y/_/ /;
	if(ref $name_param eq 'SCALAR'){
		$$name_param = $normalized;
	}
	return $normalized;
}

sub notify_for_maintenance{
	my $self  = shift;
	my $rc_msg = shift;
	my $page_text_ref = shift;
	unless(defined $rc_msg && ref $rc_msg eq 'HASH'){
		$self->msg(0, 'sub notifier needs a rc_msg hash ref as param', 'warning');
		return;
	}
	if(defined($rc_msg->{'rc_this_oldid'}) && $rc_msg->{'rc_this_oldid'} == 0){
		$self->msg(1, 'page revision seems to be deleted');
		return;
	}
	my $page = $rc_msg->{'move_target'} // $rc_msg->{'page_with_ns'};
	# get newest version of page content (if not already given as param)
	unless(defined $page_text_ref && defined $$page_text_ref){
		$$page_text_ref = $self->get_text(
			$page,
			{'rvstartid' => $rc_msg->{'rc_this_oldid'}}
		);
		unless(defined $$page_text_ref){
			$self->msg(1, "page '$page' doesn't seem to exist anymore.", 'notice');
			return;
		}
	}
	my $check_triggering = 1;
	$self->msg(4, 'checking current revision');
	my $matches = $self->get_all_maintenance_triggers_of_page(
		$rc_msg, $page_text_ref, $check_triggering);
	# any matches in present page?
	if(0 == grep {@$_ > 0} values %$matches){
		$self->msg(4, "nothing to notify");
		return;
	}
	my $old_matches = {};
	if(($rc_msg->{'rc_last_oldid'} // 0) == 0){
		$self->msg(1, "page '$page' seems to be newly created");
	}else{
		# get older version of page if available
		my $page_text_prev_version = $self->get_text(
			$page, {'rvstartid' => $rc_msg->{'rc_last_oldid'}});
		unless(defined $page_text_prev_version){
			$self->msg(1, 'previous revision seems to be deleted');
			return;
		}
		$self->msg(3, 'checking previous revision');
		$old_matches = $self->get_all_maintenance_triggers_of_page(
			undef, \$page_text_prev_version, !$check_triggering);
	}
	my @notifs = ();
	for my $ntf_type(keys %{$self->{'maintenance'}{'notifiers'}}){
		# normalize before comparison
		if(defined($self->{'maintenance'}{'post-process_match'}{$ntf_type})){
			map {
				$_ = $self->{'maintenance'}{'post-process_match'}{$ntf_type}->($_);
			} (@{$matches->{$ntf_type}}, @{$old_matches->{$ntf_type}});
		}
		# compare with older version
		my $matches_added = $self->reduce_array_by_array(
			$matches->{$ntf_type}, $old_matches->{$ntf_type});
		next if @$matches_added == 0;
		$self->msg(2, "'$ntf_type' triggered (by quality)");
		# just compare number of hits
		next if @{$old_matches->{$ntf_type} // []} > @{$matches->{$ntf_type} // []};
		$self->msg(2, "'$ntf_type' triggered (by quantity)");
		$self->msg(1, "triggering '$ntf_type' hits in new version: "
			. scalar(@{$matches->{$ntf_type}}) . "; "
			. "diff to previous version: " . scalar(@$matches_added));
		if($self->notify_user(
			$rc_msg->{'user'},
			$self->{'maintenance'}{'notifiers'}{$ntf_type},
			$self->create_notification(
				$ntf_type, $page, $rc_msg->{'rc_this_oldid'}, $matches_added)
		)){
			push @notifs, $ntf_type;
		}
	}
	return \@notifs;
}

sub notify_user{
	my $self = shift;
	my $user_to_inform = shift;
	my $heading = shift;
	my $message = shift;
	my $heading_depth = shift // 2;
	unless(defined $user_to_inform){
		$self->msg(0, 'user to inform is undefined.', 'warning');
		return;
	}
	unless(defined $message){
		$self->msg(0, 'message is undefined.', 'warning');
		return;
	}
	my $heading_incl_in_msg = 0;
	unless(defined $heading){
		if($message =~ /^(=+)\s*+(.*\S)\s*\1/){
			$heading_incl_in_msg = 1;
			$heading_depth = length($1);
			$heading = $2;
		}else{
			$self->msg(1,
				"heading undefined. cannot check, whether user has been notified already.");
		}
	}
	my $userpage = 'User talk:' . $user_to_inform;
	my $text = $self->get_text($userpage) // '';
	my $text_bak = $text;
	if(defined($text) && defined($heading) && $text =~ /=\s*\Q$heading\E\s*=/){
		$self->msg(1, "user '$user_to_inform' seems to be informed already.");
		return 0;
	}else{
		if($text ne ''){
			$text =~ s/(?:\n*)\z/\n\n/;
		}
		if(defined($heading) && !$heading_incl_in_msg){
			$text .= ('='x $heading_depth) . " $heading " . ('='x $heading_depth) . "\n";
		}
		$text .= "$message\n";
		$self->msg(2, "notifying on '$userpage'");
		my $minor = 0;
		return $self->save_wiki_page(
			$userpage, $heading, \$text, \$text_bak, $minor) > 0;
	}
	return;
}

sub notify_user_predefined{
	my $self = shift;
	my $user = shift;
	my $heading = shift;
	my $predefined_msg = shift;
	my $page = shift;
	my $revid = shift;
	my $matches = shift;
	my $msg = $self->create_notification($predefined_msg, $page, $revid, $matches);
	my $notified = $self->notify_user($user, $heading, $msg);
	if($notified){
		$self->msg(1, "informed user '$user' with "
			. "'$predefined_msg' for page '$page'");
	}elsif(!defined($notified)){
		$self->msg(0, "could not inform user '$user' with "
			. "'$predefined_msg' for page '$page'",
			'warning');
	}# else do nothing on notified == 0, because then a message was given already
	return $notified;
}

sub num_unique_elem{ ## no critic (RequireArgUnpacking)
	my $self = shift;
	my %hash;
	@hash{@_} = 1;
	return '' . (keys %hash);
}

sub _decompose_ad_filter_line{
	my $self = shift;
	my $line = shift;
	my $ad_params = {};
	if($line =~ /^\s*(?:!|$)/){
		# skip empty lines and comments
	}elsif($line =~ /(\@\@|)(.*|)\$(.*)/){
		$ad_params->{'exception'} = 1 if $1 eq '@@';
		$ad_params->{'pattern'} = $2;
		my @mods = split /(?<!\\),/, $3;
		for my $mod(@mods){
			if($mod =~ /^([^=]+)=(.*)$/){
				$ad_params->{'modifiers'}{$1} = $2;
				if($1 eq 'domain'){
					$ad_params->{'modifiers'}{'domain'} = [split /\|/, $2];
					#map {
					#	$_ = qr/\Q$_\E\z/;
					#	$_ =~ s/\.\\\*/.[a-z0-9.]+/;
					#} @{$ad_params->{'modifiers'}{'domain'}};
				}elsif($1 eq 'removeparam'){
					if($ad_params->{'modifiers'}{'removeparam'} =~ /^\//){  # if regexp
						$ad_params->{'modifiers'}{'removeparam'} =~ s/\\,/,/g;  # unescape meta char
					}
				}
			}else{
				$ad_params->{'modifiers'}{$mod} = undef;
			}
		}
	# e.g. wenku.baidu.com##+js(set,history.replaceState,noopFunc)
	}elsif($line =~ /##(?:\+js\(|\.hello_)/){
		# skip js lines and other weird stuff
	}elsif($line eq "/auto-append-spmid.js"){
		# skip wtf lines
	}else{
		$self->msg(0, 'could not parse ad filter line', 'warning');
		$self->msg(1, $line);
		return;
	}
	return $ad_params;
}

sub parse_ad_filter_line{
	# see
	# https://help.adblockplus.org/hc/en-us/articles/360062733293-How-to-write-filters
	# for syntax information
	my $self = shift;
	my $line = shift;
	unless(defined $line){
		$self->msg(0, 'first param (line) undefined', 'warning');
		return;
	}
	unless(defined $self->{'re_url_tracking_filter'}){
		$self->msg(0, 'member re_url_tracking_filter not initialized. '
			. 'call init_re_url_tracking_filter() before calling this function '
			. 'or set member manually to empty hash', 'error');
		return;
	}
	# clean and decompose line
	my $re_doc_type = qr/(?:doc(?:ument|)|~css|~?media|~?script|~?stylesheet|~?xmlhttprequest)/;
	my $orig_line = $line;
	$line =~ s/[\r\n]+\z//;
	$line =~ s/,$re_doc_type//g;
	$line =~ s/\$\K$re_doc_type,//g;
	my $ad_params = $self->_decompose_ad_filter_line($line);
	my $debug;
	#my $debug = $orig_line =~ /ref_url/;
	$self->msg(1, $orig_line) if $debug;
	if(!defined($ad_params)
		|| keys(%$ad_params) == 0
		|| !exists($ad_params->{'modifiers'}{'removeparam'})
		|| defined($ad_params->{'modifiers'}{'app'})
		|| exists($ad_params->{'modifiers'}{'font'})
		|| exists($ad_params->{'modifiers'}{'script'})
		|| exists($ad_params->{'modifiers'}{'cookie'})
		|| $ad_params->{'pattern'} eq '|wss://'
	){
		return 0;
	}
	$self->msg(1, Dumper($ad_params)) if $debug;
	my $ptr = $ad_params->{'exception'} ? $self->{'re_url_tracking_filter'}{whitelist}
		: $self->{'re_url_tracking_filter'};
	my $mods = $ad_params->{'modifiers'};
	my $from_domain_restriction = 0;
	if(defined $mods->{'domain'}){
		$from_domain_restriction = 0 == grep {/^~/} @{$mods->{'domain'}};
	}
	$self->msg(1, "\$from_domain_restriction = $from_domain_restriction") if $debug;
	return 0 if $from_domain_restriction;
	# check pattern
	# ||example.org^
	my $re_domain_part = qr/[a-z0-9][a-z0-9.-]*/;
	if($ad_params->{'pattern'} =~ /^\|\|($re_domain_part)(\^|)\z/){
		$self->msg(1, 'case ||example.org') if $debug;
		my $re_domain = $2 eq '' ? qr/\Q$1\E/ : qr/\Q$1\E\z/;
		unless(defined $ptr->{'domain-specific'}{$re_domain}){
			$ptr->{'domain-specific'}{$re_domain} = {};
		}
		$ptr = $ptr->{'domain-specific'}{$re_domain};
	}elsif($ad_params->{'pattern'} eq ''){
		# ok
	# paths (not just domains)
	# e.g. rtkcid=*&clickid=$removeparam=clickid
	# e.g. ||stvkr.com/v*/click-$removeparam=sa
	# e.g. .userapi.com/impg^$removeparam=proxy
	# e.g. ||example.org/tracking?device=*&path=$removeparam=/^(device|country|path)=/,xmlhttprequest
	# e.g. ://www.google.*/search^$removeparam=kgs
	# e.g. ://yandex.$removeparam=clid
	# e.g. @@||amazon.*/gp/slredirect/picassoRedirect.html^$removeparam
	# e.g. ||tiktok.com/@*/video/*$removeparam=q
	}elsif($ad_params->{'pattern'} =~ /^(?:\|\||:\/\/|)[a-z0-9A-Z_=?*%&;.^\/\@-]+\z/){
		# skip, because it makes my rules to complicated
		$self->msg(1, 'case long pattern1 (skip)') if $debug;
		$self->msg(4, "unused tracking url param list item:\n$orig_line");
		return 0;
	# e.g. ?uclick=*&uclickhash=$removeparam
	}elsif($ad_params->{'pattern'} =~ /^[?\/&]?([a-z0-9A-Z_=?*&;]+)/){
		# i don't understand the original syntax. i guess it means
		# "if the url matches the pattern, then remove all query params."
		# however, i think that might lead to problems, if there are additional needed
		# query params. so i'll skip this.
		$self->msg(1, 'case long pattern2 (skip)') if $debug;
		$self->msg(4, "unused tracking url param list item:\n$orig_line");
		return 0;
	}elsif($ad_params->{'pattern'} eq '||google.*&tbm=isch&*#imgrc='
		|| $ad_params->{'pattern'} =~ /^\|\|ad\.doubleclick\.net\/ddm\/trackclk/
	){
		# skip, because it makes my rules to complicated
		$self->msg(4, "unused tracking url param list item:\n$orig_line");
		return 0;
	}else{
		# unrecognized pattern
		$self->msg(1, "unused tracking url param list item:\n$line original line:\n$orig_line)");
		return 0;
	}
	
	# check 'removeparam' param
	my $re_param = qr/(?:\.src|[a-zA-Z0-9_%\[\]~-])[a-zA-Z0-9_;:.%\[\]~-]*/;
	# ||example.org^$removeparam
	if(!defined($mods->{'removeparam'})){
		$self->msg(1, 'case ||example.org all') if $debug;
		push @{$ptr->{'keyval'}}, qr/./;
	# $removeparam=/some_regexp/
	}elsif($mods->{'removeparam'} =~ /^\/([^\/]*)\/(i|)\z/){
		$self->msg(1, 'case regexp') if $debug;
		my $url_param = $2 eq 'i' ? qr/$1/i : qr/$1/;
		push @{$ptr->{'keyval'}}, $url_param;
	# $removeparam=param_name
	}elsif($mods->{'removeparam'} =~ /^$re_param\z/){
		$self->msg(1, 'case single param') if $debug;
		$ptr->{'key'}{$mods->{'removeparam'}} = 1;
		$self->msg(1, Dumper($self->{'re_url_tracking_filter'})) if $debug;
	}elsif($mods->{'removeparam'} =~ /^\/^\\\/_ui\\\//
		|| $mods->{'removeparam'} =~ /^\?/
	){
		# wtf? skip
	}else{
		$self->msg(1, 'case else (skip)') if $debug;
		$self->msg(1, "unused tracking url param list item:\n$line\n original line:\n$orig_line)");
		$self->msg(1, Dumper($ad_params));
		#exit 1; # for debugging
		return 0;
	}
	return 1;
}

sub parse_diff_html{
	my $self = shift;
	my $diff_text = shift;
	my @diff_lines = split /\n/, $diff_text;
	my $diff_struct = [];
	my $lin_num = 0;
	for my $line(@diff_lines){ # loop over all diff lines
		next if $line =~ /^(?:<\/?tr>|<!-- .* -->)?\z/; # skip tr-tags
		unless($line =~/^\s*<td [^>]*class="([^"]+)"[^>]*>(.*)<\/td>\z/){
			$self->msg(0, "could not parse $line", 'error');
		}
		my $type = $1;
		my $text = $2;
		next if $type =~ /^diff-(?:lineno|marker|context)\b/;
		$type =~ s/ *diff-side-(?:added|deleted) *//;
		$text =~ s/^<div>|<\/div>\z//g; # truncate
		$text = [
			map {
				$self->decode_html_entities(\$_);
				my $text_type = ($_=~/^<(ins|del)\b[^>]*>(.*)<\/\1>\z/) ? $1 : 'equal';
				{
					'text' => ($text_type eq 'equal' ? $_ : $2),
					'type' => $text_type,
				};
			} split /(<(?:(?:del|ins)\b[^>]*>[^<>]*<\/(?:del|ins))>)/, $text
		];
		if($type !~ /^diff-((?:added|deleted)line|empty)\z/){
			$self->msg(0, "unknown type '$type'", 'warning');
		}
		$diff_struct->[$lin_num]->{$type} = $text;
		++$lin_num if $type eq 'diff-addedline';
	}
	# walk through struct and search for completely added/deleted lines
	for my $d(@$diff_struct){
		next unless defined $d->{'diff-empty'};
		delete $d->{'diff-empty'};
		if(defined $d->{'diff-addedline'}){
			$d->{'diff-addedline'}[0]{type} = 'ins';
			$d->{'diff-deletedline'} = [];
		}elsif(defined $d->{'diff-deletedline'}){
			$d->{'diff-deletedline'}[0]{type} = 'del';
			$d->{'diff-addedline'} = [];
		}
	}
	#print Dumper $diff_struct;
	#looks like
	#[{
	#	'diff-deletedline' => [
	#		{
	#			'text' => 'some text ',
	#			'type' => 'equal'
	#		},
	#		{
	#			'type' => 'del',
	#			'text' => 'some deleted text '
	#		},
	#		{
	#			'text' => 'some other text',
	#			'type' => 'equal'
	#		}
	#	],
	#	'diff-addedline' => [
	#		{
	#			'text' => 'some text',
	#			'type' => 'equal'
	#		},
	#		{
	#			'text' => 'some better text',
	#			'type' => 'ins'
	#		},
	#		{
	#			'text' => 'some other text',
	#			'type' => 'equal'
	#		}
	#	]
	#}, ... ]
	return $diff_struct;
}

sub parse_page{
	my $self = shift;
	my $page = shift;
	my $text = $self->api_simple({
		action => 'parse',
		page => $page,
		prop => 'text|headhtml',
	});
	my $title = $text->{'parse'}{'title'};
	my $html = $text->{'parse'}{'text'}{'*'};
	my $headhtml = $text->{'parse'}{'headhtml'}{'*'};
	# delete final comments
	$html =~s/\n\n<!--\s+NewPP.*?\n-->$//s;
	return ($html, $title, $headhtml);
}

sub parse_irc_rc{
	my $self = shift;
	my $msg = shift;
	my %parsed_msg;
	if($msg =~ /^\cC14
		\[\[\cC07([^\]]+)\cC14]]\cC4\x20   # page
		([a-z0-9_-]+|[!NMB]*)\cC10\x20     # flags
		\cC02([^\cC]*)\cC\x20\cC5\*\cC\x20 # url
		\cC03([^\cC]*)\cC\x20\cC5\*\cC\x20 # user
		(\(\cB?[+-]\d+\cB?\)|)\x20         # diffbytes
		\cC10(.*[^\cC]|)\cC?\z/x){         # summary
		$parsed_msg{'page_with_ns'} = $1;
		$parsed_msg{'flags'} = $2;
		$parsed_msg{'url'} = $3;
		$parsed_msg{'user'} = $4;
		$parsed_msg{'diffbytes'} = $5;
		$parsed_msg{'summary'} = $6;
		if($parsed_msg{'diffbytes'} =~ /^\(\cB?([+-]\d+)\cB?\)$/){
			$parsed_msg{'diffbytes'} = $1;
		}
		$parsed_msg{'ns_id'} = $self->get_namespace_id($parsed_msg{'page_with_ns'});
		$parsed_msg{'page'} = $parsed_msg{'page_with_ns'};
		$parsed_msg{'page'} =~ s/^[^:]+:// if $parsed_msg{'ns_id'} != 0;
		$parsed_msg{'timestamp_unix'} = time();
		$parsed_msg{'timestamp'} = $self->get_time_iso($parsed_msg{'timestamp_unix'});
		$self->msg(5, Dumper(\%parsed_msg));
	}else{
		$self->msg(2, $msg);
	}
	return \%parsed_msg;
}

sub parse_wikitext{
	my $self = shift;
	my $wikitext = shift;
	my $text = $self->api_simple({
		action => 'parse',
		text => $wikitext,
		prop => 'text',
	});
	my $html = $text->{'parse'}{'text'}{'*'};
	# delete final comments
	$html =~s/\n\n<!--\s+NewPP.*?\n-->$//s;
	return $html;
}

# searches for all links in a given piece of wikitext
# original code got from
# http://svn.wikimedia.org/viewvc/mediawiki/trunk/phase3/includes/parser/Parser.php?view=markup&pathrev=74981
# ported to perl because phps regexp-handling (\\\\\\\\\\\\\\\\\) sucks.
sub php_code_unused{
	my $self = shift;
	# get wikitext as param
	my $wikitext = shift; # $text in orig code
	# building regexp for matching of "[...]"-like links
	my @wgUrlProtocols = (
		'http://',
		'https://',
		'ftp://',
		'irc://',
		'gopher://',
		'telnet://',
		'nntp://',
		'worldwind://',
		'mailto:',
		'news:',
		'svn://',
		'git://',
		'mms://'
	);
	my $re_prot = join('|', @wgUrlProtocols);
	my $ext_link_url_class = qr/[^\][<>"\x00-\x20\x7F\p{Zs}]/;
	my $re_url_wo_prot =     qr/$ext_link_url_class+/;
	my $re_descr = qr/[^\]\x00-\x08\x0a-\x1F]*?/;
	my $mExtLinkBracketedRegex = qr/\[(($re_prot)$re_url_wo_prot) *($re_descr)\]'/;
	# $1 = url, $2 = prot, $3 = descr
	# split wikitext by links
	my @bits = split /$mExtLinkBracketedRegex/, $wikitext;
	# first part is something non-link-like (i.e. an empty string or some wikitext).
	my $s = shift @bits;
	# loop over parts (of 4 different classes:
	#  wikitext, url, prot, descr, wikitext, url, prot, descr, ...)
	for(my $i = 0; $i < @bits;){
		my $url = $bits[++$i];
		my $prot = $bits[++$i];
		my $descr = $bits[++$i]; # $text in orig code
		my $wtext = $bits[++$i] // ''; # $trail in orig code
		# will continue later on
	}
	return;
}

sub post_process_html{
	my $self = shift;
	my $html = shift;
	my $image_prefix = shift // '';
	#  images
	$html=~s/<a\x20                                 # <a
				href="\/wiki\/File:(?<filename>[^"]++)"   # href="\/wiki\/File:..."
			[^>]*+>                                     # ...>
			(?<imgpre><img\x20[^>]*src=")               # <img ... src="
				[^"]++                                    # ...
			(?<imgpost>"[^>]*+>)                        # ...>
		<\/a>                                         # <\/a>
		/$+{'imgpre'}$image_prefix$+{'filename'}$+{'imgpost'}/gx;
	$html =~ s/
		src="\K
			\/\Q$self->{'rel_url_path'}\E\/images\/thumb
			\/[a-zA-Z0-9_\/-]+\/([a-zA-Z0-9_.-]*)\/[0-9]+px-\1
		"/$1"/gx;
	$html =~ s/ srcset="[^"]++"//g;
	#  html tidy
	my $html_cleaned = '';
	my $err = '';
	IPC::Run::run([qw(tidy -iq --tidy-mark 0 -f /dev/null -w 5000 -utf8)],
		\$html, \$html_cleaned, \$err);
	return $html_cleaned;
}

sub read_json_file{
	my $self = shift;
	my $filename = shift;
	my $json_text = do{
		 open(my $json_fh, "<:encoding(UTF-8)", $filename)
				or die("Can't open \"$filename\": $!\n");
		 local $/;
		 <$json_fh>
	};
	# TODO: which of the following lines has to be used?
	#return JSON->new->utf8(0)->decode($json_text);
	return JSON->new->decode($json_text);
}

sub read_file_binary{
	my $self = shift;
	my $filename = shift;
	open(my $FILE, '<:encoding(UTF-8)', $filename) or die $!;
		binmode $FILE;
		my $buffer;
		my $data = '';
		while(read($FILE, $buffer, 65536)){
			$data .= $buffer;
		}
	close($FILE);
	return \$data;
}

sub read_password_from_file{
	my $self = shift;
	my $wiki_password;
	unless(exists $self->{'pw_file'}){
		$self->{'pw_file'} = undef;
		# /bot/ needed for toolserver (for historical reasons)
		my @pwfiles = (".password", "$ENV{HOME}/.password", "$ENV{HOME}/bot/.password");
		for(@pwfiles){
			if(-e $_){
				$self->{'pw_file'} = $_;
				$self->msg(1, "using standard password file '$_'");
				last;
			}
		}
		unless(defined $self->{'pw_file'}){
			if($self->{'verbosity'} >= 1){
				$self->msg(1, 'could not read password file at places: ');
				print '  ' . join(', ', @pwfiles) . "\n";
			}
		}
	}
	if(defined $self->{'pw_file'}){
		if(-s $self->{'pw_file'} > 0){
			open(my $INFILE, '<', $self->{'pw_file'})
				or die "error concerning file '$self->{'pw_file'}': $!";
				chomp($wiki_password = <$INFILE>);
			close($INFILE);
		}
		unless(defined $wiki_password){
			$self->msg(1, "could not read password from file: '$self->{'pw_file'}'.",
				'warning');
		}
	}
	return $wiki_password;
}

sub read_www_cache{
	my $self = shift;
	my $force = shift;
	if($force || ($self->{'use_www_cache'} && !defined($self->{'www_cache'}))){
		if(defined($self->{'www_cache_file'}) && -e $self->{'www_cache_file'}){
			$self->{'www_cache'} = $self->read_json_file($self->{'www_cache_file'});
			return 1;
		}else{
			$self->{'www_cache'} = {};
			return 0;
		}
	}
	return -1;
}

sub rebuild_table{
	my $self      = shift;
	my $page      = shift;         # wiki page
	my $tables    = shift;         # tables to insert
	my $summary   = shift;         # wiki summary
	my $text = shift // $self->get_text($page);
	# convert table to wikitext string
	my $full_auto = shift // 1;
	if(not defined $text){
		$self->msg(1, "page $page does not exist.", 'notice');
		$text = '';
	}elsif($text eq ''){
		$self->msg(1, "page $page is empty.", 'notice');
	}
	my $text_bak = $text;
	if($text eq ''){
		for my $table(@$tables){
			my $wikitable = $self->table2wikitext($table, $full_auto);
			if(defined $table->{'section'}){
				$text .= "== " . $table->{'section'} . " ==\n";
			}
			$text .= $wikitable;
		}
	}else{
		# find place to insert table and replace table
		for my $table(@$tables){
			my $wikitable = $self->table2wikitext($table, $full_auto);
			if(defined $table->{'section'}){
				my $section = $table->{'section'};
				my $re_section_heading =
					qr/^=(?<equals>=+)\x20?\Q$section\E\x20?\g{equals}=\n/msx;
				if($text =~ /$re_section_heading/g){
					my $table_begin = pos($text); # begin of table to replace
					my $tmp = substr($text, $table_begin);
					my $table_length;
					# find end of table
					if($tmp =~ /(?=\n(?:=|\[\[[cC]ategory:))/g){
						$table_length = pos($tmp); # length of table to replace
					}else{ # table goes till end
						$table_length = length($tmp);
					}
					$text = substr($text, 0, $table_begin) . $wikitable .
						substr($tmp, $table_length);
				}else{
					$self->msg(0,
						'could not create/modify section. please ensure that wiki page ' .
							"[[$page]] either is empty or the section '$section' exists!",
						'error'
					);
					die;
				}
			}else{
				if($text =~/(?:^<!--.*?-->\n)?^\{\|.*\|\}/ms){
					$text =~ s/(?:^<!--.*?-->\n)?^\{\|.*\|\}(\n|)\n*/$wikitable$1/ms;
				}else{
					$self->msg(0,
						'could not create/modify section. please ensure that wiki page ' .
							"[[$page]] either is empty or the appropriate section exists!",
						'error'
					);
					die;
				}
			}
		}
	}
	$self->msg(2, $text) if $self->{'simulation'};
	return $self->save_wiki_page($page, $summary, \$text, \$text_bak);
}

sub reduce_array_by_array{
	my $self = shift;
	my $arr_A = shift;
	my $arr_B = shift;
	unless(defined $arr_A){
		return [];
	}
	unless(defined $arr_B){
		return [@$arr_A];
	}
	my $arr = [];
	my $i = -1;
	my $idx_B = {map {++$i => 1} @$arr_B}; # {0 => 1, 1 => 1, ..., n => 1}
	for my $elem_A(@$arr_A){
		my $is_in_B = 0;
		for($i = @$arr_B - 1; $i >= 0; --$i){
			next unless $idx_B->{$i};
			if($elem_A eq $arr_B->[$i]){
				$idx_B->{$i} = 0;
				$is_in_B = 1;
				last;
			}
		}
		if(!$is_in_B){
			push @$arr, $elem_A;
		}
	}
	return $arr;
}

sub refill_maintenance_list{
	my $self = shift;
	my $maintenance_lists = shift;
	my $mt_type = shift;
	my $table = shift;
	my $num;
	if(defined $maintenance_lists){
		$num = 0;
		$maintenance_lists->{$mt_type} = {};
		for my $entry(@{$table->{'body'}}){
			if($entry->[0] =~ /^\[\[:?+(.+?)\]\]/){
				$maintenance_lists->{$mt_type}{$1} = 1;
				++$num;
			}
		}
	}
	return $num;
}

sub refresh_maintenance_params{
	my $self = shift;
	my $tracked_domains = [
		#'buzer.de',             # no notifier
		#'finanznachrichten.de', # no notifier, 2024-11-26: not needed any longer
		'allmovie.com',
		'geni.com',
		'gerontology.fandom.com',
		'mapcarta.com',
		'reppa.de',
		'the110club.com',
		'traditionsverband.de', # no notifier
		'warheroes.ru',
	];
	my @christian_domains = (
		'heiligenlexikon.de',  # https://de.wikipedia.org/w/index.php?title=Wikipedia_Diskussion:WikiProjekt_Christentum&oldid=250399467#%C3%96kumenisches_Heiligenlexikon
	);
	my @seo_law_domains = (
		'123recht.de',  # https://de.wikipedia.org/w/index.php?title=Portal_Diskussion:Recht&diff=prev&oldid=238734079
		'abofalle-anwalt.de',  # https://de.wikipedia.org/w/index.php?title=Portal_Diskussion:Recht/Weblinks_und_Belege&oldid=241212818#Kommentarseite
		'anwalt.de',  # https://de.wikipedia.org/w/index.php?title=Portal_Diskussion:Recht/Weblinks_und_Belege&oldid=241212818#Kommentarseite
		'anwalt.org',
		'anwaltsblogs.de',  # https://de.wikipedia.org/w/index.php?title=Portal_Diskussion:Recht/Weblinks_und_Belege&oldid=241212818#Kommentarseite
		'anwalt-wechseln.de',  # https://de.wikipedia.org/w/index.php?title=Benutzer_Diskussion:CamelBot/maintenance_list/seo_law_pages&diff=prev&oldid=239986142
		'arbeitsrecht.de',  # https://de.wikipedia.org/w/index.php?title=Portal_Diskussion:Recht&diff=prev&oldid=238734079
		'arbeitsrechte.de',
		'arbeitsvertrag.org',
		'aufrecht.de',  # https://de.wikipedia.org/w/index.php?title=Portal_Diskussion:Recht/Weblinks_und_Belege&oldid=241212818#Kommentarseite
		'av-anwalt.de',  # https://de.wikipedia.org/w/index.php?title=Portal_Diskussion:Recht/Weblinks_und_Belege&oldid=241212818#Kommentarseite
		'bg-anwalt.de',  # https://de.wikipedia.org/w/index.php?title=Portal_Diskussion:Recht/Weblinks_und_Belege&oldid=241212818#Kommentarseite
		'bgb.kommentar.de',
		'bussgeld-info.de',
		'bussgeldkatalog.de',
		'bussgeldkataloge.de',
		'bussgeldkatalog.org',
		'bussgeldrechner.org',
		'dahag.de',  # https://de.wikipedia.org/w/index.php?title=Portal_Diskussion:Recht/Weblinks_und_Belege&oldid=244685196#Neue_Links_(April_2024)
		'datenschutz.org',
		'dsgvo-gesetz.de',  # https://de.wikipedia.org/w/index.php?title=Portal_Diskussion:Recht&diff=prev&oldid=239599409
		'easy-divorce.de',  # https://de.wikipedia.org/w/index.php?title=Benutzer_Diskussion:CamelBot/maintenance_list/seo_law_pages&diff=prev&oldid=239986142
		'ehe.de',  # https://de.wikipedia.org/w/index.php?title=Benutzer_Diskussion:CamelBot/maintenance_list/seo_law_pages&diff=prev&oldid=239986142
		'erbrecht-heute.de',  # https://de.wikipedia.org/w/index.php?title=Portal_Diskussion:Recht/Weblinks_und_Belege&oldid=241212818#Kommentarseite
		'e-recht24.de',  # https://de.wikipedia.org/w/index.php?title=Portal_Diskussion:Recht/Weblinks_und_Belege&oldid=241212818#Kommentarseite
		'fach-anwalt.de',  # https://de.wikipedia.org/w/index.php?title=Portal_Diskussion:Recht/Weblinks_und_Belege&oldid=241212818#Kommentarseite
		'fachanwalt.de',  # https://de.wikipedia.org/w/index.php?title=Portal_Diskussion:Recht/Weblinks_und_Belege&oldid=241212818#Kommentarseite
		'fachanwalt-strafrecht-muenchen.org',  # https://de.wikipedia.org/w/index.php?title=Portal_Diskussion:Recht/Weblinks_und_Belege&oldid=244685196#Neue_Links_(April_2024)
		'fahrerlaubnisrecht.de',  # https://de.wikipedia.org/w/index.php?title=Portal_Diskussion:Recht/Weblinks_und_Belege&oldid=247842751#Neue_Links_(Juni_2024)
		'familienrecht.de',  # https://de.wikipedia.org/w/index.php?title=Portal_Diskussion:Recht&diff=prev&oldid=238734079
		'finde-einen-anwalt.de',  # https://de.wikipedia.org/w/index.php?title=Portal_Diskussion:Recht/Weblinks_und_Belege&oldid=241212818#Kommentarseite
		'frag-einen-anwalt.de',  # https://de.wikipedia.org/w/index.php?title=Portal_Diskussion:Recht&diff=prev&oldid=238734079
		'friedhof.de',  # https://de.wikipedia.org/w/index.php?title=Benutzer_Diskussion:CamelBot/maintenance_list/seo_law_pages&diff=prev&oldid=239986142
		'go.rechtsuniversum.de',  # https://de.wikipedia.org/w/index.php?title=Portal_Diskussion:Recht/Weblinks_und_Belege&oldid=241212818#Kommentarseite
		'immobilienscout24.de',  # https://de.wikipedia.org/w/index.php?title=Portal_Diskussion:Recht/Weblinks_und_Belege&oldid=244685196#Neue_Links_(April_2024)
		'internetrecht-rostock.de',  # https://de.wikipedia.org/w/index.php?title=Portal_Diskussion:Recht/Weblinks_und_Belege&oldid=244685196#Neue_Links_(April_2024)
		'juraforum.de',  # https://de.wikipedia.org/w/index.php?title=Portal_Diskussion:Recht&diff=prev&oldid=238734079
		'jurawiki.de',  # https://de.wikipedia.org/w/index.php?title=Portal_Diskussion:Recht/Weblinks_und_Belege&oldid=241212818#Kommentarseite
		'jurinsight.de',  # https://de.wikipedia.org/w/index.php?title=Portal_Diskussion:Recht/Weblinks_und_Belege&diff=prev&oldid=244727277
		'k3s-rechtsanwaelte.de',  # https://de.wikipedia.org/w/index.php?title=Portal_Diskussion:Recht/Weblinks_und_Belege&oldid=247842751#Neue_Links_(Juni_2024)
		'karrierebibel.de',  # https://de.wikipedia.org/w/index.php?title=Portal_Diskussion:Recht/Weblinks_und_Belege&oldid=244685196#Neue_Links_(April_2024)
		'kostenlose-urteile.de',  # https://de.wikipedia.org/w/index.php?title=Portal_Diskussion:Recht&diff=prev&oldid=238734079
		'lebenspartnerschaft.de',  # https://de.wikipedia.org/w/index.php?title=Benutzer_Diskussion:CamelBot/maintenance_list/seo_law_pages&diff=prev&oldid=239986142
		'mietkaution.org',  # https://de.wikipedia.org/w/index.php?title=Portal_Diskussion:Recht/Weblinks_und_Belege&oldid=241212818#Kommentarseite
		'noerr.com',  # https://de.wikipedia.org/w/index.php?title=Portal_Diskussion:Recht/Weblinks_und_Belege&oldid=244685196#Neue_Links_(April_2024)
		'opinioiuris.de',  # https://de.wikipedia.org/w/index.php?title=Portal_Diskussion:Recht/Weblinks_und_Belege&oldid=247842751#Neue_Links_(Juni_2024)
		'politische-union.de',  # https://de.wikipedia.org/w/index.php?title=Wikipedia:Redaktion_Geschichte&oldid=241374278#verfassungen.de
		'privatinsolvenz.net',
		'proverbia-iuris.de',  # https://de.wikipedia.org/w/index.php?title=Portal_Diskussion:Recht/Weblinks_und_Belege&oldid=244685196#Neue_Links_(April_2024)
		'ra-abesser.de',  # https://de.wikipedia.org/w/index.php?title=Portal_Diskussion:Recht/Weblinks_und_Belege&oldid=244685196#Neue_Links_(April_2024)
		'radarfalle.de',  # https://de.wikipedia.org/w/index.php?title=Portal_Diskussion:Recht/Weblinks_und_Belege&oldid=247842751#Neue_Links_(Juni_2024)
		'randstad.de',  # https://de.wikipedia.org/w/index.php?title=Portal_Diskussion:Recht/Weblinks_und_Belege&oldid=244685196#Neue_Links_(April_2024)
		'ra.smixx.de',  # https://de.wikipedia.org/w/index.php?title=Portal_Diskussion:Recht/Weblinks_und_Belege&oldid=244685196#Neue_Links_(April_2024)
		'ratgeber.immowelt.de',  # https://de.wikipedia.org/w/index.php?title=Portal_Diskussion:Recht/Weblinks_und_Belege&oldid=244685196#Neue_Links_(April_2024)
		'rechtslexikon.net',  # https://de.wikipedia.org/w/index.php?title=Portal_Diskussion:Recht/Weblinks_und_Belege&oldid=241212818#Kommentarseite
		'rechtswoerterbuch.de',  # https://de.wikipedia.org/w/index.php?title=Portal_Diskussion:Recht/Weblinks_und_Belege&oldid=244685196#Neue_Links_(April_2024)
		'reichsgesetzblatt.de',  # https://de.wikipedia.org/w/index.php?title=Portal_Diskussion:Recht/Weblinks_und_Belege&oldid=244685196#Neue_Links_(April_2024)
		'sachenrecht.de',  # https://de.wikipedia.org/w/index.php?title=Portal_Diskussion:Recht&diff=prev&oldid=238734079
		'scheidung.de',  # https://de.wikipedia.org/w/index.php?title=Benutzer_Diskussion:CamelBot/maintenance_list/seo_law_pages&diff=prev&oldid=239986142
		'scheidung.org',
		'scheidungs-kosten.de',  # https://de.wikipedia.org/w/index.php?title=Benutzer_Diskussion:CamelBot/maintenance_list/seo_law_pages&diff=prev&oldid=239986142
		'schuldnerberatung.de',
		'schuldrecht.de',  # https://de.wikipedia.org/w/index.php?title=Portal_Diskussion:Recht&diff=prev&oldid=238734079
		'sie-hoeren-von meinem-anwalt.de',  # https://de.wikipedia.org/w/index.php?title=Portal_Diskussion:Recht&diff=prev&oldid=238734079
		'staatsvertraege.de',  # https://de.wikipedia.org/w/index.php?title=Wikipedia:Redaktion_Geschichte&oldid=241374278#verfassungen.de
		'strafzettel.de',  # https://de.wikipedia.org/w/index.php?title=Portal_Diskussion:Recht/Weblinks_und_Belege&oldid=247842751#Neue_Links_(Juni_2024)
		'terminsvertretung.de',  # https://de.wikipedia.org/w/index.php?title=Portal_Diskussion:Recht/Weblinks_und_Belege&oldid=247842751#Neue_Links_(Juni_2024)
		'testament-erben.de',  # https://de.wikipedia.org/w/index.php?title=Benutzer_Diskussion:CamelBot/maintenance_list/seo_law_pages&diff=prev&oldid=239986142
		'trennung.de',  # https://de.wikipedia.org/w/index.php?title=Benutzer_Diskussion:CamelBot/maintenance_list/seo_law_pages&diff=prev&oldid=239986142
		'unfallhelden.de',  # https://de.wikipedia.org/w/index.php?title=Portal_Diskussion:Recht/Weblinks_und_Belege&oldid=247842751#Neue_Links_(Juni_2024)
		'unterhalt.com',  # https://de.wikipedia.org/w/index.php?title=Benutzer_Diskussion:CamelBot/maintenance_list/seo_law_pages&diff=prev&oldid=239986142
		'verfassungen.at',  # https://de.wikipedia.org/w/index.php?title=Wikipedia:Redaktion_Geschichte&oldid=241374278#verfassungen.de
		'verfassungen.ch',  # https://de.wikipedia.org/w/index.php?title=Wikipedia:Redaktion_Geschichte&oldid=241374278#verfassungen.de
		'verfassungen.de',  # https://de.wikipedia.org/w/index.php?title=Wikipedia:Redaktion_Geschichte&oldid=241374278#verfassungen.de
		'verfassungen.eu',  # https://de.wikipedia.org/w/index.php?title=Wikipedia:Redaktion_Geschichte&oldid=241374278#verfassungen.de
		'verfassungen.net',  # https://de.wikipedia.org/w/index.php?title=Wikipedia:Redaktion_Geschichte&oldid=241374278#verfassungen.de
		'verkehrslexikon.de',  # https://de.wikipedia.org/w/index.php?title=Portal_Diskussion:Recht/Weblinks_und_Belege&oldid=244685196#Neue_Links_(April_2024)
		'verkehrsrecht-fuer-alle.de',  # https://de.wikipedia.org/w/index.php?title=Benutzer_Diskussion:CamelBot/maintenance_list/seo_law_pages&diff=prev&oldid=239986142
		'versailler-vertrag.de',  # https://de.wikipedia.org/w/index.php?title=Wikipedia:Redaktion_Geschichte&oldid=241374278#verfassungen.de
	);
	my $seo_law_domains_re = $self->array2regexp(\@seo_law_domains);
	my $christian_domains_re = $self->array2regexp(\@christian_domains);
	my $wp_lang_re = qr/(?:[a-z]{2,3}(?:-[a-z-]+)?|simple)/;
	$self->{'maintenance'} = {
		'text_re'    => { # regexps in text that triggers some action
			(map {$_ => qr/\b(?<!-)\Q$_\E\b/} @$tracked_domains),
			'archive.today'        => $self->{'archive.today'}{'re_short_url'},
			'ASIN'                 =>
				qr/\bASIN(?:\]\])?[: \x{200e}\x{200f}]+(?:B[0-9]{2}[A-Z0-9]{7}|[0-9]{9}[0-9X])/,
			'cat dead'             => qr/\[\[\s*(?i:category|kategorie):
					\s*[Gg]estorben[\s|_]2[0-9]*\s*(?:\|[^\]]*)?\]\]/x,
			'christian_pages'      => qr/\b(?<!-)$christian_domains_re\b/,
			'commons-ref'          => qr/
					(?:https?:|\[)\/\/commons\.wikimedia\.org\/wiki\/[Ff]ile
					|\[\[(?:(?::?w)?:)?c(?:ommons)?:(?:[fF]ile|[dD]atei):
					|upload\.wikimedia\.org\/
				/x,
			'edit-links'           => qr/(?:https?:|)\/\/[a-z0-9_-]+\.wikipedia\.org\/
				(?:wiki\/[^\s\]]+|w\/index\.php)\?(?:[^=\s\]]+=[^=\s\]]*&)*(?:ve|)action=(?:edit|submit)\b/x,
			'faz.net'              => qr/
				https?:\/\/(?:www\.)?faz\.net\/[0-9a-zA-Z_.-]{1,12}+(?!\/)/x,
			'ip_creates_own_talk_page' => qr/.?/,
			'local-file'           => qr/\bfile\:\/{3}|\/{3}[A-Z]:\//i,
			'media-viewer'          => qr/
				\[\[[^\]]*\#\/media\/.*?\]\]
				|(?:https?:|)\/\/[a-z_0-9-]+\.wikipedia\.org\/wiki\/[^\]\s]+
				\#\/media\/[^\s\]]+/x,
			'self-ref'             => qr/<\s*ref\b([^>\/](?!group=))*>\s*\K
				(?:
					# external link to wikipedia (1. except from images and
					# 2. only if there is no other external link following)
					 \[?(?:https?\:)?\/\/[a-z.-]+\.wikipedia\.org\/wiki\/
						[^\x{20}\x{23}\]}<>]++(?!\x{23}\/media)(?:(?!http)(?!<\/ref>)\S)*+(?!http)
					# external link to wikidata
					|\[?(?:https?\:)?\/\/www\.wikidata\.org\/wiki\/Q[1-9]
					# internal link to wikipedia
					|\[\[
						(?::?w|:$wp_lang_re)
						:(?!commons:)(?![cs]:)             # skip commons
						(?![fF]ile:|[dD]atei:)             # skip files
						(?:(?!\]\]).)++\]\]                # remaining characters of link
						(?=(?:(?!http)(?!<\/ref>).)*+(?!http)) # if additional ext. link, then skip
				) # wikipedia link inside of template
				|[ \|]Online\s*=\s*https?\:\/\/[a-z-]+\.wikipedia\.org
				/x,
			'seo_law_pages'        => qr/\b(?<!-)$seo_law_domains_re\b/,
			#'test'        => qr/./,
		},
		'wait_till_list' => { # mid-term or short-term
			(map {$_ => 'mid-term'} @$tracked_domains),
			'archive.today'            => 'mid-term',
			'ASIN'                     => 'mid-term',
			'cat dead'                 => 'mid-term',
			'christian_pages'          => 'mid-term',
			'commons-ref'              => 'mid-term',
			'edit-links'               => 'mid-term',
			'faz.net'                  => 'mid-term',
			'ip_creates_own_talk_page' => 'short-term',
			'local-file'               => 'mid-term',
			'media-viewer'             => 'mid-term',
			'self-ref'                 => 'mid-term',
			'seo_law_pages'            => 'mid-term',
		},
		'user'    => { # regexps if only distinct users shall trigger
			'ip_creates_own_talk_page' => qr/^(?:(?:[0-9]{1,3}\.){3}[0-9]{1,3}|2.*:.*:.*)\z/,
		},
		'exceptions' => { # regexps of pages that must be spared
			'allmovie.com'         => qr/^AllMovie\z/,
			'archive.today'        => undef,
			'ASIN'                 => qr/^Amazon Standard Identification Number\z/,
			'cat dead'             => undef,
			'christian_pages'      => qr/^(?:\x{d6}kumenisches\sHeiligenlexikon)\z/,
			'commons-ref'          => undef,
			'faz.net'              => undef,
			'geni.com'             => qr/
				^(?:
					Geni\.com
					|Liste\sgenealogischer\sDatenbanken
				)
			\z/x,
			'local-file'           => qr/
				^(?:
					Job\sDefinition\sFormat
					|QXML\sDOM
					|Uniform\sResource\s(?:Identifier|Locator)
					|XML\sBase
					|XML\sShareable\sPlaylist\sFormat
					|Zwischenablage
				)
			\z/x,
			# 'Wikipedia', 'Spanische Wikipedia', 'Wikipedia Zero', ...
			'self-ref'             => qr/Enzyklop\x{34}dien\saus\sdem\schinesischen\sKulturkreis
				|MediaWiki|Wikipedia|(?:^(Baidu\sBaike|Dariusz\sJemielniak|Kaschuben)\z)/x,
			'seo_law_pages'        => qr/^
				(?:Bund-Verlag|Deniz\sAytekin|ImmoScout24|Jurawiki|Michael\sTerhaag|Scout24\sSE)\z/x,
		},
		'intros'     => {}, # intros of listing pages will be set later
		# the following requires page user:CamelBot/notice-... to exist
		'notifiers'  => { # heading of notifiers on talk pages
			#'archive.today' => 'Hinweis zu Verlinkungen von archive.(today,is,fo,li,md,ph,vn)',
			(map {$_ => 'Hinweis zu Verlinkung von ' . $_}
				('allmovie.com',
					'geni.com',
					'gerontology.fandom.com',
					'mapcarta.com',
					'reppa.de',
					'the110club.com',
					'warheroes.ru'
				)
			),
			'ASIN'          => 'Hinweis zu Katalog-Nummern von Amazon',
			'christian_pages' => 'Hinweis zur deiner Verlinkung mit christlichem Bezug',
			'edit-links'    => 'Hinweis zu Verlinkung von Seiten im Edit-Modus',
			'faz.net'       => 'Hinweis zu Kurz-URL-Verlinkung von faz.net',
			'local-file'    => 'Hinweis auf Verlinkung lokaler Dateien',
			'media-viewer'  => 'Hinweis zu Verlinkung von Bildern',
			'self-ref'      => 'Hinweis auf Selbstreferenzierungen',
			'seo_law_pages' => 'Hinweis zur deiner Verlinkung mit juristischem Bezug',
		},
		'page_name'  => { # page name of maintenance list in category
			'cat dead'    => 'categories of the dead',
			# if not defined, use key
		},
		'namespaces' => { # namespaces in which text_re will be used as trigger
			(map {$_ => [0, 6, 10, 14]} @$tracked_domains),
			'archive.today'        => [0, 6, 10, 14],
			'ASIN'                 => [0],
			'cat dead'             => [0],
			'christian_pages'      => [0, 6, 10, 14],
			'commons-ref'          => [0],
			'edit-links'           => [0],
			'faz.net'              => [0, 6, 10, 14],
			'ip_creates_own_talk_page' => [3],
			'local-file'           => [0, 6, 10, 14],
			'media-viewer'         => [0],
			'self-ref'             => [0],
			'seo_law_pages'        => [0, 6, 10, 14],
			#'test'                 => [0],
		},
		'check_entries' => { # re-check all entries on any modification
			(map {$_ => 1} @$tracked_domains),
			'archive.today'        => 1,
			'ASIN'                 => 1,
			'cat dead'             => 0,
			'christian_pages'      => 1,
			'commons-ref'          => 1,
			'edit-links'           => 1,
			'faz.net'              => 0,
			'ip_creates_own_talk_page' => 0,
			'local-file'           => 1,
			'media-viewer'         => 1,
			'self-ref'             => 1,
			'seo_law_pages'        => 1,
		},
		'delete_after_days' => { # delete entries of listing pages
			'allmovie.com'         => 365,
			'cat dead'             => 14,
			'geni.com'             => 365,
			'mapcarta.com'         => 61,
		},
		'delete_if_deleted' => { # delete entries of deleted pages
			(map {$_ => 1} @$tracked_domains),
			'christian_pages'          => 1,
			'edit-links'               => 1,
			'ip_creates_own_talk_page' => 1,
			'media-viewer'             => 1,
			'seo_law_pages'            => 1,
		},
		'special_conditions' => { # additional conditions
			'ip_creates_own_talk_page' => sub{
				my $rc_msg = shift;
				return # $rc_msg->{'rc_old_len'} // 0 == 0 &&
					$rc_msg->{'page'} eq $rc_msg->{'user'};
			},
		},
		'post-process_match' => {
			'self-ref' => sub{
				my $match = shift;
				$match =~ s/\[\[:[a-z]+:\K([^|}]*)/$self->normalize_page_title($1)/e;
				return $match;
			},
		},
		'call_back' => { # functions that will be called if text_re matches
			'immediate' => {
				#'test' => sub{ say "moep"; },
			},
			'short-term' => {
				#not implemented yet
			},
			'mid-term' => {
				#not implemented yet
			},
		},
	};
	# setting intros of maintenance lists
	my %i_parts = (
		'intro' => 'dies ist eine durch CamelBot befuellte wartungsliste, '
			. 'die kuerzlich (auf wikipedia/wikidata) bearbeitete seiten auflistet',
		'general' => 'CamelBot lauscht auf den \'\'recent changes\'\', d.h., '
			. 'er schaut sich alle seiten an, die aktuell geaendert wurden.',
		'condition_link' => 'faellt ihm dabei auf, dass sich in einem artikel '
			. 'ein verweis auf jene domain befindet',
		'action' => 'meldet er das nach ca. 30 minuten hier auf dieser seite',
		'deleting_link' => 'auch das loeschen von eintraegen, die '
			. 'einen solchen verweis nicht mehr enthalten, uebernimmt CamelBot.',
		'list_section' => "== liste ==\n",
	);
	$self->{'maintenance'}{'intros'} = {
		'archive.today' => $i_parts{'intro'} .
			", die kurz-urls auf archive.(fo|is|li|md|ph|vn|today) " .
			" enthalten.\n\n" . $i_parts{'general'} .
			" faellt ihm dabei auf, dass sich in einem artikel" .
			" ein link von der form archive.is/XXXXX" .
			" befindet, wobei X fuer ein alphanumerisches Zeichen steht, " .
			$i_parts{'action'} . '. ' . $i_parts{'deleting_link'} . "\n\n" .
			$i_parts{'list_section'},
		'ASIN' => $i_parts{'intro'} .
			", die mind. eine [[Amazon Standard Identification Number|ASIN]]" .
			" enthalten.\n\n" . $i_parts{'general'} .
			" faellt ihm dabei auf, dass in einem artikel eine ASIN steht, " .
			$i_parts{'action'} . '. ' .
			"auch das loeschen von eintraegen, die keine ASIN mehr enthalten, " .
			"uebernimmt CamelBot.\n\n" .
			"$i_parts{'list_section'}",
		'cat dead' => $i_parts{'intro'} .
			", die der category ''gestorben 2xxx" . #(strftime('%Y', gmtime())) .
			"'' hinzugefuegt wurden.\n\n" .  $i_parts{'general'} .
			" faellt ihm dabei auf, " .
			"dass ein artikel dieser category hinzugefuegt wird, " .
			'meldet er das hier auf dieser seite. ' .
			"geloescht werden die eintraege von CamelBot automatisch " .
			"nach einer bestimmten zeit.\n\n" .
			"$i_parts{'list_section'}",
		'christian_pages' => $i_parts{'intro'} .
			", die mind. einen verweis auf eine der domains "
			. join(', ', @christian_domains)
			. " enthalten.\n\n" .
			"$i_parts{'general'} $i_parts{'condition_link'}, $i_parts{'action'}. " .
			"$i_parts{'deleting_link'}\n\n$i_parts{'list_section'}",
		'edit-links' => $i_parts{'intro'} . ", die mind. einen link auf die " .
			"editier-ansicht eines artikels haben, anstatt einen konventionellen " .
			"link.\n\n" .
			$i_parts{'general'} .
			" faellt ihm dabei auf, dass ein artikel einen solchen link enthaelt, " .
			$i_parts{'action'} . '. ' .
			"auch das loeschen von eintraegen, " .
			"die keinen solchen editier-link mehr enthalten, uebernimmt CamelBot.\n\n" .
			$i_parts{'list_section'},
		'ip_creates_own_talk_page' =>
			'dies ist eine durch CamelBot befuellte wartungsliste, ' .
			"die edits von ip-adressen auf deren eigenen talk pages auflistet.\n\n" .
			$i_parts{'general'} . ' faellt ihm dabei auf, dass eine ' .
			"''user talk page'' einer ip-adresse von dieser ip-adresse bearbeitet " .
			"wurde, meldet er dies hier auf dieser seite.\n\n" .
			"$i_parts{'list_section'}",
		'local-file' => $i_parts{'intro'} .
			", die mind. eine relative adresse auf die festplatte enthalten.\n\n" .
			$i_parts{'general'} .
			" faellt ihm dabei auf, " .
			"dass in einem artikel eine solche adresse steht, " .
			$i_parts{'action'} . '. ' .
			"auch das loeschen von eintraegen, " .
			"die keine solche adresse mehr enthalten, uebernimmt CamelBot.\n\n" .
			"$i_parts{'list_section'}",
		'media-viewer' => $i_parts{'intro'} . ", die mind. einen link auf eine " .
			"problematische bild-ansicht enthalten. diskutiert wurde das ["
			. "https://de.wikipedia.org/w/index.php?oldid=227930843#Wikilinks_auf_den_Wikipedia-Mediaviewer"
			. " 2022 auf WP:FZW].\n\n" .
			$i_parts{'general'} .
			" faellt ihm dabei auf, dass ein artikel einen solchen link enthaelt, " .
			$i_parts{'action'} . '. ' .
			"auch das loeschen von eintraegen, " .
			"die keinen solchen bild-link mehr enthalten, uebernimmt CamelBot.\n\n" .
			$i_parts{'list_section'},
		'self-ref' => $i_parts{'intro'} . ", die mind. eine " .
			"[[WP:Belege#Wikipedia_ist_keine_Quelle|selbstreferenz]] enthalten, " .
			"also einen unzulaessigen beleg durch einen anderen wikipedia-artikel " .
			"oder aehnliches.\n" .
			"siehe auch [[user:Krdbot/ANR-Schwesterprojekt-Links]].\n\n" .
			$i_parts{'general'} .
			" faellt ihm dabei auf, dass ein artikel eine selbstreferenz enthaelt, " .
			$i_parts{'action'} . '. ' .
			"auch das loeschen von eintraegen, " .
			"die keine selbstreferenz mehr enthalten, uebernimmt CamelBot.\n\n" .
			$i_parts{'list_section'},
		'seo_law_pages' => $i_parts{'intro'} .
			", die mind. einen verweis auf eine der domains "
			. join(', ', @seo_law_domains)
			. " enthalten.\n\n" .
			"$i_parts{'general'} $i_parts{'condition_link'}, $i_parts{'action'}. " .
			"$i_parts{'deleting_link'}\n\n$i_parts{'list_section'}",
	};
	for my $domain(@$tracked_domains){
		$self->{'maintenance'}{'intros'}{$domain} = $i_parts{'intro'} .
			", die mind. einen verweis auf $domain enthalten. "
			. "zum hintergrund siehe [[WP:Weblinks/Block/$domain]].\n\n" .
			"$i_parts{'general'} $i_parts{'condition_link'}, $i_parts{'action'}. " .
			"$i_parts{'deleting_link'}\n\n$i_parts{'list_section'}";
	}
	return 1;
}

sub resolve_description_from_wikilinks{
	my $self = shift;
	my $wikitext = shift;
	# [[link|description]] -> description
	$wikitext =~ s/\[\[[^|\]]*\|([^|\]]*)\]\]/$1/g;
	# [[description]] -> description
	$wikitext =~ s/\[\[|\]\]//g;
	return $wikitext;
}

sub save_wiki_page{
	my $self = shift;
	my $page = shift;
	my $summary = shift;
	my $text = shift;
	my $orig_text = shift;
	my $minor = shift // $self->{'minor'};
	my $success = 1; # no change or just simulation
	if($$text ne $$orig_text){
		return 0 unless $self->is_allowed($orig_text, $page);
		my $user_answer_diff;
		if(defined $self->{'showdiff'} && $self->{'showdiff'} > 0){
			$user_answer_diff = $self->show_diff($text, $orig_text);
		}
		unless(defined $summary){
			$self->msg(0, 'undefined summary, using a general summary', 'warning');
			$summary = $self->i18n('automated edit');
		}
		if(!(defined $self->{'simulation'}) || !$self->{'simulation'}){
			$self->time_management();
			my $user_input;
			$user_input = $user_answer_diff ? 'yes' : 'no' if defined $user_answer_diff;
			if(!defined($user_input)
					&& defined($self->{'ask_user'}) && $self->{'ask_user'}){
				print "execute? ('y' = yes, else = no) ";
				chomp($user_input = <STDIN>);
			}
			if(!(defined $user_input) || $user_input=~/y(?:es)?/
				|| ($user_input eq '' && $self->{'user_answer'} eq 'y')
			){
				$self->{'user_answer'} = 'y';
				for(my $num_tries = 1; $num_tries < 4 and $success <= 1; ++$num_tries){
					$self->msg(1, "saving page [[$page]] with summary '$summary'");
					push @{$self->{'time_stack'}}, time;
					my $res = $self->{'mw_bot'}->edit({
							minor    => $minor,
							page     => $page,
							text     => $$text,
							bot      => 1,
							summary  => "Bot: $summary",
					});
					if($res){
						$success = 2; # real success
					}else{
						$success = -1; # failed
						$self->_handle_api_error('bot');
						if($self->{'mw_bot'}->{'api'}->{'error'}{'code'} =~ /\b3\b/ &&
							$self->{'mw_bot'}->{'api'}->{'error'}{'details'} =~
								/assertbotfailed|Assertion .* failed/){
							# restart bot
							$self->{'logged_in'} = 0;
							$self->login() or die;
						}
						if($self->{'mw_bot'}->{'api'}->{'error'}{'code'} =~ /\b3\b/ &&
							$self->{'mw_bot'}->{'api'}->{'error'}{'details'} =~
								/protectedpage|spamblacklist|editconflict/){
							last;
						}
						$self->msg(0,
							"saving page failed (trying again: num_tries = $num_tries) ",
							'error');
						sleep(5 * $num_tries);
					}
				}
				$self->msg(2, "mem usage:\n" . $self->_get_mem_usage(undef, 'after saving a page'));
			}else{
				$success = 0; # user said: no
				$self->{'user_answer'} = '';
			}
		}else{
			$self->msg(1, "simulation of saving of page [[$page]] with summary '$summary'");
			$self->msg(4, Dumper($text));
		}
	}
	return $success;
}

sub search_contribs{
	my $self     = shift;
	my $username = shift;
	my $search   = shift; # {del =>, add =>...}
	my $params   = shift; # {limit => ..., skip_pages_re => ...}
	my $contribs = $self->get_user_contribs({
			'username' => $username,
			'limit'  => $params->{'limit'},
		});
	$self->msg(1, "got " . scalar(keys %$contribs) . " contribs");
	my $filtered_contribs;
	$self->msg(4, Dumper($contribs));
	while(my ($title, $edits) = each %$contribs){
		if(defined $params->{'skip_pages_re'} && $title =~ $params->{'skip_pages_re'}){
			next;
		}
		for my $edit(@$edits){
			my $edit = $self->get_diff({'revid' => $edit->{'revid'}});
			my $found = 0;
			for my $d(@{$edit->{'diff'}}){
				if(defined $search->{'add'}){
					for my $del(@{$d->{'diff-deletedline'}}){
						if($del->{'type'} eq 'add' && $del->{'text'} =~ $search->{'add'}){
							++$found;
						}
					}
				}
				if(defined $search->{'del'}){
					for my $del(@{$d->{'diff-deletedline'}}){
						if($del->{'type'} eq 'del' && $del->{'text'} =~ $search->{'del'}){
							++$found;
						}
					}
				}
			}
			if($found){
				push @$filtered_contribs, $edit;
				$self->msg(2, "found $found occurences in '$title'");
			}
		}
	}
	return $filtered_contribs;
}

sub search_history{
	my $self       = shift;
	my $page       = shift;
	my $conditions = shift // {};
	#$conditions->{'summary_re'} = qr/.*/ unless defined $conditions->{'summary_re'};
	my $results = [];
	my $ready = 0;
	my $search_params = {};
	if(defined $conditions->{'user'}){
		$search_params->{'rvuser'} = $conditions->{'user'};
	}
	while(!$ready){
		my $hist = $self->get_history_step_by_step($page, $search_params);
		$self->msg(2, 'found ' . scalar(@$hist) . ' edits');
		# The array returned contains hashrefs with keys:
		# revid, user, comment, minor, timestamp_date, and timestamp_time.
		#$self->msg(0, Dumper([$hist, $cont]));
		if(@$hist == 0 || !defined($search_params->{'continue'})){
			$ready = 1;
		}
		push @$results, grep {
			!defined($conditions->{'summary_re'}) || (
				defined($_->{'comment'}) && $_->{'comment'} =~ $conditions->{'summary_re'}
			)
		} @$hist;
	}
	return $results;
}

# 2021-07-12 MediaWiki::Bot 5.006003 is buggy and can't cope with namespaces in search,
# so using an own function.
sub _search_wrapper {
	my $self    = shift;
	my $term    = shift;
	my $ns      = shift // 0;
	my $mw_options = shift;
	if(ref $ns eq 'ARRAY'){
		$ns = join('|', @$ns);
	}
	my $query = {
		action   => 'query',
		list     => 'search',
		srnamespace => $ns,
		srsearch => $term,
		srwhat   => 'text',
		srlimit  => 'max',
		#srinfo      => 'totalhits',
		srprop      => 'size|snippet',
		#srredirects => 0,
	};
	$mw_options->{max} = 1 unless $mw_options->{max};
	my $finished = 0;
	my @results;
	my $continue = {};
	while(!$finished){
		(my $pages, $finished) = $self->_api_cont($query, $continue, $mw_options);
		return $self->_handle_api_error() unless $pages;
		push @results, @{$pages->{'query'}{'search'}};
	}
	#return 1 unless ref $results; # not a ref when used with callback
	return \@results;
}

sub search_pages{
	my $self       = shift;
	my $search     = shift;
	my $namespaces = shift // $self->{'namespaces_used'} // [0];
	my $full_results = shift; # return page names only if undef
	#my $results = [$self->{'mw_bot'}->search($search, $namespaces)];
	my $results = $self->_search_wrapper($search, $namespaces);
	if($full_results){
		return $results;
	}else{
		return [map { $_->{'title'} } @$results];
	}
}

sub search_sbl_blocked_log{
	my $self   = shift;
	my $regexp = shift;
	my $proj   = shift // $self->{'wm_proj'};
	my @results = ();
	my $db_table;
	if($self->get_default_access_type() eq 'db'){
		$db_table = $self->db_fetch_sbl_log($proj, $regexp);
	}
	if(defined $db_table){
		for my $dataset(@$db_table){
			push @results, {
				'logid'     => $dataset->[0],
				'timestamp' => $dataset->[1],
				'ns'        => $dataset->[2],
				'title'     => $dataset->[3],
				'comment'   => $dataset->[4],
				'url'       => $dataset->[5],
				'user'      => $dataset->[6],
			};
			$results[-1]->{'url'} =~ s/^.*?"(http[^"]*)".*$/$1/;
		}
		$self->msg(2, 'found ' . scalar(@results) . ' matching blocked edits.');
	}elsif(not defined $proj or $proj eq $self->{'wm_proj'}){ # use api
		# api.php?action=query&list=logevents&letype=spamblacklist&leprop=ids|title|type|user|timestamp|comment|details&continue=
		my $query = {
			'action'         => 'query',
			'list'           => 'logevents',
			'letype'         => 'spamblacklist',
			'lelimit'        => '500',
			'leprop'         => 'title|type|user|timestamp|comment|details|ids',
			#'leend'          => '2013-01-01T00:00:00',
			#'lestart'        => '2023-07-16T00:00:00',
		};
		my $mw_options = {'max' => 1};
		my $finished = 0;
		my $num_results = 0;
		my $log_counter = 0;
		my $continue = {};
		while(!$finished){
			(my $pages, $finished) = $self->_api_cont($query, $continue, $mw_options);
			my $curr_counter = scalar(@{$pages->{'query'}{'logevents'}});
			$log_counter += $curr_counter;
			$self->msg(4, Dumper($pages->{'query'}{'logevents'}));
			push @results, grep {
					if(defined $_->{'params'}{'url'}){
						$_->{'url'} = $_->{'params'}{'url'};
						delete $_->{'params'};
						$_->{'url'} =~ /$regexp/
					}else{
						0;
					}
			} @{$pages->{'query'}{'logevents'}};
			if($self->{'verbosity'} >= 3 and $curr_counter > 0){
				my $min_ts = $pages->{'query'}{'logevents'}->[0]->{'timestamp'};
				my $max_ts = $min_ts;
				map {
					my $ts = $_->{'timestamp'};
					$min_ts = $ts if $ts lt $min_ts;
					$max_ts = $ts if $ts gt $max_ts;
				} @{$pages->{'query'}{'logevents'}};
				$self->msg(3, 'found ' . (scalar(@results) - $num_results) .
					" blocked edits ($min_ts till $max_ts).");
			}
			$num_results = scalar(@results);
		}
		map {
			delete $_->{'type'};
			delete $_->{'anon'};
			delete $_->{'action'};
		} @results;
		$self->msg(1, "log contains $log_counter entries. "
			. "found $num_results matching blocked edits.");
	}else{
		$self->msg(0, "not defined for proj = '$proj'", 'error');
	}
	return \@results;
}

sub search_sbl_attempts{
	my $self   = shift;
	my $params = shift;
	$params->{'proj'} = $self->{'wm_proj'} unless defined $params->{'proj'};
	$params->{'sbl'}  = $self->{'wm_proj'} unless defined $params->{'sbl'};
	# https://noc.wikimedia.org/conf/highlight.php?file=dblists/large.dblist
	# https://meta.wikimedia.org/wiki/List_of_Wikipedias
	my @large_projects = qw(ceb de en es fr it ja nl pl pt ru sv uk vi war zh);
	map {$_ .= 'wiki'} @large_projects; ## no critic (MutatingListFunctions)
	# search for given regexp
	if(exists $params->{'regexp'}){
		my %results = ();
		if($params->{'proj'} eq 'large'){
			# search large wikipedias for a given regexp; return all single spam
			# attempts
			for my $proj(@large_projects){
				$results{$proj} = $self->search_sbl_blocked_log(
					$params->{'regexp'}, $proj);
			}
		}elsif($params->{'proj'} eq $self->{'wm_proj'}){
			# search current project for given regexp
			$results{$params->{'proj'}} = $self->search_sbl_blocked_log(
					$params->{'regexp'});
		}elsif(grep {$_ eq $params->{'proj'}} @large_projects){
			# search current project for given regexp
			$results{$params->{'proj'}} = $self->search_sbl_blocked_log(
					$params->{'regexp'}, $params->{'proj'});
		}else{
			$self->msg(0, 'not implemented yet', 'error');
			#$results{$params->{'proj'}} = $self->search_sbl_blocked_log(
			#		$params->{'regexp'}, $params->{'proj'});
		}
		# print results
		while(my ($proj, $blocked_edits) = each %results){
			$self->msg(1, "$proj: " . scalar(@$blocked_edits));
			# the ids can be used in urls such as
			# https://$lang.wikipedia.org/w/index.php?title=Special:Log&type=spamblacklist&logid=$ids
			for my $edit(@$blocked_edits){
				$self->msg(1, " $edit->{'timestamp'}, $edit->{'logid'}, $edit->{'url'}, "
					. "$edit->{'user'}; $edit->{'title'}");
			}
		}
	}else{ # if no explicite regexp is set, then use all sbl entries
		# search large wikipedias for all entries of meta sbl
		my $table_header = '{| class="wikitable sortable" ' .  # }
			'style="table-layout:fixed; width:100%;"' . "\n" .
			'! style="width:300px;" | regexp || ';
		my $table_row_begin = "|-\n| style=\"overflow:hidden;\" |";
		if($params->{'sbl'} =~ /^meta(?:wiki)?\z/){
			my $sbl_meta = $self->get_sbl_entries('meta');
			$self->msg(1, 'found ' . scalar(@$sbl_meta) . ' entries in sbl.');
			my $projects;
			if($params->{'proj'} eq 'large'){
				$projects = \@large_projects;
			}elsif($params->{'proj'} eq 'all'){
				my ($http_status, $all_projects) = $self->get_http_content(
					'https://noc.wikimedia.org/conf/dblists/all.dblist');
				$projects = [grep {$_ !~ /^(?:
					(?:advisors|arbcom_.*|auditcom|board|boardgovcom|chair|chap|chapcom|
						checkuser|collab|electcom|exec|fdc|grants|iegcom|internal|labs|
						labtest|legalteam|movementroles|office|ombudsmen|otrs_wiki|projectcom|
						searchcom|spcom|steward|techconduct|transitionteam|wg_en|wikimania.*|
						zero
					)wiki|
					(?:ec|id_internal|il|noboard_chapters)wikimedia)$
					/x} split /\n/, $all_projects];
			}else{
				$self->msg(0, 'not implemented yet', 'error');
			}
			if(defined $projects){
				print $table_header . (join ' || ', sort @$projects) . " || sum\n";
				for my $regexp(@$sbl_meta){
					my %results = ();
					my $sum = 0;
					for my $proj(@$projects){
						$results{$proj} = $self->search_sbl_blocked_log($regexp, $proj);
						$sum += @{$results{$proj}};
					}
					print "$table_row_begin $regexp || " .
						(join ' || ', (map {scalar(@{$results{$_}})} sort keys %results));
					print ' || ' . $sum . "\n";
				}
				print "|}\n";
			}
		}elsif($params->{'sbl'} eq $self->{'wm_proj'}
				and $params->{'proj'} eq $self->{'wm_proj'}){
			# search current mediawiki for all entries of its sbl
			my $proj = $self->{'wm_proj'};
			my $sbl  = $self->get_sbl_entries();
			$self->msg(1, 'found ' . scalar(@$sbl) . ' entries in sbl.');
			print $table_header . "$proj\n";
			for my $regexp(@$sbl){
				my $results = $self->search_sbl_blocked_log($regexp, $proj);
				print "$table_row_begin $regexp || " . scalar(@$results) . "\n";
			}
			print "|}\n";
		}else{
			$self->msg(0, 'not implemented yet', 'error');
		}
	}
	return 1;
}

sub _set_api{
	my $self = shift;
	# TODO: maybe this should just be replaced by:
	# $self->{'mw_api'} = $self->{'mw_bot'}->{'api'};
	$self->{'mw_api'} = MediaWiki::API->new();
	# 2021-04-11 seth: bugfix of https://rt.cpan.org/Public/Bug/Display.html?id=120643
	$self->{'mw_api'}->{'ua'}->ssl_opts(
		'SSL_ocsp_mode' => SSL_OCSP_NO_STAPLE);
	my $wiki_base_url = 'https://' . $self->{'host'};
	$self->{'mw_api'}{'config'}{'api_url'} = $self->_get_api_url();
	if(  $self->{'cliparams'}{'delete'}
		|| $self->{'cliparams'}{'upload'}
		|| $self->{'cliparams'}{'link-replacement'}
		|| $self->{'cliparams'}{'search-sbl-attempts'}
	){
		$self->{'mw_api'}->{'config'}{'upload_url'} =
			$wiki_base_url . '/wiki/Special:Upload';
		#TODO: check/test upload_url
		$self->{'mw_api'}->{'config'}{'files_url'} = '';
	}
	return 1;
}

sub set_host{
	my $self = shift;
	my $host = shift // $self->{'host'};
	$self->{'host'} = $host;
	if($self->{'host'} =~ /^([a-z]+)\.(
			wikibooks|wikidata|wikimedia|wikinews|wikipedia|wikiquote|
			wikisource|wikiversity|wikivoyage|wiktionary
		)\.org\z/x
	){
		$self->{'wm_lang'} = $1;
		$self->{'wm_proj_type'} = $2;
		$self->{'wm_proj'} = $self->{'wm_lang'} . $self->{'wm_proj_type'};
		$self->{'wm_proj'} =~ s/wiki\K[pm]edia\z//;
	}
	my $changed;
	if(defined $self->{'mw_bot'}){
		$self->{'mw_bot'}{'host'} = $host;
		$self->{'mw_bot'}{'api'}{'config'}{'api_url'} = $self->_get_api_url();
		$changed = 1;
	}
	if(defined $self->{'mw_api'}){
		$self->{'mw_api'}{'config'}{'api_url'} = $self->_get_api_url();
		$changed = 1;
	}
	if($changed){
		$self->msg(1, 'changed host to ' . $self->_get_api_url());
	}
}

sub show_diff{
	my $self      = shift;
	my $text      = shift;
	my $orig_text = shift;
	return if($_running_on_labs || !$_loaded_mod{'Text::Diff'});
	my $diff      = Text::Diff::diff($text, $orig_text);
	my $result;
	print $diff if $self->{'showdiff'} & 1;
	if($self->{'showdiff'} & 2){
		my @text_lines = grep {s/^\s*(.*?)\s*$/$1/} split /\n/, $$text; ## no critic (MutatingListFunctions)
		my @orig_text_lines = grep {s/^\s*(.*?)\s*$/$1/} split /\n/, $$orig_text; ## no critic (MutatingListFunctions)
		my @lines_to_show = ();
		while($diff =~ /@@ -(\d+),(\d+) \+\d+,\d+ @@/g){
			push @lines_to_show, [$1-1, $1+$2-2];
		}
		#print Dumper(\@lines_to_show);
		#print Dumper(\@text_lines);
		my $shortend_text = '';
		my $shortend_orig_text = '';
		for(@lines_to_show){
			for($$_[0]..$$_[1]){
				$shortend_text.=$text_lines[$_]."\n";
				$shortend_orig_text.=$orig_text_lines[$_]."\n";
			}
		}
		chomp($shortend_text);
		chomp($shortend_orig_text);
		my $mw = Tk::MainWindow->new;
		my $w = $mw->DiffText(
			-width=>1500, -height=>500, -orient => 'horizontal', -map => 'scrolled'
		)->pack();
		$w->load('a' => $shortend_text);
		$w->load('b' => $shortend_orig_text);
		$w->compare(-granularity => 'word');
		$result = '0';
		$mw->bind('<KeyPress-y>' => sub {$result = '1'; $mw->destroy;} );
		$mw->bind('<KeyPress-q>' => sub {exit 0;} );
		$mw->bind('<KeyPress>' => sub {$result = '0'; $mw->destroy;} );
		$mw->MainLoop;
	}
	return $result;
}

sub split_wikitext{
	my $self = shift;
	my $text_ref = shift;
	unless(defined $text_ref){
		$self->msg(0, 'cannot split an undef reference', 'warning');
		return;
	}
	my $parsed = [];
	my $success;
	if($$text_ref eq ''){
		return ([{'text' => '', 'type' => 'text'}], 1);
	}
	my $begin_of_st = sub{
		my $types = shift;
		my $parsed = shift;
		my $type_name = shift;
		pop @$types if @$types == 1 && $types->[0] eq 'text';
		if(@$types == 0){
			push @$parsed, {'text' => $1, 'type' => $type_name};
		}else{
			$parsed->[-1]{'text'} .= $1;
		}
		push @$types, $type_name;
		return 1;
	};
	my @type = ();
	while(1){
		if($$text_ref =~ /\G(\{\{#[^:]+:)/gcs){
			# begin of expression template {{#..., e.g. '{{#expression:'
			$begin_of_st->(\@type, $parsed, 'expr_tpl');
		}elsif($$text_ref =~ /\G(\{\{[^{}|]*[^{}|\s]\s*)/gcs){
			# begin of template {{..., e.g. '{{template name'
			$begin_of_st->(\@type, $parsed, 'template');
		}elsif($$text_ref =~ /\G(\{\|)/gcs){
			# begin of table {|
			$begin_of_st->(\@type, $parsed, 'table');
		}elsif(@type > 0 && ($type[-1] eq 'template' || $type[-1] eq 'expr_tpl')
			&& $$text_ref =~ /\G(\}\})/gcs
		){
			# end of (expression) template }}
			$parsed->[-1]{'text'} .= $1;
			pop @type;
		}elsif(@type > 0 && $type[-1] eq 'table' && $$text_ref =~ /\G(\|\})/gcs){
			# end of table |}
			$parsed->[-1]{'text'} .= $1;
			pop @type;
		}elsif(@type == 0 && $$text_ref =~ /\G([^{}]+|.)/gcs){
			# begin of text
			push @$parsed, {'text' => $1, 'type' => 'text'};
			push @type, $parsed->[-1]{'type'};
		}elsif(@type > 0 && $$text_ref =~ /\G([^{}|]+|.)/gcs){
			# continue current state
			$parsed->[-1]{'text'} .= $1;
		}elsif(pos($$text_ref) == length($$text_ref)){
			# end of string
			#print "done\n";
			$success = 1;
			last;
		}else{
			# exception
			$self->msg(1, Dumper(\@type));
			$self->msg(0, "wtf: pos" . pos($$text_ref));
			last;
		}
	}
	return ($parsed, $success);
}

sub strip_namespace_from_title{
	my $self = shift;
	my $full_page_title = shift;
	my $ns = shift;
	unless(defined $full_page_title){
		$self->msg(0, 'missing first parameter: full page title', 'warning');
		return undef;
	}
	unless(defined $ns){
		$self->msg(0, 'missing second parameter: namespace', 'warning');
		return undef;
	}
	my $ns_names = $self->convert_ns($ns, 'id2names');
	my $page_title = $full_page_title;
	if($ns != '0'){
		for my $ns_name(@$ns_names){
			if($page_title =~ /^\Q$ns_name:/){
				$page_title =~ s/^\Q$ns_name://;
				last;
			}
		}
	}
	return $page_title;
}

sub table2wikitext{
	my $self      = shift;
	my $table     = shift;
	my $full_auto = shift // 1; # full auto message
	my $only_one_cell_per_line = shift // 1;
	my $col_widths = shift; # til now: ignored if $only_one_cell_per_line
	my $style = $table->{'style'} // {};
	$style->{'table'} = '' unless defined $style->{'table'};
	my @col_widths_cumsum;
	if(defined $col_widths){
		$col_widths_cumsum[0] = $col_widths->[0];
		for(my $i = 1; $i < @$col_widths; ++$i){
			$col_widths_cumsum[$i] = $col_widths_cumsum[$i-1] + $col_widths->[$i];
		}
	}
	my $wikitable = '';
	if($full_auto){
		$wikitable .= "<!-- this table is generated automatically. " .
			"any manual modifications will be deleted on next update. -->\n";
	}
	$wikitable .= "{| " . $style->{'table'} . "\n";
	# anonymous function for re-use with header and body
	my $gen_row = sub {
		my $sep               = shift;
		my $only_one_cell_per_line = shift;
		my $col_widths        = shift;
		my $table_row         = shift;
		my $col_widths_cumsum = shift;
		my $wikitable = '';
		if($only_one_cell_per_line){
			$wikitable .= "$sep $_\n" for @$table_row;
		}else{
			if($col_widths){
				$wikitable .= "$sep ";
				my $wikirow = '';
				for(my $col = 0; $col < @$table_row; ++$col){
					my $cell = $table_row->[$col];
					$wikirow .= " $sep$sep " if $col > 0;
					$wikirow .= $cell;
					my $num_spaces = $col_widths_cumsum->[$col]
						- 2        # spaces at begin and end
						- length($wikirow)
						+ 2 * $col;  # '!!' or '||' respectively
					$wikirow .= ' ' x $num_spaces if $num_spaces > 0;
				}
				$wikitable .= $wikirow . "\n";
			}else{
				$wikitable .= "$sep " . join(" $sep$sep ", @$table_row) . "\n";
			}
		}
		return $wikitable;
	};
	if(defined $table->{'header'} and @{$table->{'header'}} > 0){
		my $table_row = $table->{'header'};
		$wikitable .= '|- ' . $style->{'header'} . "\n" if defined $style->{'header'};
		$wikitable .= $gen_row->('!', $only_one_cell_per_line, $col_widths,
			$table_row, \@col_widths_cumsum);
	}
	for my $table_row(@{$table->{'body'}}){
		$wikitable .= "|-\n";
		$wikitable .= $gen_row->('|', $only_one_cell_per_line, $col_widths,
			$table_row, \@col_widths_cumsum);
	}
	$wikitable .= "|}\n";
	return $wikitable;
}

sub table_body2array{
	my $self = shift;
	my $table_body = shift;
	unless(defined $table_body){
		$self->msg(0, 'missing parameter of type ref to scalar', 'warning');
		return;
	}
	unless(defined $$table_body){
		$self->msg(0, 'parameter should be ref to scalar', 'warning');
		return;
	}
	# truncate table body
	$$table_body =~ s/\n\z//;
	$$table_body =~ s/^\s*\|-\s*|\s*\|-\s*$//g;
	return [
		map { ## no critic (MutatingListFunctions)
			s/^\s*\| ?//;
			s/\s+$//;
			if(index($_,'||') > -1){
				[split /\s*+\|\|\s*+/, $_];
			}else{
				[split /\s*\n\| *+/m, $_];
			}
		} split /\s*\n\|- *+\n/, $$table_body
	];
}

sub template_spon_converter{
	my $self = shift;
	my $link_template = shift;
	my $meta_data;
	if($link_template =~ /^\{\{Internetquelle\s*\|\s* #{
		(?i:autor=spiegel[^|}]+\s*\|)?
		url=\s*(?<url>https?:\/\/www\.spiegel\.de\/[^\x{20}|]*)\s\|
		titel=\s*DER\sSPIEGEL\s*\{\{!\}\}\s*Online-Nachrichten\s*\|
		(?:[a-z]+=\s*\|)*
		abruf=(?<access_date>[0-9-]+)
		(?:\s*\|[a-z]+=\s*)*
		\}\}$/x
	){
		$meta_data->{'url'} = $+{'url'};
		$meta_data->{'access_date'} = $+{'access_date'};
		$meta_data->{'work'} = '[[Der Spiegel]]';
		if($meta_data->{'url'} =~ /^https?:\/\/www\.spiegel\.de\/consent-a-\?
			targetUrl=(https?:\/\/www\.spiegel\.de\/.*)
		/x){
			$meta_data->{'url'} = $1;
			$meta_data->{'url'} =~ s/[&?]ref=[^&?]+//; # e.g. &ref=https://www.google.com
		}
	}
	unless(defined $meta_data->{'url'}){
		$self->msg(0, 'url is undefined', 'warning');
		return;
	}
	my $templatized = $self->fetch_www_meta_info($meta_data);
	return $templatized;
}

sub template_zeitde_converter{
	my $self = shift;
	my $link_template = shift;
	my $meta_data;
	if($link_template =~ /^\{\{Internetquelle\s*\|\s* #{
		(?:autor=Zeit[^|}]+\s*\|)?
		url=\s*(?<url>https?:\/\/www\.zeit\.de\/[^\x{20}|]*)\s\|
		titel=\s*ZEIT\sONLINE\s[^|]{40,50}PUR-Abo\.[^|]{15,25}\|
		(?:[a-z]+=\s*\|)*
		abruf=(?<access_date>[0-9-]+)
		(?:\s*\|[a-z]+=\s*)*
		\}\}$/x
	){
		$meta_data->{'url'} = $+{'url'};
		$meta_data->{'access_date'} = $+{'access_date'};
		$meta_data->{'work'} = '[[Die Zeit]]';
		if($meta_data->{'url'} =~ /^https:\/\/www\.zeit\.de\/zustimmung\?url=(.*)/){
			$meta_data->{'url'} = $self->uri_unescaper($1);
		}
	}
	unless(defined $meta_data->{'url'}){
		$self->msg(0, 'url is undefined', 'warning');
		return;
	}
	my $templatized = $self->fetch_www_meta_info($meta_data);
	return $templatized;
}

sub test_and_replace{
	my $self     = shift;
	my $text     = shift;
	my $regexp_s = shift;
	my $regexp_r = shift;
	my $use_eval_replacement = shift;
	my $print_non_changes = shift // 1;
	$print_non_changes = 0;
	my $strpos = 0;
	my @array_of_changes = ();
	$self->msg(3, "   " . $regexp_s);
	my $collect_repl = sub {
		my $match = shift;
		my $prematch = shift;
		my $replaced;
		if(ref $regexp_r eq 'CODE'){
			$replaced = $regexp_r->($self, $match);
		}else{
			$replaced = $use_eval_replacement ? eval($regexp_r) : $regexp_r;
		}
		unless(defined $replaced){
			$self->msg(0, '$replaced is undefined', 'error', 1);
		}
		# cope with links
		if($replaced ne $match && !$self->check_external_link(\$prematch, \$replaced)){
			$replaced = $match;
		}
		push(@array_of_changes, {'match' => $match, 'replaced' => $replaced});
		return $replaced;
	};
	$$text =~ s/$regexp_s/$collect_repl->(${^MATCH}, ${^PREMATCH})/gpme;
	if($self->{'verbosity'} >= 1){
		for my $repl(@array_of_changes){
			if($self->{'verbosity'} >= 3 || $print_non_changes
				|| $repl->{'match'} ne $repl->{'replaced'}
			){
				$self->msg(1, "   '" . $repl->{'match'} . "'");
				$self->msg(1, " ->'" . $repl->{'replaced'} . "'");
			}
		}
	}
	#$test=~s/$regexp_s/$regexp_r/gmee; # just for debugging
	#$self->msg(1, "wrong replacing with '$regexp_r'?", 'error') if $$text ne $test;
	return [grep {$_->{'match'} ne $_->{'replaced'}} @array_of_changes];
}

sub text_replacement{
	my $self       = shift;
	my $pages      = shift; # pages to search (array ref)
	my $section    = shift; # section to use
	my $patterns   = shift; # patterns to search s/X//
	my $repls      = shift; # replacing texts s//X/(e)
	my $params     = shift;
	my $use_eval_in_repl = shift;
	my $print_non_changes = 1;
	my $max_edits  = $params->{'max_edits'} // -1;
	my $skip_edits = $params->{'skip_edits'} // 0;
	my $create     = $params->{'create_if_not_exists'} // 0;
	my $cleanup    = $params->{'cleanup'} // 1;
	unless(defined $params->{'summary'}){
		$self->msg(0, 'missing summary param', 'error');
		return;
	}
	$self->msg(1, 'loop over ' . scalar(@$pages) . ' pages and replace text');
	my $edit_counter = -1;
	my $success = 0;
	if(@$patterns == 0 + @$repls){
		$success = 1;
		for my $page(@$pages){
			if(++$edit_counter < $skip_edits){
				next;
			}elsif($edit_counter >= $max_edits and $max_edits != -1){
				last;
			}
			$self->msg(1, "$edit_counter: page = '$page'");
			my $text = $self->get_text($page);
			unless(defined $text){
				if($create){
					$self->msg(1, "page content not defined, probably not existing yet. "
						. "i will try to create it for you.");
					$text = "";
				}else{
					$self->msg(0, "page content not defined, maybe page does not exist? "
						. "please use 'create_if_not_exists' option, if you want me to create "
						. "the page",
						'warning');
					$success = 0;
					next;
				}
			}
			my $text_bak = $text;
			my $summary_clean_up;
			if($cleanup){
				(my $dummy_changes, $summary_clean_up) =
					$self->cleanup_wiki_page(\$text, $page);
			}
			my $text_to_use;
			my @split_text;
			if(defined $section){
				my $i = 0;
				@split_text = grep {++$i % 3}
					split /^((=+)(?!=).+(?<!=)\2(?:\n|\z))/m, $text;
				# 0, 2, 4, ... = text
				# 1, 3, 5, ... = headings
				my $section_num = -1;
				for($i = 1; $i < @split_text; $i+=2){
					if($split_text[$i] =~ /^(=+)\s*$section\s*\1$/){
						$section_num = $i;
						last;
					}
				}
				if($section_num == -1){
					$self->msg(0, 'could not find section in page', 'error');
					$success = 0;
					next;
				}else{
					$self->msg(1, "found find section '$section' in page '$page'");
				}
				#my $text2 = join '', @split_text;
				#map {$_ = substr($_, 0, 100)} @split_text;
				#print Dumper \@split_text;
				$text_to_use = \$split_text[$section_num + 1];
			}else{
				$text_to_use = \$text;
			}
			my $found = 0;
			for(my $i = 0; $i < @$patterns; ++$i){
				$self->msg(3, "  replacing $patterns->[$i] -> $repls->[$i]");
				if($self->test_and_replace($text_to_use, $patterns->[$i], $repls->[$i],
						$use_eval_in_repl, $print_non_changes) > 0){
					++$found;
				}
			}
			if(defined $section){
				$text = join '', @split_text;
			}
			$self->msg(2, "  $found replacements") if @$patterns > 0;
			my $summary = $params->{'summary'};
			if($summary_clean_up){
				if(defined $summary && $summary ne ''){
					$summary .= '; ' . $summary_clean_up if $summary_clean_up ne '';
				}else{
					$summary = $summary_clean_up;
				}
			}
			my $saved = $self->save_wiki_page($page, $summary, \$text, \$text_bak) > 0;
			$success &&= $saved;
		}
	}else{
		$self->msg(0, 'number of patterns and number of replacements differ',
			'error');
	}
	return $success;
}

sub time_management{
	my $self = shift;
	while($self->{'max_edits_per_min'} != -1 and
		@{$self->{'time_stack'}} >= $self->{'max_edits_per_min'}){
		my $time_diff = time - $self->{'time_stack'}->[0];
		if($time_diff < 60){
			$self->msg(1, '  ...waiting ' . (60 - $time_diff) . " seconds");
			sleep(60 - $time_diff);
		}
		shift @{$self->{'time_stack'}};
	}
	return 1;
}

sub title2filename{
	my $self = shift;
	my $title = shift;
	$title =~s/\x{df}/ss/g;
	$title =~s/\x{e4}/ae/g;
	$title =~s/\x{c4}/Ae/g;
	$title =~s/\x{f6}/oe/g;
	$title =~s/\x{d6}/Oe/g;
	$title =~s/\x{fc}/ue/g;
	$title =~s/\x{dc}/Ue/g;
	$title =~s/[^a-zA-Z0-9_.~!-]/_/g;
	return $title;
}

sub title2url{
	my $self  = shift;
	my $title = shift;
	utf8::encode($title);
	$title =~ s/ /_/g;
	my $url = $self->_get_api_url();
	$url =~ s/api\.php$/index.php?title=/;
	$url .= $self->uri_escaper($title);
	return $url;
}

sub title2url_part{
	my $self = shift;
	my $title = shift;
	$title =~ s/ /_/g;
	my $url_part = $self->uri_escaper($title);
	return $url_part;
}

sub trigger_maintenance_callback{
	my $self = shift;
	my $rc_msg = shift; # see CamelBotRC::get_rc_via_db and CamelBotRC::put_new_rc
	my $prio = shift; # e.g. immediate or short-term
	my $changes = 0;
	while(my ($type, $cb) = each %{$self->{'maintenance'}{'call_back'}{$prio}}){
		$self->msg(2, "type: '$type'");
		# make two separate requests, because it might be not necessary to load the
		# text of the page
		my ($triggering, $trigger_text) = $self->triggers_maintenance_lists(
			$type, $rc_msg, undef);
		if($triggering){
			$self->msg(3, 'page triggers');
			my $text = $self->get_text($rc_msg->{'page_with_ns'});
			($triggering, $trigger_text) = $self->triggers_maintenance_lists(
				$type, undef, \$text);
			if($triggering){
				$self->msg(3, 'text triggers');
				$changes += $self->$cb($rc_msg, \$text);
			}
		}
	}
	return $changes;
}

sub triggers_maintenance_lists{
	my $self = shift;
	my $mt_type = shift;
	my $rc_msg = shift;
	my $text_ref = shift;
	my $success_code = 0;
	unless(defined $mt_type){
		$self->msg(0, 'mt_type undefined', 'error');
		return;
	}
	# check triggering conditions
	my $m = $self->{'maintenance'};
	if(defined $rc_msg && $rc_msg->{'page_with_ns'}){
		$success_code = $self->_check_triggering_namespaces(
			$m->{'namespaces'}{$mt_type}, $rc_msg);
		if($success_code == 0){
			$self->msg(5, 'skip, because namespace is not triggering');
			return (0, undef);
		}
		# if a page is moved from A to B, then check both pages for exceptions
		if(defined $m->{'exceptions'}{$mt_type}){
			if($rc_msg->{'page_with_ns'} =~ /$m->{'exceptions'}{$mt_type}/){
				$self->msg(5, 'skip, because is on list of exceptions');
				return (0, undef);
			}elsif(defined($rc_msg->{'move_target'})
				&& $rc_msg->{'move_target'} =~ /$m->{'exceptions'}{$mt_type}/
			){
				$self->msg(5,
					'skip, because target (of moved page) is on list of exceptions');
				return (0, undef);
			}
		}
		if(defined($m->{'user'}{$mt_type})
			&& $rc_msg->{'user'} !~ /$m->{'user'}{$mt_type}/
		){
			$self->msg(5, 'skip, because user is not triggering');
			return (0, undef);
		}
		if(defined $m->{'special_conditions'}{$mt_type}
			&& !$m->{'special_conditions'}{$mt_type}->($rc_msg)
		){
			$self->msg(5, 'skip, because special conditions are not fulfilled');
			return (0, undef);
		}
	}
	my $match;
	if(defined $text_ref && defined $$text_ref){
		return (0, undef) unless $$text_ref =~ /$m->{'text_re'}{$mt_type}/p;
		$match = ${^MATCH};
	}
	++$success_code;
	# $success_code meaning:
	# 1: namespace not checked
	# 2: page not moved, namespace triggered
	# 3: page moved, target namespace triggered, no movement from not-0 to 0.
	# 4: page moved, target namespace triggered, ns!=0 -> ns==0,
	my $triggering_object = (defined $rc_msg && defined $rc_msg->{'page_with_ns'})
		? $rc_msg->{'page_with_ns'} : 'text';
	if(defined($rc_msg) && defined($rc_msg->{'move_target'})){
		$triggering_object .= ' -> ' . $rc_msg->{'move_target'};
	}
	$self->msg(1, "$triggering_object triggers $mt_type");
	return ($success_code, $match);
}

sub update_index_tables{
	my $self = shift;
	my $success = 1;
	if($self->update_edit_filter_index() == 0){
		$success = 0;
	}
	if($self->update_sbl_index() == 0){
		$success = 0;
	}
	return $success;
}

sub update_index_table{
	my $self = shift;
	my $tables = shift;
	my $namespace = shift;
	my $page_name = shift;
	my $lt_page  = $self->normalize_page_title(shift);
	my $title_re = shift;
	my $skip_re = shift;
	my $max_num_talk_pages = shift // 40;
	my $max_num_days_age = shift // 5;
	# get recent changes
	my $pages = $self->get_pages_by_prefix_sorted(
		$namespace, $page_name . '/', $title_re);
	$self->msg(2, 'got ' . (0 + @$pages) . ' pages. '
		. 'now start looping over the newest.');
	# last change of latest_topics
	my $lt_last_change =
		$self->get_page_info($lt_page)->{$lt_page}{'touched'};
	$self->msg(3, "last change: $lt_last_change");
	my %new_topics;
	my $i = 0;
	for my $page(@$pages){
		next if(defined($skip_re) && $page->{'title'} =~ $skip_re);
		$self->msg(2, " $page->{'revisions'}[0]{'timestamp'}: $page->{title}");
		my $npost = $self->newest_post_info($page->{'revisions'}[0]{'slots'}{'main'}{'*'});
		$self->msg(3, Dumper($npost));
		next unless defined $npost->{'thread'};
		# date of last change of rule's page
		my $date_of_last_change =
			'{{#timel:Y-m-d H:i:s|' . $page->{'revisions'}[0]{'timestamp'}.'}}';
		$tables->[0]{'body'}[$i][1] = $date_of_last_change;
		# page title: distinguish between rules (numbers) and other pages
		if($page->{'title'} =~ /\Q$page_name\E\/(\d+)$/){
			$tables->[0]{'body'}[$i][0] = "[[$page->{'title'}|#$1]]";
		}elsif($page->{'title'} =~ /\/([^\/]+)$/){
			$tables->[0]{'body'}[$i][0] = "[[$page->{'title'}|$1]]";
		}
		my $page_short_title = $1 // 'unknown';
		# add thread title to table
		#my $thread_anchor = "{{subst:urlencode:$npost->{'thread'}|WIKI}}";
		my $thread_title = $self->resolve_description_from_wikilinks($npost->{'thread'});
		$tables->[0]{'body'}[$i][2] =
			"[[$page->{'title'}#$thread_title|$thread_title]]";
		# remember newest topics
		if(!defined($lt_last_change)
				|| $lt_last_change lt $page->{'revisions'}[0]{'timestamp'}
		){
			$new_topics{$page_short_title} = $thread_title;
		}
		# author
		$tables->[0]{'body'}[$i][3] =
			!defined($npost->{'author'}) ? 'unknown' :
			$npost->{'author'} eq '' ? '' :
			"[[special:contributions/$npost->{'author'}|$npost->{'author'}]]";
		$tables->[0]{'body'}[$i][3] .= !defined($npost->{'date'}) ? '' :
			' (' . $self->get_date_iso($npost->{'date'}) . ')';
		last if $page->{'revisions'}[0]{'timestamp'} lt
			$self->get_date_iso(time - 60 * 60 * 24 * $max_num_days_age)
			and $i >= $max_num_talk_pages;
		++$i;
	}
	#print $self->table2wikitext($tables->[0], 1);
	return \%new_topics;
}

sub update_edit_filter_index{
	my $self = shift;
	# get recent changes
	my $namespace = 4;
	my $edit_filter_name = 'Bearbeitungsfilter';
	my $lt_page = 'Wikipedia:' . $edit_filter_name . '/latest topics';
	my $tables = [{
		'style'  => {'table' => 'class="wikitable sortable"'},
		'header' => ['Regel', "letzte Seiten\xe4nderung", 'Thread', 'letzter Beitrag von'],
		'body'   => [],
	}];
	my $new_topics = $self->update_index_table(
		$tables,
		$namespace,
		$edit_filter_name,
		$lt_page,
		$self->{'re_editfilter'},
		qr/\/Archiv\//,
	);
	# save table
	if(keys %$new_topics > 0){
		my $filter_descriptions = $self->get_abusefilter_info();
		# build summary
		my $summary_thread_part = '';
		my $summary = '(' . (
			join ', ',
			map {
				$summary_thread_part .=
					($_ =~ /^[0-9]+$/ and defined $filter_descriptions->{$_}) ?
					$filter_descriptions->{$_} : $new_topics->{$_};
				$summary_thread_part .= '; ';
				/^[0-9]+$/ ? "#$_" : $_;
			} # add '#' on rule numbers
			sort {
				# TODO: there's some bug inhere. strings are recognized as numbers?
				# 2018-12-30 seth: possibly fixed; not occurred for a while
				my $result;
				if($a =~ /^[0-9]+$/ and $b =~ /^[0-9]+$/){
					# numerical sorting on rule numbers
					# print "$a X $b\n" if $a !~ /^[0-9]+$/ or $b !~ /^[0-9]+$/;
					$result = ($a <=> $b);
				}elsif($a !~ /^[0-9]+$/ and $b !~ /^[0-9]+$/){
					# alphabetical sorting on other pages
					$result = ($a cmp $b);
				}else{
					# when sorting rule number against other page, use counter-sort
					$result = ($b cmp $a);
				}
				$result;
			}
			keys %$new_topics
		) . '): ';
		$summary .= substr($summary_thread_part, 0, -2); # delete last '; '
		# now update page
		$self->rebuild_table($lt_page, $tables, $summary);
	}
	return 1;
}

sub update_sbl_index{
	my $self = shift;
	# get recent changes
	my $namespace = 4;
	my $page_name = 'Weblinks/Block';
	my $lt_page = 'Wikipedia:' . $page_name . "/Neuste_Beitr\xe4ge";
	my $tables = [{
		'style'  => {'table' => 'class="wikitable sortable"'},
		'header' => ['Domain', "letzte Seiten\xe4nderung", 'Thread', 'letzter Beitrag von'],
		'body'   => [],
	}];
	my $new_topics = $self->update_index_table(
		$tables,
		$namespace,
		$page_name,
		$lt_page,
		$self->{'re_sbl_pages'},
	);
	# save table
	if(keys %$new_topics > 0){
		# build summary
		my $summary = join '; ',
			map {
				$_ ne $new_topics->{$_} ? "$_: $new_topics->{$_}":  $_;
			} sort keys %$new_topics;
		# now update page
		$self->rebuild_table($lt_page, $tables, $summary);
	}
	return 1;
}

sub update_maintenance_lists_build_table{
	my $self              = shift;
	my $maintenance_lists = shift; # maintenance lists
	my $mt_type           = shift; # maintenance type, see refresh_maintenance_params
	my $pages             = shift; # [{'page_with_ns' => $page}, ...]
	my $additional_col    = shift // []; # ['column1', 'column2', ...]
	my $mt_type_page = $self->{'maintenance'}{'page_name'}{$mt_type} // $mt_type;
	return unless defined $self->{'maintenance'}{'intros'}{$mt_type};
	# get maintenance list
	my $ml = "User:CamelBot/maintenance list/$mt_type_page";
	my $ml_text = $self->get_text($ml) // '';
	my $ml_text_bak = $ml_text;
	# rebuild base of maintenance list
	my $ml_parts = {
		'intro' => $self->{'maintenance'}{'intros'}{$mt_type},
		'outro' => "\n\n[[Kategorie:Wikipedia:Wartungskategorie|$mt_type_page]]",
		'wikitable' => undef,
	};
	# extract (save) old intro or create a new one
	$self->extract_maintenance_list_parts(\$ml_text, $ml_parts, $additional_col);
	# get old table content
	my $table = $self->wikitable2array(\$ml_parts->{'wikitable'});
	my $num_add_cols = @{$table->{'header'}} - 2 - scalar(@$additional_col);
	if($num_add_cols < 0){
		$self->msg(1, "adapting table to (changed) number of additional cols");
		splice(@{$table->{'header'}}, 1, 0, @$additional_col);
		map{
			splice(@$_, 1, 0, '');
		} @{$table->{'body'}};
		$num_add_cols = @{$table->{'header'}} - 2 - scalar(@$additional_col);
	}
	if($num_add_cols > 0){
		$self->msg(1, "number of additional (ignored/unchanged) cols = $num_add_cols");
	}
	# optional: rebuild table: check all old entries; remove entries if possible;
	# search new entry, if not in array, then push
	my $summary = 'update maintenance list';
	# 1. @$new_entries = grep{not in list already} @$pages
	my $new_entries = [grep {
		my $page = $_;
		!$self->is_page_in_maintenance_table($page, $table, $additional_col);
	} @$pages];
	# 2. check old/solved entries for deletion
	$self->delete_from_maintenance_table($mt_type, $table);
	# 3. push @$table, @new_entries
	my $added_pages = $self->add_to_maintenance_table(
		$table, $new_entries, $additional_col);
	# create/update summary
	if(defined $added_pages && @$added_pages > 0){
		$summary = 'added: ' . join('; ', @$added_pages);
	}
	$summary .= '; num items = ' . scalar(@{$table->{'body'}});
	# reset and refill internal maintenance list
	$self->refill_maintenance_list($maintenance_lists, $table);
	# convert array to wikitable
	my $full_auto = 1;
	$ml_parts->{'wikitable'} = $self->table2wikitext($table, !$full_auto);
	# rebuild page, i.e., put pieces together and save
	$ml_text = $ml_parts->{'intro'} . $ml_parts->{'wikitable'} . $ml_parts->{'outro'};
	return $self->save_wiki_page($ml, $summary, \$ml_text, \$ml_text_bak);
}

sub update_maintenance_lists{
	my $self          = shift;
	my $params        = shift;
	# rc_msg with page title or undef;
	# undef means: force update without checking conditions
	# no rc_msg, no text => check pages in current list(s), update list
	# no rc_msg,    text => same as above
	#    rc_msg,    text => check pages in current list(s) if text matches condition
	#    rc_msg, no text => same as above, but at the beginning:
	#                       get text from page or leave function
	my $rc_msg            = $params->{'rc_msg'};
	my $text              = $params->{'text'}; # ref to string (or undef)
	# list name of maintenance list, "all_types" for all
	my $list_name         = $params->{'list_name'};
	my $maintenance_lists = $params->{'lists'}; # maintenance lists
	my $page;
	if(defined $rc_msg){
		# 2023-02-11 seth: unsure about this one.
		# when should a moved page be listed? if the original page fits?
		# or if the target page fits? or if any one of them fits?
		# see also riggers_maintenance_lists()
		$page = ($self->_check_triggering_namespaces([0], $rc_msg) >= 2)
			? $rc_msg->{'move_target'} : $rc_msg->{'page_with_ns'};
		if(defined $rc_msg->{'move_target'}){
			$self->msg(2, "page $rc_msg->{'page_with_ns'} moved to $page");
		}
	}
	if(defined($page) && !(defined($text) && defined($$text))){
		my $text_ = $self->get_text($page);
		$text = \$text_ if defined $text_;
	}
	if((defined($text) && defined($$text)) || !defined($page)){
		my $m = $self->{'maintenance'};
		for my $mt_name(keys %{$m->{'text_re'}}){
			if(($list_name eq $mt_name || $list_name eq 'all_types')
				&& (!defined($params->{'check_waiting'})
					|| $params->{'check_waiting'} eq $m->{'wait_till_list'}{$mt_name})
			){
				my $trigger_text;
				if(defined $page){
					(my $triggering, $trigger_text) = $self->triggers_maintenance_lists(
						$mt_name, $rc_msg, $text);
				}
				if(!defined($page) || $trigger_text){
					# TODO: how to cope with cat dead? right now using another function
					# cat_of_dead_monitoring
					# for 'cat dead' the $pages array ref needs to contain the additional column
					# 'category' and furthermore only the addition of new categories shall be
					# listed. so a diff is needed.
					next if($mt_name eq 'cat dead' || $mt_name eq 'faz.net'); #&& defined $$text
					$self->msg(1, "check '$mt_name'");
					#and $$text =~/(?:[cC]ategory|[kK]ategorie)\s*:\s*[nN]ekrolog/;
					my $add_column = ['trigger'];
					my $pages = [];
					if(defined $page){
						push @$pages, {
							'page_with_ns' => $page,
							'trigger' => ($trigger_text ? "<pre>$trigger_text</pre>" : ''),
						};
						# check if there is a call-back
						my $callback = $m->{'call_back'}{'mid-term'}{$mt_name};
						if(defined $callback){
							$self->$callback($rc_msg, $text);
						}
					}
					$self->update_maintenance_lists_build_table(
						$maintenance_lists, $mt_name, $pages, $add_column);
				}
			}
		}
		return 1;
	}else{
		$self->msg(1, "could not fetch page '$page'. maybe deleted (or moved away) already",
			'notice');
		return 0;
	}
}

sub upload_file{
	my $self = shift;
	my $filename_src = shift;
	my $summary = shift;
	my $filename_dest = shift // $filename_src;
	my $data = $self->read_file_binary($filename_src);
	# check, whether file exists already. If so, compare. don't upload, if same.
	$self->msg(2, " downloading file if existant");
	my $oldfile = $self->{'mw_api'}->download({'title' => 'File:'.$filename_dest});
	if(not defined $oldfile or $self->{'mw_api'}->{'error'}{'code'} != 0){
		$self->_handle_api_error();
		return 0;
	}
	$self->msg(3, " downloaded file (if existant)");
	if($oldfile eq '' or $oldfile ne $$data){
		$self->msg(3, " trying to upload new file");
		if(!$self->{'simulation'}){
			# upload
			#$mw->edit({
			# 'action'   => 'upload',
			# 'filename' => $filename_src,
			# 'comment'  => $summary,
			# 'file'     => [undef, $filename_dest, 'Content'=>$$data],
			# })
			$self->{'mw_api'}->upload({
					'data'    => $$data,
					'summary' => $summary,
					'title'   => $filename_dest,
				}) or ($self->_handle_api_error() and die);
		}
		return 1;
	}else{
		$self->msg(1, "won't force uploading. file is already online.");
		return 0;
	}
}

sub uri_escaper{
	my $self    = shift;
	my $url     = shift;
	my $escaped = $url;
	if($_loaded_mod{'URI::Escape'}){
		$escaped = uri_escape_utf8($url);
	}else{
		$self->msg(0, 'function uri_escape_utf8 not available. ' .
			'please check installation of perl module URI::Escape', 'error');
	}
	return $escaped;
}

sub uri_unescaper{
	my $self    = shift;
	my $url     = shift;
	my $type    = shift // []; # possible values: 'wiki', 'plain'
	$type = [$type] if ref $type eq '';
	$type = {map {$_ => 1} @$type};
	my $unescaped = $url;
	if($_loaded_mod{'URI::Escape'}){
		$unescaped = Encode::decode('utf8', uri_unescape($url));
		unless($type->{'plain'}){
			$unescaped =~ s/(\\)/'%' . sprintf('%x', ord($1))/ge;
		}
		if($type->{'wiki'}){
			$unescaped =~ s/([\[\]{}|'"])/'%' . sprintf('%x', ord($1))/ge;
		}
	}else{
		$self->msg(0, 'function uri_unescape not available. ' .
			'please check installation of perl module URI::Escape', 'error');
	}
	return $unescaped;
}

sub url2title{
	my $self   = shift;
	my $url    = shift;
	$url =~ /^(?:https?:\/\/$self->{'wm_lang'}
		\.$self->{'wm_proj_type'}\.org\/wiki\/)?(.*)/x;
	my $article_name = $self->uri_unescaper($1);
	$article_name =~ s/_/ /g;
	# 2014-12-23, seth: "https://de.wikipedia.org/wiki/"
	#	 . "K%C3%B6nigliche_und_barmherzige_Vereinigung_der_Ordens-_und_Medaillentr%C3%A4ger_von_Belgien"
	#	 became: "KÃ¶nigliche und barmherzige Vereinigung der Ordens- und MedaillentrÃ¤ger von Belgien"
	utf8::decode($article_name);
	$article_name =~ s/^(?=Datei:|Bild:|Image:|File:|Category:|Kategorie:)/:/;
	return $article_name;
}

sub url_converter{
	my $self = shift;
	my $url  = shift;
	unless(defined $url){
		$self->msg(0, 'url is undefined', 'warning');
		return;
	}
	$url = $self->url_converter_amp_fuck($url);
	# 12ft paywall proxy
	if($url =~ /^https?:\/\/12ft\.io\/(?:proxy\?.*?\bq=|)(http.*)/){
		$url = $self->uri_unescaper($1, 'wiki');
		$self->{'converted_urls'}{$1} = 1;
	# removepaywall.com paywall proxy
	}elsif($url =~ /^https?:\/\/(?:[^\/]+\.|)removepaywall\.com\/(http.*)/){
		$url = $self->uri_unescaper($1, 'wiki');
		$self->{'converted_urls'}{$1} = 1;
	# archive.fo, archive.is, archive.li, archive.today, ...
	}elsif($url =~ /^$self->{'archive.today'}{'re_short_url'}$/){
		my $response = $self->get_http_response($url);
		if($response->is_success){
			my $converted_url;
			if($response->content =~
				/<link\s
				rel="bookmark"\s
				href="https?:\/\/archive\.$self->{'archive.today'}{'re_tld'}
				\/([12][0-9]{13}\/[^"]+)"
				\/>/x
			){
				$converted_url = "https://archive.today/$1";
				$self->decode_html_entities(\$converted_url);
			}else{
				my $date     = $response->header('Memento-Datetime');
				my $orig_url = $response->header('Link');
				if(defined $date && defined $orig_url
					&& $orig_url =~ /^\s*<([^>]+)>; rel="original"/
				){
					$orig_url = $1;
					my $parser = DateTime::Format::Strptime->new(
						pattern => '%a, %d %b %Y %H:%M:%S %Z',
						on_error => 'croak',
					);
					my $dt = $parser->parse_datetime($date);
					$date = $dt->strftime('%Y%m%d%H%M%S');
					$converted_url = "https://archive.today/$date/$orig_url";
				}
			}
			if(defined $converted_url){
				$self->{'converted_urls'}{$1} = 1 if $url =~ /(archive\.[a-z]+)/;
				$url = $converted_url;
			}else{
				$self->msg(0, "could not get information about original url from '"
					. $url . "'.", 'warning');
			}
		}else{
			$self->msg(0, "could not get header of '" . $url . "' (http status code: "
				. $response->code . ").", 'warning');
		}
	# facebook redirects
	}elsif($url =~ /^https?:\/\/[a-z0-9-.]*facebook\..*[?&]u=(http[^&]+)/){
		$url = $self->uri_unescaper($1, 'wiki');
		$self->{'converted_urls'}{$1} = 1 if $url =~ /(facebook\.[a-z]+)/;
	# facebook short urls
	}elsif($url =~ /^https?:\/\/fb\.watch\/[a-zA-Z0-9_-]+\/?$/){
		my $response = $self->get_http_response($url);
		if($response->code == 302){
			$url = $response->header('location');
			$url =~ s/&ref=sharing//;
			$self->{'converted_urls'}{'fb.watch'} = 1;
		}else{
			$self->msg(0, "could not get header of '" . $url . "' (http status code: "
				. $response->code . ").", 'warning');
		}
	# faz/sz/taz short urls
	}elsif($url =~ /^https?:\/\/(?:m\.|www\.)?
			(faz\.net\/[0-9a-zA-Z_.-]+
			|sz\.de\/[0-9a-zA-Z_.-]+
			|taz\.de\/![0-9]+\/?
			)$/x){
		my ($response_code, $target_url) = $self->get_http_status($url);
		if(defined($target_url) && $target_url ne ''){
			my $target_wo_prot = $target_url;
			my $url_wo_prot = $url;
			$target_wo_prot =~ s/^https?//;
			$url_wo_prot =~ s/^(https?)//;
			if($url_wo_prot ne $target_wo_prot){
				my $domain = $self->get_domain_from_url($url);
				$url = $target_url;
				$self->{'converted_urls'}{$domain} = 1;
			}
		}
	## google cache
	#}elsif($url =~ /^https?:\/\/webcache\.googleusercontent\.com\/search\?
	#		q=cache:[a-zA-Z0-9_-]{12}:([^+\x{20}]+)/x){
	#	$url = $1;
	#	$url = 'https://' . $url unless $url =~ /^(?:https?|ftp):\/\//;
	#	$url = $self->uri_unescaper($url, 'wiki');
	# google redirects
	}elsif(#$url =~ /[?&](?:img)?url=([^&]+)/ ||
		$url =~ /^http[^ ]*?google\.[a-z]+\/?(?:url|imgres)\?.*\b(?:q|(?:img)?url)=(https?[^&]+)/
	){
		$url = $self->uri_unescaper($1, 'wiki');
		$self->{'converted_urls'}{'google-redirect'} = 1;
	# images.app.goo.gl/...
	}elsif($url =~ /^https?:\/\/images\.app\.goo\.gl\/[a-zA-Z0-9]+$/){
		my $response = $self->get_http_response($url, {'max_redirect' => 0});
		if($response->code == 302){
			$url = $response->header('location');
			$url = $self->url_converter($url);
		}else{
			$self->msg(0, "could not get header of '" . $url . "' (http status code: "
				. $response->code . ").", 'warning');
		}
	# jsessionid # in addition to delete_tracking_params_form_url
	}elsif($url =~ m~^(https?://[^/]+/.*);jsessionid=[^?&]++(.*)$~){
		$url = $1 . $2;
		$self->{'converted_urls'}{'jsessionid'} = 1;
	# spon redirects
	}elsif($url =~ /^https?:\/\/(?:m\.|www\.)?spiegel\.de\/consent-a-\?targetUrl=(.*)/){
		$url = $1;
		$url = $self->uri_unescaper($url, 'wiki');
		$self->{'converted_urls'}{'spon-redirect'} = 1;
	# yasni
	}elsif($url =~ /^https?:\/\/(?:www\.)?yasni\.[a-zA-Z]+\/.+[?&]url=([^&]+)/){
		$url = $self->uri_unescaper($1, 'wiki');
		$self->{'converted_urls'}{'yasni-redirect'} = 1;
	# youtube redirects
	}elsif($url =~ /^https?:\/\/youtu\.be\/(.+)/){
		$url = 'https://www.youtube.com/watch?v=' . $1;
		$url =~ s/\?v=.*\K\?/&/;
		$url = $self->uri_unescaper($url, 'wiki');
		$self->{'converted_urls'}{'youtube-redirect'} = 1;
	# zeit redirects
	}elsif($url =~ /^https?:\/\/(?:www\.|m\.)?zeit\.de\/zustimmung\?url=(.+)/){
		$url = $1;
		$url = $self->uri_unescaper($url, 'wiki');
		$self->{'converted_urls'}{'zeit-redirect'} = 1;
	}
	my $url_wo_tracking = $self->delete_tracking_params_form_url($url);
	if(length($url_wo_tracking) < length($url) - 2){ # don't delete leading ?, because impact is too small
		$url = $url_wo_tracking;
		$self->{'converted_urls'}{'removal of tracking param'} = 1;
	}
	return $url;
}

sub url_converter_amp_fuck{
	my $self = shift;
	my $url = shift;
	my $url_bak = $url;
	# convert google amp urls to original urls
	if($url =~ /https?:\/\/(?:[a-z]+\.)google\.[a-z]+\/amp\/(?:[a-z]\/|)(.*)/x){
		my $response = $self->get_http_response($url, {'max_redirect' => 0});
		if($response->code == 302){
			$url = $response->header('location');
			$url = $self->url_converter($url);
			if($url =~ m~^http://www.neues-deutschland.de/~){
				substr($url, 0, 4) = 'https';
			}
		}else{
			$self->msg(0, "could not get header of '" . $url . "' (http status code: "
				. $response->code . ").", 'warning');
		}
	}
	# convert ampproject.org links to original urls
	if($url =~ /https?:\/\/(?:[a-z0-9.-]+\.)ampproject\.org\/(?:[a-z]\/)++(.*)/x){
		$self->msg(1, Dumper("before: $url"));
		$url = $1;
		if($url =~ /[?&]ampshare=/){
			$url =~ s/.*[?&]ampshare=//;
			$url = $self->uri_unescaper($url, 'wiki');
		}else{
			$url =~ s/^amp\.//;
			$url =~ s/mobil\.nwzonline\.de/www.nwzonline.de/;
			$url = 'https://' . $url;
			$url =~ s/\?.*//;
			$url =~ s/\/\.?amp\//\//;
			$url =~ s/[\/-]amp$//;
			$url =~ s/\.amp\.?/./;
			$url =~ s/-amp\././;
		}
		$self->msg(1, Dumper("after: $url"));
	}
	# convert "mobile" urls such as https://m.example.org to standard urls
	$url =~ s~^https?://(?:(?:m|amp)\.)?(faz\.net/[a-zA-Z0-9/-]+)\.amp(?=\.html)~https://www.$1~;
	$url =~ s~^https?://amp\.(focus\.de/[a-zA-Z0-9/_-]+\.html)
		(?:\x23amp_[a-z]+=[a-zA-Z%0-9&=.-]+$)?
		~https://www.$1~x;
	$url =~ s~^https?://(?:(?:m|amp)\.)?(collider\.com/[a-zA-Z0-9/-]+/)amp/?~https://$1~;
	if($url =~ /https?:\/\/(?:m|amp)\.((?:
		faz\.net
		|spiegel\.de
		|tagesspiegel\.de
		|zdf\.de
		).*)/x){
		$url = "https://www.$1";
	}
	$url =~ s~^https?://amp\.cnn\.com/cnn/~https://www.cnn.com/~;
	$url =~ s~^https?://((?:www\.|)spiegel\.de/.*)-amp(?=\.html\z)~https://$1~;
	if($url =~ m~^(https?)://(?:www\.|)(?:aljazeera\.com|salzburg\.com|sn\.at)~){
		substr($url, 0, length($1)) = 'https';
		$url =~ s~/amp/~/~;
		if(substr($url_bak, 4) eq substr($url, 5)){
			$url = $url_bak;
		}
	}
	if($url_bak ne $url){
		$self->{'converted_urls'}{'mobile/amp'} = 1;
	}
	return $url;
}

sub wikitableHeader2array{
	my $self = shift;
	my $header = shift;
	unless(defined $header){
		$self->msg(0, 'missing parameter of type scalar', 'warning');
		return undef; # undef has to be returned here explicitely
	}
	$header =~ s/^\s*!\s*//s;
	$header =~ s/\s+$//s;
	if($header =~ /!!|\|\|/){
		$header = [split /\s*(?:!!|\|\|)\s*/, $header];
	}else{
		$header = [split /\s*!\s*/, $header];
	}
	return $header;
}

sub wikitable2array{
	my $self = shift;
	my $wikitable = shift;
	unless(defined $wikitable){
		$self->msg(0, 'missing parameter of type ref to scalar', 'warning');
		return;
	}
	unless(ref $wikitable eq 'SCALAR'){
		$self->msg(0, 'parameter should be ref to scalar', 'warning');
		my $tmp = $wikitable;
		$wikitable = \$tmp;
	}
	$$wikitable =~/^\{\|   # begin of table
		\x20*((?-s:.*))\n+   # class, id, ...
		((?:!\s*[^\n]*\n+)*) # table header
		(.*)                 # table body
		(?<=\n)\|\}\n?$      # end of table
	/sx;
	my $table = {
		'style' => {'table' => $1},
		'header' => $self->wikitableHeader2array($2),
		'body' => $3,
	};
	# decomposing wiki-table to array
	$table->{'body'} = $self->table_body2array(\$table->{'body'});
	return $table;
}

sub write_json_file{
	my $self = shift;
	my $data = shift;
	my $filename = shift;
	my $json = JSON->new->utf8;
	my $json_text = $json->encode($data);
	write_file($filename, {binmode => ':utf8'}, $json_text);
	return 1;
}

sub write_csv{
	my $self = shift;
	my $array = shift;
	my $filename = shift;
	my $separator = shift // ';';
	open(my $OUTFILE, '>:encoding(UTF-8)', $filename) or die "$!\n";
		for my $row(@$array){
			map { ## no critic (MutatingListFunctions)
				s/([\\'])/\\$1/g; # escape \ and '
				$_ = "'$_'";  # entry -> 'entry'
			} @$row;
			print $OUTFILE join $separator, @$row;
			print $OUTFILE "\n";
		}
	close($OUTFILE);
	return 1;
}

sub write_www_cache{
	my $self = shift;
	if($self->{'use_www_cache'} && defined($self->{'www_cache_file'})){
		$self->write_json_file($self->{'www_cache'}, $self->{'www_cache_file'});
	}
}

END { } # module clean-up code here (global destructor)
1;

=head1 NAME

CamelBot - Perl extension for mediawiki functions built upon L<MediaWiki::Bot>.

=head1 SYNOPSIS

  use CamelBot;
  my $camelbot = CamelBot->new({
  	'max_edits_per_min' => 5,
  	'mw_username'   => $params->{'username'},
  	#'host'         => 'wiki.selfhtml.org',
  	#'rel_url_path' => 'mediawiki',
  	'simulation'    => $params->{'test'},
  	'verbosity'     => $params->{'verbose'},
  	'db_access'     => $params->{'db-access'},
  });
  $camelbot->login($params->{'mw_password'}) or die;
  $camelbot->cat_rename('Wurstsalat', 'Salatwurst');

=head1 DESCRIPTION

CamelBot is a library built upon L<MediaWiki::Bot>. Some functions are written
especially for dewiki.

=head2 Constructor

=over 4

=item new(I<HASHREF>)

Returns a newly created C<CamelBot> object. The first argument is an
anonymous hash of settings.

=back

=head2 Methods/Functions for MediaWiki

=over 4

=item DESTROY

destructor

=item add_to_maintenance_table

given a table struct, an array ref of new entries and an array ref of additional
columns, this function adds a row in the table for each new entry.
a row consists of
[[page_name]], additional columns, timestamp, (optional empty cells, if needed)

=item _api_cont

use MediaWiki::API using 'continue' param for a loop

=item api_simple

use MediaWiki::API and catch errors

=item api_reviewstate

use MediaWiki::API to get review states

=item archive_ext_links

find all links in given text ref and call saving procedure on archive.org	

=item cat_add

adds a given bunch of pages to a given category by adding [[category:new]] at
the end

=item cat_rename

moves a category and replaces all [[category:old]] by [[category:new]]

=item change_review_request

given a text ref containing a review request (in format from :de:WP:GV/A),
an old page to be reviewed and a new page to be reviewed, this function replaces
the old page with the new page in the text.

returns numbers of replacements.
returns undef on failure

=item check_abusefilter_match
example:

	my $check = $camelbot->check_abusefilter_match(
		$log_id,
		'(contains_any(added_lines, "== Überschrift ==", "Media:Beispiel.ogg"))'
	); # can be 0 or 1

=item check_external_link

given two successive parts $a1, $a2 of a string $a this function checks,
whether second part of string $a2 is an url with http status 200. the first
part of the string $a1 is used to detect the end of url in part two $a2.

=item check_password

if password is not given, check typical password files or ask user

=item check_review_request

given a review request (as text) and review states, this function extracts and
returns pagename along with a action ('hidden', 'changed' or 'invalid').

=item check_username

if user name is not given, ask user;
normalize use name for mediawiki

=item _check_triggering_namespaces

given namespaces (ref to array of ints) and rc_msg (hash ref),
returns 0, 1, 2, or 3

1: page was not moved and ns of rc_msg is in array
2: page was moved and ns of dest is in array (but ns of source is 0 or ns of dest is not 0)
3: page was moved and ns of dest is in array and ns of source is not 0 and ns of dest is 0
0: else (page not moved and ns not in array; or page moved, but ns of dest not in array)

=item check_mw_version

compare version of mediawiki with given operator and version number (string,
e.g. ">=1.30.0")

=item cleanup_wiki_pages

given an array ref of page names, this function calls cleanup_wiki_page() for
every page and saves the pages if changed. an optional second param can be set to
set a max number of pages to be changed.

returns number of changed pages.

=item cleanup_wiki_page

given a ref to page text (call by reference!), a page name and optional params,
this does standard cleaning up (especially in german wikipedia).

changes the content of the page text and returns a a tuple of a hash ref with
number of changes per task and a summary. the hash ref may contain e.g.

{'linkfix: www.example.org' => 4, 'trailing whitespace' => 0, ...}

=item convert_time_iso2signature_string

given an iso timestamp this function returns the time in a format as it used in
signatures in talk pages.

=item convert_time_log_string2iso

given a date string in a log-style format such as 'Thu Dec 01 07:39:41 CET 2016',
this function returns the timestamp in iso format.

=item convert_time_signature2iso

given a date string in a signature-style format such as
'08:34, 23 March 2021 (CET)', this function returns the timestamp in iso format.

=item convert_ns

converts namespace id to its name or vice versa

=item convert_ns_title2fulltitle

converts namespace id and title to full title.

=item createMWBot

create instance of L<MediaWiki::Bot> (but don't login yet)

=item create_notification

given a notification type, a page name and a revision id, this function gets the
corresponding notification from the wiki, resolves the contained variables
$diff, $article, $signature, and returns the result.

=item create_signature

given a user, an iso timestamp and an optional language, this function returns a
signature as used on talk pages.

=item _db_init

init db connection

=item _db_parse_init

given a filehandle to a config file, this function sets the db connection params.

=item _db_query

send query to db

=item _db_simplequery

wrapper for simple db queries

=item db_fetch_completed_review_requests

use db to get completed review request from corresponding page

=item db_fetch_externallinks

use db to get external links for a given range of el_ids of el-table

=item db_fetch_pages_of_cats_of_the_dead

use db to get pages that are contained in a category concerning dead people

=item db_fetch_recentchanges

use db to get recent changes for a given time period

=item db_fetch_sbl_log

use db to get all entries of the sbl log of blocked edits; optional
(but recommended) given a regexp for a searched url

=item db_table2array_of_hash

converts a db_table (fetchall_arrayref) to a array where each element is a
hashref with {'rowname0'=>'content', 'rowname1'=>'content', ...}

=item delete_empty_ref

given a text reference, this function searches for wiki references (<ref>...</ref>
and alike). if not found, then "<references />" is deleted.

returns number of changes or undef if something went wrong.

=item delete_marked_pages

delete pages that are in a special category

=item delete_old_from_maintenance_table

given a table_body and a number of days to keep, delete old entries from
maintenance table.

=item delete_deleted_from_maintenance_table

given a table_body, delete entries of deleted pages from maintenance table.

=item delete_resolved_from_maintenance_table

given a table body (array of rows) and a maintenance type (string), this function
deletes all rows in the table body where

- the corresponding page (in first column) does not exist anymore or

- the content of the page does not match the regexp pattern (for the given
maintenance type).

but if a page still exists and the content matches, then the second column is filled with the currently first matched string.

returns number of deleted rows.

=item delete_tracking_params_form_url

given a url, delete all tracking parameters from url and return result.

=item delete_wiki_page

deletes a wiki page

=item download_css

download a css file (not implemented yet: and all files loaded inside css)

=item download_files

downloads files from the wiki

=item download_pages_by_prefix

downloads all pages with the given title prefix.
if second parameter is true (default = false), download images, too.

=item extract_maintenance_list_parts

given a text ref, this function returns (by reference) a hash with parts 'intro',
'wikitable' and 'outro'.

the actual returned value can be 'update' or 'create'.

=item extract_templates

given a text ref, return all templates and their params in an array of hashes with
'name' and 'params'. optionally you can set a regexp in order to find only specific
templates.

nested templates are detected but are not returned separately.

however, if you set a regexp, then nested templates might be return (unless their
parent template matches already).

=item fetch_completed_review_requests

use api to get completed review requests for a given page (by default:
use wikis review request page)

=item fetch_pages_of_cats_of_the_dead

get page titles of pages that are categorized as dead persons.

=item fetch_recentchanges

given begin and end timestamps (default = last 12 hours) return all recently changes pages

=item fetch_www_meta_info

given a url (of a newspaper), try to get meta information about author, title, ...

=item fetch_www_meta_info_from_ldjson

special case of fetch_www_meta_info

=item fill_template

given a hash, use the params to fill a template e.g. {{Internetquelle| ...}} and
return that string

=item get_abusefilter_info

return some info on abuse filter rules

=item get_abusefilter_info_global

return some info on global abuse filter rules

=item get_abusefilter_log_ids

example:

	my $log_entries = $camelbot->get_abusefilter_log_ids(6);
	# $log_entries = [791067, ...];

=item get_all_maintenance_triggers_of_page

given a page (by text ref) get all maintenance triggers of this page

=item get_all_pages

return all pages that match a given pattern

=item _get_api_url

return mediawiki api url

=item get_archived_url_timestamp

given url, 2 timestamps (start, end), and a preferred max year,
this function returns the newest archived version of the url between the given
timestamps and if possible older than the preferred year.

=item get_cookie_filename

return filename of cookie that is generated by MediaWiki::Bot

=item get_diff

a more detailed version of MediaWiki::Bot::diff

=item get_mw_version

return the mediawiki version of the currently used wiki using the api.

=item get_namespace_id

return namespace id of a given page

=item get_page_info

get information about a given page (or given pages)

=item get_pages

get page titles by search or explicitely

=item get_pages_content

get content of wikipages

=item get_pages_by_prefix

get all pages and their latest content by prefix

=item get_review_state

given a page title this function returns the review (flagged revicions) state.

=item get_sbl_entries

get all sbl entries of a given wiki

=item get_signature_from_last_edit

given a page (and an optional language) this function returns the
language-dependent "signature" of the last user who edited the page.

this will not search the page content for the newest signature, but just uses the
history.

=item get_table_sum

given a table and some options, returns two hash refs with all numbers each row
summed up and the numbers of values respectively.

e.g. a table with header => 'a', 'b', 'c' and body => ['foo', 1, 2], ['bar', 3, 4]
leads to {b => 4, c => 6}, {b => 2, c => 2}

with the options the behaviour can me modified

=item get_tables_from_wikitext

tries to return all tables from a given wikitext. (this will fail on tables in
tables!)

=item get_user_contribs

get list of pages edited by user

=item get_wikitable_ascii_col_widths

get col widths defined by ascii art

=item _handle_api_error

just a centralized short version of mw api error handling

=item is_allowed

checks whether bot-template is placed on a page and forbids editing

=item is_page_in_maintenance_table

given a page struct, a table struct and an optional array ref of additional
columns, this function returns, whether the page is already included in the table, i.e. there is a row in the table where the page name is the same (and the optional attributes are the same, too).

possible return values:
 undef, if table has no body
 1,     if page is already in table
 '',    else (if page has no row in table)

=item link_delete_in_text

delete links from a given text

=item link_modification

replace/delete links in all/many wiki pages

=item link_replace_in_page

replaces links in a given page

=item link_replace_in_text

replaces links in given text

=item external_link_search

use api to search for all links of a given website

=item link_search_by_user

search for all additions of a given link of a given user

=item link_search_pages

search for pages that shall be used for link_modification

=item login

logs a bot in to some wiki

=item _login_basic_auth

given a L<LWP::UserAgent>, test reachability of mediawiki, use basic auth if
needed. should be called for mw_bot->{api}->{ua}

=item newest_post_info

\return hash ref with thread, author and date of newest post
\param[in] $wikitext content of a talk page

=item normalize_page_title

given a name as string or string ref, this function normalizes the name by
replacing underscores with spaces, trimming, and transform the first char to
upper case.
if the name is given as a reference, the name is replaced inplace.
returns the normalized name.

=item notify_for_maintenance

notify on user or pre-defined wiki pages, if particular edits occur

=item notify_user

given a username, a heading and a message, this function adds a new
section to the user's talk page, unless a section with the same heading
exists already.
if a page does not exist, the page will be created.
returns 1 on success, else undef.

=item notify_user_predefined

just a wrapper for notify_user in combination with create_notification.
returns 1 on success, else undef.

=item parse_page

use mediawiki-api of a wiki to get a parsed wikipage

=item parse_irc_rc

parse recent changes told by bot via irc. return ref to hash with content.

=item parse_wikitext

use mediawiki-api of a wiki to parse wikitext

=item php_code_unused

searches for all links in a given piece of wikitext

=item post_process_html

clean up html that was generated by parsing wikitext

=item rebuild_table

builds a table and pastes it to a section (overwriting the section)

=item refresh_maintenance_params

init (or refresh) all necessary params for maintenance lists

=item save_wiki_page

\brief saves a wiki page
\param[in] $bot a logged in MediaWiki::Bot
\param[in] $page page name
\param[in] $summary an edit summary (without 'Bot: ') that will be used when
saving
\param[in] $text a ref to the new wikitext
\param[in] $orig_text a ref to the old wikitext
\param[in] $time_stack a ref to an array that is used for low frequency editing
\param[in] $user_answer a ref to a string containing '' (nothing) or 'y' to
save the last user's answer

=item search_history

search history of a given page

=item search_pages

search mediawiki for a given string

=item search_sbl_blocked_log

search the list of blocked edits (blocked by sbl) for a given url-regexp

=item search_sbl_attempts

search list(s) of blocked edits (blocked by sbl)

=item template_spon_converter

convert a special stupid/wrong ''{{Internetquelle ...}}'' to a sane one

=item table2wikitext

converts a special table hash ref to wikitext

=item table_body2array

convert wiki table body to perl ref to 2d array

=item test_and_replace

tests whether a regexp matches and replaces it. give some verbose output.
checks urls, breaks if http status != 200
returns number of replacements

=item text_replacement

replaces text in wiki pages

=item time_management

forces that only x edits per minute are done

=item title2filename

converts a page title to a filename

=item title2url

convert article name to url

=item title2url_part

convert article name to part of url with article name
'w t f' => 'w_t_f'

=item trigger_maintenance_callback

given a hash (with key 'page_with_ns') and a priority ('immediate' or 'short-term'),
this function checks whether there exists a call-back function to call -- and calls
that function.
returns number of changes.

=item triggers_maintenance_lists

given a maintenance type, an optional rc_msg, and an optional text_ref, this
function checks, whether a maintenance action may/shall be triggered. in that
case, (1, $matching_text) is returned, else (0, undef). $matching_text might
be undef if text_ref is not defined.
returns undef on failure.

=item update_edit_filter_index

update page with index of present threads about edit filters

=item update_sbl_index

update page with index of present threads about blocked domains

=item update_maintenance_lists_build_table

build a table for update_maintenance_lists

=item update_maintenance_lists

update some maintenance lists on given edit

=item upload_file

uploads a file to the wiki

=item url2title

convert url to article name

=item wikitableHeader2array

convert wiki table header to perl ref to 1d array

=item wikitable2array

convert wiki table to perl ref to hash, containing a header and a body
containing a 2d array

=item template_zeitde_converter

convert a special stupid/wrong ''{{Internetquelle ...}}'' to a sane one

=back

=head2 other Methods/Functions

=over 4

=item array_contains_elem

given list and elem, return true iff there is an x in list with elem eq x

=item compose_url

given an url struct (see decompose_url), return url

=item convert_time_iso_strip_TZ

given a timestamp in iso format, this function returns just returns the date in
format yyyy-mm-dd hh:mm:dd.

=item convert_time_iso_sep

toggle iso date with separators (yyyy-mm-dd hh:mm:ss) to iso date without
separators yyyymmddhhmmss and vice versa

=item convert_time_iso2unix

convert iso date (with or without separators) to unix timestamp

=item convert_time_unix2unix_tz

given a unix timestamp, a direction param ('from'/'to'), and a timezone
(e.g. 'CET'), this function returns the unix timestamp converted from/to given
timezone.

e.g. convert_time_unix2unix_tz(100_000, 'from', 'CET') returns 96400
and convert_time_unix2unix_tz(100_000, 'to', 'CET') returns 103600.

=item decode_html_entities

decode &gt;, &lt;, and &amp; in-place

given a text ref (call-by-reference), this function replaces some html entities to
the real chars.

=item decompose_url

decompose given url in to syntactical parts (hash)

=item get_date_iso

get date as formatted string "%Y-%m-%d" from given (or current) unix timestamp

=item get_http_content

return content of a given url

=item get_http_response

return response object of a given url

=item get_http_status

return http status code of a given url

=item get_time_iso

get date and time as formatted string "%Y-%m-%d%H:%M:%S" from given (or current)
unix timestamp

=item get_time_iso_

get date and time as formatted string "%Y-%m-%d %H:%M:%S" from given
(or current) unix timestamp

=item match_any

given a string and an array of regexps, return 1 if array is defined and contains
at least one regexp pattern that matches the string.

=item msg

print message to stdout

=item num_unique_elem

return number of elements, without counting double entries

=item read_file_binary

read a file binary

=item show_diff

shows a diff via L<Tk::DiffText>

=item uri_escaper

wrapper for L<URI::Escape>::uri_escape_utf8

=item uri_unescaper

wrapper for L<URI::Escape>::uri_unescape

=item url_converter

convert short-urls (of specific kind) into long url

=item write_csv

write a 2d-array (= ref to array of refs to arrays) to a csv file

=back

=head2 EXPORT

None by default.

=head1 SEE ALSO

* L<MediaWiki::Bot>
* https://gitlab.com/wp-seth/camelbot

=head1 AUTHOR

seth, https://gitlab.com/wp-seth/

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2019 by seth

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself, either Perl version 5.28.1 or,
at your option, any later version of Perl 5 you may have available.

=cut
