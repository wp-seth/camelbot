# camelbot
this package consists of an executable `camelbot` (and some libraries) that can be used to automize several tasks in a MediaWiki.

camelbot is built on top of the perl modules [MediaWiki::Bot](https://metacpan.org/pod/MediaWiki::Bot) and [MediaWiki::API](https://metacpan.org/pod/MediaWiki::API).

## usage

see [camelbot.md](docs/camelbot.md) for the main documentation of cli tool.

furthermore there's (incomplete) documentation of the libraries that camelbot depends on:
- [CamelBot.md](docs/CamelBot.md) -- the main library of camelbot consists of many functions for fetching/manipulation information of a MediaWiki
- [CamelBotRC.md](docs/CamelBotRC.md) -- additional package for working with recent changes of a mediawiki (fetched via db access)
- [CamelBotIRC.md](docs/CamelBotIRC.md) -- using an IRC channel to fetch the recent changes
