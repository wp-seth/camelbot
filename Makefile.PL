use strict;
use warnings;
use 5.018002;

use ExtUtils::MakeMaker;
WriteMakefile(
	NAME          => 'camelbot',
	AUTHOR        => 'seth',
	VERSION       => '2.1.1',
	ABSTRACT      => 'camelbot manipulates or gets information from mediawiki pages',
	LICENSE       => 'freebsd',
	PREREQ_PM     => {
		'Bot::BasicBot'         => '0.93',
		'DateTime::Format::Strptime' => '1.76',
		'Data::Dumper'          => '0',
		'DBI'                   => '0',
		'Carp'                  => '0',
		'File::Slurp'           => '9999.26',
		'FindBin'               => '1.51',
		'Getopt::Long'          => '0',
		'IPC::Run'              => '20180523.0',
		'JSON'                  => '0',
		'LWP::UserAgent'        => '6.36',
		'MediaWiki::Bot'        => '5.006003',
		'PHP::Serialization'    => '0.34',
		'Pod::Usage'            => '1.69',
		'POSIX'                 => '0',
		'Term::ReadKey'         => '2.37',
		'Time::Local'           => '1.25',
	},
	TEST_REQUIRES => {
		'Test::More' => '1.302133'
	},
	EXE_FILES => [
		'camelbot',
	],
	META_MERGE    => {
		'meta-spec' => { version => 2 },
		resources => {
			repository => {
				type => 'git',
				url  => 'https://gitlab.com/wp-seth/camelbot.git',
				web  => 'https://gitlab.com/wp-seth/camelbot',
			},
			bugtracker => {web => 'https://gitlab.com/wp-seth/camelbot/-/issues'},
			homepage   => 'https://gitlab.com/wp-seth/camelbot',
		},
	},
);

