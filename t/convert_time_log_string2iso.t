#/usr/bin/perl
use strict;
use warnings;
#use utf8; # ü
#use open ':std', ':encoding(utf8)'; # must be loaded before Test::More
use Test::More;# tests => 2;
use FindBin;
use lib "$FindBin::Bin/../lib";
use CamelBot;
use Data::Dumper; # for debugging only

my $test_counter = 0;
my $cb = new_ok('CamelBot' => [{
	'ask_user'      => 0,
	'mw_username'   => 'CamelBot', 
	'simulation'    => 1,
	'showdiff'      => 0,
	'verbosity'     => 0,
	'db_access'     => 0,
	'cliparams'     => {},
}]);
++$test_counter;
can_ok($cb, 'createMWBot');
++$test_counter;
$cb->createMWBot();
$cb->{'offline'} = 1;
$cb->{'verbosity'} = -1;

can_ok($cb, 'convert_time_log_string2iso');
++$test_counter;
my $test_data = [
	{ 'name' => 'undef',
		'in' => undef,
		'out' => undef,
	},
	{ 'name' => 'empty string',
		'in' => '',
		'out' => '',
	},
	{ 'name' => 'zero',
		'in' => 0,
		'out' => 0,
	},
	{ 'name' => 'some date format',
		'in' => 'Thu Dec 01 07:39:41 CET 2016',
		'out' => '2016-12-01T07:39:41',
	},

];

for my $e(@$test_data){
	my $to = $cb->convert_time_log_string2iso($e->{'in'});
	is($to, $e->{'out'}, $e->{'name'});
	++$test_counter;
}
done_testing($test_counter);


