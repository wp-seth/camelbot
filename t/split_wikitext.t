#/usr/bin/perl
use strict;
use warnings;
use Test::More;# tests => 2;
use FindBin;
use lib "$FindBin::Bin/../lib";
use CamelBot;
use CamelBotRC;
use Data::Dumper; # for debugging only

my $test_counter = 0;
my $cb = new_ok('CamelBot' => [{
	'ask_user'      => 0,
	'mw_username'   => 'CamelBot',
	'simulation'    => 1,
	'showdiff'      => 0,
	'verbosity'     => 0,
	'db_access'     => 0,
	'cliparams'     => {},
}]);
++$test_counter;
$cb->{'verbosity'} = -1;
can_ok($cb, 'split_wikitext');
++$test_counter;
my $test_data = [
	{ 'name' => 'undef',
		'in' => undef,
		'out' => [],
	},
	{ 'name' => 'empty',
		'in' => \'',
		'out' => [[{'text' => '', 'type' => 'text'}], 1],
	},
	{ 'name' => 'start with template',
		'in' => \'{{tpl|...}} lala',
		'out' => [[
				{'text' => '{{tpl|...}}', 'type' => 'template'},
				{'text' => ' lala', 'type' => 'text'}
			], 1],
	},
	{ 'name' => 'start with space',
		'in' => \' {{tpl|...}} lala',
		'out' => [[
				{'text' => ' ', 'type' => 'text'},
				{'text' => '{{tpl|...}}', 'type' => 'template'},
				{'text' => ' lala', 'type' => 'text'}
			], 1],
	},
	{ 'name' => 'two templates',
		'in' => \'foo {{tpl|...|...}} bar bar {{tpl moep}} baz',
		'out' => [[
				{'text' => 'foo ', 'type' => 'text'},
				{'text' => '{{tpl|...|...}}', 'type' => 'template'},
				{'text' => ' bar bar ', 'type' => 'text'},
				{'text' => '{{tpl moep}}', 'type' => 'template'},
				{'text' => ' baz', 'type' => 'text'}]
			, 1],
	},
	{ 'name' => 'two templates without space in between',
		'in' => \'foo {{tpl|...|...}}{{!}} bar bar {{tpl moep}} baz',
		'out' => [[
				{'text' => 'foo ', 'type' => 'text'},
				{'text' => '{{tpl|...|...}}', 'type' => 'template'},
				{'text' => '{{!}}', 'type' => 'template'},
				{'text' => ' bar bar ', 'type' => 'text'},
				{'text' => '{{tpl moep}}', 'type' => 'template'},
				{'text' => ' baz', 'type' => 'text'}]
			, 1],
	},
	{ 'name' => 'nested template',
		'in' => \'foo {{!}} lala {{tpl|lala{{!}}moep}} bar bar 
			lala {{tpl moep|{{sonstwadd}}}} baz',
		'out' => [[
				{'text' => 'foo ', 'type' => 'text'},
				{'text' => '{{!}}', 'type' => 'template'},
				{'text' => ' lala ', 'type' => 'text'},
				{'text' => '{{tpl|lala{{!}}moep}}', 'type' => 'template'},
				{'text' => " bar bar \n\t\t\tlala ", 'type' => 'text'},
				{'text' => '{{tpl moep|{{sonstwadd}}}}', 'type' => 'template'},
				{'text' => ' baz', 'type' => 'text'}]
			, 1],
	},
	{ 'name' => 'nested template',
		'in' => \'Genf {{formatnum:{{#expr:{{Metadaten Einwohnerzahl CH-GE|6621}}/15.89 round 0}}}}; New York 10.532;',
		'out' => [[
				{'text' => 'Genf ', 'type' => 'text'},
				{'text' => '{{formatnum:{{#expr:{{Metadaten Einwohnerzahl CH-GE|6621}}/15.89 round 0}}}}',
					'type' => 'template'},
				{'text' => '; New York 10.532;', 'type' => 'text'}]
			, 1],
	},

];

for my $e(@$test_data){
	my $result = [$cb->split_wikitext($e->{'in'})];
	is_deeply($result, $e->{'out'}, $e->{'name'});
	++$test_counter;
}
done_testing($test_counter);

