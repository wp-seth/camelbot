#/usr/bin/perl
use strict;
use warnings;
use utf8;
use open ':std', ':encoding(utf8)'; # must be loaded before Test::More
use Test::More;# tests => 2;
use FindBin;
use lib "$FindBin::Bin/../lib";
use CamelBot;
use Data::Dumper; # for debugging only

my $test_counter = 0;
my $cb = new_ok('CamelBot' => [{
	'ask_user'      => 0,
	'mw_username'   => 'CamelBot', 
	'simulation'    => 1,
	'showdiff'      => 0,
	'verbosity'     => 0,
	'db_access'     => 0,
	'cliparams'     => {},
}]);
++$test_counter;
can_ok($cb, 'createMWBot');
++$test_counter;
$cb->createMWBot();
$cb->{'offline'} = 1;
$cb->{'verbosity'} = -1;

can_ok($cb, 'fill_template');
++$test_counter;

my $test_data = [
	['undef',
		undef,
		undef,
		undef,
		{set_params => [], str => undef},
  ],
	['wrong param',
		'Internetquelle',
		123,
		undef,
		{set_params => [], str => undef},
  ],
	['no content',
		'Internetquelle',
		{},
		0,
		{set_params => [], str => '{{Internetquelle}}'},
  ],
	['just a author',
		'Internetquelle',
		{author => 'moep'},
		0,
		{set_params => ['author'], str => '{{Internetquelle |autor=moep}}'},
	],
	['normal params',
		'Internetquelle',
		{author => 'moep moep', url => 'https://example.org', title=>'some title', work => '[[some journal]]', date => '2010-09-30', access_date => '2020-10-12'},
		0,
		{ set_params => ['author', 'url', 'title', 'work', 'date', 'access_date'], 
			str => '{{Internetquelle |autor=moep moep |url=https://example.org |titel=some title |werk=[[some journal]] |datum=2010-09-30 |abruf=2020-10-12}}',
		},
  ],
];

for my $e(@$test_data){
	my $to = $cb->fill_template($e->[1], $e->[2], $e->[3]);
	is_deeply($to, $e->[4], $e->[0]);
	++$test_counter;
}
done_testing($test_counter);

