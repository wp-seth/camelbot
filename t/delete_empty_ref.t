#/usr/bin/perl
use strict;
use warnings;
use utf8; # ü
use open ':std', ':encoding(utf8)'; # must be loaded before Test::More
use Test::More;# tests => 2;
use FindBin;
use lib "$FindBin::Bin/../lib";
use CamelBot;
use Data::Dumper; # for debugging only

my $test_counter = 0;
my $cb = new_ok('CamelBot' => [{
	'ask_user'      => 0,
	'mw_username'   => 'CamelBot', 
	'simulation'    => 1,
	'showdiff'      => 0,
	'verbosity'     => 0,
	'db_access'     => 0,
	'cliparams'     => {},
}]);
++$test_counter;
can_ok($cb, 'createMWBot');
++$test_counter;
$cb->createMWBot();
$cb->{'offline'} = 1;

can_ok($cb, 'delete_empty_ref');
++$test_counter;

my $test_data = [
	['undef', undef, undef, undef],
	['wrong string', '', '', undef],
	['empty', \'', \'', 0],
	['simple (no change)', \'foo', \'foo', 0],
	['simple (change)', 
		\"Der '''Messerrücken''' bezeichnet bei einem [[Messer]] ... gegenüberliegend ... [[Klinge]].\n\nIn der [[Fischzucht]] ... hochrückigen [[Karpfenfische]]n wie etwa dem [[Brachse|Blei]]. Er äußert ... Tiere.\n\n== Belege ==\n<references>\n\n</references>\n\n{{SORTIERUNG:Messerrucken}}\n[[Kategorie:Anatomie der Fische]]", 
		\"Der '''Messerrücken''' bezeichnet bei einem [[Messer]] ... gegenüberliegend ... [[Klinge]].\n\nIn der [[Fischzucht]] ... hochrückigen [[Karpfenfische]]n wie etwa dem [[Brachse|Blei]]. Er äußert ... Tiere.\n\n{{SORTIERUNG:Messerrucken}}\n[[Kategorie:Anatomie der Fische]]",
		1],
];

for my $e(@$test_data){
	my $param = $e->[1];
	if(defined $e->[1] && ref $e->[1] eq 'SCALAR'){
		my $tmp = ${$e->[1]};
		$param = \$tmp;
	}
	print Dumper $param;
	my $res = $cb->delete_empty_ref($param);
	is_deeply($param, $e->[2], $e->[0] . ' (string modification)');
	++$test_counter;
	is($res, $e->[3], $e->[0] . ' (return value)');
	++$test_counter;
}
done_testing($test_counter);

