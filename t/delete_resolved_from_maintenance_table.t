#/usr/bin/perl
use strict;
use warnings;
#use utf8; # ü
#use open ':std', ':encoding(utf8)'; # must be loaded before Test::More
use Test::More;# tests => 2;
use FindBin;
use lib "$FindBin::Bin/../lib";
use CamelBot;
use Data::Dumper; # for debugging only

my $test_counter = 0;
my $cb = new_ok('CamelBot' => [{
	'ask_user'      => 0,
	'mw_username'   => 'CamelBot', 
	'simulation'    => 1,
	'showdiff'      => 0,
	'verbosity'     => 0,
	'db_access'     => 0,
	'cliparams'     => {},
}]);
++$test_counter;
can_ok($cb, 'createMWBot');
++$test_counter;
$cb->createMWBot();
$cb->{'offline'} = 1;
$cb->{'verbosity'} = -1;

can_ok($cb, 'delete_resolved_from_maintenance_table');
++$test_counter;
my $test_data = [
	{ 'name' => 'all undef',
		'in' => [undef, undef],
		'offline_mw_bot_get_text' => undef,
		'out' => undef,
		'out_table' => undef,
	},
	{ 'name' => 'table body undef',
		'in' => [undef, 'moep'],
		'offline_mw_bot_get_text' => undef,
		'out' => undef,
		'out_table' => undef,
	},
	{ 'name' => 'mt_type undef',
		'in' => ['moep', undef],
		'offline_mw_bot_get_text' => undef,
		'out' => undef,
		'out_table' => 'moep',
	},
	{ 'name' => 'non-existing mt_type',
		'in' => ['moep', 'non-existing test 123'],
		'offline_mw_bot_get_text' => undef,
		'out' => undef,
		'out_table' => 'moep',
	},
	{ 'name' => 'empty table',
		'in' => [[], 'cat dead'],
		'offline_mw_bot_get_text' => '',
		'out' => 0,
		'out_table' => [],
	},
	{ 'name' => 'entry resolved',
		'in' => [[['[[some page]]', 'gestorben 2000', '2020-01-01']], 'cat dead'],
		'offline_mw_bot_get_text' => '',
		'out' => 1,
		'out_table' => [],
	},
	{ 'name' => 'entry not resolved',
		'in' => [[['[[some page]]', 'Gestorben 2000', '2020-01-01']], 'cat dead'],
		'offline_mw_bot_get_text' => "moep\n[[category:Gestorben 2000]]",
		'out' => 0,
		'out_table' => [['[[some page]]', '<pre>[[category:Gestorben 2000]]</pre>', '2020-01-01']],
	},
];

for my $e(@$test_data){
	$cb->{'offline_mw_bot_get_text'} = $e->{'offline_mw_bot_get_text'};
	my $to = $cb->delete_resolved_from_maintenance_table(
		$e->{'in'}[0], $e->{'in'}[1]);
	is($to, $e->{'out'}, $e->{'name'} . ': number of deleted entries');
	++$test_counter;
	is_deeply($e->{'in'}[0], $e->{'out_table'}, $e->{'name'} . ': table change');
	++$test_counter;
}
done_testing($test_counter);

