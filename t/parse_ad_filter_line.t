#/usr/bin/perl
use strict;
use warnings;
#use utf8; # ü
#use open ':std', ':encoding(utf8)'; # must be loaded before Test::More
use Test::More;# tests => 2;
use FindBin;
use lib "$FindBin::Bin/../lib";
use CamelBot;
use Data::Dumper; # for debugging only

my $test_counter = 0;
my $cb = new_ok('CamelBot' => [{
	'ask_user'      => 0,
	'mw_username'   => 'CamelBot',
	'simulation'    => 1,
	'showdiff'      => 0,
	'verbosity'     => 0,
	'db_access'     => 0,
	'cliparams'     => {},
}]);
++$test_counter;
can_ok($cb, 'createMWBot');
++$test_counter;
$cb->createMWBot();
$cb->{'verbosity'} = -1;

can_ok($cb, 'parse_ad_filter_line');
++$test_counter;
$cb->{'re_url_tracking_filter'} = {};
my $test_data = [
	#in, out, struct, description
	[undef, undef, {}, 'undef'],
	['', 0, {}, 'empty string'],
	['$removeparam=foo', 1, {'key'=>{'foo' => 1}}, 'simple setting of removeparam'],
	['!$removeparam=foo', 0, {}, 'comment'],
	['$removeparam=/foo/', 1, {'keyval' => [qr/foo/u]}, 'regexp'],
	['||example.org^$removeparam=foo', 1,
		{'domain-specific' => {qr/example\.org\z/u => {'key' => {'foo' => 1}}}}, 'domain-specific'],
	['||example.org^$removeparam=/foo/', 1,
		{'domain-specific' => {qr/example\.org\z/u => {'keyval' => [qr/foo/u]}}}, 'domain-specific regexp'],
	['||example.$removeparam=foo', 1,
		{'domain-specific' => {qr/example\./u => {'key' => {'foo' => 1}}}}, 'domain part'],
	['||example.$removeparam=/foo/', 1,
		{'domain-specific' => {qr/example\./u => {'keyval' => [qr/foo/u]}}}, 'domain part regexp'],
	# TODO: more tests needed, whitelists test for example
];

for my $e(@$test_data){
	my $to = $cb->parse_ad_filter_line($e->[0]);
	is($to, $e->[1], $e->[3] . ' binary result');
	++$test_counter;
	is_deeply($cb->{'re_url_tracking_filter'}, $e->[2], $e->[3] . ' re_url_tracking_filter');
	++$test_counter;
	$cb->{'re_url_tracking_filter'} = {};  # reset for next test
}
done_testing($test_counter);

