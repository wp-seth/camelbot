#/usr/bin/perl
use strict;
use warnings;
#use utf8; # ü
#use open ':std', ':encoding(utf8)'; # must be loaded before Test::More
use Test::More;# tests => 2;
use FindBin;
use lib "$FindBin::Bin/../lib";
use CamelBot;
use Data::Dumper; # for debugging only

my $test_counter = 0;
my $cb = new_ok('CamelBot' => [{
	'ask_user'      => 0,
	'mw_username'   => 'CamelBot',
	'simulation'    => 1,
	'showdiff'      => 0,
	'verbosity'     => 0,
	'db_access'     => 0,
	'cliparams'     => {},
}]);
++$test_counter;
can_ok($cb, 'createMWBot');
++$test_counter;
$cb->createMWBot();
$cb->{'offline'} = 1;
#$cb->{'verbosity'} = -1;

can_ok($cb, 'reduce_array_by_array');
++$test_counter;
my $test_data = [
	{ 'name' => 'all undef',
		'in' => [undef, undef],
		'out' => [],
	},
	{ 'name' => 'first undef',
		'in' => [undef, []],
		'out' => [],
	},
	{ 'name' => 'second undef, first empty',
		'in' => [[], undef],
		'out' => [],
	},
	{ 'name' => 'second undef, first non-empty',
		'in' => [[32], undef],
		'out' => [32],
	},
	{ 'name' => 'both equal, empty',
		'in' => [[], []],
		'out' => [],
	},
	{ 'name' => 'both equal, single element',
		'in' => [['moep'], ['moep']],
		'out' => [],
	},
	{ 'name' => 'both equal, 3 different elems',
		'in' => [['moep', 'foo', 'lala'], ['foo', 'moep', 'lala']],
		'out' => [],
	},
	{ 'name' => 'both equal, 3 elems, partly redundant',
		'in' => [['moep', 'foo', 'moep'], ['moep', 'moep', 'foo']],
		'out' => [],
	},
	{ 'name' => 'first with 3 elems, but double elem',
		'in' => [['moep', 'foo', 'moep'], ['moep', 'foo']],
		'out' => ['moep'],
	},
	{ 'name' => 'first with 4 elems, but double elem',
		'in' => [['moep', 'foo', 'moep', 'lala'], ['moep', 'foo']],
		'out' => ['moep', 'lala'],
	},
];

for my $e(@$test_data){
	my $to = $cb->reduce_array_by_array($e->{'in'}[0], $e->{'in'}[1]);
	is_deeply($to, $e->{'out'}, $e->{'name'});
	++$test_counter;
}
done_testing($test_counter);

