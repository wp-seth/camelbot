#/usr/bin/perl
use strict;
use warnings;
use utf8; # ü
use open ':std', ':encoding(utf8)'; # must be loaded before Test::More
use Test::More;# tests => 2;
use FindBin;
use lib "$FindBin::Bin/../lib";
use CamelBot;
use Data::Dumper; # for debugging only

my $test_counter = 0;
my $cb = new_ok('CamelBot' => [{
	'ask_user'      => 0,
	'mw_username'   => 'CamelBot',
	'simulation'    => 1,
	'showdiff'      => 0,
	'verbosity'     => 0,
	'db_access'     => 0,
	'cliparams'     => {},
}]);
++$test_counter;
can_ok($cb, 'createMWBot');
++$test_counter;
$cb->createMWBot();
$cb->{'offline'} = 1;
$cb->{'verbosity'} = -1;

can_ok($cb, 'wikitable2array');
++$test_counter;
my $test_data = [
	[undef, undef, 'undef'],
	['', {'style'=> {table => undef}, 'header' => undef, 'body' => undef}, 'empty string'],
	["{|\n|}", {'style'=> {'table' => ''}, 'header' => [], 'body' => []}, 'empty table'],
	['{| class="wikitable sortable"
! page
! date of detection by CamelBot
|-
| [[Kopenhagener Psalter]]
| 2021-01-31
|}', {
	'style'=> {'table' => 'class="wikitable sortable"'},
	'header' => ['page', 'date of detection by CamelBot'],
	'body' => [['[[Kopenhagener Psalter]]', '2021-01-31']]
}, 'simple table'],
	['{| class="wikitable sortable"
! page !! date of detection by CamelBot
|-
| [[Kopenhagener Psalter]]
| 2021-01-31
|}', {
	'style'=> {'table' => 'class="wikitable sortable"'},
	'header' => ['page', 'date of detection by CamelBot'],
	'body' => [['[[Kopenhagener Psalter]]', '2021-01-31']]
}, 'simple table with alternative header formatting'],
	['{| class="wikitable sortable"
! page || date of detection by CamelBot
|-
| [[Kopenhagener Psalter]]
| 2021-01-31
|}', {
	'style'=> {'table' => 'class="wikitable sortable"'},
	'header' => ['page', 'date of detection by CamelBot'],
	'body' => [['[[Kopenhagener Psalter]]', '2021-01-31']]
}, 'simple table with strange alternative header formatting'],
	['{|
! page
! date
! foo
! bar
! baz
|-
| [[Kopenhagener Psalter]]       ||        moep     || moeep
||            mu
||            mu2
|-
|1  ||   2     || 3
||   ||     5
|}', {
	'style'=> {'table' => ''},
	'header' => ['page', 'date', 'foo', 'bar', 'baz'],
	'body' => [['[[Kopenhagener Psalter]]', 'moep', 'moeep', 'mu', 'mu2'], ['1', '2', '3', '', '5']]
}, 'ugly cell formatting'],
];

for my $e(@$test_data){
	my $to = $cb->wikitable2array($e->[0]);
	is_deeply($to, $e->[1], $e->[2]);
	++$test_counter;
}
done_testing($test_counter);

