#/usr/bin/perl
use strict;
use warnings;
#use utf8; # ü
#use open ':std', ':encoding(utf8)'; # must be loaded before Test::More
use Test::More;# tests => 2;
use FindBin;
use lib "$FindBin::Bin/../lib";
use CamelBot;
use Data::Dumper; # for debugging only

my $test_counter = 0;
my $cb = new_ok('CamelBot' => [{
	'ask_user'      => 0,
	'mw_username'   => 'CamelBot', 
	'simulation'    => 1,
	'showdiff'      => 0,
	'verbosity'     => 0,
	'db_access'     => 0,
	'cliparams'     => {},
}]);
++$test_counter;
can_ok($cb, 'createMWBot');
++$test_counter;
$cb->createMWBot();
$cb->{'offline'} = 1;
$cb->{'verbosity'} = -1;

can_ok($cb, 'convert_time_signature2iso');
++$test_counter;
my $test_data = [
	{ 'name' => 'undef',
		'in' => undef,
		'out' => undef,
	},
	{ 'name' => 'empty string',
		'in' => '',
		'out' => '',
	},
	{ 'name' => 'zero',
		'in' => 0,
		'out' => 0,
	},
	{ 'name' => 'sig date format (utc)',
		'in' => '08:34, 23 March 2021 (UTC)',
		'out' => '2021-03-23T08:34:00',
	},
	{ 'name' => 'sig date format (cet)',
		'in' => '08:34, 23 March 2021 (CET)',
		'out' => '2021-03-23T07:34:00',
	},
	{ 'name' => 'sig date format one-digit-day (cet)',
		'in' => '15:46, 2 March 2021 (CET)',
		'out' => '2021-03-02T14:46:00',
	},
	{ 'name' => 'sig date format (cet, de, umlaut)',
		'in' => '08:34, 23. Mär. 2021 (CET)',
		'out' => '2021-03-23T07:34:00',
	},
	{ 'name' => 'sig date format (cet, de, umlaut coded)',
		'in' => "08:34, 23. M\xe4r. 2021 (CET)",
		'out' => '2021-03-23T07:34:00',
	},
	{ 'name' => 'sig date format (cest, de, umlaut coded)',
		'in' => "08:34, 3. Jun. 2021 (CEST)",
		'out' => '2021-06-03T06:34:00',
	},

];

for my $e(@$test_data){
	my $to = $cb->convert_time_signature2iso($e->{'in'});
	is($to, $e->{'out'}, $e->{'name'});
	++$test_counter;
}
done_testing($test_counter);


