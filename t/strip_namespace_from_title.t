#/usr/bin/perl
use strict;
use warnings;
#use utf8; # ü
#use open ':std', ':encoding(utf8)'; # must be loaded before Test::More
use Test::More;# tests => 2;
use FindBin;
use lib "$FindBin::Bin/../lib";
use CamelBot;
use Data::Dumper; # for debugging only

my $test_counter = 0;
my $cb = new_ok('CamelBot' => [{
	'ask_user'      => 0,
	'cliparams'     => {},
	'db_access'     => 0,
	'mw_username'   => 'CamelBot',
	'pw_ask'        => 0,
	'showdiff'      => 0,
	'simulation'    => 0,
	'verbosity'     => 0,
}]);
++$test_counter;
can_ok($cb, 'login');
++$test_counter;
$cb->{'offline'} = undef; # this test must be done online
$cb->{'verbosity'} = -1;

can_ok($cb, 'strip_namespace_from_title');
++$test_counter;
my $test_data = [
	{ 'name' => 'all undef',
		'in' => [],
		'out' => undef,
	},
	{ 'name' => 'deep undef',
		'in' => [undef, undef],
		'out' => undef,
	},
	{ 'name' => 'main namespace',
		'in' => ['Tralala', 0],
		'out' => 'Tralala',
	},
	{ 'name' => 'user namespace',
		'in' => ['Benutzer:Lustiger seth', 2],
		'out' => 'Lustiger seth',
	},
	{ 'name' => 'user namespace gender gedoens',
		'in' => ['Benutzerin:Lustiger seth', 2],
		'out' => 'Lustiger seth',
	},
];

if($cb->login()){
	for my $e(@$test_data){
		my $to = $cb->strip_namespace_from_title($e->{'in'}[0], $e->{'in'}[1]);
		is($to, $e->{'out'}, $e->{'name'});
		++$test_counter;
	}
}else{
	warn "camelbot needs to login for this test";
}
done_testing($test_counter);

