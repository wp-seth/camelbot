#/usr/bin/perl
use strict;
use warnings;
use utf8;
use open ':std', ':encoding(utf8)'; # must be loaded before Test::More
use Test::More;# tests => 2;
use FindBin;
use lib "$FindBin::Bin/../lib";
use CamelBot;
use Data::Dumper; # for debugging only

my $test_counter = 0;
my $cb = new_ok('CamelBot' => [{
	'ask_user'      => 0,
	'mw_username'   => 'CamelBot', 
	'simulation'    => 1,
	'showdiff'      => 0,
	'verbosity'     => 0,
	'db_access'     => 0,
	'cliparams'     => {},
}]);
++$test_counter;
can_ok($cb, 'createMWBot');
++$test_counter;
$cb->createMWBot();
$cb->{'offline'} = 1;
$cb->{'verbosity'} = -1;

can_ok($cb, 'delete_tracking_params_form_url');
++$test_counter;

my $test_data = [
	['undef',
	 undef,
	 undef,
  ],
	['empty string',
	 '',
	 '',
  ],
	['simple url w/o change',
	 'https://amnesty.org',
	 'https://amnesty.org',
  ],
	['simple url with # w/o change',
	 'https://amnesty.org#foo=bar?fbclid=foo',
	 'https://amnesty.org#foo=bar?fbclid=foo',
  ],
	['ebay',
	 'https://www.ebay.de/itm/194727781082?chn=ps&norover=1&mkevt=1&mkrid=707-134425-41852-0&mkcid=2&device=c&mktype=pla&poi=&mkgroupid=121830191225&abcId=1145991&merchantid=7364532',
	 'https://www.ebay.de/itm/194727781082',
  ],
	['fbclid',
	 'https://www.nzfootball.co.nz/newsarticle/100693?fbclid=IwAR3LxazQXWaFucKFaCEboOK3dSDG46zAxOKG0V3-CaFmzsUcKsxPU0MdNFs',
	 'https://www.nzfootball.co.nz/newsarticle/100693',
  ],
	['fbclid with anchor1',
	 'https://www.japantimes.co.jp/culture/2018/03/29/music/kiyoshi-koyama-life-lived-jazz/?fbclid=IwAR0mWjD5b8Y2-fXNGa-qDBBQbsHnbTAZ-ux7M7kt8-c5riXcg4W5JYN8ckE#.XFgQ1s0xmM9',
	 'https://www.japantimes.co.jp/culture/2018/03/29/music/kiyoshi-koyama-life-lived-jazz/',
  ],
	['fbclid with anchor2',
	 'http://tempotimor.com/ekonomia/576-ofisialmente-trans-nusa-hahu-semo-dili-kupang?fbclid=IwAR3E5YjraH0YUFIl5PgWUABUBxEnTMfYTAIh-bJO_8GfMVBMxNU8OYCDaOg#.XQNVZcbfOSA.facebook',
	 'http://tempotimor.com/ekonomia/576-ofisialmente-trans-nusa-hahu-semo-dili-kupang',
  ],
	['example.org url with # and fbclid as first param',
	 'https://example.org?fbclid=foo&foo=bar#bar',
	 'https://example.org?fbclid=foo&foo=bar#bar',
  ],
	['example.orgurl with # and fbclid as last param',
	 'https://example.org?foo=bar&fbclid=foo#bar',
	 'https://example.org?foo=bar&fbclid=foo#bar',
  ],
	['example.org url with # and fbclid as middle param',
	 'https://example.org?foo=bar&fbclid=foo&baz=quux#bar',
	 'https://example.org?foo=bar&fbclid=foo&baz=quux#bar',
  ],
	['url with # and fbclid as first param',
	 'https://amnesty.org?fbclid=foo&foo=bar#bar',
	 'https://amnesty.org?foo=bar#bar',
  ],
	['url with # and fbclid as last param',
	 'https://amnesty.org?foo=bar&fbclid=foo#bar',
	 'https://amnesty.org?foo=bar#bar',
  ],
	['url with # and fbclid as middle param',
	 'https://amnesty.org?foo=bar&fbclid=foo&baz=quux#bar',
	 'https://amnesty.org?foo=bar&baz=quux#bar',
  ],
	['utm_referrer',
	 'https://www.zeit.de/politik/ausland/2017-08/ungarn-viktor-orban-eu-gelder-autokratie-rechtsstaat?utm_referrer=https://de.wikipedia.org',
	 'https://www.zeit.de/politik/ausland/2017-08/ungarn-viktor-orban-eu-gelder-autokratie-rechtsstaat',
  ],
	['utm_referrer',
	 'https://www.israelnetz.com/politik-wirtschaft/politik/2021/05/31/anerkennung-mit-risiko/?utm_source=newsletter&utm_medium=email&utm_campaign%5BcObj%5D%5Bdata%5D=date%3AU&utm_campaign%5BcObj%5D%5Bstrftime%5D=%25y-%25m-%25d',
	 'https://www.israelnetz.com/politik-wirtschaft/politik/2021/05/31/anerkennung-mit-risiko/',
  ],
	['dmcid',
	 'https://www.rheinische-anzeigenblaetter.de/mein-blatt/extra-blatt/ruppichteroth/gottesdienst-mit-bratwurstduft-oberlueckerath-feierte-mit-grossem-fest-seine-kapelle-32824682?dmcid=sm_fb&fbclid=IwAR3lkJnPNx_R9URmTVRUAHmrh4b9KSY2aVWhOva1IQ_8Tv_cxqFci5YBxYQ',
	 'https://www.rheinische-anzeigenblaetter.de/mein-blatt/extra-blatt/ruppichteroth/gottesdienst-mit-bratwurstduft-oberlueckerath-feierte-mit-grossem-fest-seine-kapelle-32824682',
  ],
	['focus',
	 'https://www.focus.de/politik/ausland/ukraine-krieg-zu-russland-uebergelaufener-parlamentsabgeordneter-ermordet_id_137478105.html?fbc=facebook-focus-online-panorama&ts=202208291949&cid=29082022',
	 'https://www.focus.de/politik/ausland/ukraine-krieg-zu-russland-uebergelaufener-parlamentsabgeordneter-ermordet_id_137478105.html',
  ],
	['not focus',
	 'https://www.example.org/politik/ausland/ukraine-krieg-zu-russland-uebergelaufener-parlamentsabgeordneter-ermordet_id_137478105.html?fbc=facebook-focus-online-panorama&ts=202208291949&cid=29082022',
	 'https://www.example.org/politik/ausland/ukraine-krieg-zu-russland-uebergelaufener-parlamentsabgeordneter-ermordet_id_137478105.html?ts=202208291949&cid=29082022',
  ],
	['handelsblatt',
	 'https://www.handelsblatt.com/unternehmen/energie/oelkonzerne-in-der-krise-shell-bp-total-das-ende-des-oelzeitalters-naht/26249416.html?fbclid=IwAR36iJ8FrNy_HrP40CEgGN691jEAi822jo3TiYikBni1in8oL2jbqLs-goE&ticket=ST-114272-bzdmWjBmkmxlgD46ADJc-ap2',
	 'https://www.handelsblatt.com/unternehmen/energie/oelkonzerne-in-der-krise-shell-bp-total-das-ende-des-oelzeitalters-naht/26249416.html',
  ],
	['not handelsblatt',
	 'https://www.example.org/unternehmen/energie/oelkonzerne-in-der-krise-shell-bp-total-das-ende-des-oelzeitalters-naht/26249416.html?fbclid=IwAR36iJ8FrNy_HrP40CEgGN691jEAi822jo3TiYikBni1in8oL2jbqLs-goE&ticket=ST-114272-bzdmWjBmkmxlgD46ADJc-ap2',
	 'https://www.example.org/unternehmen/energie/oelkonzerne-in-der-krise-shell-bp-total-das-ende-des-oelzeitalters-naht/26249416.html?ticket=ST-114272-bzdmWjBmkmxlgD46ADJc-ap2',
  ],
	['imdb ref_ param',
	 'https://www.imdb.com/title/tt15103682/?ref_=ttfc_fc_tt',
	 'https://www.imdb.com/title/tt15103682/',
  ],
	['igshid', # https://de.wikipedia.org/w/index.php?title=Genesis_der_Daleks&diff=prev&oldid=235184184
	 'https://www.instagram.com/p/BgBv-udB88z/?igshid=1btjtjf651kq8&fbclid=IwAR06YUh9_mNW9bqgCSAH-upHt_dI0245hrGNHdueN2gXX2czlpm_8L-aR9o',
	 'https://www.instagram.com/p/BgBv-udB88z/',
  ],
	['twitter', # https://de.wikipedia.org/w/index.php?title=Genesis_der_Daleks&diff=prev&oldid=235184184
	 'https://twitter.com/daverich1/status/995553744183091200/photo/1?tfw_site=haaretzcom&ref_src=twsrc%5Etfw&ref_url=https://www.haaretz.com/israel-news/chickensong-wins-eurovision-toys-with-israel-haters-on-twitter-1.6077594',
	 'https://twitter.com/daverich1/status/995553744183091200/photo/1',
  ],
	['welt.de',
	 'https://www.welt.de/vermischtes/article123453167/Der-Neger-im-taeglichen-Facebook-Faschismus.html?wtrid=onsite.onsitesearch',
	 'https://www.welt.de/vermischtes/article123453167/Der-Neger-im-taeglichen-Facebook-Faschismus.html',
  ],
	['wiki-encoding',
	 'http://www.simplicissimus.info/index.php?id=7&tx_lombkswjournaldb_pi2&#91;personid&#93;=568&tx_lombkswjournaldb_pi2&#91;action&#93;=nameFilter&tx_lombkswjournaldb_pi2&#91;controller&#93;=PersonRegister',
	 'http://www.simplicissimus.info/index.php?id=7&tx_lombkswjournaldb_pi2&#91;personid&#93;=568&tx_lombkswjournaldb_pi2&#91;action&#93;=nameFilter&tx_lombkswjournaldb_pi2&#91;controller&#93;=PersonRegister',
  ],
	['combinated tracking nzz',
	 'https://www.nzz.ch/feuilleton/relotius-revisited-der-ehemalige-spiegel-reporter-ist-zurueck-und-verbreitet-neue-maerchen-ld.1628754?mktcid=smch&mktcval=fbpost_2021-06-07&intpro=content_nzzde_fbpost_paid&mktfbcampn=%7B%7Bcampaign.name%7D&mktfbadsetn=%7B%7Badset.name%7D&mktfbadn=AWA%20%7C%20Ressort%3A%20International%20%7C%20Datum%3A%202021-06-07%20%7C%20Relotius%20revisited%20%7C%20Post%20ID%3A%205734738469902036&mktfbp=Facebook_Desktop_Feed&mktfbplat=fb&fbclid=IwAR3jWChwdRrjg4qa_80VXckupa1HCk-VXXTOLyn1t7QHbjARFoNJkJl7iuQ',
	 'https://www.nzz.ch/feuilleton/relotius-revisited-der-ehemalige-spiegel-reporter-ist-zurueck-und-verbreitet-neue-maerchen-ld.1628754',
	],
	['combinated tracking sz',
	 'https://www.sueddeutsche.de/kultur/achille-mbembe-antisemitismus-brief-1.4910464?sc_src=email_1552917&sc_lid=136288399&sc_uid=RWGujff4wl&sc_llid=35945&utm_medium=email&utm_source=emarsys&utm_content=www.sueddeutsche.de%2Fkultur%2Fachille-mbembe-antisemitismus-brief-1.4910464&utm_campaign=Espresso+am+Abend+18.+Mai+2020',
	 'https://www.sueddeutsche.de/kultur/achille-mbembe-antisemitismus-brief-1.4910464',
  ],
	['combinated tracking welt',
	 'https://www.welt.de/politik/article815914/Die-dritte-Angriffswelle-auf-Europa-rollt.html?wtmc=socialmedia.facebook.shared.web&fbclid=IwAR1wpWNiGm4C7VjSkg4T5X1JhgfpRsEut2mGSjIw9Og9bCfdDGmPTghjfeY',
	 'https://www.welt.de/politik/article815914/Die-dritte-Angriffswelle-auf-Europa-rollt.html',
  ],
	# https://de.wikipedia.org/w/index.php?title=Brendon_Hartley&diff=224717506&oldid=224453745
	['nzherald.co.nz: keep c_id',
	 'http://www.nzherald.co.nz/motorsport/news/article.cfm?c_id=66&objectid=10816386',
	 'http://www.nzherald.co.nz/motorsport/news/article.cfm?c_id=66&objectid=10816386',
  ],
	['archive.org: don\'t delete archived params, php session id',
	 'https://web.archive.org/web/20210706120000/https://example.org?PHPSESSION=123',
	 'https://web.archive.org/web/20210706120000/https://example.org?PHPSESSION=123',
  ],
	['archive.org: don\'t delete archived params, fuckbook',
	 'https://web.archive.org/web/20210706120000/https://www.nzfootball.co.nz/newsarticle/100693?fbclid=IwAR3LxazQXWaFucKFaCEboOK3dSDG46zAxOKG0V3-CaFmzsUcKsxPU0MdNFs',
	 'https://web.archive.org/web/20210706120000/https://www.nzfootball.co.nz/newsarticle/100693?fbclid=IwAR3LxazQXWaFucKFaCEboOK3dSDG46zAxOKG0V3-CaFmzsUcKsxPU0MdNFs',
  ],
	# https://de.wikipedia.org/w/index.php?title=Kylie_Sonique_Love&diff=prev&oldid=230796067
	['goal param',
	 'https://deadline.com/2021/03/outtv-media-group-producer-entertainment-group-outtv-usa-lgbtq-diversity-representation-inclusion-1234723463/?goal=0_fb57ec5ec0-209a6b9133-125807816&mc_cid=209a6b9133&mc_eid=909b825a99',
	 'https://deadline.com/2021/03/outtv-media-group-producer-entertainment-group-outtv-usa-lgbtq-diversity-representation-inclusion-1234723463/',
  ],
	['text fragment links',
	 'https://www.kirche-ll.de/gemeinden/innenstadtgemeinden/st-marien/nachrichten/nachrichten-details/zwei-neue-glocken-fuer-st-marien-eingetroffen.html#:~:text=Auch%20der%20S%C3%BCdturm%20erh%C3%A4lt%20eine,erst%20nach%20den%20Nordturm%2DGlocken.',
	 'https://www.kirche-ll.de/gemeinden/innenstadtgemeinden/st-marien/nachrichten/nachrichten-details/zwei-neue-glocken-fuer-st-marien-eingetroffen.html',
	],
	# https://de.wikipedia.org/w/index.php?title=M%C3%BCnchen_Hauptbahnhof&diff=prev&oldid=239452513
	['no false positive',
	 'https://presseservice.pressrelations.de/standard/result_main.cfm?pfach=1&n_firmanr_=101744&sektor=pm&detail=1&r=478290&sid=&aktion=jour_pm&quelle=0&query=%22besonders%20stark%20nachgefragten%22',
	 'https://presseservice.pressrelations.de/standard/result_main.cfm?pfach=1&n_firmanr_=101744&sektor=pm&detail=1&r=478290&sid=&aktion=jour_pm&quelle=0&query=%22besonders%20stark%20nachgefragten%22',
  ],
	['aid param',
	 'http://www.historische-wertpapiere.de/de/HSK-Auktion-XXXI/?AID=86087&AKTIE=Deutsch-Schweizerische+Verwaltungsbank+AG',
	 'http://www.historische-wertpapiere.de/de/HSK-Auktion-XXXI/?AID=86087&AKTIE=Deutsch-Schweizerische+Verwaltungsbank+AG',
  ],
	# https://de.wikipedia.org/w/index.php?title=Kino_in_Hamburg&diff=241939501&oldid=241938566
	['keep ds_id',
	 'https://www.filmmuseum-hamburg.de/kinos/kino-datenbank/kinos-von-a-z.html?ds_id=511',
	 'https://www.filmmuseum-hamburg.de/kinos/kino-datenbank/kinos-von-a-z.html?ds_id=511',
  ],
	# https://de.wikipedia.org/w/index.php?title=Liste_der_Fu%C3%9Fballspielerinnen_mit_mindestens_100_L%C3%A4nderspielen&diff=prev&oldid=253019495
	['keep data1',
	 'https://www.thecfa.cn/src/PlayerBio/WomanPlayerBio.html?data1=420154',
	 'https://www.thecfa.cn/src/PlayerBio/WomanPlayerBio.html?data1=420154',
  ],
];

for my $e(@$test_data){
	my $to = $cb->delete_tracking_params_form_url($e->[1]);
	is($to, $e->[2], $e->[0]);
	++$test_counter;
}
done_testing($test_counter);

