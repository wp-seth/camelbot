#/usr/bin/perl
use strict;
use warnings;
use Test::More;# tests => 2;
use FindBin;
use lib "$FindBin::Bin/../lib";
use CamelBot;
use CamelBotRC;
use Data::Dumper; # for debugging only

my $test_counter = 0;
my $cb = new_ok('CamelBot' => [{
	'ask_user'      => 0,
	'mw_username'   => 'CamelBot', 
	'simulation'    => 1,
	'showdiff'      => 0,
	'verbosity'     => 0,
	'db_access'     => 0,
	'cliparams'     => {},
}]);
++$test_counter;
$cb->{'verbosity'} = -1;
my $cb_rc = new_ok('CamelBotRC' => [$cb, {'skip_maintenance' => 1}]);
++$test_counter;
can_ok($cb_rc, 'delete_redundant_pages_from_array');
++$test_counter;
my $timestamp_before = '1999-01-01 00:00';
my $timestamp1 = '2000-01-01 00:00';
my $timestamp2 = '2000-01-01 00:00';
my $timestamp_later = '2000-01-10 00:00';
my $test_data = [
	{ 'name' => 'undef',
		'in' => [undef, undef, undef],
		'mod' => [undef, undef, undef],
	},
	{ 'name' => 'empty',
		'in' => [[], [], \$timestamp1],
		'mod' => [[], [], \$timestamp1],
	},
	{ 'name' => 'single new',
		'in' => [
			[{'page_with_ns' => 'moep', 'timestamp' => '2000-01-10 00:00'}], 
			[], 
			\$timestamp_before],
		'mod' => [
			[{'page_with_ns' => 'moep', 'timestamp' => '2000-01-10 00:00'}], 
			[{'page_with_ns' => 'moep', 'timestamp' => '2000-01-10 00:00'}],
			\$timestamp_later],
	},
	{ 'name' => 'single on time; no time change',
		'in' => [
			[{'page_with_ns' => 'moep', 'timestamp' => '2000-01-01 00:00'}], 
			[{'page_with_ns' => 'moep', 'timestamp' => '2000-01-01 00:00'}], 
			\$timestamp1],
		'mod' => [
			[], 
			[{'page_with_ns' => 'moep', 'timestamp' => '2000-01-01 00:00'}],
			\$timestamp2],
	},
	{ 'name' => 'single2',
		'in' => [
			[{'page_with_ns' => 'moep', 'timestamp' => '2000-01-10 00:00'}], 
			[{'page_with_ns' => 'foo', 'timestamp' => '2000-01-01 00:00'}], 
			\$timestamp_before],
		'mod' => [
			[{'page_with_ns' => 'moep', 'timestamp' => '2000-01-10 00:00'}], 
			[{'page_with_ns' => 'moep', 'timestamp' => '2000-01-10 00:00'}],
			\$timestamp_later],
	},
	{ 'name' => 'full',
		'in' => [
			[
				{'page_with_ns' => 'moep', 'timestamp' => '2000-01-01 00:00'},
				{'page_with_ns' => 'foo', 'timestamp' => '2000-01-01 00:00'},
				{'page_with_ns' => 'quux', 'timestamp' => '2000-01-01 00:00'},
				{'page_with_ns' => 'bar', 'timestamp' => '2000-01-07 00:00'},
				{'page_with_ns' => 'baz', 'timestamp' => '2000-01-10 00:00'},
				{'page_with_ns' => 'moep', 'timestamp' => '2000-01-10 00:00'},
			], 
			[
				{'page_with_ns' => 'moep', 'timestamp' => '2000-01-01 00:00'},
				{'page_with_ns' => 'foo', 'timestamp' => '2000-01-01 00:00'},
			], 
			\$timestamp1],
		'mod' => [
			[
				{'page_with_ns' => 'quux', 'timestamp' => '2000-01-01 00:00'},
				{'page_with_ns' => 'bar', 'timestamp' => '2000-01-07 00:00'},
				{'page_with_ns' => 'baz', 'timestamp' => '2000-01-10 00:00'},
				{'page_with_ns' => 'moep', 'timestamp' => '2000-01-10 00:00'},
			], 
			[
				{'page_with_ns' => 'moep', 'timestamp' => '2000-01-10 00:00'},
				{'page_with_ns' => 'baz', 'timestamp' => '2000-01-10 00:00'},
			], 
			\$timestamp_later],
	},
];

for my $e(@$test_data){
	my $bak = $e->{'in'}[2];
	$bak = $$bak if defined $bak;
	$cb_rc->delete_redundant_pages_from_array(@{$e->{'in'}});
	is_deeply($e->{'in'}, $e->{'mod'}, $e->{'name'});
	++$test_counter;
	${$e->{'in'}[2]} = $bak;
}
done_testing($test_counter);



