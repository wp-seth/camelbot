#/usr/bin/perl
use strict;
use warnings;
use utf8;
use open ':std', ':encoding(utf8)'; # must be loaded before Test::More
use Test::More;# tests => 2;
use File::Slurp qw/slurp/;
use FindBin;
use lib "$FindBin::Bin/../lib";
use CamelBot;
use Data::Dumper; # for debugging only

my $test_counter = 0;
my $cb = new_ok('CamelBot' => [{
	'ask_user'      => 0,
	'mw_username'   => 'CamelBot',
	'simulation'    => 1,
	'showdiff'      => 0,
	'verbosity'     => 0,
	'db_access'     => 0,
	'cliparams'     => {},
}]);
++$test_counter;
can_ok($cb, 'createMWBot');
++$test_counter;
$cb->createMWBot();
$cb->{'offline'} = 1;

can_ok($cb, 'get_usernames_from_signatures');
++$test_counter;

# test data
my $test_data = [
	['undef', undef, undef],
	['empty string', '', []],
	['ip address', '== [https://de.wikipedia.org/w/index.php?title=Marek_Ulrich&diff=review Marek Ulrich] ==' . "\n\n" . '{{Sichten|Marek Ulrich}} --[[Spezial:Beiträge/2A00:DEAD:BEAF|2A00:DEAD:BEAF]] 08:37, 9. Jul. 2023 (CEST)', 
		['2A00:DEAD:BEAF']],
	['logged-in user', '== [https://de.wikipedia.org/w/index.php?title=Fortnite&diff=review Fortnite] ==' . "\n\n" . '{{Sichten|Fortnite}} --[[Benutzer:Example User|Herbert]] ([[Benutzer Diskussion:Example User|Diskussion]]) 01:06, 9. Jul. 2023 (CEST)', 
		['Example User']],
];

for my $e(@$test_data){
	my $to = $cb->get_usernames_from_signatures($e->[1]);
	is_deeply($to, $e->[2], $e->[0]);
	++$test_counter;
}
done_testing($test_counter);

