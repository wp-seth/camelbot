#/usr/bin/perl
use strict;
use warnings;
#use utf8;
#use open ':std', ':encoding(utf8)'; # must be loaded before Test::More
use Test::More;# tests => 2;
use FindBin;
use lib "$FindBin::Bin/../lib";
use CamelBot;
use Data::Dumper; # for debugging only

my $test_counter = 0;
my $cb = new_ok('CamelBot' => [{
	'ask_user'      => 0,
	'mw_username'   => 'CamelBot', 
	'simulation'    => 1,
	'showdiff'      => 0,
	'verbosity'     => 0,
	'db_access'     => 0,
	'cliparams'     => {},
}]);
++$test_counter;
can_ok($cb, 'createMWBot');
++$test_counter;
$cb->createMWBot();
$cb->{'offline'} = 1;
$cb->{'verbosity'} = -1;

can_ok($cb, 'get_diff');
++$test_counter;
my $test_data = [
	{ 'name' => 'undef',
		'in' => [undef, undef],
		'out' => undef,
	},
	{ 'name' => 'wrong param: string instead of hash ref',
		'in' => ['', undef],
		'out' => undef,
	},
	{ 'name' => 'empty hash',
		'in' => [{}, undef],
		'out' => undef,
	},
	{ 'name' => 'page by name',
		'in' => [{'revid'=>'137582010'}, undef],
		'out' => {
			'tocomment' => '/* tools */ einies altes entfernt',
			'diff' => [{
					'diff-deletedline' => [{
							'text' => '* firefox addons: [https://addons.mozilla.org/de/firefox/addon/',
							'type' => 'equal'
						},{
							'text' => '4125',
							'type' => 'del'
						},{
							'type' => 'equal',
							'text' => ' it\'s all text!]'
						}],
					'diff-addedline' => [{
							'text' => '* firefox addons: [https://addons.mozilla.org/de/firefox/addon/',
							'type' => 'equal'
						},{
							'text' => 'its-all-text/',
							'type' => 'ins'
						},{
							'text' => ' it\'s all text!]',
							'type' => 'equal'
						}]
				},{
					'diff-deletedline' => [{
							'text' => '* [http://tools.wmflabs.org/searchsbl find sbl-entries]',
							'type' => 'equal'
						},{
							'text' => ' <small>[http://toolserver.org/~seth/grep_regexp_from_url.cgi alternativ-link]</small>',
							'type' => 'del'
						}],
					'diff-addedline' => [{
							'text' => '* [http://tools.wmflabs.org/searchsbl find sbl-entries]',
							'type' => 'equal'
						}]
				},{
					'diff-addedline' => [{
							'type' => 'equal',
							'text' => '* [http://tools.wmflabs.org/url-converter converter of ugly google-urls]'
						}],
					'diff-deletedline' => [{
							'type' => 'equal',
							'text' => '* [http://tools.wmflabs.org/url-converter converter of ugly google-urls]'
						},{
							'type' => 'del',
							'text' => ' <small>[http://toolserver.org/~seth/google_url_converter.cgi alternativ-link]</small>'
						}]
				},{
					'diff-deletedline' => [],
					'diff-addedline' => [{
							'type' => 'ins',
							'text' => '* https://tools.wmflabs.org/intersect-contribs/'
						}]
				},{
					'diff-deletedline' => [{
							'text' => '* http://toolserver.org/~pietrodn/intersectContribs.php',
							'type' => 'del'
						}],
					'diff-addedline' => []
				}
			],
			'fromrevid' => '125887190',
			'torevid' => '137582010',
			'totimestamp' => '2015-01-08T22:11:48Z',
			'totitle' => 'Benutzer:Lustiger seth',
			'touser' => 'Lustiger seth',
		},
	},
];

if($cb->login()){
	for my $e(@$test_data){
		my $res = $cb->get_diff(@{$e->{'in'}});
		is_deeply($res, $e->{'out'}, $e->{'name'});
		++$test_counter;
	}
}else{
	warn "camelbot needs to login for this test";
}
done_testing($test_counter);

