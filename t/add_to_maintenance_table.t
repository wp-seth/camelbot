#/usr/bin/perl
use strict;
use warnings;
#use utf8; # ü
#use open ':std', ':encoding(utf8)'; # must be loaded before Test::More
use Test::More;# tests => 2;
use FindBin;
use lib "$FindBin::Bin/../lib";
use CamelBot;
use Data::Dumper; # for debugging only

my $test_counter = 0;
my $cb = new_ok('CamelBot' => [{
	'ask_user'      => 0,
	'mw_username'   => 'CamelBot',
	'simulation'    => 1,
	'showdiff'      => 0,
	'verbosity'     => 0,
	'db_access'     => 0,
	'cliparams'     => {},
}]);
++$test_counter;
can_ok($cb, 'createMWBot');
++$test_counter;
$cb->createMWBot();
$cb->{'offline'} = 1;
$cb->{'verbosity'} = -1;
$cb->{'namespaces'} = {6 => 'Datei'};
$cb->{'namespaces_canonical'} = {6 => 'File'};
$cb->{'namespacesaliaes'} = {'Image' => 6};

can_ok($cb, 'add_to_maintenance_table');
++$test_counter;
my $test_data = [
	{ 'name' => 'all undef',
		'in' => [undef, undef, undef],
		'out' => undef,
		'out_table' => undef,
	},
	{ 'name' => 'table undef',
		'in' => [undef, [], []],
		'out' => undef,
		'out_table' => undef,
	},
	{ 'name' => 'empty table, no new entries',
		'in' => [{'header' => ['x', 'y'], 'body' => []}, [], []],
		'out' => [],
		'out_table' => {'header' => ['x', 'y'], 'body' => []},
	},
	{ 'name' => 'empty table, one new entry',
		'in' => [
			{'header' => ['x', 'y'], 'body' => []},
			[{'page_with_ns' => 'moep'}],
			[]],
		'out' => ['[[moep]]'],
		'out_table' => {'header' => ['x', 'y'], 'body' => [
				['[[moep]]', $cb->get_date_iso()],
			]},
	},
	{ 'name' => 'empty table, one new file as entry',
		'in' => [
			{'header' => ['x', 'y'], 'body' => []},
			[{'page_with_ns' => 'File:moep'}],
			[]],
		'out' => ['[[:File:moep]]'],
		'out_table' => {'header' => ['x', 'y'], 'body' => [
				['[[:File:moep]]', $cb->get_date_iso()],
			]},
	},
	{ 'name' => 'empty table, two new entries',
		'in' => [
			{'header' => ['x', 'y'], 'body' => []},
			[{'page_with_ns' => 'moep'}, {'page_with_ns' => 'moep2'}],
			[]],
		'out' => ['[[moep]]', '[[moep2]]'],
		'out_table' => {'header' => ['x', 'y'], 'body' => [
				['[[moep]]', $cb->get_date_iso()],
				['[[moep2]]', $cb->get_date_iso()],
			]},
	},
	{ 'name' => 'non-empty table, two new entries',
		'in' => [
			{'header' => ['1', '2'], 'body' => [
					['[[old]]', '123'],
				]},
			[{'page_with_ns' => 'moep'}, {'page_with_ns' => 'moep2'}],
			[]],
		'out' => ['[[moep]]', '[[moep2]]'],
		'out_table' => {'header' => ['1', '2'], 'body' => [
				['[[old]]', '123'],
				['[[moep]]', $cb->get_date_iso()],
				['[[moep2]]', $cb->get_date_iso()],
			]},
	},
];

for my $e(@$test_data){
	my $to = $cb->add_to_maintenance_table(
		$e->{'in'}[0], $e->{'in'}[1], $e->{'in'}[2]);
	is_deeply($to, $e->{'out'}, $e->{'name'});
	++$test_counter;
	is_deeply($e->{'in'}[0], $e->{'out_table'}, $e->{'name'});
	++$test_counter;
}
done_testing($test_counter);

