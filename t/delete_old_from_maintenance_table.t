#/usr/bin/perl
use strict;
use warnings;
#use utf8; # ü
#use open ':std', ':encoding(utf8)'; # must be loaded before Test::More
use Test::More;# tests => 2;
use Time::Local; # array to unix timestamp
use FindBin;
use lib "$FindBin::Bin/../lib";
use CamelBot;
use Data::Dumper; # for debugging only

my $test_counter = 0;
my $cb = new_ok('CamelBot' => [{
	'ask_user'      => 0,
	'mw_username'   => 'CamelBot', 
	'simulation'    => 1,
	'showdiff'      => 0,
	'verbosity'     => 0,
	'db_access'     => 0,
	'cliparams'     => {},
}]);
++$test_counter;
can_ok($cb, 'createMWBot');
++$test_counter;
$cb->createMWBot();
$cb->{'offline'} = 1;
$cb->{'verbosity'} = -1;

can_ok($cb, 'delete_old_from_maintenance_table');
++$test_counter;
my $test_data = [
	{ 'name' => 'undef table',
		'in' => [
			undef, 
			undef,
		],
		'out' => undef,
		'out_table' => undef,
	},
	{ 'name' => 'undef days',
		'in' => [
			[], 
			undef,
		],
		'out' => 0,
		'out_table' => [],
	},
	{ 'name' => 'empty table',
		'in' => [
			[], 
			5,
		],
		'out' => 0,
		'out_table' => [],
	},
	{ 'name' => 'one new entry',
		'in' => [
			[['no matter', 'still no matter', '2020-06-28']], 
			5,
			Time::Local::timegm(0, 0, 0, 29, 6 - 1, 2020),
		],
		'out' => 0,
		'out_table' => [['no matter', 'still no matter', '2020-06-28']],
	},
	{ 'name' => 'one old entry, and no others',
		'in' => [
			[['no matter', 'still no matter', '2020-06-28']], 
			5,
			Time::Local::timegm(0, 0, 0, 28, 7 - 1, 2020),
		],
		'out' => 1,
		'out_table' => [],
	},
	{ 'name' => 'two new entries',
		'in' => [[
				['no matter', 'still no matter', '2020-06-28'],
				['no matter', 'still no matter', '2020-06-27'],
			], 
			5,
			Time::Local::timegm(0, 0, 0, 29, 6 - 1, 2020),
		],
		'out' => 0,
		'out_table' => [
				['no matter', 'still no matter', '2020-06-28'],
				['no matter', 'still no matter', '2020-06-27'],
			], 
	},
	{ 'name' => 'one old entry, and one new entry',
		'in' => [[
				['no matter', 'still no matter', '2020-06-28'], 
				['no matter', 'still no matter', '2020-07-28'],
			], 
			5,
			Time::Local::timegm(0, 0, 0, 28, 7 - 1, 2020),
		],
		'out' => 1,
		'out_table' => [
				['no matter', 'still no matter', '2020-07-28'],
			], 
	},
];

for my $e(@$test_data){
	my $out = $cb->delete_old_from_maintenance_table(
		$e->{'in'}[0], $e->{'in'}[1], $e->{'in'}[2]);
	is($out, $e->{'out'}, $e->{'name'} . ': number of deleted entries');
	++$test_counter;
	is_deeply($e->{'in'}[0], $e->{'out_table'}, $e->{'name'} . ': table change');
	++$test_counter;
}
done_testing($test_counter);

