#/usr/bin/perl
use strict;
use warnings;
use utf8; # ü
use open ':std', ':encoding(utf8)'; # must be loaded before Test::More
use Test::More;# tests => 2;
use FindBin;
use lib "$FindBin::Bin/../lib";
use CamelBot;
use Data::Dumper; # for debugging only

my $test_counter = 0;
my $cb = new_ok('CamelBot' => [{
	'ask_user'      => 0,
	'mw_username'   => 'CamelBot',
	'simulation'    => 1,
	'showdiff'      => 0,
	'verbosity'     => 0,
	'db_access'     => 0,
	'cliparams'     => {},
}]);
++$test_counter;
can_ok($cb, 'createMWBot');
++$test_counter;
$cb->createMWBot();
$cb->{'offline'} = 1;
$cb->{'verbosity'} = -1;

can_ok($cb, 'decompose_url');
++$test_counter;
can_ok($cb, 'compose_url');
++$test_counter;
my $test_data = [
	[undef, undef, 'undef'],
	['', undef, 'empty string'],
	['x', undef, 'no url'],
	['http://example.org', {
			'protocol' => 'http:',
			'credentials' => '',
			'port' => '',
			'host' => 'example.org',
			'path' => '',
			'query' => [],
			'fragment' => '',
		}, 'simplest url'
	],
	['https://example.org?foo', {
			'protocol' => 'https:',
			'credentials' => '',
			'port' => '',
			'host' => 'example.org',
			'path' => '',
			'query' => ['foo'],
			'fragment' => '',
		}, 'primitive GET param'
	],
	['https://example.org/#wtf?quuux', {
			'protocol' => 'https:',
			'credentials' => '',
			'port' => '',
			'host' => 'example.org',
			'path' => '/',
			'query' => [],
			'fragment' => '#wtf?quuux',
		}, 'fragment'
	],
	['https://example.org#foo=bar?baz=foo', {
			'protocol' => 'https:',
			'credentials' => '',
			'port' => '',
			'host' => 'example.org',
			'path' => '',
			'query' => [],
			'fragment' => '#foo=bar?baz=foo',
		}, 'fragment that looks like GET param'
	],
	['https://example.org?lala=foo&foo=bar#bar', {
			'protocol' => 'https:',
			'credentials' => '',
			'port' => '',
			'host' => 'example.org',
			'path' => '',
			'query' => ['lala=foo', 'foo=bar'],
			'fragment' => '#bar',
		}, 'GET param and fragment'
	],
	['https://example.org?foo=bar&baz=quux#wtf?quuux', {
			'protocol' => 'https:',
			'credentials' => '',
			'port' => '',
			'host' => 'example.org',
			'path' => '',
			'query' => ['foo=bar', 'baz=quux'],
			'fragment' => '#wtf?quuux',
		}, 'GET param and fragment that looks like GET param'
	],
	['http://example.org/&#x70;&#97;th#foo&amp;bar', {
			'protocol' => 'http:',
			'credentials' => '',
			'port' => '',
			'host' => 'example.org',
			'path' => '/&#x70;&#97;th',
			'query' => [],
			'fragment' => '#foo&amp;bar',
		}, 'ugly encoding with html entities'
	],
	['http://example.org/&#x70;&#97;th&#x23;foo&amp;bar', {
			'protocol' => 'http:',
			'credentials' => '',
			'port' => '',
			'host' => 'example.org',
			'path' => '/&#x70;&#97;th',
			'query' => [],
			'fragment' => '&#x23;foo&amp;bar',
		}, 'even uglier encoding with html entities'
	],
	['https://www.ww2.dk/Airfields%20-%20Germany%20&#x5B;1937%20Borders&#x5D;.pdf', {
			'protocol' => 'https:',
			'credentials' => '',
			'port' => '',
			'host' => 'www.ww2.dk',
			'path' => '/Airfields%20-%20Germany%20&#x5B;1937%20Borders&#x5D;.pdf',
			'query' => [],
			'fragment' => '',
		}, 'encoding of brackets with html entities'
	],
];

for my $e(@$test_data){
	my $to = $cb->decompose_url($e->[0]);
	is_deeply($to, $e->[1], $e->[2]);
	++$test_counter;
	if(defined $to){
		my $from = $cb->compose_url($to);
		is($from, $e->[0], "reverse: $e->[2]");
		++$test_counter;
	}
}
done_testing($test_counter);

