#/usr/bin/perl
use strict;
use warnings;
use utf8;
use open ':std', ':encoding(utf8)'; # must be loaded before Test::More
use Test::More;# tests => 2;
use File::Slurp qw/slurp/;
use FindBin;
use lib "$FindBin::Bin/../lib";
use CamelBot;
use Data::Dumper; # for debugging only

my $test_counter = 0;
my $cb = new_ok('CamelBot' => [{
	'ask_user'      => 0,
	'mw_username'   => 'CamelBot',
	'simulation'    => 1,
	'showdiff'      => 0,
	'verbosity'     => 0,
	'db_access'     => 0,
	'cliparams'     => {},
}]);
++$test_counter;
can_ok($cb, 'createMWBot');
++$test_counter;
$cb->createMWBot();
$cb->{'offline'} = 1;

can_ok($cb, 'newest_post_info');
++$test_counter;

# test data
my $test_data = [
	['undef', undef, undef],
	['empty string', '', {
		'date' => 0,
		'author' => undef,
		}],
	['talk page',
		scalar(slurp('t/test_data/talk_page.txt', { binmode => ':encoding(UTF-8)'})),	{
			'date' => 1699314480,
			'author' => 'Lußtigär Säht',
			'thread' => 'Spam-Blacklist: example.org',
		}
	],
];

for my $e(@$test_data){
	my $to = $cb->newest_post_info($e->[1]);
	is_deeply($to, $e->[2], $e->[0]);
	++$test_counter;
}
done_testing($test_counter);

