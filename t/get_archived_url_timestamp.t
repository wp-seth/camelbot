#/usr/bin/perl
use strict;
use warnings;
use utf8;
use open ':std', ':encoding(utf8)'; # must be loaded before Test::More
use Test::More;# tests => 2;
use File::Slurp qw/slurp/;
use FindBin;
use lib "$FindBin::Bin/../lib";
use CamelBot;
use Data::Dumper; # for debugging only

my $test_counter = 0;
my $cb = new_ok('CamelBot' => [{
	'ask_user'      => 0,
	'mw_username'   => 'CamelBot',
	'simulation'    => 1,
	'showdiff'      => 0,
	'verbosity'     => 0,
	'db_access'     => 0,
	'cliparams'     => {},
}]);
++$test_counter;
can_ok($cb, 'createMWBot');
++$test_counter;
$cb->createMWBot();
$cb->{'offline'} = undef; # this test must be done online

can_ok($cb, 'get_archived_url_timestamp');
++$test_counter;

# test data
my $test_data = [
	['undef', undef, {
			'start_time' => undef,
			'end_time' => undef,
			'preferred_max_time' => undef,
			'retries' => undef,
			'delay' => undef,
		}, undef],
	['empty string', '', {
			'start_time' => undef,
			'end_time' => undef,
			'preferred_max_time' => undef,
			'retries' => undef,
			'delay' => undef,
		}, -1],
	['wiki page start/end',
		'https://de.wikipedia.org/wiki/Wikipedia', {
			'start_time' => '20160101',
			'end_time' => '20200101',
			'preferred_max_time' => undef,
			'retries' => 4,
			'delay' => 20,
		}, '20191222160850'],
	['wiki page full constraints',
		'https://de.wikipedia.org/wiki/Wikipedia', {
			'start_time' => '20160101',
			'end_time' => '20200101',
			'preferred_max_time' => '20181231',
			'retries' => 4,
			'delay' => 20,
		}, '20181219002929'],
];

if($cb->login()){
	for my $e(@$test_data){
		my $to = $cb->get_archived_url_timestamp($e->[1], $e->[2]);
		is($to, $e->[3], $e->[0]);
		++$test_counter;
	}
}else{
	warn "camelbot needs to login for this test";
}
done_testing($test_counter);
