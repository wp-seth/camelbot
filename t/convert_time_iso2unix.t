#/usr/bin/perl
use strict;
use warnings;
#use utf8; # ü
#use open ':std', ':encoding(utf8)'; # must be loaded before Test::More
use Test::More;# tests => 2;
use FindBin;
use lib "$FindBin::Bin/../lib";
use CamelBot;
use Data::Dumper; # for debugging only

my $test_counter = 0;
my $cb = new_ok('CamelBot' => [{
	'ask_user'      => 0,
	'mw_username'   => 'CamelBot', 
	'simulation'    => 1,
	'showdiff'      => 0,
	'verbosity'     => 0,
	'db_access'     => 0,
	'cliparams'     => {},
}]);
++$test_counter;
can_ok($cb, 'createMWBot');
++$test_counter;
$cb->createMWBot();
$cb->{'offline'} = 1;
$cb->{'verbosity'} = -1;

can_ok($cb, 'convert_time_iso2unix');
++$test_counter;
my $test_data = [
	{ 'name' => 'undef',
		'in' => undef,
		'out' => undef,
	},
	{ 'name' => 'empty string',
		'in' => '',
		'out' => undef,
	},
	{ 'name' => 'zero',
		'in' => 0,
		'out' => undef,
	},
	{ 'name' => 'the begin: 1970, sep',
		'in' => '1970-01-01 00:00:00',
		'out' => 0,
	},
	{ 'name' => 'year 2000, sep',
		'in' => '2000-01-01 00:00:00',
		'out' => '946684800',
	},
	{ 'name' => 'second before year 2000, sep',
		'in' => '1999-12-31 23:59:59',
		'out' => '946684799',
	},
	{ 'name' => 'the begin: 1970, Tsep',
		'in' => '1970-01-01T00:00:00',
		'out' => 0,
	},
	{ 'name' => 'the begin: 1970, Tsep with Z',
		'in' => '1970-01-01T00:00:00Z',
		'out' => 0,
	},
	{ 'name' => 'year 2000, Tsep',
		'in' => '2000-01-01T00:00:00',
		'out' => 946684800,
	},
	{ 'name' => 'year 2000, Tsep with Z',
		'in' => '2000-01-01T00:00:00Z',
		'out' => 946684800,
	},
	{ 'name' => 'second before year 2000, Tsep->nosep',
		'in' => '1999-12-31T23:59:59',
		'out' => 946684799,
	},
	{ 'name' => 'the begin: 1970, nosep',
		'in' => '19700101000000',
		'out' => 0,
	},
	{ 'name' => 'year 2000, nosep-',
		'in' => '20000101000000',
		'out' => 946684800,
	},
	{ 'name' => 'second before year 2000, nosep',
		'in' => '19991231235959',
		'out' => 946684799,
	},
];

for my $e(@$test_data){
	my $to = $cb->convert_time_iso2unix($e->{'in'});
	is($to, $e->{'out'}, $e->{'name'});
	++$test_counter;
}
done_testing($test_counter);


