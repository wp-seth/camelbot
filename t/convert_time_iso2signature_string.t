#/usr/bin/perl
use strict;
use warnings;
#use utf8; # ü
#use open ':std', ':encoding(utf8)'; # must be loaded before Test::More
use Test::More;# tests => 2;
use FindBin;
use lib "$FindBin::Bin/../lib";
use CamelBot;
use Data::Dumper; # for debugging only

my $test_counter = 0;
my $cb = new_ok('CamelBot' => [{
	'ask_user'      => 0,
	'mw_username'   => 'CamelBot',
	'simulation'    => 1,
	'showdiff'      => 0,
	'verbosity'     => 0,
	'db_access'     => 0,
	'cliparams'     => {},
}]);
++$test_counter;
can_ok($cb, 'createMWBot');
++$test_counter;
$cb->createMWBot();
$cb->{'offline'} = 1;
$cb->{'verbosity'} = -1;

can_ok($cb, 'convert_time_iso2signature_string');
++$test_counter;
my $test_data = [
	{ 'name' => 'undef',
		'in' => [undef],
		'out' => undef,
	},
	{ 'name' => 'empty string',
		'in' => [''],
		'out' => undef,
	},
	{ 'name' => 'zero',
		'in' => [0],
		'out' => undef,
	},
	{ 'name' => 'sig date format',
		'in' => ['2021-02-23T08:34:00'],
		'out' => '08:34, 23 February 2021 (UTC)',
	},
	{ 'name' => 'sig date format (de)',
		'in' => ['2021-02-23T08:34:00', 'de'],
		'out' => '08:34, 23. Feb. 2021 (UTC)',
	},
	{ 'name' => 'sig date format (cet)',
		'in' => ['2021-02-23T08:34:00', undef, 'CET'],
		'out' => '09:34, 23 February 2021 (CET)',
	},
	{ 'name' => 'sig date format one-digit-day',
		'in' => ['2021-03-02T15:46:00'],
		'out' => '15:46, 2 March 2021 (UTC)',
	},
	{ 'name' => 'sig date format one-digit-day',
		'in' => ['2021-03-02T15:46:00'],
		'out' => '15:46, 2 March 2021 (UTC)',
	},

];

for my $e(@$test_data){
	my $to = $cb->convert_time_iso2signature_string(@{$e->{'in'}});
	is($to, $e->{'out'}, $e->{'name'});
	++$test_counter;
}
done_testing($test_counter);



