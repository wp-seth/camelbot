#/usr/bin/perl
use strict;
use warnings;
#use utf8;
#use open ':std', ':encoding(utf8)'; # must be loaded before Test::More
use Test::More;# tests => 2;
use FindBin;
use lib "$FindBin::Bin/../lib";
use CamelBot;
use Data::Dumper; # for debugging only

my $test_counter = 0;
my $cb = new_ok('CamelBot' => [{
	'ask_user'      => 0,
	'cliparams'     => {},
	'db_access'     => 0,
	'mw_username'   => 'CamelBot',
	'pw_ask'        => 0,
	'showdiff'      => 0,
	'simulation'    => 1,
	'verbosity'     => 0,
}]);
++$test_counter;
can_ok($cb, 'createMWBot');
++$test_counter;
$cb->createMWBot();
$cb->{'verbosity'} = -1;

can_ok($cb, 'is_allowed');
++$test_counter;
my $test_data = [
	{ 'name' => 'undef',
		'page' => undef,
		'text' => undef,
		'res' => 1,
	},{'name' => 'some text',
		'page' => undef,
		'text' => \'lala',
		'res' => 1,
	},{'name' => 'nobots template without param',
		'page' => undef,
		'text' => \'{{nobots}}',
		'res' => 0,
	},{'name' => 'nobots template all',
		'page' => undef,
		'text' => \'{{nobots|all}}',
		'res' => 0,
	},{'name' => 'nobots template with name at first place',
		'page' => undef,
		'text' => \'{{nobots|CamelBot}}',
		'res' => 0,
	},{'name' => 'nobots template with name at second/last place',
		'page' => undef,
		'text' => \'{{nobots|MoepBot,CamelBot}}',
		'res' => 0,
	},{'name' => 'nobots template with other names',
		'page' => undef,
		'text' => \'{{nobots|MoepBot,FooBot}}',
		'res' => 1,
	},{'name' => 'bots template with deny all',
		'page' => undef,
		'text' => \'{{bots|deny=all}}',
		'res' => 0,
	},{'name' => 'bots template with allow none',
		'page' => undef,
		'text' => \'{{bots|allow=none}}',
		'res' => 0,
	},{'name' => 'bots template with allow other than me',
		'page' => undef,
		'text' => \'{{bots|allow=MoepBot,FooBot}}',
		'res' => 0,
	},{'name' => 'bots template with allow me',
		'page' => undef,
		'text' => \'{{bots|allow=MoepBot,CamelBot}}',
		'res' => 1,
	},{'name' => 'bots template with deny',
		'page' => undef,
		'text' => \'{{bots|deny=MoepBot,CamelBot}}',
		'res' => 0,
	},{'name' => 'bots template with deny, but wo name',
		'page' => undef,
		'text' => \'{{bots|deny=MoepBot,FooBot}}',
		'res' => 1,
	},{'name' => 'inuse template wo param',
		'page' => undef,
		'text' => \'{{inuse}}',
		'res' => 0,
	},{'name' => 'inuse template',
		'page' => undef,
		'text' => \'{{inuse|any name}}',
		'res' => 0,
	},
];

for my $e(@$test_data){
	my $res = $cb->is_allowed($e->{'text'}, $e->{'page'});
	is_deeply($res, $e->{'res'}, $e->{'name'});
	++$test_counter;
}
done_testing($test_counter);

