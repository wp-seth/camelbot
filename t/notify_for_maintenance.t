#/usr/bin/perl
use strict;
use warnings;
#use utf8;
#use open ':std', ':encoding(utf8)'; # must be loaded before Test::More
use Test::More;# tests => 2;
use FindBin;
use lib "$FindBin::Bin/../lib";
use CamelBot;
use Data::Dumper; # for debugging only

my $test_counter = 0;
my $cb = new_ok('CamelBot' => [{
	'ask_user'      => 0,
	'cliparams'     => {},
	'db_access'     => 0,
	'mw_username'   => 'CamelBot',
	'pw_ask'        => 0,
	'showdiff'      => 0,
	'simulation'    => 1,
	'verbosity'     => 0,
}]);
++$test_counter;
can_ok($cb, 'createMWBot');
++$test_counter;
$cb->createMWBot();
$cb->{'verbosity'} = -1;

can_ok($cb, 'notify_for_maintenance');
++$test_counter;
my $test_data = [
	{ 'name' => 'undef',
		'rc_msg' => undef,
		'res' => undef,
	},
	{ 'name' => 'edit that triggered',
		'rc_msg' => {
			'diffbytes' => 166,
			'ns_id' => 0,
			'page' => 'Luckenwalde',
			'page_with_ns' => 'Luckenwalde',
			'rc_new_len' => 60795,
			'rc_old_len' => 60629,
			'rc_this_oldid' => 227145918,
			'rc_last_oldid' => 226552635,
			'summary' => 'moep',
			'timestamp' => '20221018150555',
			'timestamp_unix' => $cb->convert_time_iso2unix('20221018150555'),
			'user'           => 'Lustiger seth',
			},
		'res' => ['self-ref'],
	},
	# https://de.wikipedia.org/w/index.php?title=Irish_Pub&diff=prev&oldid=235599883
	# https://de.wikipedia.org/w/index.php?title=Benutzer_Diskussion:Aka&oldid=235939494#Hinweis_auf_Selbstreferenzierungen
	{ 'name' => 'edit that did not trigger',
		'rc_msg' => {
			'diffbytes' => 16,
			'ns_id' => 0,
			'page' => 'Irish Pub',
			'page_with_ns' => 'Irish Pub',
			'rc_new_len' => 9362,
			'rc_old_len' => 9378,
			'rc_this_oldid' => 235599883,
			'rc_last_oldid' => 230663800,
			'summary' => 'Tippfehler entfernt, ISBN-Format',
			'timestamp' => '20230719074009',
			'timestamp_unix' => $cb->convert_time_iso2unix('20230719074009'),
			'user'           => 'Aka',
			},
		'res' => [],
	},
	{ 'name' => 'edit that should not trigger self-ref1',
		'rc_msg' => {
			'diffbytes' => 24,
			'ns_id' => 0,
			'page' => 'Sergei Alexandrowitsch Sawjalow',
			'page_with_ns' => 'Sergei Alexandrowitsch Sawjalow',
			'rc_new_len' => 5461,
			'rc_old_len' => 5437,
			'rc_this_oldid' => 242966009,
			'rc_last_oldid' => 242960072,
			'summary' => 'moep',
			'timestamp' => '20240309T195447',
			'timestamp_unix' => $cb->convert_time_iso2unix('20240309T195447'),
			'user'           => 'Lustiger seth',
		},
		'res' => [],
	},
	{ 'name' => 'edit that should not trigger self-ref2',
		'rc_msg' => {
			'diffbytes' => -1,
			'ns_id' => 0,
			'page' => "Ji\x{0159}\x{ed} Horsk\x{fd}",
			'page_with_ns' => "Ji\x{0159}\x{ed} Horsk\x{fd}",
			'rc_new_len' => 3731,
			'rc_old_len' => 3732,
			'rc_this_oldid' => 242536485,
			'rc_last_oldid' => 242536309,
			'summary' => 'moep',
			'timestamp' => '20240225T010300',
			'timestamp_unix' => $cb->convert_time_iso2unix('20240225T010300'),
			'user'           => 'Lustiger seth',
		},
		'res' => [],
	},
	{ 'name' => 'edit that should not trigger self-ref3',
		'rc_msg' => {
			'diffbytes' => 3,
			'ns_id' => 0,
			'page' => 'Tiepolos Geburtshaus',
			'page_with_ns' => 'Tiepolos Geburtshaus',
			'rc_new_len' => 4489,
			'rc_old_len' => 4486,
			'rc_this_oldid' => 248476605,
			'rc_last_oldid' => 248473527,
			'summary' => 'moep',
			'timestamp' => '20240910T104300',
			'timestamp_unix' => $cb->convert_time_iso2unix('20240910T104300'),
			'user'           => 'Lustiger seth',
		},
		'res' => [],
	},
	{ 'name' => 'edit that should not trigger self-ref4',
		'rc_msg' => {
			'diffbytes' => -57,
			'ns_id' => 0,
			'page' => 'Luigia Polzelli',
			'page_with_ns' =>'Luigia Polzelli',
			'rc_new_len' => 9170,
			'rc_old_len' => 9227,
			'rc_this_oldid' => 244869035,
			'rc_last_oldid' => 244867231,
			'summary' => 'moep',
			'timestamp' => '20240511T110500',
			'timestamp_unix' => $cb->convert_time_iso2unix('20240511T110500'),
			'user'           => 'Lustiger seth',
		},
		'res' => [],
	},
	{ 'name' => 'newly created page without trigger',
		'rc_msg' => {
			'diffbytes' => 25,
			'ns_id' => 0,
			'page' => 'Merit_Order',
			'page_with_ns' => 'Merit Order',
			'rc_new_len' => 25,
			'rc_old_len' => 0,
			'rc_this_oldid' => 226693759,
			'rc_last_oldid' => 0,
			'summary' => 'moep',
			'timestamp' => '20221002142036',
			'timestamp_unix' => $cb->convert_time_iso2unix('20221002142036'),
			'user'           => 'Lustiger seth',
			},
		'res' => undef,
	},
	{ 'name' => 'newly created page with trigger',
		'rc_msg' => {
			'diffbytes' => 5604,
			'ns_id' => 0,
			'page' => 'Ljubow Mala',
			'page_with_ns' => 'Ljubow Mala',
			'rc_new_len' => 5604,
			'rc_old_len' => 0,
			'rc_this_oldid' => 220658474,
			'rc_last_oldid' => 0,
			'summary' => 'moep',
			'timestamp' => '20220228105514',
			'timestamp_unix' => $cb->convert_time_iso2unix('20220228105514'),
			'user'           => 'Lustiger seth',
			},
		'res' => ['warheroes.ru'],
	},
	{ 'name' => 'newly created page with trigger2',
		'rc_msg' => {
			'diffbytes' => 4957,
			'ns_id' => 0,
			'page' => 'Nikolai Timofejewitsch Antoschkin',
			'page_with_ns' => 'Nikolai Timofejewitsch Antoschkin',
			'rc_new_len' => 4957,
			'rc_old_len' => undef,
			'rc_this_oldid' => 248816998,
			'rc_last_oldid' => 0,
			'summary' => 'moep',
			'timestamp' => '20240922T211418',
			'timestamp_unix' => $cb->convert_time_iso2unix('20240922T211418'),
			'user'           => 'Lustiger seth',
			},
		'res' => ['warheroes.ru'],
	},
	{ 'name' => 'diff to deleted/hidden revision with trigger in current page',
		'rc_msg' => {
			'diffbytes' => 10,
			'ns_id' => 0,
			'page' => 'Ramsan Achmatowitsch Kadyrow',
			'page_with_ns' => 'Ramsan Achmatowitsch Kadyrow',
			'rc_new_len' => 100,
			'rc_old_len' => 90,
			'rc_this_oldid' => 227264165,
			'rc_last_oldid' => 227264146,
			'summary' => 'moep',
			'timestamp' => '20221022165850',
			'timestamp_unix' => $cb->convert_time_iso2unix('20221022165850'),
			'user'           => 'Lustiger seth',
			},
		'res' => undef,
	},
	{ 'name' => 'ignore normalization of links',
		'rc_msg' => {
			'diffbytes' => 8,
			'ns_id' => 0,
			'page' => 'Glocke von Chersonesos',
			'page_with_ns' => 'Glocke von Chersonesos',
			'rc_new_len' => 5577,
			'rc_old_len' => 5585,
			'rc_this_oldid' => 240531562,
			'rc_last_oldid' => 239368770,
			'summary' => 'moep',
			'timestamp' => '20231226T083801',
			'timestamp_unix' => $cb->convert_time_iso2unix('20231226T083801'),
			'user'           => 'Lustiger seth',
			},
		'res' => [],
	},
];

if($cb->login()){
	for my $e(@$test_data){
		my $res = $cb->notify_for_maintenance($e->{'rc_msg'});
		is_deeply($res, $e->{'res'}, $e->{'name'});
		++$test_counter;
	}
}else{
	warn "camelbot needs to login for this test";
}
done_testing($test_counter);

