#/usr/bin/perl
use strict;
use warnings;
#use utf8; # ü
use open ':std', ':encoding(utf8)'; # must be loaded before Test::More
use Test::More;# tests => 2;
use FindBin;
use lib "$FindBin::Bin/../lib";
use CamelBot;
use CamelBotRC;
use Data::Dumper; # for debugging only

my $test_counter = 0;
my $cb = new_ok('CamelBot' => [{
	'ask_user'      => 0,
	'mw_username'   => 'CamelBot',
	'simulation'    => 1,
	'showdiff'      => 0,
	'verbosity'     => 0,
	'db_access'     => 0,
	'cliparams'     => {},
}]);
++$test_counter;
can_ok($cb, 'createMWBot');
++$test_counter;
$cb->createMWBot();
$cb->{'offline'} = 1;
$cb->{'verbosity'} = -1;
$cb->{'mw_version'} = '1.31.0';
$cb->{'namespaces'} = {0 => '', 2 => 'Benutzer', 14 => 'Kategorie'};
$cb->{'namespacealiases'} = {'' => 0, 'Benutzer' => 2, 'Kategorie' => 14};
my $cb_rc = new_ok('CamelBotRC' => [$cb, {'skip_maintenance' => 1}]);
++$test_counter;

can_ok($cb_rc, 'get_rc');
++$test_counter;
my $test_data = [
	{ 'name' => 'simple example',
		'names' => [
			'timestamp',
			'user',
			'ns_id',
			'page',
			'summary',
			'rc_this_oldid',
			'rc_last_oldid',
			'rc_old_len',
			'rc_new_len',
			'rc_log_type',
			'move_target',
		],
		'rows' => [
			[
				'20221121183735',
				'Lustiger seth',
				0,
				'LNG_Endeavour',
				'Relevanz',
				228181847,
				0,
				undef,
				undef,
				'move',
				'a:3:{s:9:"4::target";s:20:"LNG Adventure-Klasse";s:10:"5::noredir";s:1:"0";s:17:"associated_rev_id";i:228181847;}'
			],[
				'20221121183735',
				'Lustiger seth',
				14,
				'Wikipedia:Weiterleitung_mit_Wikidata-Datenobjekt',
				'[[:LNG Endeavour]] zur Kategorie hinzugefuegt',
				228181848,
				0,
				undef,
				undef,
				undef,
				undef
			],[
				'20221121201019',
				'Lustiger seth',
				2,
				'Lustiger seth/Sudan-Airways-Flug_2241',
				'verschoben blabla',
				228184206,
				0,
				undef,
				undef,
				'move',
				'a:3:{s:9:"4::target";s:23:"Sudan-Airways-Flug 2241";s:10:"5::noredir";s:1:"0";s:17:"associated_rev_id";i:228184206;}'
			],
		],
		'result' => [
			{
				'timestamp' => '2022-11-21 18:37:35',
				'rc_last_oldid' => 0,
				'move_target' => 'LNG Adventure-Klasse',
				'diffbytes' => 0,
				'user' => 'Lustiger seth',
				'rc_this_oldid' => 228181847,
				'page' => 'LNG Endeavour',
				'page_with_ns' => 'LNG Endeavour',
				'summary' => 'Relevanz',
				'rc_new_len' => undef,
				'timestamp_unix' => 1669055855,
				'rc_log_type' => 'move',
				'rc_old_len' => undef,
				'ns_id' => 0
			},{
				'summary' => '[[:LNG Endeavour]] zur Kategorie hinzugefuegt',
				'page_with_ns' => 'Kategorie:Wikipedia:Weiterleitung mit Wikidata-Datenobjekt',
				'timestamp_unix' => 1669055855,
				'rc_new_len' => undef,
				'rc_log_type' => undef,
				'rc_old_len' => undef,
				'ns_id' => 14,
				'timestamp' => '2022-11-21 18:37:35',
				'move_target' => undef,
				'rc_last_oldid' => 0,
				'rc_this_oldid' => 228181848,
				'page' => 'Wikipedia:Weiterleitung mit Wikidata-Datenobjekt',
				'user' => 'Lustiger seth',
				'diffbytes' => 0
			},{
				'page_with_ns' => 'Benutzer:Lustiger seth/Sudan-Airways-Flug 2241',
				'diffbytes' => 0,
				'summary' => 'verschoben blabla',
				'timestamp' => '2022-11-21 20:10:19',
				'rc_log_type' => 'move',
				'rc_last_oldid' => 0,
				'move_target' => 'Sudan-Airways-Flug 2241',
				'timestamp_unix' => 1669061419,
				'rc_this_oldid' => 228184206,
				'rc_old_len' => undef,
				'user' => 'Lustiger seth',
				'rc_new_len' => undef,
				'ns_id' => 2,
				'page' => 'Lustiger seth/Sudan-Airways-Flug 2241'
			}
		],
	},
];

for my $e(@$test_data){
	$cb->{'offline_db_query_names'} = $e->{'names'};
	$cb->{'offline_db_query_rows'} = $e->{'rows'};
	my $out = $cb_rc->get_rc();
	is_deeply($out, $e->{'result'}, $e->{'name'});
	++$test_counter;
}
done_testing($test_counter);

