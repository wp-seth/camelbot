#/usr/bin/perl
use strict;
use warnings;
#use utf8; # ü
#use open ':std', ':encoding(utf8)'; # must be loaded before Test::More
use Test::More;# tests => 2;
use FindBin;
use lib "$FindBin::Bin/../lib";
use CamelBot;
use Data::Dumper; # for debugging only

my $test_counter = 0;
my $cb = new_ok('CamelBot' => [{
	'ask_user'      => 0,
	'mw_username'   => 'CamelBot',
	'simulation'    => 1,
	'showdiff'      => 0,
	'verbosity'     => 0,
	'db_access'     => 0,
	'cliparams'     => {},
}]);
++$test_counter;
can_ok($cb, 'createMWBot');
++$test_counter;
$cb->createMWBot();
$cb->{'offline'} = 1;
$cb->{'verbosity'} = -1;

can_ok($cb, 'is_page_in_maintenance_table');
++$test_counter;
my $test_data = [
	{ 'name' => 'empty/undef',
		'in' => [
			{},
			{},
			undef,
		],
		'out' => undef,
	},
	{ 'name' => 'empty',
		'in' => [
			{},
			{'body' => []},
			undef,
		],
		'out' => '',
	},
	{ 'name' => 'minimal true',
		'in' => [
			{'page_with_ns' => 'page title'},
			{'body' => [['[[page title]]']]},
			undef,
		],
		'out' => 1,
	},
	{ 'name' => 'minimal false',
		'in' => [
			{'page_with_ns' => 'page title'},
			{'body' => [['[[wrong page title]]']]},
			undef,
		],
		'out' => '',
	},
	{ 'name' => 'minimal false with second column',
		'in' => [
			{'page_with_ns' => 'page title'},
			{'body' => [['[[wrong page title]]', '2020-06-01']]},
			undef,
		],
		'out' => '',
	},
	{ 'name' => 'true with second row',
		'in' => [
			{'page_with_ns' => 'page title'},
			{'body' => [
					['[[wrong page title]]', '2020-06-01'],
					['[[page title]]', '2020-06-01']
				]},
			undef,
		],
		'out' => 1,
	},
	{ 'name' => 'false with second row',
		'in' => [
			{'page_with_ns' => 'page title'},
			{'body' => [
					['[[wrong page title]]', '2020-06-01'],
					['[[other page title]]', '2020-06-01']
				]},
			undef,
		],
		'out' => '',
	},
	{ 'name' => 'true with second row with colon',
		'in' => [
			{'page_with_ns' => 'page title'},
			{'body' => [
					['[[:wrong page title]]', '2020-06-01'],
					['[[:page title]]', '2020-06-01']
				]},
			undef,
		],
		'out' => 1,
	},
	{ 'name' => 'true with wrong additional col',
		'in' => [
			{'page_with_ns' => 'page title'},
			{'body' => [
					['[[wrong page title]]', '2020-06-01'],
					['[[page title]]', '2020-06-01']
				]},
			['category'],
		],
		'out' => '',
	},
	{ 'name' => 'page without additional col',
		'in' => [
			{'page_with_ns' => 'page title', 'category' => 'moep'},
			{'body' => [
					['[[wrong page title]]', '2020-06-01'],
					['[[page title]]', '2020-06-01']
				]},
			['category'],
		],
		'out' => '',
	},
	{ 'name' => 'page with additional col',
		'in' => [
			{'page_with_ns' => 'page title', 'category' => 'moep'},
			{'body' => [
					['[[wrong page title]]', 'lala', '2020-06-01'],
					['[[page title]]', 'moep', '2020-06-01']
				]},
			['category'],
		],
		'out' => 1,
	},
];

for my $e(@$test_data){
	my $to = $cb->is_page_in_maintenance_table(
		$e->{'in'}[0], $e->{'in'}[1], $e->{'in'}[2]);
	is($to, $e->{'out'}, $e->{'name'});
	++$test_counter;
}
done_testing($test_counter);

