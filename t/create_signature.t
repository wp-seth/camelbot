#/usr/bin/perl
use strict;
use warnings;
#use utf8; # ü
#use open ':std', ':encoding(utf8)'; # must be loaded before Test::More
use Test::More;# tests => 2;
use FindBin;
use lib "$FindBin::Bin/../lib";
use CamelBot;
use Data::Dumper; # for debugging only

my $test_counter = 0;
my $cb = new_ok('CamelBot' => [{
	'ask_user'      => 0,
	'mw_username'   => 'CamelBot',
	'simulation'    => 1,
	'showdiff'      => 0,
	'verbosity'     => 0,
	'db_access'     => 0,
	'cliparams'     => {},
}]);
++$test_counter;
can_ok($cb, 'createMWBot');
++$test_counter;
$cb->createMWBot();
$cb->{'offline'} = 1;
$cb->{'verbosity'} = -1;

can_ok($cb, 'create_signature');
++$test_counter;
my $test_data = [
	{ 'name' => 'undef',
		'in' => [undef, undef],
		'out' => undef,
	},
	{ 'name' => 'undef user',
		'in' => [undef, '2000-01-01T00:00:00'],
		'out' => undef,
	},
	{ 'name' => 'undef date',
		'in' => ['foo', undef],
		'out' => undef,
	},
	{ 'name' => 'wrong date',
		'in' => ['foo', 'lala'],
		'out' => undef,
	},
	{ 'name' => 'sig date format',
		'in' => ['lustiger seth', '2021-03-23T08:34:00'],
		'out' => '-- [[user:lustiger seth|lustiger seth]] ([[user talk:lustiger seth|talk]]) 08:34, 23 March 2021 (UTC)',
	},
	{ 'name' => 'sig date format one-digit-day',
		'in' => ['lustiger seth', '2021-03-02T15:46:00'],
		'out' => '-- [[user:lustiger seth|lustiger seth]] ([[user talk:lustiger seth|talk]]) 15:46, 2 March 2021 (UTC)',
	},

];

for my $e(@$test_data){
	my $to = $cb->create_signature($e->{'in'}[0], $e->{'in'}[1]);
	is($to, $e->{'out'}, $e->{'name'});
	++$test_counter;
}
done_testing($test_counter);


