#/usr/bin/perl
use strict;
use warnings;
use utf8;
use open ':std', ':encoding(utf8)'; # must be loaded before Test::More
use Test::More;# tests => 2;
use File::Slurp qw/slurp/;
use FindBin;
use lib "$FindBin::Bin/../lib";
use CamelBot;
use Data::Dumper; # for debugging only

my $test_counter = 0;
my $cb = new_ok('CamelBot' => [{
	'ask_user'      => 0,
	'mw_username'   => 'CamelBot',
	'simulation'    => 1,
	'showdiff'      => 0,
	'verbosity'     => 0,
	'db_access'     => 0,
	'cliparams'     => {},
}]);
++$test_counter;
can_ok($cb, 'createMWBot');
++$test_counter;
$cb->createMWBot();
$cb->{'offline'} = 1;

can_ok($cb, 'change_review_request');
++$test_counter;

# test data
my $test_data = [
	['undef', [undef, undef, undef], undef, undef],
	['empty string and undef', ['', undef, undef], '', undef],
	['empty string', ['', '', ''], '', 0],
	['correct', [
		'== [https://de.wikipedia.org/w/index.php?title=Liste_von_B%C3%A4nden_der_Sammlung_G%C3%B6schen&diff=review Liste von Bänden der Sammlung Göschen] ==

{{Sichten|Liste von Bänden der Sammlung Göschen}} --[[user:lustiger_seth|seth]] ([[user talk:lustiger_seth|Diskussion]]) 00:00, 1. Sep. 2023 (CEST)', 'Liste von Bänden der Sammlung Göschen', 'Sammlung Göschen'],
		'== [https://de.wikipedia.org/w/index.php?title=Sammlung_G%C3%B6schen&diff=review Sammlung Göschen] ==

{{Sichten|Sammlung Göschen}} --[[user:lustiger_seth|seth]] ([[user talk:lustiger_seth|Diskussion]]) 00:00, 1. Sep. 2023 (CEST)
:redirect "Liste von Bänden der Sammlung Göschen" ersetzt durch "Sammlung Göschen". -- ~~~~
', 3],
];

for my $e(@$test_data){
	my $textref = \$e->[1][0];
	my $to = $cb->change_review_request($textref, $e->[1][1], $e->[1][2]);
	is($to, $e->[3], $e->[0] . ' (return value)');
	++$test_counter;
	is($$textref, $e->[2], $e->[0] . ' (actual change)');
	++$test_counter;
}
done_testing($test_counter);
