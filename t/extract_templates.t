#/usr/bin/perl
use strict;
use warnings;
#use utf8; # ü
#use open ':std', ':encoding(utf8)'; # must be loaded before Test::More
use Test::More;# tests => 2;
use FindBin;
use lib "$FindBin::Bin/../lib";
use CamelBot;
use Data::Dumper; # for debugging only

my $test_counter = 0;
my $cb = new_ok('CamelBot' => [{
	'ask_user'      => 0,
	'mw_username'   => 'CamelBot', 
	'simulation'    => 1,
	'showdiff'      => 0,
	'verbosity'     => 0,
	'db_access'     => 0,
	'cliparams'     => {},
}]);
++$test_counter;
can_ok($cb, 'createMWBot');
++$test_counter;
$cb->createMWBot();
$cb->{'offline'} = 1;
$cb->{'verbosity'} = -1;

can_ok($cb, 'extract_templates');
++$test_counter;
my $test_data = [
	{ 'name' => 'undef',
		'in' => [undef, undef],
		'out' => undef,
	},
	{ 'name' => 'empty string',
		'in' => [\'', undef],
		'out' => [],
	},
	{ 'name' => 'simplest template',
		'in' => [\'{{!}}', undef],
		'out' => [{'name'=>'!', 'params'=> {}}],
	},
	{ 'name' => 'simple template, simple param',
		'in' => [\'{{moep|lala}}', undef],
		'out' => [{'name'=>'moep', 'params'=> {'1' => 'lala'}}],
	},
	{ 'name' => 'simple template, 2 simple params',
		'in' => [\'{{moep|lala|lolo}}', undef],
		'out' => [{'name'=>'moep', 'params'=> {'1' => 'lala', '2' => 'lolo'}}],
	},
	{ 'name' => 'simple template, key value param',
		'in' => [\'{{moep|foo=bar}}', undef],
		'out' => [{'name'=>'moep', 'params'=> {'foo' => 'bar'}}],
	},
	{ 'name' => 'simple template, key value params',
		'in' => [\'{{moep|foo=bar|baz=quux}}', undef],
		'out' => [{'name'=>'moep', 'params'=> {'foo' => 'bar', 'baz' => 'quux'}}],
	},
	{ 'name' => 'simple template, key value params, linebreaks and tabs',
		'in' => [\"{{moep\n\t|foo=bar\n\t|baz=quux\n}}", undef],
		'out' => [{'name'=>'moep', 'params'=> {'foo' => 'bar', 'baz' => 'quux'}}],
	},
	{ 'name' => 'simple template, key value params, linebreaks, tabs, and spaces',
		'in' => [\"{{moep\n\t| foo = bar\n\t| baz = quux\n}}", undef],
		'out' => [{'name'=>'moep', 'params'=> {'foo' => 'bar', 'baz' => 'quux'}}],
	},
	{ 'name' => 'simple template, key value params, linebreaks, tabs, and diffuse spaces',
		'in' => [\"{{moep | \n\t foo = bar  \n |\t  baz  =  quux  \n}}", undef],
		'out' => [{'name'=>'moep', 'params'=> {'foo' => 'bar', 'baz' => 'quux'}}],
	},
	{ 'name' => 'two templates',
		'in' => [\"bla {{moep|foo}} bla {{moep|bar}}", undef],
		'out' => [
			{'name'=>'moep', 'params'=> {'1' => 'foo'}},
			{'name'=>'moep', 'params'=> {'1' => 'bar'}},
		],
	},
	{ 'name' => 'a nested template',
		'in' => [\"bla {{moep|foo = {{moep|bar}} |baz={{mi}}}}", undef],
		'out' => [
			{'name'=>'moep', 'params'=> {'foo' => '{{moep|bar}}', 'baz' => '{{mi}}'}},
		],
	},
	{ 'name' => 'a nested template with filter',
		'in' => [\"bla {{moep|foo = {{moep|bar}} |baz={{mi}}}}", qr/mi/],
		'out' => [
			{'name'=>'mi', 'params'=> {}},
		],
	},
];

for my $e(@$test_data){
	my $to = $cb->extract_templates(@{$e->{'in'}});
	is_deeply($to, $e->{'out'}, $e->{'name'});
	++$test_counter;
}
done_testing($test_counter);

