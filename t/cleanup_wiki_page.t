#/usr/bin/perl
use strict;
use warnings;
use utf8;
use open ':std', ':encoding(utf8)'; # must be loaded before Test::More
use Test::More;# tests => 2;
use File::Slurp qw/slurp/;
use FindBin;
use lib "$FindBin::Bin/../lib";
use CamelBot;
use Data::Dumper; # for debugging only

my $test_counter = 0;
my $cb = new_ok('CamelBot' => [{
	'ask_user'      => 0,
	'mw_username'   => 'CamelBot',
	'simulation'    => 1,
	'showdiff'      => 0,
	'verbosity'     => 0,
	'db_access'     => 0,
	'cliparams'     => {},
}]);
++$test_counter;
can_ok($cb, 'createMWBot');
++$test_counter;
$cb->createMWBot();
$cb->init_re_url_tracking_filter(); # needed for tracking param removal
$cb->{'offline'} = 1;

can_ok($cb, 'cleanup_wiki_page');
++$test_counter;
# a mandatory replacement is needed for the optional replacements
my $mand_repl = ".]]. ";
my $mand_res  = ".]] ";

# test data
my $textrepls = [
	['double protocol',
		# [in, cbr_out // in, subtest_name // auto_generated, summary]
		['', '', 'empty string (no change)'],
		['moep', undef, 'normal text (no change)'],
		['http://www.example.org', undef, 'normal link (no change)'],
		['http://https://www.example.org', 'https://www.example.org'],
		['http://http://www.example.org', 'http://www.example.org'],
		['http://http//www.example.org', 'http://www.example.org'],
		['http://http//www.example.org.', 'http://www.example.org.'],
		['[http://http//www.example.org. lala]', '[http://www.example.org. lala]'],
		['[//https://www.example.org', '[https://www.example.org'],
	],['font tag',
		# https://de.wikipedia.org/w/index.php?title=Grevel&diff=prev&oldid=214604163
		# https://de.wikipedia.org/w/index.php?title=Marie-Antoinette_von_%C3%96sterreich-Lothringen&diff=prev&oldid=209383896
		['Die Faulheit und insbesondere die Leichtfertigkeit der Prinzessin mache es ihm schwer, sie zu unterrichten.<ref><font _mstmutation="1" _msthash="752895" _msttexthash="164788" _msthidden="1">Nancy Plain: </font>\'\'Louis XVI, Marie Antoinette, and the French Revolution.\'\'<font _mstmutation="1" _msthash="752896" _msttexthash="919295" _msthidden="1"> Marshall Cavendish, Singapur 2002, S. 11.</font></ref>',
		 'Die Faulheit und insbesondere die Leichtfertigkeit der Prinzessin mache es ihm schwer, sie zu unterrichten.<ref>Nancy Plain: \'\'Louis XVI, Marie Antoinette, and the French Revolution.\'\' Marshall Cavendish, Singapur 2002, S. 11.</ref>'],
		['Griëwel<ref>[https://www.lwl.org/komuna/pdf/Bd_15.pdf Schleef, Wilhelm: Dortmunder Wörterbuch. 1967. XXII, 298 S. Ln.]<font _mstmutation="1" _msthash="752895" _msttexthash="101023" _msthidden="1"> (PDF; 3,9MB)</font></ref>',
		 'Griëwel<ref>[https://www.lwl.org/komuna/pdf/Bd_15.pdf Schleef, Wilhelm: Dortmunder Wörterbuch. 1967. XXII, 298 S. Ln.] (PDF; 3,9MB)</ref>'],
		['<ref name="weissensteiner"><font _mstmutation="1" _msthash="1615406" _msttexthash="593957" _msthidden="1">Friedrich Weissensteiner: </font>\'\'Die Töchter Maria Theresias.\'\'<font _mstmutation="1" _msthash="1615407" _msttexthash="532077" _msthidden="1"> Kremayr & Scheriau, Wien 1994</font></ref>',
		 '<ref name="weissensteiner">Friedrich Weissensteiner: \'\'Die Töchter Maria Theresias.\'\' Kremayr & Scheriau, Wien 1994</ref>'],
		['bla.<ref>[[Michael Stegemann]]<font _mstmutation="1" _msthash="753805" _msttexthash="5278" _msthidden="1">: </font>\'\'Komponist. Reformator der alten Oper. Vor 300 Jahren wurde Christoph Willibald Gluck geboren\'\'<font _mstmutation="1" _msthash="753806" _msttexthash="35334" _msthidden="1">, in: </font>\'\'Deutschlandradio Kultur\'\'<font _mstmutation="1" _msthash="753807" _msttexthash="125034" _msthidden="1">, 2. Juli 2014 (</font>[http://www.deutschlandradiokultur.de/komponist-reformator-der-alten-oper.932.de.html?dram:article_id=290632 online])</ref>',
		 'bla.<ref>[[Michael Stegemann]]: \'\'Komponist. Reformator der alten Oper. Vor 300 Jahren wurde Christoph Willibald Gluck geboren\'\', in: \'\'Deutschlandradio Kultur\'\', 2. Juli 2014 ([http://www.deutschlandradiokultur.de/komponist-reformator-der-alten-oper.932.de.html?dram:article_id=290632 online])</ref>'],
	],['span in doi link',
		# https://de.wikipedia.org/w/index.php?oldid=207381129&diff=207824660
		['[[doi:10.1007/s10562-009-0227-1|doi:10.1007/s10562-009-0227-1 <span></span>]]', '[[doi:10.1007/s10562-009-0227-1]]'],
		# https://de.wikipedia.org/w/index.php?oldid=216309063&diff=216312260
		['[[doi: 10.1038/s43247-021-00267-8|doi: 10.1038/s43247-021-00267-8 <span></span>]]', '[[doi:10.1038/s43247-021-00267-8]]'],

		['[[doi:10.1038/nature05923|doi:10.1038/nature05923 <span></span>]]', '[[doi:10.1038/nature05923]]'],
		['[[doi:10.1002/anie.201107390|doi:10.1002/anie.201107390 <span></span>]]', '[[doi:10.1002/anie.201107390]]'],
		# https://de.wikipedia.org/w/index.php?oldid=214106723&diff=214645812 +
		# https://de.wikipedia.org/w/index.php?title=Ethylendiamintetraessigs%C3%A4ure&diff=214645833&oldid=214645812
		['[[DOI:10.1002/3527600418.mb6000d0046|DOI:10.1002/3527600418.mb6000d0046 <span></span> <span></span>]]', '[[DOI:10.1002/3527600418.mb6000d0046]]'],
	],['wrong tel: link',
		['[tel:276–2778 276–2778]', '276–2778'],
		['[tel:2005-10/2017 2005-10/2017]', '2005-10/2017'],
		['[tel:10/2017–12 10/2017–12]/2017', '10/2017–12/2017'],
		['[tel:02/2016–05 02/2016–05]/2018', '02/2016–05/2018'],
		['[tel:07/2015&#x20;–&#x20;04 07/2015 – 04]/2017', '07/2015 – 04/2017'],
		['[tel:01/2013&#x20;–&#x20;04 01/2013 – 04]/2014', '01/2013 – 04/2014'],
		['[tel:(1999)&#x20;303–312 (1999) 303–312]', '(1999) 303–312'],
		['[tel:9&#x20;(1994)&#x20;173–204 9 (1994) 173–204]', '9 (1994) 173–204'],
		# https://de.wikipedia.org/w/index.php?title=Al-Dschalabiyya&type=revision&diff=215769001&oldid=215151304
		['[tel:+96321&#x20;/&#x20;0096321 +96321 / 0096321]', '+96321 / 0096321'],
		# https://de.wikipedia.org/w/index.php?diff=210948779
		['[tel:1.181.090.879.000 1.181.090.879.000]', '1.181.090.879.000'],
	],['wrong left bracket',
		['<ref>[https://www.example.org</ref>', '<ref>https://www.example.org</ref>'],
		['<ref>[https://www.example.org .</ref>', '<ref>https://www.example.org</ref>'],
		['<ref>[https://www.example.org]</ref>', undef],
		['<ref>https://www.example.org</ref>', undef],
		['<ref>[https://www.example.org moep moep]</ref>', undef],
		['<ref>https://www.example.org moep moep</ref>', undef],
		['<ref>[https://www.example.org moep moep</ref>', undef], # this might/should be changed!
	],['wrong right bracket',
		['<ref>https://www.example.org]</ref>', '<ref>https://www.example.org</ref>'],
		['<ref>https://www.example.org moep]</ref>', undef], # this might/should be changed!
	],['abbr..',
		['5 vor Chr.', undef],
		['5 vor Chr. Moep', undef],
		['5 vor Chr.. Moep', '5 vor Chr. Moep'],
		['Moep e.V.', undef],
		['Moep e.V. Moep', undef],
		['Moep e.V.. Moep', 'Moep e.V. Moep'],
		['Moep ff.', undef],
		['Moep ff. Moep', undef],
		['Moep ff.. Moep', 'Moep ff. Moep'],
		['Moep Jh.', undef],
		['Moep Jh. Moep', undef],
		['Moep Jh.. Moep', 'Moep Jh. Moep'],
		['Moep usw.', undef],
		['Moep usw. Moep', undef],
		['Moep usw.. Moep', 'Moep usw. Moep'],
	],['abbr.. in links', # [[ [[ [[
		['.]]', undef],
		['.]].', '.]]'], # [[ [[
		['Moep.]]. Moep', 'Moep.]] Moep'],
	],['double full stop',
		['{{Literatur}}', undef],
		['{{Literatur|moep|moep|moep}}', undef],
		["{{Literatur\n|Autor=Foo Bar\n|Titel=sonstwas\n|moep...\n}}\n", undef],
		["{{Literatur\n|Autor=Foo Bar\n|Zitat=sonstwas\n|moep...\n}}.\n", undef],
		['{{Literatur}}.', '{{Literatur}}'],
		['{{Literatur|moep|moep|moep}}.', '{{Literatur|moep|moep|moep}}'],
		["{{Literatur\n|Autor=Foo Bar\n|Titel=sonstwas\n|moep...\n}}.\n", "{{Literatur\n|Autor=Foo Bar\n|Titel=sonstwas\n|moep...\n}}\n"],
		['<ref>{{Literatur}}</ref>', undef],
		['<ref>{{Literatur|moep|moep|moep}}</ref>', undef],
		["<ref>{{Literatur\n|Autor=Foo Bar\n|Titel=sonstwas\n|moep...\n}}\n</ref>", undef],
		["<ref>{{Literatur |Online=https://books.google.de/books?id=0q5BAAAAcAAJ&pg=PT21}}</ref>\n\n...\n\n''Johann Tserclas'' kaufte 1548 die Herrschaft.", undef],
		['<ref>{{Literatur}}.</ref>', '<ref>{{Literatur}}</ref>'],
		['<ref>{{Literatur|moep|moep|moep}}.</ref>', '<ref>{{Literatur|moep|moep|moep}}</ref>'],
		["<ref>{{Literatur\n|Autor=Foo Bar\n|Titel=sonstwas\n|moep...\n}}.\n</ref>",
			"<ref>{{Literatur\n|Autor=Foo Bar\n|Titel=sonstwas\n|moep...\n}}\n</ref>"],
		["<ref>{{Literatur |Autor=Moep}}</ref>\n<syntaxhighlight lang=\"text\">\n      .Programmcode4 …\n      .Programmcode5 …\n      .IF w>1 DO      ; zweite \n      ..Programmcode6...\n      ..Programmcode7 ... \n</syntaxhighlight>",
			undef],
	],['%ig',
		['%ig', '%ig'],
		['5%ig', '5%ig'],
		['5%-ig', '5%ig'],
		['5 %-ig', '5%ig'],
		['5 %ig', undef], # this might/should be changed!
		['5&nbsp;%ig', undef], # this might/should be changed!
		['5&nbsp;%-ig', '5%ig'],
	],['comma before "sondern"',
		['mehr sondern', 'mehr, sondern'],
		['mehr, sondern', undef],
	],['whitespace before comma',
		['moep, abgerufen', undef],
		['moep , abgerufen', 'moep, abgerufen'],
		['moep] , abgerufen', 'moep], abgerufen'],
	],['linkfix: google-redirect', # https://de.wikipedia.org/w/index.php?title=Donauschwaben&diff=193517000&oldid=193514230
		['<!--http://www.google.de/url?sa=t&rct=j&q=patriarch%20arsenije%20slwawen%20ansiedeln&source=web&cd=7&ved=0CEkQFjAG&url=http%3A%2F%2Feva.unibas.ch%2F%3Fc%3D59640&ei=s6u4TsDCG6yfiAei_82xBw&usg=AFQjCNGmgFgXO2S3KPOeDzVYOKBJl3xoUA&sig2=c31V8sPiJ4l1IUD_7vbEzQ-->',
			'<!--http://eva.unibas.ch/?c=59640-->'],
	],['pipe in normal url',
		['https://example.org/foo|bar', undef],
	],['linkfix: youtube-redirect',
		# https://de.wikipedia.org/w/index.php?title=MotoGP&diff=next&oldid=210076953
		['<ref>{{Internetquelle|url=https://youtu.be/D-HJPOk_I0E?t=136|titel=MotoGP Regeln 2021: Alle Änderungen im Überblick!|titelerg=|autor=Markus Zörweg|hrsg=Motorsport-Magazin.com auf YouTube|werk=|seiten=|datum=2021-03-18|abruf=2021-03-22|format=|kommentar=|zitat=|offline=}}</ref>',
			'<ref>{{Internetquelle|url=https://www.youtube.com/watch?v=D-HJPOk_I0E&t=136|titel=MotoGP Regeln 2021: Alle Änderungen im Überblick!|titelerg=|autor=Markus Zörweg|hrsg=Motorsport-Magazin.com auf YouTube|werk=|seiten=|datum=2021-03-18|abruf=2021-03-22|format=|kommentar=|zitat=|offline=}}</ref>'],
		# https://de.wikipedia.org/w/index.php?title=Bob_Ross_(Maler)&diff=next&oldid=211166868
		['<ref>{{cite web |author=Emily Rhyne, Larry Buchanan, Aaron Byrd |title=Where Are All the Bob Ross Paintings? We Found Them. |publisher=Youtube : The New York Times | date=2019-07-12 | url= https://youtu.be/rDs3o1uLEdU}}</ref>',
			'<ref>{{cite web |author=Emily Rhyne, Larry Buchanan, Aaron Byrd |title=Where Are All the Bob Ross Paintings? We Found Them. |publisher=Youtube : The New York Times | date=2019-07-12 | url= https://www.youtube.com/watch?v=rDs3o1uLEdU}}</ref>'],
		# https://de.wikipedia.org/w/index.php?title=Bundespr%C3%A4sidentenwahl_in_%C3%96sterreich_2022&diff=prev&oldid=226397344
		['{|
| moep<ref>{{Internetquelle|url=https://youtu.be/tlCJHQ8gaoA|titel=Livestream von OE24.TV|abruf=2022-09-22}}</ref>
|}
','{|
| moep<ref>{{Internetquelle|url=https://www.youtube.com/watch?v=tlCJHQ8gaoA|titel=Livestream von OE24.TV|abruf=2022-09-22}}</ref>
|}
'],
	],['article with webarchive template',
		# https://de.wikipedia.org/w/index.php?title=Antje_Wagner&diff=prev&oldid=214376090
		[scalar(slurp('./t/test_data/article_with_webarchive_template.txt', {'binmode' => ':utf8'})), undef, 'article with webarchive template'],
	],['linkfix: entfernung von tracking-parameter',
		# https://de.wikipedia.org/w/index.php?title=RuPaul%E2%80%99s_Drag_Race&diff=prev&oldid=214982281
		["{|\n|}\n" . '<ref>{{cite web|url=https://www.out.com/television/2021/3/06/drag-race-down-under-season-1-queens-judges-and-details?fbclid=IwAR076JBC1X8esw7G7-ZHSmGKa_yKKQ2zl4pUKM5J7Q7aXi5GKRLK5Y4l0DI|title=\'Drag Race Down Under\' Just Announced When Its Season Will Debut|work=Out|date=2021-04-08|accessdate=2021-04-08}}</ref>',
			"{|\n|}\n" . '<ref>{{cite web|url=https://www.out.com/television/2021/3/06/drag-race-down-under-season-1-queens-judges-and-details|title=\'Drag Race Down Under\' Just Announced When Its Season Will Debut|work=Out|date=2021-04-08|accessdate=2021-04-08}}</ref>',
			'cite web template with tracking param after table'],
		# https://de.wikipedia.org/w/index.php?title=Liste_von_Auslandshilfen_f%C3%BCr_die_Ukraine_seit_2014&diff=prev&oldid=237857041
		["{|\n|}\n" . '<ref>{{Cite web|title=[우크라 침공] 삼성전자, 난민 인도적 지원에 600만달러 기부(종합)|trans-title=[Ukraine Invasion] Samsung Electronic, $6 million donation to refugee humanitarian assistance |url=https://www.yna.co.kr/view/AKR20220305020551009?section=industry/all&site=major_news02_related|website=Yonhap News Agency|date=2022-03-05 |language=ko|accessdate=2022-03-05}}</ref>',
			"{|\n|}\n" . '<ref>{{Cite web|title=[우크라 침공] 삼성전자, 난민 인도적 지원에 600만달러 기부(종합)|trans-title=[Ukraine Invasion] Samsung Electronic, $6 million donation to refugee humanitarian assistance |url=https://www.yna.co.kr/view/AKR20220305020551009?section=industry/all|website=Yonhap News Agency|date=2022-03-05 |language=ko|accessdate=2022-03-05}}</ref>'],
		# https://de.wikipedia.org/w/index.php?title=Veikkausliiga_2016&diff=240682634&oldid=222775990
		# TODO: ICID param might be deleted?
		["{|\n| Spieler" . '<ref>{{Internetquelle |url=https://de.soccerway.com/national/finland/veikkausliiga/2016/regular-season/r34245/players/?ICID=PL_3N_04|titel=Veikkausliiga 2015: Torschützen |werk=soccerway.com |zugriff=2019-03-31}}</ref>' . "\n|}\n",
			"{|\n| Spieler" . '<ref>{{Internetquelle |url=https://de.soccerway.com/national/finland/veikkausliiga/2016/regular-season/r34245/players/?ICID=PL_3N_04|titel=Veikkausliiga 2015: Torschützen |werk=soccerway.com |zugriff=2019-03-31}}</ref>' . "\n|}\n",
			'template Internetquelle with tracking param and directly following template param inside of table'],
		# https://de.wikipedia.org/w/index.php?title=Wasserstoff&diff=prev&oldid=235049258
		['<ref>{{Internetquelle |autor=Spektrum |url=https://www.spektrum.de/lexikon/physik/spinpolarisierter-wasserstoff/13649#:~:text=Wasserstoff%20besteht%20aus%20einem%20Proton,getroffen%20werden%2C%20dies%20zu%20verhindern. |titel=Spinpolarisierter Wasserstoff |abruf=2023-06-19}}</ref>',
			'<ref>{{Internetquelle |autor=Spektrum |url=https://www.spektrum.de/lexikon/physik/spinpolarisierter-wasserstoff/13649 |titel=Spinpolarisierter Wasserstoff |abruf=2023-06-19}}</ref>'],
		# https://de.wikipedia.org/w/index.php?title=George_McCrae_(Politiker)&diff=prev&oldid=235103879
		['<ref>[https://en.m.wikipedia.org/wiki/George_McCrae_(politician)#:~:text=He%20died%20at%20Gallipoli%20on,1948)%20was%20a%20Senior%20Commander. lala]</ref>',
			'<ref>[https://en.m.wikipedia.org/wiki/George_McCrae_(politician) lala]</ref>'],
		# https://de.wikipedia.org/w/index.php?title=George_McCrae_(Politiker)&diff=prev&oldid=235103879
		['<ref>[https://typografie.de/autor/veronika-classen/#:~:text=Veronika%20Classen%20(*%2018.,und%20DMB%26B%2FD\'Arcy lala]</ref>',
			'<ref>[https://typografie.de/autor/veronika-classen/ lala]</ref>'],
		['<ref>[https://typografie.de/autor/veronika-classen/#:~:text=Veronika%20Classen%20(*%2018.,und%20DMB%26B%2FD- lala]</ref>',
			'<ref>[https://typografie.de/autor/veronika-classen/ lala]</ref>'],
		# https://de.wikipedia.org/w/index.php?title=Brendon_Hartley&diff=224717506&oldid=224453745
		['<ref>{{internetquelle|url=http://www.nzherald.co.nz/motorsport/news/article.cfm?c_id=66&objectid=10816386|titel=“Motorsport: Kiwis double success”|hrsg=nzherald.co.nz|autor=Eric Thompson|datum=2012-06-30|zugriff=2012-07-15}}</ref>',
		 '<ref>{{internetquelle|url=http://www.nzherald.co.nz/motorsport/news/article.cfm?c_id=66&objectid=10816386|titel=“Motorsport: Kiwis double success”|hrsg=nzherald.co.nz|autor=Eric Thompson|datum=2012-06-30|zugriff=2012-07-15}}</ref>'],
		# https://de.wikipedia.org/w/index.php?title=Adam_Page_(Wrestler)&diff=next&oldid=239400934
		['<ref>[https://www.example.org/2023/bla-bla/?utm_source=ground.news&utm_medium=referral\'\'AEW\'s Adam\'\'], tmz.com.</ref>',
		 '<ref>[https://www.example.org/2023/bla-bla/\'\'AEW\'s Adam\'\'], tmz.com.</ref>'],
		['<ref>[https://www.example.org/2023/bla-bla/?utm_source=ground.news&utm_medium=referral\'AEW\'s Adam], tmz.com.</ref>',
		 '<ref>[https://www.example.org/2023/bla-bla/ Adam], tmz.com.</ref>'],
		['<ref>{{Internetquelle |url=https://twitter.com/daverich1/status/995553744183091200/photo/1?tfw_site=haaretzcom&ref_src=twsrc%5Etfw&ref_url=https://www.haaretz.com/israel-news/chickensong-wins-eurovision-toys-with-israel-haters-on-twitter-1.6077594 |titel=Gruppe aus dem Vereinigten Königreich möchte Boykott |werk=twitter.com |datum=2018-05-14 |sprache=en |abruf=2018-05-14}}</ref>',
		 '<ref>{{Internetquelle |url=https://twitter.com/daverich1/status/995553744183091200/photo/1 |titel=Gruppe aus dem Vereinigten Königreich möchte Boykott |werk=twitter.com |datum=2018-05-14 |sprache=en |abruf=2018-05-14}}</ref>'],
	],['rel. text fragment links',
		['über den [[Geschichte Saudi-Arabiens#:~:text=Der%20zweite%20saudische%20Staat%20(1824%E2%80%931891),-Flagge%20des%20zweiten&text=Dieser%20zweite%20saudische%20Staat%20konnte,Clan%20der%20%C4%80l%20Rasch%C4%ABd%20gepr%C3%A4gt.|zweiten Saudi-Staat]]. Seine Mutter',
			'über den [[Geschichte Saudi-Arabiens|zweiten Saudi-Staat]]. Seine Mutter'],
	]];

# optional fixes
my $textrepls_optionals = [
	['u200e', # [[ [[
		["\x{200e}]]", ']]'],
		["\x{200e}}}", '}}'],
		["\x{200e}|", '|'],
	],['superfluous space in cats',
		['[[Kategorie: Moep]]', '[[Kategorie:Moep]]'],
	],['superfluous explicit "template:"',
		['{{Vorlage:Moep}}', '{{Moep}}'],
		['{{Vorlage: Moep}}', '{{Moep}}'],
	],['wrong br-tag',
		['</br>', '<br />'],
		['<\\br>', '<br />'],
		['</br >', '<br />'],
		['<\\br >', '<br />'],
		['<br.>', '<br />'],
		['<br\\>', '<br />'],
	],['superfluous br-tag in lists',
		["\n* moep<br />\n", "\n* moep\n"],
		["\n*moep<br />\n", "\n*moep\n"],
	],['wrong space before %; see [[WP:SVZ]]',
		['5&nbsp;%', '5 %'],
		['5&#x202f;%', '5 %'],
		['5&#8239;%', '5 %'],
		['5&thinsp;%', '5 %'],
	],['whitespace before ref-tag',
		['Moep <ref>', 'Moep<ref>'],
		['Moep  <ref>', 'Moep<ref>'],
		['Moep.<ref>', undef],
		['Moep. <ref>', 'Moep.<ref>'],
		['Moep.  <ref>', 'Moep.<ref>'],
		["\n* <ref>", undef],
		["\n: <ref>", undef],
		["\n; <ref>", undef],
		# https://de.wikipedia.org/w/index.php?title=Liste_der_meistaufgerufenen_YouTube-Videos&diff=prev&oldid=193651093
		["\n! <ref>", undef],
		["!! <ref>", undef],
		["moep! <ref>", "moep!<ref>"],
		["moep!  <ref>", "moep!<ref>"],
		['| <ref>', undef],
		['= <ref>', undef],
		['° <ref>', undef],
		# https://de.wikipedia.org/wiki/Benutzer_Diskussion:Winternacht/Archiv/2015#Danke!
		['15 m² <ref>', undef],
		# https://de.wikipedia.org/wiki/Benutzer_Diskussion:Winternacht/Archiv/2015#Danke!
		["180°&nbsp;<ref>", undef],
		# https://de.wikipedia.org/w/index.php?title=Ringelnatter&diff=prev&oldid=142164794
		["a</ref>\n<ref>", undef],
		['</sup> <ref>', undef],
		# https://de.wikipedia.org/w/index.php?title=Griechische_Staatsschuldenkrise&diff=143759791&oldid=143757358
		['{{Rp|53-55}} <ref>', undef],
		# https://de.wikipedia.org/w/index.php?title=Ringelnatter&diff=prev&oldid=142164794
		[scalar(slurp('./t/test_data/reflist.txt')), undef],
	],['whitespace before ref end tag',
		['Moep </ref>', 'Moep</ref>'],
		['Moep. </ref>', 'Moep.</ref>'],
		['Moep&nbsp;</ref>', 'Moep</ref>'],
	],['whitespace in link right before comma',
		['moep ], abgerufen', 'moep], abgerufen'],
	],["\x{B0}C ([[WP:SVZ]])",
		['moep °C', 'moep °C'],
		['0 ° C', '0&nbsp;°C'],
		['0° C', '0&nbsp;°C'],
		# https://de.wikipedia.org/w/index.php?title=Johannes_Busereuth&diff=prev&oldid=193652858
		['0° Cod', undef],
		['0°&nbsp;C', '0&nbsp;°C'],
		['0°&thinsp;C', '0&nbsp;°C'],
	],['linkfix pseudo-external links',
		['[https://de.wikipedia.org/wiki/Moep]',
			'[https://de.wikipedia.org/wiki/Moep]'],
		['[[https://de.wikipedia.org/wiki/Moep]]',
			'[[https://de.wikipedia.org/wiki/Moep]]'],
		['[https://de.wikipedia.org/wiki/Moep moep]',
			'[[Moep|moep]]'],
		['[https://de.wikipedia.org/wiki/Moep Moep]',
			'[[Moep]]'],
		['[https://de.wikipedia.org/wiki/Moep#lala Moep]',
			'[[Moep#lala|Moep]]'],
		['D<ref>[https://de.wikipedia.org/wiki/Moep moep]',
			'D<ref>[https://de.wikipedia.org/wiki/Moep moep]'],
		['[https://de.wikipedia.org/wiki/Moep#mediaviewer moep]',
			'[https://de.wikipedia.org/wiki/Moep#mediaviewer moep]'],
		['[https://de.wikipedia.org/wiki/Moep#mediaviewer?lala moep]',
			'[https://de.wikipedia.org/wiki/Moep#mediaviewer?lala moep]'],
	],['redundant link description',
		['[[Moep]]', '[[Moep]]'],
		['[[ Moep | Moep ]]', '[[Moep]]'],
		['[[ Moep | Moep]]', '[[Moep]]'],
		['[[ Moep |Moep ]]', '[[Moep]]'],
		['[[ Moep |Moep]]', '[[Moep]]'],
		['[[ Moep| Moep ]]', '[[Moep]]'],
		['[[ Moep| Moep]]', '[[Moep]]'],
		['[[ Moep|Moep ]]', '[[Moep]]'],
		['[[ Moep|Moep]]', '[[Moep]]'],
		['[[Moep | Moep ]]', '[[Moep]]'],
		['[[Moep | Moep]]', '[[Moep]]'],
		['[[Moep |Moep ]]', '[[Moep]]'],
		['[[Moep |Moep]]', '[[Moep]]'],
		['[[Moep|Moep]]', '[[Moep]]'],
		['[[ Moep Moep | Moep Moep]]', '[[Moep Moep]]'],
		['[[ Moep#Moep | Moep#Moep]]', '[[Moep#Moep]]'],
	],['zeros',
		['{{0}}', '{{0}}'],
		['{{0|00}}', '{{0|00}}'],
		['{{0}}{{0}}', '{{0|00}}'],
		['{{0}}{{0}}{{0}}', '{{0|000}}'],
		['{{0}}{{0}}{{0}}{{0}}', '{{0|0000}}'],

	],['[[archive.today]]->templ. with concatenated archives',
		["== Weblinks ==\n\n*lala\n* "
			. '[https://archive.today/20160121014341/'
			. 'http://wayback.archive.org/web/20120815110504/'
			. 'http://www.tagesschau.de/inland/wahlanalyse114.html]',
			"== Weblinks ==\n\n*lala\n* " . '{{Webarchiv |url='
			. 'http://www.tagesschau.de/inland/wahlanalyse114.html '
			. '|wayback=20120815110504}}'],
		['la.<ref name="lala">blubb '
			. '[https://archive.today/20160121014341/'
			. 'http://wayback.archive.org/web/20120815110504/'
			. 'http://www.tagesschau.de/inland/wahlanalyse114.html]',
			'la.<ref name="lala">blubb {{Webarchiv |url='
			. 'http://www.tagesschau.de/inland/wahlanalyse114.html '
			. '|wayback=20120815110504}}'],
	],['[[archive.today]]->templ. with concatenated archives with title',
		["== Weblinks ==\n\n*lala\n* "
			.'[https://archive.today/20160121014341/'
			. 'http://wayback.archive.org/web/20120815110504/'
			. 'http://www.tagesschau.de/inland/wahlanalyse114.html moep]',
			"== Weblinks ==\n\n*lala\n* " . '{{Webarchiv |url='
			. 'http://www.tagesschau.de/inland/wahlanalyse114.html '
			. '|wayback=20120815110504 |text=moep}}'],
		['la.<ref name="lala">blubb '
			.'[https://archive.today/20160121014341/'
			. 'http://wayback.archive.org/web/20120815110504/'
			. 'http://www.tagesschau.de/inland/wahlanalyse114.html moep]',
			'la.<ref name="lala">blubb {{Webarchiv |url='
			. 'http://www.tagesschau.de/inland/wahlanalyse114.html '
			. '|wayback=20120815110504 |text=moep}}'],
	],['[[archive.org]]->templ. with title',
		["== Weblinks ==\n\n*lala\n* "
			.'[https://web.archive.org/web/20141009211727/'
			. 'http://www.cdep.ro/pls/legis/legis_pck.htp_act_text?idt=9206%3E'
			. ' Text der Verfassung von 1938]',
			"== Weblinks ==\n\n*lala\n* " . '{{Webarchiv |url='
			. 'http://www.cdep.ro/pls/legis/legis_pck.htp_act_text?idt=9206%3E '
			. '|wayback=20141009211727 |text=Text der Verfassung von 1938}}'],
		# https://de.wikipedia.org/w/index.php?title=Tom_Horn&diff=236960630&oldid=236959934
		["== Weblinks ==\n\n*lala\n* "
			.'[https://web.archive.org/web/20141009211727/'
			. 'http://www.cdep.ro/pls/legis/legis_pck.htp_act_text?idt=9206%3E'
			. ' Text der Verfassung von 1938]'
			."\n* "
			.'[https://web.archive.org/web/20141009211728/'
			. 'http://www.example.org/lala.php?query=foo&bar=baz'
			. ' Lala]',
			"== Weblinks ==\n\n*lala\n* " . '{{Webarchiv |url='
			. 'http://www.cdep.ro/pls/legis/legis_pck.htp_act_text?idt=9206%3E '
			. '|wayback=20141009211727 |text=Text der Verfassung von 1938}}'
			. "\n* " . '{{Webarchiv |url='
			. 'http://www.example.org/lala.php?query=foo&bar=baz '
			. '|wayback=20141009211728 |text=Lala}}'],
		['la.<ref name="lala">blubb '
			.'[https://web.archive.org/web/20141009211727/'
			. 'http://www.cdep.ro/pls/legis/legis_pck.htp_act_text?idt=9206%3E '
			. 'Text der Verfassung von 1938]',
			'la.<ref name="lala">blubb {{Webarchiv |url='
			. 'http://www.cdep.ro/pls/legis/legis_pck.htp_act_text?idt=9206%3E '
			. '|wayback=20141009211727 |text=Text der Verfassung von 1938}}'],
	],['[[archive.org]]->templ.',
		["== Weblinks ==\n\n*lala\n* "
			.'[https://web.archive.org/web/20141009211727/'
			. 'http://www.cdep.ro/pls/legis/legis_pck.htp_act_text?idt=9206%3E]',
			"== Weblinks ==\n\n*lala\n* " . '{{Webarchiv |url='
			. 'http://www.cdep.ro/pls/legis/legis_pck.htp_act_text?idt=9206%3E '
			. '|wayback=20141009211727}}'],
		['la.<ref name="lala">blubb '
			.'[https://web.archive.org/web/20141009211727/'
			. 'http://www.cdep.ro/pls/legis/legis_pck.htp_act_text?idt=9206%3E]',
			'la.<ref name="lala">blubb {{Webarchiv |url='
			. 'http://www.cdep.ro/pls/legis/legis_pck.htp_act_text?idt=9206%3E '
			. '|wayback=20141009211727}}'],
	],['[[archive.today]]->templ. with title',
		["== Weblinks ==\n\n*lala\n* "
			.'[https://archive.today/20130105181225/'
			. 'http://www.fr-o.de/a-in-a/der-schatz-der-mubaraks,7151782,7189774.html'
			. ' Der Schatz der Mubaraks]',
			"== Weblinks ==\n\n*lala\n* " . '{{Webarchiv |url='
			. 'http://www.fr-o.de/a-in-a/der-schatz-der-mubaraks,7151782,7189774.html '
			. '|archive-today=20130105181225 |text=Der Schatz der Mubaraks}}'],
		['la.<ref name="lala">blubb '
			.'[https://archive.today/20130105181225/'
			. 'http://www.fr-o.de/a-in-a/der-schatz-der-mubaraks,7151782,7189774.html'
			. ' Der Schatz der Mubaraks]',
			'la.<ref name="lala">blubb {{Webarchiv |url='
			. 'http://www.fr-o.de/a-in-a/der-schatz-der-mubaraks,7151782,7189774.html '
			. '|archive-today=20130105181225 |text=Der Schatz der Mubaraks}}'],
		["== Weblinks ==\n\n*lala\n* "
			.'Ignacio Coluccio: '
			. '[https://archive.md/20130223065132/http://www.maelstrom.nu/ezine/review_iss34_2393.php'
			. ' \'\'NIHILIST - Nihilist - CD - Candlelight Records - 2005\'\']',
			"== Weblinks ==\n\n*lala\n* " . 'Ignacio Coluccio: {{Webarchiv |url='
			. 'http://www.maelstrom.nu/ezine/review_iss34_2393.php '
			. '|archive-today=20130223065132 '
			. '|text=\'\'NIHILIST - Nihilist - CD - Candlelight Records - 2005\'\'}}'],
		['la.<ref name="lala">blubb '
			.'Ignacio Coluccio: '
			. '[https://archive.md/20130223065132/http://www.maelstrom.nu/ezine/review_iss34_2393.php'
			. ' \'\'NIHILIST - Nihilist - CD - Candlelight Records - 2005\'\']',
			'la.<ref name="lala">blubb Ignacio Coluccio: {{Webarchiv |url='
			. 'http://www.maelstrom.nu/ezine/review_iss34_2393.php '
			. '|archive-today=20130223065132 '
			. '|text=\'\'NIHILIST - Nihilist - CD - Candlelight Records - 2005\'\'}}'],
	],['[[archive.today]]->templ.',
		["== Weblinks ==\n\n*lala\n* "
			.'[https://archive.today/20130105181225/'
			. 'http://www.fr-o.de/a-in-a/der-schatz-der-mubaraks,7151782,7189774.html]',
			"== Weblinks ==\n\n*lala\n* " . '{{Webarchiv |url='
			. 'http://www.fr-o.de/a-in-a/der-schatz-der-mubaraks,7151782,7189774.html '
			. '|archive-today=20130105181225}}'],
		['la.<ref name="lala">blubb '
			.'[https://archive.today/20130105181225/'
			. 'http://www.fr-o.de/a-in-a/der-schatz-der-mubaraks,7151782,7189774.html]',
			'la.<ref name="lala">blubb {{Webarchiv |url='
			. 'http://www.fr-o.de/a-in-a/der-schatz-der-mubaraks,7151782,7189774.html '
			. '|archive-today=20130105181225}}'],

	],
];

my $mand_repl_summary_canonical = 'abbr.. in links';
can_ok($cb, 'i18n');
++$test_counter;
my @mand_repl_summary = $cb->i18n($mand_repl_summary_canonical);

sub main_test{
	my $type = shift;
	my $textrepl = shift;
	my $name = $textrepl->[0];
	shift @$textrepl;
	plan tests => 3 * scalar(@$textrepl) * ($type eq 'optional' ? 2 : 1);
	for my $text(@$textrepl){
		my $input = $text->[0];
		my $output = $text->[1];
		my $sub_name = $text->[2];
		my $summary = $text->[3];
		if(defined $output){
			$sub_name = "'$input' -> '$output'" unless defined $sub_name;
		}else{
			$output = $input;
			$sub_name = "leave '$input' unchanged" unless defined $sub_name;
		}
		if(!defined($summary) && $input eq $output){
			$summary = '';
		}
		my ($inputs_outputs);
		if($type eq 'mandatory'){
			$inputs_outputs = [{'input' => $input, 'output' => $output}];
		}elsif($type eq 'optional'){
			$inputs_outputs = [
				{'input' => $mand_repl . $input, 'output' => $mand_res . $output},
				{'input' => $input . $mand_repl, 'output' => $output . $mand_res},
			];
		}
		for my $input_output(@$inputs_outputs){
			my $input = $input_output->{'input'};
			my $output = $input_output->{'output'};
			# test modification
			my ($changes, $out_summary) = $cb->cleanup_wiki_page(
				\$input, 'page', {'ns' => 0});
			is($input, $output, $sub_name);
			# test return values
			my @changes_names = grep {0 < @{$changes->{$_}}} keys %$changes;
			my $summary_bak = $summary;
			if(defined $summary){
				if($summary eq '' && $type eq 'optional'){
					$summary = "$mand_repl_summary[0]"
					. "; siehe [[user:CamelBot]].";
					is($changes_names[0], $mand_repl_summary_canonical, $sub_name . ' changes');
				}elsif($type eq 'mandatory'){
					is_deeply(\@changes_names, [], $sub_name . ' changes');
				}
			}else{
				my @long_name = $cb->i18n($name);
				$summary = '' . (@changes_names > 1 ?
					join(', ', sort($long_name[0], $mand_repl_summary[0]))
					: $long_name[0]
				) . "; siehe [[user:CamelBot]].";
				if(@changes_names == 1){
					is($changes_names[0], $name, $sub_name . ' changes');
				}else{
					is_deeply([sort @changes_names],
						[sort($name, $mand_repl_summary_canonical)],
						$sub_name . ' changes');
				}
			}
			is($out_summary, $summary, $sub_name . ' summary');
			$summary = $summary_bak;
		}
	}
}

for my $textrepl(@$textrepls){
	my $name = $textrepl->[0];
	subtest "subtest for '$name'", sub {main_test('mandatory', $textrepl);};
	++$test_counter;
}
for my $textrepl(@$textrepls_optionals){
	my $name = $textrepl->[0];
	subtest "subtest for '$name'", sub {main_test('optional', $textrepl);};
	++$test_counter;
}
done_testing($test_counter);

