#/usr/bin/perl
use strict;
use warnings;
#use utf8;
#use open ':std', ':encoding(utf8)'; # must be loaded before Test::More
use Test::More;# tests => 2;
use FindBin;
use lib "$FindBin::Bin/../lib";
use CamelBot;
use Data::Dumper; # for debugging only

my $test_counter = 0;
my $cb = new_ok('CamelBot' => [{
	'ask_user'      => 0,
	'cliparams'     => {},
	'db_access'     => 0,
	'mw_username'   => 'CamelBot',
	'pw_ask'        => 0,
	'showdiff'      => 0,
	'simulation'    => 1,
	'verbosity'     => 0,
}]);
++$test_counter;
can_ok($cb, 'createMWBot');
++$test_counter;
$cb->createMWBot();
$cb->{'verbosity'} = -1;

can_ok($cb, 'get_magicwords_alias');
++$test_counter;
my $test_data = [
	{ 'name' => 'undef',
		'word' => undef,
		'res' => undef,
	},
	{ 'name' => 'nothing',
		'word' => '',
		'res' => undef,
	},
	{ 'name' => 'redirect',
		'word' => 'redirect',
		'res' => ['#WEITERLEITUNG', '#REDIRECT'],
	},
];

if($cb->login()){
	for my $e(@$test_data){
		my $res = $cb->get_magicwords_alias($e->{'word'});
		is_deeply($res, $e->{'res'}, $e->{'name'});
		++$test_counter;
	}
}else{
	warn "camelbot needs to login for this test";
}
done_testing($test_counter);

