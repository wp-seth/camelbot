#/usr/bin/perl
use strict;
use warnings;
#use utf8; # ü
#use open ':std', ':encoding(utf8)'; # must be loaded before Test::More
use Test::More;# tests => 2;
use FindBin;
use lib "$FindBin::Bin/../lib";
use CamelBot;
use Data::Dumper; # for debugging only

my $test_counter = 0;
my $cb = new_ok('CamelBot' => [{
	'ask_user'      => 0,
	'mw_username'   => 'CamelBot', 
	'simulation'    => 1,
	'showdiff'      => 0,
	'verbosity'     => 0,
	'db_access'     => 0,
	'cliparams'     => {},
}]);
++$test_counter;
can_ok($cb, 'createMWBot');
++$test_counter;
$cb->createMWBot();
$cb->{'offline'} = 1;
$cb->{'verbosity'} = -1;

can_ok($cb, 'get_table_sum');
++$test_counter;
my $test_data = [
	{ 'name' => 'undef',
		'in' => [undef, undef],
		'out' => [undef, undef],
	},
	{ 'name' => 'empty string',
		'in' => ['', undef],
		'out' => [undef, undef],
	},
	{ 'name' => 'empty hash',
		'in' => [{}, undef],
		'out' => [undef, undef],
	},
	{ 'name' => 'empty table',
		'in' => [{'header' => [], 'body' => []}, undef],
		'out' => [{}, {}],
	},
	{ 'name' => 'simple table',
		'in' => [{'header' => ['moep'], 'body' => [[4]]}, undef],
		'out' => [{'moep' => 4}, {'moep' => 1}],
	},
	{ 'name' => 'simple table, two cols',
		'in' => [{'header' => ['foo', 'bar'], 'body' => [[4, 3.5]]}, undef],
		'out' => [{'foo' => 4, 'bar' => 3.5}, {'foo' => 1, 'bar' => 1}],
	},
	{ 'name' => 'simple table, two cols, two rows',
		'in' => [{'header' => ['foo', 'bar'], 'body' => [[4, 3.5], [5, 1.5]]}, undef],
		'out' => [{'foo' => 9, 'bar' => 5}, {'foo' => 2, 'bar' => 2}],
	},
	# TODO: many tests missing
];

for my $e(@$test_data){
	my ($s, $n) = $cb->get_table_sum(@{$e->{'in'}});
	is_deeply($s, $e->{'out'}[0], $e->{'name'} . ', sum');
	++$test_counter;
	is_deeply($n, $e->{'out'}[1], $e->{'name'} . ', num');
	++$test_counter;
}
done_testing($test_counter);

