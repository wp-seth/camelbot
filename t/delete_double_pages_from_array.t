#/usr/bin/perl
use strict;
use warnings;
use Test::More;# tests => 2;
use FindBin;
use lib "$FindBin::Bin/../lib";
use CamelBot;
use CamelBotRC;
use Data::Dumper; # for debugging only

my $test_counter = 0;
my $cb = new_ok('CamelBot' => [{
	'ask_user'      => 0,
	'mw_username'   => 'CamelBot', 
	'simulation'    => 1,
	'showdiff'      => 0,
	'verbosity'     => 0,
	'db_access'     => 0,
	'cliparams'     => {},
}]);
++$test_counter;
$cb->{'verbosity'} = -1;
my $cb_rc = new_ok('CamelBotRC' => [$cb, {'skip_maintenance' => 1}]);
++$test_counter;
can_ok($cb_rc, 'delete_double_pages_from_array');
++$test_counter;
my $test_data = [
	{ 'name' => 'undef',
		'in' => undef,
		'mod' => undef,
	},
	{ 'name' => 'empty',
		'in' => [],
		'mod' => [],
	},
	{ 'name' => 'single',
		'in' => [{'page_with_ns' => 'moep', 'timestamp' => '2000-01-01 00:00'}],
		'mod' => [{'page_with_ns' => 'moep', 'timestamp' => '2000-01-01 00:00'}],
	},
	{ 'name' => 'three entries, one double',
		'in' => [
			{'page_with_ns' => 'moep', 'timestamp' => '2000-01-01 00:00'},
			{'page_with_ns' => 'moep', 'timestamp' => '2000-01-02 00:00'},
			{'page_with_ns' => 'foo', 'timestamp' => '2000-01-04 00:00'},
		],
		'mod' => [
			{'page_with_ns' => 'moep', 'timestamp' => '2000-01-02 00:00'},
			{'page_with_ns' => 'foo', 'timestamp' => '2000-01-04 00:00'},
		],
	},
	{ 'name' => 'four entries, two times two double',
		'in' => [
			{'page_with_ns' => 'moep', 'timestamp' => '2000-01-01 00:00'},
			{'page_with_ns' => 'foo', 'timestamp' => '2000-01-02 00:00'},
			{'page_with_ns' => 'moep', 'timestamp' => '2000-01-03 00:00'},
			{'page_with_ns' => 'foo', 'timestamp' => '2000-01-04 00:00'},
		],
		'mod' => [
			{'page_with_ns' => 'moep', 'timestamp' => '2000-01-03 00:00'},
			{'page_with_ns' => 'foo', 'timestamp' => '2000-01-04 00:00'},
		],
	},
	{ 'name' => 'three entries, no double',
		'in' => [
			{'page_with_ns' => 'moep', 'timestamp' => '2000-01-01 00:00'},
			{'page_with_ns' => 'foo', 'timestamp' => '2000-01-02 00:00'},
			{'page_with_ns' => 'bar', 'timestamp' => '2000-01-03 00:00'},
		],
		'mod' => [
			{'page_with_ns' => 'moep', 'timestamp' => '2000-01-01 00:00'},
			{'page_with_ns' => 'foo', 'timestamp' => '2000-01-02 00:00'},
			{'page_with_ns' => 'bar', 'timestamp' => '2000-01-03 00:00'},
		],
	},
];

for my $e(@$test_data){
	$cb_rc->delete_double_pages_from_array($e->{'in'});
	is_deeply($e->{'mod'}, $e->{'in'}, $e->{'name'});
	++$test_counter;
}
done_testing($test_counter);


