#/usr/bin/perl
use strict;
use warnings;
use utf8;
use open ':std', ':encoding(utf8)'; # must be loaded before Test::More
use Test::More;# tests => 2;
use File::Slurp qw/slurp/;
use FindBin;
use lib "$FindBin::Bin/../lib";
use CamelBot;
use Data::Dumper; # for debugging only

my $test_counter = 0;
my $cb = new_ok('CamelBot' => [{
	'ask_user'      => 0,
	'mw_username'   => 'CamelBot',
	'simulation'    => 1,
	'showdiff'      => 0,
	'verbosity'     => 0,
	'db_access'     => 0,
	'cliparams'     => {},
}]);
++$test_counter;
can_ok($cb, 'createMWBot');
++$test_counter;
$cb->createMWBot();
$cb->{'offline'} = undef; # this test must be done online

can_ok($cb, 'normalize_page_title');
++$test_counter;

# test data
my $test_data = [
	['undef', undef, undef],
	['empty string', '', ''],
	['correct', 'Lala', 'Lala'],
	['spaces', '  Lala  ', 'Lala'],
	['ucfirst', 'lala', 'Lala'],
	['ucfirst and spaces', ' lala ', 'Lala'],
	['underscores', 'La_la_la_(La?)', 'La la la (La?)'],
	['underscores, spaces, and ucfirst', 'la_la_la (La?)  ', 'La la la (La?)'],
	['fragment', 'la_la_la (La?)#Moep_Moep', 'La la la (La?)#Moep Moep'],
	['interwiki', 'mw:blubb_blubb', 'mw:blubb blubb'],
	['interwiki', ':en:blubb_blubb', 'en:blubb blubb'], # might change in future?
];

if($cb->login()){
	for my $e(@$test_data){
		my $to = $cb->normalize_page_title($e->[1]);
		is($to, $e->[2], $e->[0]);
		++$test_counter;
	}
}else{
	warn "camelbot needs to login for this test";
}
done_testing($test_counter);
