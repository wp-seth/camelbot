#/usr/bin/perl
use strict;
use warnings;
#use utf8; # ü
#use open ':std', ':encoding(utf8)'; # must be loaded before Test::More
use Test::More;# tests => 2;
use File::Slurp qw/slurp/;
use FindBin;
use lib "$FindBin::Bin/../lib";
use CamelBot;
use Data::Dumper; # for debugging only

my $test_counter = 0;
my $cb = new_ok('CamelBot' => [{
	'ask_user'      => 0,
	'mw_username'   => 'CamelBot', 
	'simulation'    => 1,
	'showdiff'      => 0,
	'verbosity'     => 0,
	'db_access'     => 0,
	'cliparams'     => {},
}]);
++$test_counter;
can_ok($cb, 'createMWBot');
++$test_counter;
$cb->createMWBot();
$cb->{'offline'} = 1;
$cb->{'verbosity'} = -1;

can_ok($cb, 'extract_maintenance_list_parts');
++$test_counter;
my $test_data = [
	{ 'name' => 'empty',
		'in' => [
			'', 
			{
				'intro' => "intro\n== Liste ==\n",
				'wikitable' => undef,
				'outro' => "outro",
			}, 
			undef,
		],
		'out' => [
			{
				'intro' => "intro\n== Liste ==\n",
				'wikitable' => "{| class=\"wikitable sortable\"\n" 
					. "! page !! date of detection by CamelBot\n|}",
				'outro' => "outro",
			}, 
			'create',
		],
	},
	{ 'name' => 'empty, additional col',
		'in' => [
			'', 
			{
				'intro' => "intro\n== Liste ==\n",
				'wikitable' => undef,
				'outro' => "outro",
			}, 
			['addi col'],
		],
		'out' => [
			{
				'intro' => "intro\n== Liste ==\n",
				'wikitable' => "{| class=\"wikitable sortable\"\n" 
					. "! page !! addi col !! date of detection by CamelBot\n|}",
				'outro' => "outro",
			}, 
			'create',
		],
	},
	{ 'name' => 'simple table',
		'in' => [
			"in tro\n== Liste ==\n" 
			. "{| class=\"wikitable sortable\"\n" 
			. "! page !! date of detection by CamelBot\n" 
			. "| [[Some Article]] || 2020-06-01\n" 
			. "|}\n"
			. "out ro",
			{
				'intro' => "intro\n",
				'wikitable' => undef,
				'outro' => "outro",
			}, 
			undef,
		],
		'out' => [
			{
				'intro' => "in tro\n== Liste ==\n",
				'wikitable' => "{| class=\"wikitable sortable\"\n" 
					. "! page !! date of detection by CamelBot\n" 
					. "| [[Some Article]] || 2020-06-01\n" 
					. "|}\n",
				'outro' => "out ro",
			}, 
			'update',
		],
	},
	# this works (2020-06-01), but not sure, whether this should be intended
	#	{ 'name' => 'simple table with addition column',
	#		'in' => [
	#			"in tro\n== Liste ==\n" 
	#			. "{| class=\"wikitable sortable\"\n" 
	#			. "! page !! date of detection by CamelBot\n" 
	#			. "| [[Some Article]] || 2020-06-01\n" 
	#			. "|}\n"
	#			. "out ro",
	#			{
	#				'intro' => "intro\n",
	#				'wikitable' => undef,
	#				'outro' => "outro",
	#			}, 
	#			['addi col'],
	#		],
	#		'out' => [
	#			{
	#				'intro' => "in tro\n== Liste ==\n",
	#				'wikitable' => "{| class=\"wikitable sortable\"\n" 
	#					. "! page !! date of detection by CamelBot\n" 
	#					. "| [[Some Article]] || 2020-06-01\n" 
	#					. "|}\n",
	#				'outro' => "out ro",
	#			}, 
	#			'update',
	#		],
	#	},
	{ 'name' => 'original cat dead file',
		'in' => [
			scalar(slurp('./t/test_data/user_camelbot_maintenance_list_cat_dead.txt')), 
			{}, 
			undef,
		],
		'out' => [
			do './t/test_data/user_camelbot_maintenance_list_cat_dead.txt_split', 
			'update',
		],
	},
	{ 'name' => 'long cat dead file',
		'in' => [
			scalar(slurp('./t/test_data/user_camelbot_maintenance_list_cat_dead_long.txt')), 
			{}, 
			undef,
		],
		'out' => [
			do './t/test_data/user_camelbot_maintenance_list_cat_dead_long.txt_split', 
			'update',
		],
	},
];

for my $e(@$test_data){
	my $to = $cb->extract_maintenance_list_parts(
		\$e->{'in'}[0], $e->{'in'}[1], $e->{'in'}[2]);
	is_deeply($e->{'in'}[1], $e->{'out'}[0], $e->{'name'} . ' parts array');
	++$test_counter;
	is($to, $e->{'out'}[1], $e->{'name'} . ' return value');
	++$test_counter;
}
done_testing($test_counter);

