#/usr/bin/perl
use strict;
use warnings;
#use utf8; # ü
#use open ':std', ':encoding(utf8)'; # must be loaded before Test::More
use Test::More;# tests => 2;
use FindBin;
use lib "$FindBin::Bin/../lib";
use CamelBot;
use Data::Dumper; # for debugging only

my $test_counter = 0;
my $cb = new_ok('CamelBot' => [{
	'ask_user'      => 0,
	'cliparams'     => {},
	'db_access'     => 0,
	'mw_username'   => 'CamelBot',
	'pw_ask'        => 0,
	'showdiff'      => 0,
	'simulation'    => 0,
	'verbosity'     => 0,
}]);
++$test_counter;
can_ok($cb, 'login');
++$test_counter;
$cb->{'offline'} = undef; # this test must be done online
$cb->{'verbosity'} = -1;

can_ok($cb, 'convert_ns');
++$test_counter;
my $test_data = [
	{ 'name' => 'all undef',
		'in' => [],
		'out' => undef,
	},
	{ 'name' => 'talk pages, auto',
		'in' => [1],
		'out' => 'Diskussion',
	},
	{ 'name' => 'talk pages, name2id 2',
		'in' => ['Benutzer', 'name2id'],
		'out' => 2,
	},
	{ 'name' => 'talk pages, name2id 2 gender gedoens',
		'in' => ['Benutzerin', 'name2id'],
		'out' => 2,
	},
	{ 'name' => 'talk pages, name2id 3 gender gedoens',
		'in' => ['Benutzerin Diskussion', 'name2id'],
		'out' => 3,
	},
	{ 'name' => 'talk pages, id2name',
		'in' => [1, 'id2name'],
		'out' => 'Diskussion',
	},
	{ 'name' => 'user page',
		'in' => [2, 'id2names'],
		'out' => ['Benutzer', 'Benutzerin'],
	},
	{ 'name' => 'talk pages, name2id 0',
		'in' => [1, 'name2id'],
		'out' => 0,
	},
	{ 'name' => 'talk pages, id2canonical',
		'in' => [1, 'id2canonical'],
		'out' => 'Talk',
	},
];

if($cb->login()){
	for my $e(@$test_data){
		my $to = $cb->convert_ns($e->{'in'}[0], $e->{'in'}[1]);
		is_deeply($to, $e->{'out'}, $e->{'name'});
		++$test_counter;
	}
	#print Dumper $cb->{'namespaces'};
	#print Dumper $cb->{'namespaces_canonical'};
	#print Dumper $cb->{'namespacealiases'};
}else{
	warn "camelbot needs to login for this test";
}
done_testing($test_counter);

